package com.ablueclive.sinch;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ablueclive.R;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.Constants;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;
import com.sinch.android.rtc.video.VideoScalingType;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class CallScreenActivity extends BaseActivity {
    static final String TAG = CallScreenActivity.class.getSimpleName();
    static final String ADDED_LISTENER = "addedListener";
    static final String VIEWS_TOGGLED = "viewsToggled";
    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private boolean mAddedListener = false;
    private boolean mLocalVideoViewAdded = false;
    private boolean mRemoteVideoViewAdded = false;
    private String isVideo = "", userId = "", RECEIVER_ID, CALLER_ID;

    private TextView mCallDuration;
    private TextView mCallState;
    private TextView mCallerName;
    private ImageView actionMice;
    private ImageView actionSpeaker;
    boolean mToggleVideoViewPositions = false;
    private ImageView remoteUserImage, img_gradiant;
    private LinearLayout ll3;
    Context context;


    private class UpdateCallDurationTask extends TimerTask {
        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(CallScreenActivity.this::updateCallDuration);
        }
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
        savedInstanceState.putBoolean(VIEWS_TOGGLED, mToggleVideoViewPositions);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
        mToggleVideoViewPositions = savedInstanceState.getBoolean(VIEWS_TOGGLED);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callscreen);
        context = CallScreenActivity.this;
        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = findViewById(R.id.callDuration);
        mCallerName = findViewById(R.id.remoteUser);
        mCallState = findViewById(R.id.callState);
        actionMice = findViewById(R.id.actionMice);
        actionSpeaker = findViewById(R.id.actionSpeaker);

        ll3 = findViewById(R.id.ll3);
        img_gradiant = findViewById(R.id.img_gradiant);
        remoteUserImage = findViewById(R.id.remoteUserImage);

        Button hangUpButton = findViewById(R.id.hangUpButton);
        hangUpButton.setOnClickListener(v -> endCall());

        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
        String aa = getIntent().getStringExtra("USER_NAME");
        userId = getIntent().getStringExtra("USER_Id");
        mCallerName.setText(aa);
        isVideo = getIntent().getStringExtra("IS_VIDEO");
        CALLER_ID = getIntent().getStringExtra("CALLER_ID");
        RECEIVER_ID = getIntent().getStringExtra("RECEIVER_ID");
        String userimage = getIntent().getStringExtra("USER_IMAGE");
        if (userimage != null && !userimage.isEmpty()) {
            if (userimage.contains("http")) {
                Picasso.get().load(userimage).into(remoteUserImage);
            } else {
                Picasso.get().load(Constants.PROFILE_IMAGE_URL + userimage).into(remoteUserImage);
            }
        }
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (!mAddedListener) {
                call.addCallListener(new SinchCallListener());
                mAddedListener = true;
            }
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }

        updateUI();
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallState.setText(getMyText(call.getState().toString()));
            if (call.getDetails().isVideoOffered()) {
                setVideoViewsVisibility(true, call.getState() == CallState.ESTABLISHED);
            }
        } else {
            setVideoViewsVisibility(false, false);
        }
    }

    private String getMyText(String string) {
        if (string.isEmpty()) return "";
        return string.toLowerCase().replaceAll("initiating", "Connecting").replaceAll("established", "Connected");
    }

    @Override
    public void onStop() {
        super.onStop();
        mDurationTask.cancel();
        mTimer.cancel();
        if (isVideo.equalsIgnoreCase("1")) {
            removeVideoViews();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        updateUI();
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            CallEndCause cause = call.getDetails().getEndCause();
            switch (isVideo) {
                case "1":
                    if (cause.toString().equals("HUNG_UP")) {
                        submitCallHistory(call.getDetails().getDuration() + "", userId, "3", "1");
                    } else {
                        submitCallHistory("", userId, "4", "1");
                    }
                    break;
                case "0":
                    if (cause.toString().equals("HUNG_UP")) {
                        submitCallHistory(call.getDetails().getDuration() + "", userId, "3", "2");
                    } else {
                        submitCallHistory("", userId, "4", "2");
                    }
                    break;
            }
            call.hangup();
        }
        finish();
    }

    private String formatTimespan(int totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallDuration.setText(formatTimespan(call.getDetails().getDuration()));
        }
    }

    private ViewGroup getVideoView(boolean localView) {
        if (mToggleVideoViewPositions) {
            localView = !localView;
        }
        return localView ? findViewById(R.id.localVideo) : findViewById(R.id.remoteVideo);
    }

    private void addLocalView() {

        if (mLocalVideoViewAdded || getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                ll3.setVisibility(View.VISIBLE);
                remoteUserImage.setVisibility(View.GONE);
                img_gradiant.setVisibility(View.GONE);
                ViewGroup localView = getVideoView(true);
                localView.addView(vc.getLocalView());
                localView.setOnClickListener(v -> vc.toggleCaptureDevicePosition());
                mLocalVideoViewAdded = true;
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
            });
        }
    }

    private void addRemoteView() {
        if (mRemoteVideoViewAdded || getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            vc.setResizeBehaviour(VideoScalingType.ASPECT_FILL);
            vc.setCaptureDevicePosition(Camera.CameraInfo.CAMERA_FACING_FRONT);
            runOnUiThread(() -> {
                ViewGroup remoteView = getVideoView(false);
                remoteView.addView(vc.getRemoteView());
                View mremoteView = vc.getRemoteView();
                if (mremoteView instanceof SurfaceView) {
                    ((SurfaceView) mremoteView).setZOrderOnTop(true);
                }
                remoteView.setOnClickListener((View v) -> {
                    ll3.setVisibility(View.VISIBLE);
                    remoteUserImage.setVisibility(View.GONE);
                    img_gradiant.setVisibility(View.GONE);
                    removeVideoViews();
                    mToggleVideoViewPositions = !mToggleVideoViewPositions;
                    addRemoteView();
                    addLocalView();
                });
                mRemoteVideoViewAdded = true;
                vc.setLocalVideoZOrder(!mToggleVideoViewPositions);
            });
        }
    }


    private void removeVideoViews() {
        try {
            Log.d("isVideo", "Video Caling");
            if (getSinchServiceInterface() == null) {
                return; // early
            }

            VideoController vc = getSinchServiceInterface().getVideoController();
            if (vc != null) {
                runOnUiThread(() -> {
                    ((ViewGroup) (vc.getRemoteView().getParent())).removeView(vc.getRemoteView());
                    ((ViewGroup) (vc.getLocalView().getParent())).removeView(vc.getLocalView());
                    mLocalVideoViewAdded = false;
                    mRemoteVideoViewAdded = false;
                });
            }
        } catch (Exception e) {
            finish();
        }

    }

    private void setVideoViewsVisibility(final boolean localVideoVisibile, final boolean remoteVideoVisible) {
        if (getSinchServiceInterface() == null)
            return;
        if (!mRemoteVideoViewAdded) {
            addRemoteView();
        }
        if (!mLocalVideoViewAdded) {
            addLocalView();
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            runOnUiThread(() -> {
                vc.getLocalView().setVisibility(localVideoVisibile ? View.VISIBLE : View.GONE);
                vc.getRemoteView().setVisibility(remoteVideoVisible ? View.VISIBLE : View.GONE);
            });
        }
    }// 6024dce195f05d40c6e7a07a

    private class SinchCallListener implements VideoCallListener {
        @Override
        public void onCallEnded(Call call) {
            userId = RECEIVER_ID;
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            Log.e("Call ended: ", "" + endMsg + " call status" + call.getState());
            //HUNG_UP..//DENIED
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.e(TAG, "Call onCallEstablished");
            if (!BMSPrefs.getString(CallScreenActivity.this, Constants._ID).equals(CALLER_ID)) {
                if (isVideo.equals("1")) {
                    submitCallHistory("", CALLER_ID, "2", "1");
                } else {
                    submitCallHistory("", CALLER_ID, "2", "2");
                }
            }

            mAudioPlayer.stopProgressTone();
            mCallState.setText(getMyText(call.getState().toString()));
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            AudioController audioController = getSinchServiceInterface().getAudioController();
            audioController.enableSpeaker();

            actionMice.setBackgroundResource(R.drawable.mute_off);
            actionSpeaker.setBackgroundResource(R.drawable.speaker_off);
            actionMice.setTag(1);
            actionSpeaker.setTag(1);

            //actionMice.setColorFilter(ContextCompat.getColor(context, R.color.whiteCommon));
            //actionSpeaker.setColorFilter(ContextCompat.getColor(context, R.color.whiteCommon));

            actionMice.setOnClickListener(view -> {
                int tag = (Integer) actionMice.getTag();
                if (tag == 1) {
                    actionMice.setTag(2);
                    actionMice.setBackgroundResource(R.drawable.mute_onn);
                    audioController.mute();
                } else {
                    actionMice.setTag(1);
                    actionMice.setBackgroundResource(R.drawable.mute_off);
                    audioController.unmute();
                }
            });


            actionSpeaker.setOnClickListener(view -> {
                int speakerTag = (Integer) actionSpeaker.getTag();
                if (speakerTag == 1) {
                    actionSpeaker.setTag(2);
                    actionSpeaker.setBackgroundResource(R.drawable.speaker_off);
                    audioController.disableSpeaker();
                } else {
                    actionSpeaker.setTag(1);
                    actionSpeaker.setBackgroundResource(R.drawable.speakr_on);
                    audioController.enableSpeaker();
                }
            });


            audioController.disableAutomaticAudioRouting();

            if (call.getDetails().isVideoOffered()) {
                ll3.setVisibility(View.GONE);
                setVideoViewsVisibility(true, true);
                audioController.enableSpeaker();
            } else {
                ll3.setVisibility(View.VISIBLE);
                audioController.disableSpeaker();
            }
            Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
            Log.d("CALL CALL ID", call.getCallId());
            Log.d("CALL REMOTE ID", call.getRemoteUserId());
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
        }

        @Override
        public void onVideoTrackPaused(Call call) {
        }

        @Override
        public void onVideoTrackResumed(Call call) {
        }
    }

}