package com.ablueclive.sinch;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.StrictMode;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.ablueclive.R;
import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.networkClass.RetrofitClient;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.Constants;
import com.sinch.android.rtc.calling.Call;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.os.Environment.DIRECTORY_DOWNLOADS;


public class BaseActivity extends AppCompatActivity implements ServiceConnection {

    private SinchService.SinchServiceInterface mSinchServiceInterface;
    String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            String sinchRegisterID = BMSPrefs.getString(this, Constants._ID);
            if (sinchRegisterID != null && !sinchRegisterID.isEmpty()) {
                bindService();

                String sinchUser = getSinchServiceInterface().getUserName();
                if (!TextUtils.isEmpty(sinchUser)) {
                    if (!sinchRegisterID.equals(sinchUser)) {
                        getSinchServiceInterface().stopClient();
                    }
                }

                if (!getSinchServiceInterface().isStarted()) {
                    getSinchServiceInterface().startClient(sinchRegisterID, BMSPrefs.getString(this, Constants.USER_NAME));
                }

            }
        } catch (Exception r) {
            r.printStackTrace();
        }
    }


    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
// for subclasses
    }

    protected void onServiceDisconnected() {
// for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    @SuppressLint("HandlerLeak")
    private final Messenger messenger = new Messenger(new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == SinchService.MESSAGE_PERMISSIONS_NEEDED) {
                Bundle bundle = msg.getData();
                String requiredPermission = bundle.getString(SinchService.REQUIRED_PERMISSION);
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{requiredPermission}, 0);
            }
        }
    });

    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    bindService();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermission();
                                        }
                                    });
                        }
                    }
                }
                break;

            case 2:
                boolean granted = grantResults.length > 0;
                for (int grantResult : grantResults) {
                    granted &= grantResult == PackageManager.PERMISSION_GRANTED;
                }
                if (granted) {
                    Toast.makeText(this, "You may now place a call", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "This application needs permission to use your microphone and camera to function properly.", Toast.LENGTH_LONG).show();
                }
                mSinchServiceInterface.retryStartAfterPermissionGranted();
                break;
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void bindService() {
        try {
            Intent serviceIntent = new Intent(this, SinchService.class);
            serviceIntent.putExtra(SinchService.MESSENGER, messenger);
            getApplicationContext().bindService(serviceIntent, this, BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(BaseActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*+=_])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    public void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage("You need to allow access to both the permissions")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    public void actionVideoCall(String userId, String toUserName, String toUserImage) {
        if (checkPermission()) {
            requestPermission();
        } else {
            submitCallHistory("", userId, "1", "1");
            String userName = BMSPrefs.getString(this, Constants._ID);
            if (!userName.equals(getSinchServiceInterface().getUserName())) {
                getSinchServiceInterface().stopClient();
            }
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().startClient(userName, toUserName);
            }
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("USER_IMAGE", BMSPrefs.getString(this, Constants.PROFILE_IMAGE));
                map.put("USER_NAME", BMSPrefs.getString(this, Constants.USER_NAME));
                map.put("IS_VIDEO", "1");
                map.put("id", userId);
                map.put("RECEIVER_ID", userId);
                map.put("CALLER_ID", BMSPrefs.getString(this, Constants._ID));
                map.put("message", "Hello greetings");
                Call call = getSinchServiceInterface().callUserVideo(userId, map);
                String callId = call.getCallId();
                Intent callScreen = new Intent(this, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                callScreen.putExtra("USER_NAME", toUserName);
                callScreen.putExtra("USER_Id", userId);
                callScreen.putExtra("USER_IMAGE", toUserImage);

                callScreen.putExtra("CALLER_ID", BMSPrefs.getString(this, Constants._ID));
                callScreen.putExtra("RECEIVER_ID", userId);
//                callScreen.putExtra(SinchService.IS_VIDEO, true);
                callScreen.putExtra("IS_VIDEO", "1");
                startActivity(callScreen);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void actionVoiceCall(String userId, String toUserName, String toUserImage) {
        if (checkPermission()) {
            requestPermission();
        } else {
            submitCallHistory("", userId, "1", "2");
            String userName = BMSPrefs.getString(this, Constants._ID);
            if (!userName.equals(getSinchServiceInterface().getUserName())) {
                getSinchServiceInterface().stopClient();
            }
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().startClient(userName, toUserName);
            }
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("USER_IMAGE", BMSPrefs.getString(this, Constants.PROFILE_IMAGE));
                map.put("USER_NAME", BMSPrefs.getString(this, Constants.USER_NAME));
                map.put("IS_VIDEO", "0");
                map.put("id", userId);
                map.put("RECEIVER_ID", userId);
                map.put("CALLER_ID", BMSPrefs.getString(this, Constants._ID));
                map.put("message", getString(R.string.app_name));
                Call call = getSinchServiceInterface().callUserVoice(userId, map);
                String callId = call.getCallId();
                Intent callScreen = new Intent(this, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                callScreen.putExtra("USER_NAME", toUserName);
                callScreen.putExtra("USER_Id", userId);
                callScreen.putExtra("IS_VIDEO", "0");
                callScreen.putExtra("CALLER_ID", BMSPrefs.getString(this, Constants._ID));
                callScreen.putExtra("USER_IMAGE", toUserImage);
                callScreen.putExtra("RECEIVER_ID", userId);
//                callScreen.putExtra(SinchService.IS_VIDEO, false);

                startActivity(callScreen);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private boolean checkPermission() {
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        return result2 != PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO, Manifest.permission.CAMERA}, 1);
    }


    public void showAlert(String message, String positiveButton, String negativeButton) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(message);
        if (!positiveButton.isEmpty()) {
            builder.setPositiveButton(positiveButton, (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });
        }
        if (!negativeButton.isEmpty()) {
            builder.setNegativeButton(negativeButton, (dialogInterface, i) -> dialogInterface.dismiss());
        }
        builder.create().show();
    }


    public void downFileFromURL(String url, String fileName) {
        Uri uri = Uri.parse(url);

        // Create request for android download manager
        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);

        // set title and description
        request.setTitle(fileName);
        request.setDescription("Android Data download using DownloadManager.");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        //set the local destination for download file to a path within the application's external files directory
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, fileName);
        request.setMimeType("*/*");
        downloadManager.enqueue(request);
    }

    public long checkFileExist(String url, String fileName, String fileExtension) {
        Uri uri = Uri.parse(url);
        String badgePath = uri.getPath();
        String badgeFile = getExternalFilesDir(DIRECTORY_DOWNLOADS) + "/" + fileName;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (new File(badgeFile).exists()) {
            File file = new File(badgeFile);
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            String extension = MimeTypeMap.getFileExtensionFromUrl(url);

            target.setDataAndType(Uri.fromFile(file), getTypeOfChooser("." + fileExtension));
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(target, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, Uri.fromFile(file), Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            Intent intent = Intent.createChooser(target, "Open File");
            try {
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setDescription("Android Data download using DownloadManager.");
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, fileName);
            request.setMimeType("*/*");
            downloadmanager.enqueue(request);
            return downloadmanager.enqueue(request);


        }
        return 0;
    }

    private String getTypeOfChooser(String url) {
        if (url.contains(".doc") || url.contains(".docx")) {
            // Word document
            return "application/msword";
        } else if (url.contains(".pdf")) {
            // PDF file
            return "application/pdf";
        } else if (url.contains(".ppt") || url.contains(".pptx")) {
            // Powerpoint file
            return "application/vnd.ms-powerpoint";
        } else if (url.contains(".xls") || url.contains(".xlsx")) {
            // Excel file
            return "application/vnd.ms-excel";
        } else if (url.contains(".zip")) {
            // ZIP file
            return "application/zip";
        } else if (url.contains(".rar")) {
            // RAR file
            return "application/x-rar-compressed";
        } else if (url.contains(".rtf")) {
            // RTF file
            return "application/rtf";
        } else if (url.contains(".wav") || url.contains(".mp3")) {
            // WAV audio file
            return "audio/x-wav";
        } else if (url.contains(".gif")) {
            // GIF file
            return "image/gif";
        } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            // JPG file
            return "image/jpeg";
        } else if (url.contains(".txt")) {
            // Text file
            return "text/plain";
        } else if (url.contains(".3gp") || url.contains(".mpg") ||
                url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
            // Video files
            return "video/*";
        } else {
            return "*/*";
        }
    }


    public void submitCallHistory(String duration, String toUserId, String callStatus, String callType) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("session_token", BMSPrefs.getString(this, Constants.SESSION_TOKEN));
        params.put("to_user_id", toUserId);
        params.put("call_status", callStatus);
        params.put("call_type", callType);
        params.put("call_duration", duration);

        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.callHistorySubmit(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull ResponseModel responseModel) {

                    }

                    @Override
                    public void onError(@NotNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}