package com.ablueclive.sinch;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import com.ablueclive.R;
import com.ablueclive.activity.mainActivity.MainActivity;
import com.ablueclive.utils.Constants;
import com.sinch.android.rtc.NotificationResult;

import java.util.Map;


public class IncomingTransparentCallActivity extends BaseCallActivity {

    private NotificationCallVo mCallVo;
    public static String callerName = "";
    public static String callerImage = "";
    public static String isVideo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getParcelableExtra(Constants.PARCELABLE) != null && getIntent().getParcelableExtra(Constants.PARCELABLE) instanceof NotificationCallVo) {
            mCallVo = getIntent().getParcelableExtra(Constants.PARCELABLE);
        }
    }

    @Override
    protected void onServiceConnected(IBinder iBinder) {
        try {
            SinchService.SinchServiceInterface sinchServiceInterface = getSinchServiceInterface();
            Intent intent = new Intent(IncomingTransparentCallActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (mCallVo != null) {
                NotificationResult result = sinchServiceInterface.relayRemotePushNotificationPayload(mCallVo.getData());
                Map<String, String> headerData = result.getCallResult().getHeaders();
                if (headerData != null && headerData.size() > 0) {
                    callerName = headerData.get("USER_NAME");
                    callerImage = headerData.get("USER_IMAGE");
                    isVideo = headerData.get("IS_VIDEO");
                }
            }
            setContentView(R.layout.activity_splash);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
