package com.ablueclive.sinch;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.Constants;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.Beta;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.ManagedPush;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.video.VideoController;

import java.util.HashMap;
import java.util.Map;


public class SinchService extends Service {

/*
IMPORTANT!

This sample application was designed to provide the simplest possible way
to evaluate Sinch Android SDK right out of the box, omitting crucial feature of handling
incoming calls via managed push notifications, which requires registering in FCM console and
procuring google-services.json in order to build and work.

Android 8.0 (API level 26) imposes limitation on background services and we strongly encourage
you to use Sinch Managed Push notifications to handle incoming calls when app is closed or in
background or phone is locked.

DO NOT USE THIS APPLICATION as a skeleton of your project!

Instead, use:
- sinch-rtc-sample-push (for audio calls) and
- sinch-rtc-sample-video-push (for video calls)
*/


  /*public static final String APP_KEY = "afbc4b28-221e-45e5-989b-94b1de191521";
    public static final String APP_SECRET = "8cQXjeafkkytCYmC4Bfv1g==";*/

    public static final String APP_KEY = "ea6207b6-ae33-4e34-90e9-816daad145fe";
    public static final String APP_SECRET = "5s1sUKUfC0ilwdWZbbf2zw==";
    public static final String ENVIRONMENT = "clientapi.sinch.com";

    public static final int MESSAGE_PERMISSIONS_NEEDED = 1;
    public static final String REQUIRED_PERMISSION = "REQUIRED_PESMISSION";
    public static final String MESSENGER = "MESSENGER";
    private Messenger messenger;

    public static final String CALL_ID = "CALL_ID";
    public static final String DATA = "DATA";
    public static final String IS_VIDEO = "IS_VIDEO";
    // public static final String DATA = "DATA";
    static final String TAG = SinchService.class.getSimpleName();

    private SinchServiceInterface mSinchServiceInterface = new SinchServiceInterface();
    public static SinchClient mSinchClient;
    public String mUserId="";

    private static StartFailedListener mListener;
    private PersistedSettings mSettings;

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new PersistedSettings(getApplicationContext());
        attemptAutoStart();
    }

    private void attemptAutoStart() {
        String userName = mSettings.getUsername();
        if (!userName.isEmpty() && messenger != null) {
            start(userName, mSettings.getUsername());
        }
    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminateGracefully();
        }
        super.onDestroy();
    }


    private void start(String userName, String toUserName) {
        boolean permissionsGranted = true;
        if (mSinchClient == null) {
            mSettings.setUsername(userName);
            mUserId = userName;
            createClient(userName, toUserName);
        }
        try {
//mandatory checks
            mSinchClient.checkManifest();
//auxiliary check
            if (getApplicationContext().checkCallingOrSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                throw new MissingPermissionException(Manifest.permission.CAMERA);
            }
        } catch (MissingPermissionException e) {
            permissionsGranted = false;
            if (messenger != null) {
                Message message = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putString(REQUIRED_PERMISSION, e.getRequiredPermission());
                message.setData(bundle);
                message.what = MESSAGE_PERMISSIONS_NEEDED;
                try {
                    messenger.send(message);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        if (permissionsGranted) {
            Log.d(TAG, "Starting SinchClient");
// mSinchClient.start();
        }
    }

    private void createClient(String userName, String toUserName) {
        if (mSinchClient == null) {
            try {
                mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(userName)
                        .applicationKey(APP_KEY)
                        .applicationSecret(APP_SECRET)
                        .environmentHost(ENVIRONMENT).build();

                mSinchClient.setSupportCalling(true);
                mSinchClient.setSupportActiveConnectionInBackground(true);
                mSinchClient.startListeningOnActiveConnection();
                mSinchClient.setSupportPushNotifications(true);
                mSinchClient.setSupportManagedPush(true);
                mSinchClient.setPushNotificationDisplayName(toUserName);
                mSinchClient.addSinchClientListener(new MySinchClientListener());
// mSinchClient.registerPushNotificationData(SharedpreferenceUtility.getInstance().getString(Constants.PREF_KEYS.DEVICE_TOKEN).getBytes());
                mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());
                mSinchClient.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminateGracefully();
            mSinchClient = null;
        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        messenger = intent.getParcelableExtra(MESSENGER);
        return mSinchServiceInterface;
    }


    public class SinchServiceInterface extends Binder {
        public Call callUserVideo(String userId) {
            return mSinchClient.getCallClient().callUserVideo(userId);
        }

        public Call callUserVideo(String userId, HashMap<String, String> map) {
            return mSinchClient.getCallClient().callUserVideo(userId, map);
        }

        public Call callUserVoice(String userId) {
            return mSinchClient.getCallClient().callUser(userId);
        }

        public Call callUserVoice(String userId, HashMap<String, String> map) {
            return mSinchClient.getCallClient().callUser(userId, map);
        }

        public String getUserName() {
            return mUserId;
        }

        public void retryStartAfterPermissionGranted() {
            SinchService.this.attemptAutoStart();
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName, String toUserName) {
            start(userName, toUserName);
        }

        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public Call getCall(String callId) {
            return mSinchClient.getCallClient().getCall(callId);
        }

        public CallClient getCallClient() {
            try {
                return mSinchClient.getCallClient();
            } catch (Exception e) {
                return null;
            }
        }

        public VideoController getVideoController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getVideoController();
        }

        public AudioController getAudioController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getAudioController();
        }

        public ManagedPush getManagedPush(String username) {
// create client, but you don't need to start it
            createClient(username, BMSPrefs.getString(getApplicationContext(), Constants.USER_NAME));
// retrieve ManagedPush
            return Beta.createManagedPush(mSinchClient);
        }

        public NotificationResult relayRemotePushNotificationPayload(HashMap payload) {

            if (mSinchClient == null && !mSettings.getUsername().isEmpty()) {

                startClient(mSettings.getUsername(), mSettings.getUsername());
            } else if (mSinchClient == null && mSettings.getUsername().isEmpty()) {
                Log.e(TAG, "Can't start a SinchClient as no username is available, unable to relay push.");
                return null;
            }
            return mSinchClient.relayRemotePushNotificationPayload(payload);

        }
    }

    public interface StartFailedListener {

        void onStartFailed(SinchError error);

        void onStarted();
    }

    public static class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                                                      ClientRegistration clientRegistration) {
        }
    }

    public class SinchCallClientListener implements CallClientListener {
        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            if (!BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN).isEmpty()) {
                Log.d(TAG, "Incoming call");
                Intent intent = new Intent(SinchService.this, IncomingCallScreenActivity.class);
                Map<String, String> headerData = call.getHeaders();
                if (headerData != null && headerData.size() > 0) {
                    intent.putExtra("USER_IMAGE", headerData.get("USER_IMAGE"));
                    intent.putExtra("USER_NAME", headerData.get("USER_NAME"));
                    intent.putExtra("IS_VIDEO", headerData.get("IS_VIDEO"));
                    intent.putExtra("id", headerData.get("id"));
                    intent.putExtra("RECEIVER_ID", headerData.get("RECEIVER_ID"));
                    intent.putExtra("CALLER_ID", headerData.get("CALLER_ID"));

                } else {
                    intent.putExtra("USER_IMAGE", "");
                    intent.putExtra("USER_NAME", "");
                    intent.putExtra("IS_VIDEO", "");
                    intent.putExtra("id", "");
                    intent.putExtra("RECEIVER_ID", "");
                    intent.putExtra("CALLER_ID", "");
                }
                intent.putExtra(CALL_ID, call.getCallId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SinchService.this.startActivity(intent);
            }
        }
    }

    public class PersistedSettings {

        private SharedPreferences mStore;

        private static final String PREF_KEY = "Sinch";

        public PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        public String getUsername() {
            return mStore.getString("Username", "");
        }

        public void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.apply();
        }
    }
}