package com.ablueclive.sinch;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import androidx.appcompat.app.AppCompatActivity;


public class BaseCallActivity extends AppCompatActivity implements ServiceConnection {
    private SinchService.SinchServiceInterface mSinchServiceInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindService(new Intent(this, SinchService.class), this,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder iBinder) {
        if (SinchService.class.getName().equals(name.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected(iBinder);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (SinchService.class.getName().equals(name.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    @Override
    public void onBindingDied(ComponentName name) {

    }

    protected void onServiceConnected(IBinder iBinder) {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    public void setmSinchServiceInterface(SinchService.SinchServiceInterface mSinchServiceInterface) {
        this.mSinchServiceInterface = mSinchServiceInterface;
    }


    @Override
    protected void onDestroy() {
        unbindService(this);
        super.onDestroy();
    }
}