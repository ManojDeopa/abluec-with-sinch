package com.ablueclive.sinch;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.ablueclive.R;
import com.ablueclive.fcmClass.MyFirebaseMessagingService;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.Constants;
import com.bumptech.glide.Glide;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.video.VideoCallListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.ablueclive.sinch.SinchService.CALL_ID;
import static com.ablueclive.sinch.SinchService.DATA;


public class IncomingCallScreenActivity extends BaseActivity {

    static final String TAG = IncomingCallScreenActivity.class.getSimpleName();
    private String mCallId;
    private AudioPlayer mAudioPlayer;
    private boolean mAcceptVideo = true;
    private ImageView gifImageview;
    private Call incomingCall;
    private TextView remoteUser;
    private String isVideo, mCallerId, CALLER_ID, RECEIVER_ID;
    String mRemoteUserImage, mCallerImage, mCallerName;
    private TextView callState;
    private AppCompatImageView remoteUserImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming);
        Button answer = findViewById(R.id.answerButton);
        RelativeLayout answerRelative = findViewById(R.id.answerRelative);
        answer.setOnClickListener(mClickListener);
        answerRelative.setOnClickListener(mClickListener);
        RelativeLayout declineRelative = findViewById(R.id.declineRelative);
        Button decline = findViewById(R.id.declineButton);
        gifImageview = (ImageView) findViewById(R.id.gif);
        decline.setOnClickListener(mClickListener);
        declineRelative.setOnClickListener(mClickListener);

        remoteUserImage = findViewById(R.id.remoteUserImage);
        remoteUser = findViewById(R.id.remoteUser);
        callState = findViewById(R.id.callState);

        mAudioPlayer = new AudioPlayer(this);
        mAudioPlayer.playRingtone();

        getCallerData();


    }

    private void getCallerData() {

        mCallId = getIntent().getStringExtra(CALL_ID);
        mCallerName = getIntent().getStringExtra("USER_NAME");
        mCallerId = getIntent().getStringExtra("id");
        mCallerImage = getIntent().getStringExtra("USER_IMAGE");
        mRemoteUserImage = mCallerImage;
        isVideo = getIntent().getStringExtra("IS_VIDEO");
        RECEIVER_ID = getIntent().getStringExtra("RECEIVER_ID");
        CALLER_ID = getIntent().getStringExtra("CALLER_ID");

        if (TextUtils.isEmpty(mCallerName)) {
            mCallerName = IncomingTransparentCallActivity.callerName;
            mCallerImage = IncomingTransparentCallActivity.callerImage;
            isVideo = IncomingTransparentCallActivity.isVideo;
            IncomingTransparentCallActivity.callerName = "";
            IncomingTransparentCallActivity.callerImage = "";
            IncomingTransparentCallActivity.isVideo = "";
        }

        remoteUser.setText(mCallerName);

        if (!TextUtils.isEmpty(mCallerImage)) {
            Picasso.get().load(Constants.PROFILE_IMAGE_URL + mCallerImage).into(remoteUserImage);
        }

        if (!isVideo.isEmpty() && isVideo.equalsIgnoreCase("1")) {
            callState.setText("aBlueC Video Call from");
        }

        Glide.with(this).load(R.raw.call_animation).into(gifImageview);

    }

    @Override
    protected void onServiceConnected() {
        if (mCallId != null) {
            if (getSinchServiceInterface().getCallClient() != null) {
                Log.e(TAG, "mCallIddd: " + mCallId);
                Call call = getSinchServiceInterface().getCall(mCallId);
                incomingCall = call;
                if (call != null) {
                    call.addCallListener(new SinchCallListener());
                } else {
                    Log.e(TAG, "Started with invalid callId, aborting");
                    finish();
                }
            } else {
                new ServiceConnection() {
                    private Map payload;

                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        String finalUserName = BMSPrefs.getString(IncomingCallScreenActivity.this, Constants._ID);
                        if (payload != null) {
                            Log.d("payload", "payload : " + payload);
                            SinchService.SinchServiceInterface sinchService = (SinchService.SinchServiceInterface) service;
                            if (sinchService != null && !sinchService.isStarted() && finalUserName != null && !finalUserName.isEmpty()) {
                                Log.d("payload", "name : " + finalUserName);
                                SinchService.mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(finalUserName)
                                        .applicationKey(SinchService.APP_KEY)
                                        .applicationSecret(SinchService.APP_SECRET)
                                        .environmentHost(SinchService.ENVIRONMENT).build();
                                Log.d("payload", "name2 : " + finalUserName);
                                SinchService.mSinchClient.setSupportCalling(true);
                                SinchService.mSinchClient.setSupportActiveConnectionInBackground(true);
                                SinchService.mSinchClient.startListeningOnActiveConnection();
                                SinchService.mSinchClient.setSupportPushNotifications(true);
                                SinchService.mSinchClient.setSupportManagedPush(true);
                                SinchService.mSinchClient.start();
                                SinchService.mSinchClient.addSinchClientListener(new MySinchClientListener());
                                SinchService.mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

                                try {
                                    Log.d(TAG, "Incoming call");
                                    JSONObject object = new JSONObject(Objects.requireNonNull(MyFirebaseMessagingService.Companion.getRemoteData()));
                                    String aa = object.getString("sinch");
                                    JSONObject object1 = new JSONObject(aa);
                                    String session_id = object1.getString("session_id");
                                    Log.e(TAG, "user_idMM: " + object.toString() + " hgrfg " + object1.toString());


                                    incomingCall = SinchService.mSinchClient.getCallClient().getCall(session_id);
                                    if (incomingCall != null) {
                                        incomingCall.addCallListener(new SinchCallListener());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        }
                        payload = null;
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                    }

                    public void relayMessageData(Map<String, String> data) {
                        payload = data;
                        getApplicationContext().bindService(new Intent(getApplicationContext(), SinchService.class), this, BIND_AUTO_CREATE);
                    }
                }.relayMessageData(Objects.requireNonNull(MyFirebaseMessagingService.Companion.getRemoteData()));
            }

        }
    }

    private void answerClicked() {
        mAudioPlayer.stopRingtone();
        if (incomingCall != null) {
            incomingCall.answer();
            Intent intent = new Intent(this, CallScreenActivity.class);
            intent.putExtra(CALL_ID, mCallId);
            intent.putExtra(DATA, "shubham");
            intent.putExtra("USER_NAME", remoteUser.getText().toString());
            intent.putExtra("USER_Id", mCallerId);
            intent.putExtra("IS_VIDEO", isVideo);
            intent.putExtra("CALLER_ID", CALLER_ID);
            intent.putExtra("RECEIVER_ID", RECEIVER_ID);
            intent.putExtra("USER_IMAGE", mRemoteUserImage);
            startActivity(intent);
        } else {
            finish();
        }
    }


    public class SinchCallClientListener implements CallClientListener {
        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            if (callClient != null) {
                incomingCall = call;
                Log.e(TAG, "mCallIddd: " + mCallId);
                call = getSinchServiceInterface().getCall(mCallId);
                if (call != null) {
                    call.addCallListener(new SinchCallListener());
                    TextView remoteUser = (TextView) findViewById(R.id.remoteUser);
                    remoteUser.setText(call.getRemoteUserId());
                } else {
                    Log.e(TAG, "Started with invalid callId, aborting");
                    finish();
                }
            }
        }
    }

    private void declineClicked() {
        mAudioPlayer.stopRingtone();
        if (incomingCall != null) {
            submitCallHistory(incomingCall.getDetails().getDuration() + "", RECEIVER_ID, "3", "1");
            incomingCall.hangup();
        }
        finish();
    }


    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());
            mAudioPlayer.stopRingtone();
            finish();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d("qwerty", "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
// Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
// Display some kind of icon showing it's a video call
// and pass it to the CallScreenActivity via Intent and mAcceptVideo
            mAcceptVideo = true;
        }

        @Override
        public void onVideoTrackPaused(Call call) {
// Display some kind of icon showing it's a video call
        }

        @Override
        public void onVideoTrackResumed(Call call) {
// Display some kind of icon showing it's a video call
        }
    }


    public class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            client.terminate();
            SinchService.mSinchClient.terminate();
            SinchService.mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            String finalUserName = BMSPrefs.getString(IncomingCallScreenActivity.this, Constants._ID);
            incomingCall = client.getCallClient().getCall(finalUserName);
        }

        @Override
        public void onClientStopped(SinchClient client) {

        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client, ClientRegistration clientRegistration) {
        }
    }

    @SuppressLint("NonConstantResourceId")
    private final OnClickListener mClickListener = v -> {
        switch (v.getId()) {
            case R.id.answerButton:
            case R.id.answerRelative:
                answerClicked();
                break;
            case R.id.declineButton:
            case R.id.declineRelative:
                declineClicked();
                break;
        }
    };

}