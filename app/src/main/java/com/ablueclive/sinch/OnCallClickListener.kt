package com.ablueclive.sinch

interface OnCallClickListener {
    fun onActionVideoCall(userId: String, toUserName: String, toUserImage: String)
    fun onActionVoiceCall(userId: String, toUserName: String, toUserImage: String)
}