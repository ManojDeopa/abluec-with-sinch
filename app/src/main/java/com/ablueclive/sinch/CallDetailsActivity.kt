package com.ablueclive.sinch


import android.os.Bundle
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_call_details.*
import java.util.*


class CallDetailsActivity : BaseActivity(), OnCallClickListener {

    private var callType = ""
    public var selectedPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_details)

        ivBack.setOnClickListener { onBackPressed() }

        callType = intent.getStringExtra("CallType")!!
        if (callType == "2") {
            tvTitle.text = getString(R.string.txt_audio_call_details)
        } else {
            tvTitle.text = getString(R.string.txt_video_call_details)
        }

        addTabViewPager()

    }


    override fun onActionVideoCall(userId: String, toUserName: String, toUserImage: String) {
        actionVideoCall(userId, toUserName, toUserImage)
    }

    override fun onActionVoiceCall(userId: String, toUserName: String, toUserImage: String) {
        actionVoiceCall(userId, toUserName, toUserImage)

    }


    private fun addTabViewPager() {
        val list = listOf(getString(R.string.all_calls), getString(R.string.missed))
       /* val adapter = ViewPagerAdapter(this)
        viewPager.adapter = adapter*/
        TabLayoutMediator(tabLayout, viewPager,
                TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                    tab.text = list[position]
                }).attach()
    }


   /* internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {
        @NonNull
        override fun createFragment(position: Int): Fragment {
            val fragment: Fragment? = when (position) {

                0 -> {
                    CallHistoryFragment(callType)
                }
                else -> {
                    MissedCallFragments(callType)
                }
            }

            return fragment!!
        }

        override fun getItemCount(): Int {
            return 2
        }
    }*/
}