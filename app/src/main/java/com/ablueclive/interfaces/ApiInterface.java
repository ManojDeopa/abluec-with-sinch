package com.ablueclive.interfaces;

import com.ablueclive.activity.profile.GetImageVideosResponse;
import com.ablueclive.activity.profile.MyProfileResponse;
import com.ablueclive.common.CommonResponse;
import com.ablueclive.common.SearchDisplayResponse;
import com.ablueclive.common.SimpleResponse;
import com.ablueclive.global.response.CombineFriendResponse;
import com.ablueclive.global.response.CountryStateListResponse;
import com.ablueclive.global.response.FavSProvidersResponse;
import com.ablueclive.global.response.FollowListResponse;
import com.ablueclive.global.response.JobsListResponse;
import com.ablueclive.global.response.SearchListResponse;
import com.ablueclive.global.response.SearchStoreListResponse;
import com.ablueclive.global.response.SimilarProductResponse;
import com.ablueclive.global.response.TopicsResponse;
import com.ablueclive.jobSection.categoryFragment.CategoryListResponse;
import com.ablueclive.jobSection.completeJob.SCompleteJobResponse;
import com.ablueclive.jobSection.newJobFragment.JobListingResponse;
import com.ablueclive.jobSection.newJobFragment.NewJobDetailResponse;
import com.ablueclive.jobSection.runningJob.PJobDetailResponse;
import com.ablueclive.jobSection.runningJob.RunningJobListResponse;
import com.ablueclive.jobSection.runningJob.SRunningJobResponse;
import com.ablueclive.jobSection.searchJob.JobRequestResponse;
import com.ablueclive.jobSection.serviceProviders.ServiceProvidersResponse;
import com.ablueclive.jobSection.subCategory.SubCategoryResponse;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.SharePostResponse;
import com.ablueclive.network.response.MyFriendListResponse;
import com.ablueclive.storeSection.response.AdminCategoryResponse;
import com.ablueclive.storeSection.response.AgentDetailResponse;
import com.ablueclive.storeSection.response.AgentListResponse;
import com.ablueclive.storeSection.response.CartListResponse;
import com.ablueclive.storeSection.response.OtherCategoryResponse;
import com.ablueclive.storeSection.response.SkuProductResponse;
import com.ablueclive.storeSection.response.StoreCalendarResponse;
import com.ablueclive.storeSection.response.StoreCategoryResponse;
import com.ablueclive.storeSection.response.StoreListResponse;
import com.ablueclive.storeSection.response.StoreOrderListResponse;
import com.ablueclive.storeSection.response.StoreProductListResponse;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface ApiInterface {

    @POST("users/register")
    io.reactivex.Observable<ResponseModel> register(@Body Map<String, String> map);

    @POST("users/login")
    io.reactivex.Observable<ResponseModel> login(@Body Map<String, String> map);

    @POST("users/forgetpassword")
    io.reactivex.Observable<ResponseModel> forgetpassword(@Body Map<String, String> map);

    @POST("users/resetpassword")
    io.reactivex.Observable<ResponseModel> resetpassword(@Body Map<String, String> map);

    @POST("users/logout")
    io.reactivex.Observable<ResponseModel> logout(@Body Map<String, String> map);

    @POST("store/addStore")
    @Multipart
    io.reactivex.Observable<ResponseModel> addStore(@PartMap Map<String, RequestBody> map);

    @POST("store/editStore")
    @Multipart
    io.reactivex.Observable<ResponseModel> editStore(@PartMap Map<String, RequestBody> map);

    @POST("store/getStoreInfo")
    io.reactivex.Observable<ResponseModel> getStoreInfo(@Body Map<String, String> map);

    @POST("store/categoryListing")
    io.reactivex.Observable<ResponseModel> categoryListing(@Body Map<String, String> map);

    @POST("store/addProduct")
    io.reactivex.Observable<ResponseModel> addProduct(@Body RequestBody file);

    @POST("store/getAllStoreProducts")
    io.reactivex.Observable<ResponseModel> getAllStoreProducts(@Body Map<String, String> map);

    @POST("store/getProductDetails")
    io.reactivex.Observable<ResponseModel> getProductDetails(@Body Map<String, String> map);

    @POST("store/storePage")
    io.reactivex.Observable<ResponseModel> storePage(@Body Map<String, String> map);

    @POST("store/favoritesPage")
    io.reactivex.Observable<ResponseModel> favoritesPage(@Body Map<String, String> map);

    @POST("store/makeFavorite")
    io.reactivex.Observable<ResponseModel> makeFavorite(@Body Map<String, String> map);

    @POST("post/sharePost")
    io.reactivex.Observable<ResponseModel> shareWorkBench(@Body Map<String, Object> map);

    @POST("store/allCategoriesProduct")
    io.reactivex.Observable<ResponseModel> allCategoriesProduct(@Body Map<String, String> map);


    @POST("store/favoriteStores")
    io.reactivex.Observable<ResponseModel> favoriteStores(@Body Map<String, String> map);

    @POST("store/favoriteProducts")
    io.reactivex.Observable<ResponseModel> favoriteProducts(@Body Map<String, String> map);

    @POST("store/getSingleCategoryProducts")
    io.reactivex.Observable<ResponseModel> getSingleCategoryProducts(@Body Map<String, String> map);


    @POST("pages/page-details")
    io.reactivex.Observable<ResponseModel> getStaticApi(@Body Map<String, String> map);

    @POST("post/profileImage")
    io.reactivex.Observable<ResponseModel> profileImage(@Body RequestBody map);

    @POST("users/uploadCoverPhoto")
    io.reactivex.Observable<CommonResponse> uploadCoverPhoto(@Body RequestBody map);


    @POST("post/backgroundProfile")
    @Multipart
    io.reactivex.Observable<ResponseModel> backgroundProfile(@PartMap Map<String, RequestBody> map);

    @POST("users/editProfile")
    io.reactivex.Observable<ResponseModel> editProfile(@Body Map<String, String> map);

    // friend module api
    @POST("friend/searchUserLists")
    io.reactivex.Observable<ResponseModel> searchUserLists(@Body Map<String, String> map);

    // Add Friend or Send Friend Request
    @POST("friend/addFriend")
    io.reactivex.Observable<ResponseModel> addFriend(@Body Map<String, String> map);

    @POST("friend/UnFriendRequest")
    io.reactivex.Observable<ResponseModel> UnFriendRequest(@Body Map<String, String> map);


    // on Network Tab
    @POST("friend/getFriendList")
    io.reactivex.Observable<ResponseModel> getFriendList(@Body Map<String, String> map);

    @POST("post/getpostListsProfile")
    io.reactivex.Observable<ResponseModel> getpostListsProfile(@Body Map<String, String> map);








    /* try{}catch checked ... */

    // on Network Tab - list - on connect/confirm Click

    @POST("friend/confirmFriendInList")
    io.reactivex.Observable<ResponseModel> confirmFriendInList(@Body Map<String, String> map);

    // on Network Tab - list - on cross/cancel Click
    @POST("friend/deleteFriendInList")
    io.reactivex.Observable<ResponseModel> deleteFriendInList(@Body Map<String, String> map);

    @POST("friend/getAllFriendsList")
    io.reactivex.Observable<ResponseModel> getAllFriendsList(@Body Map<String, String> map);

    //post related api
    @POST("post/deletePost")
    io.reactivex.Observable<ResponseModel> deletePost(@Body Map<String, String> map);

    @POST("post/getAllImages")
    io.reactivex.Observable<ResponseModel> getAllImages(@Body Map<String, String> map);

    @POST("post/postLike")
    io.reactivex.Observable<ResponseModel> postLike(@Body Map<String, String> map);

    @POST("post/getpostLists")
    io.reactivex.Observable<ResponseModel> getpostLists(@Body Map<String, String> map);

    @POST("post/addPostReport")
    io.reactivex.Observable<ResponseModel> addPostReport(@Body Map<String, String> map);

    @POST("post/addPost")
    io.reactivex.Observable<ResponseModel> addPost(@Body RequestBody file);

    // comment
    @POST("post/getComments")
    io.reactivex.Observable<ResponseModel> getComments(@Body Map<String, String> map);

    @POST("post/addComments")
    io.reactivex.Observable<ResponseModel> addComments(@Body Map<String, String> map);

    @POST("post/deleteComments")
    io.reactivex.Observable<ResponseModel> deleteComments(@Body Map<String, String> map);

    @POST("post/addSubComments")
    io.reactivex.Observable<ResponseModel> addSubComments(@Body Map<String, String> map);

    @POST("post/deleteSubComments")
    io.reactivex.Observable<ResponseModel> deleteSubComments(@Body Map<String, String> map);

    @POST("post/senderCommentsReport")
    io.reactivex.Observable<ResponseModel> senderCommentsReport(@Body Map<String, String> map);

    @POST("friend/lastChatMessage")
    io.reactivex.Observable<ResponseModel> lastChatMessage(@Body Map<String, String> map);

    @POST("store/sendReportItem")
    io.reactivex.Observable<ResponseModel> sendReportItem(@Body Map<String, String> map);

    @POST("post/getCheerList")
    io.reactivex.Observable<ResponseModel> getCheerList(@Body Map<String, String> map);

    @POST("post/getNotificationList")
    io.reactivex.Observable<ResponseModel> getNotificationList(@Body Map<String, String> map);

    @POST("store/getProductFavoriteStatus")
    io.reactivex.Observable<ResponseModel> getProductFavoriteStatus(@Body Map<String, String> map);

    @POST("users/changePassword")
    io.reactivex.Observable<ResponseModel> changePassword(@Body Map<String, String> map);


    @POST("friend/readUnreadMesg")
    io.reactivex.Observable<ResponseModel> readUnreadMesg(@Body Map<String, String> map);

    @POST("store/sellarChat")
    io.reactivex.Observable<ResponseModel> sellarChat(@Body Map<String, String> map);

    @POST("store/sellarList")
    io.reactivex.Observable<ResponseModel> sellarList(@Body Map<String, String> map);

    @POST("store/editProduct")
    io.reactivex.Observable<ResponseModel> editProduct(@Body RequestBody file);


    @POST("users/addFavServiceProvider")
    io.reactivex.Observable<CommonResponse> addFavServiceProvider(@Body Map<String, String> map);

    @POST("users/removeFavServiceProvider")
    io.reactivex.Observable<CommonResponse> removeFavServiceProvider(@Body Map<String, String> map);


    // Feed Fragment-> Suggestion List -
    @POST("friend/suggestionFriendList")
    io.reactivex.Observable<ResponseModel> suggestionFriendList(@Body Map<String, String> map);

    @POST("store/deleteProduct")
    io.reactivex.Observable<ResponseModel> deleteProduct(@Body Map<String, String> map);

    @POST("post/readNotification")
    io.reactivex.Observable<ResponseModel> readNotification(@Body Map<String, String> map);

    @POST("post/editPost")
    io.reactivex.Observable<ResponseModel> editPost(@Body RequestBody file);

    @POST("store/deleteStore")
    io.reactivex.Observable<ResponseModel> deleteStore(@Body Map<String, String> map);


    /*@POST("users/socialRegister")
    io.reactivex.Observable<ResponseModel> SocialLogin(@Body Map<String, String> map);*/

    @POST("users/socialRegisterNew")
    io.reactivex.Observable<ResponseModel> SocialLogin(@Body Map<String, String> map);

   /* @POST("users/getprofileInfo")
    io.reactivex.Observable<ResponseModel> GetProfileData(@Body Map<String, String> map);*/

    @POST("post/getpostListsProfile")
    io.reactivex.Observable<MyProfileResponse> GetProfileData(@Body Map<String, String> map);

    @POST("post/getPostListUserProfile")
    io.reactivex.Observable<MyProfileResponse> getFriendProfileData(@Body Map<String, String> map);

    @POST("users/deleteSkills")
    io.reactivex.Observable<CommonResponse> deleteSkills(@Body Map<String, String> map);


    @POST("users/addSkills")
    io.reactivex.Observable<CommonResponse> addSkills(@Body JsonObject jsonObject);

    @POST("users/editSkills")
    io.reactivex.Observable<CommonResponse> editSkills(@Body Map<String, String> map);

    @POST("users/getjobUserList")
    io.reactivex.Observable<JobsListResponse> getjobUserList(@Body Map<String, String> map);

    @POST("users/getAppliedjobList")
    io.reactivex.Observable<JobsListResponse> getAppliedjobList(@Body Map<String, String> map);

    @POST("friend/searchUserLists")
    io.reactivex.Observable<SearchListResponse> searchUserListsApi(@Body Map<String, String> map);

    @POST("store/searchProductByName")
    io.reactivex.Observable<StoreProductListResponse> searchProductByName(@Body Map<String, String> map);


    @POST("users/getFavServiceProvider")
    io.reactivex.Observable<FavSProvidersResponse> getFavServiceProvider(@Body Map<String, String> map);

    @POST("post/addExperience")
    io.reactivex.Observable<CommonResponse> addExperience(@Body Map<String, String> map);

    @POST("post/deleteExperience")
    io.reactivex.Observable<CommonResponse> deleteExperience(@Body Map<String, String> map);

    @POST("users/getCountries")
    io.reactivex.Observable<CountryStateListResponse> getCountries(@Body Map<String, String> map);

    @POST("users/getStateFromCountry")
    io.reactivex.Observable<CountryStateListResponse> getStateFromCountry(@Body Map<String, String> map);

    @POST("users/saveAddress")
    io.reactivex.Observable<CommonResponse> saveAddress(@Body Map<String, String> map);

    @POST("post/getAllImages")
    io.reactivex.Observable<GetImageVideosResponse> getAllImagesApi(@Body Map<String, String> map);



    /* ----------------------------------------------------------- */

    /* Job Section APIs */


    @POST("category/list")
    io.reactivex.Observable<CategoryListResponse> categoryList(@Body Map<String, String> map);

    @POST("category/subCategories")
    io.reactivex.Observable<SubCategoryResponse> subCategoryList(@Body Map<String, String> map);


    @POST("users/sendjobrequest")
    io.reactivex.Observable<CommonResponse> sendJobRequest(@Body RequestBody file);

    /* 1= New Jobs , 2= Running  4= Completed */
    @POST("users/getjoblisting")
    io.reactivex.Observable<JobListingResponse> getJobListing(@Body Map<String, String> map);

    @POST("users/getjoblisting")
    io.reactivex.Observable<JobRequestResponse> getNewPostedJobListing(@Body Map<String, String> map);

    @POST("users/getjoblisting")
    io.reactivex.Observable<RunningJobListResponse> getRunningJobListing(@Body Map<String, String> map);

    @POST("users/getjobDetails")
    io.reactivex.Observable<NewJobDetailResponse> getJobDetail(@Body Map<String, String> map);

    @POST("users/getjobUserDetails")
    io.reactivex.Observable<PJobDetailResponse> getJobUserDetails(@Body Map<String, String> map);

    @POST("users/deletejobUser")
    io.reactivex.Observable<CommonResponse> deleteJob(@Body Map<String, String> map);


    @POST("users/getJobRequestApi")
    io.reactivex.Observable<JobRequestResponse> getJobRequestApi(@Body Map<String, String> map);

    @POST("users/acceptJobRequestApi")
    io.reactivex.Observable<CommonResponse> acceptJobRequestApi(@Body Map<String, String> map);

    @POST("users/saveJobForLater")
    io.reactivex.Observable<CommonResponse> saveJobForLater(@Body Map<String, String> map);

    @POST("users/jobRequestApi")
    io.reactivex.Observable<CommonResponse> jobRequestApi(@Body Map<String, String> map);

    @POST("users/rejectJobRequestApi")
    io.reactivex.Observable<CommonResponse> rejectJobRequestApi(@Body Map<String, String> map);

    @POST("users/saveIgnoredJob")
    io.reactivex.Observable<CommonResponse> saveIgnoredJob(@Body Map<String, String> map);

    @POST("users/getSeekerRunningJob")
    io.reactivex.Observable<SRunningJobResponse> getSeekerRunningJob(@Body Map<String, String> map);

    @POST("users/getSeekerCompleteJob")
    io.reactivex.Observable<SCompleteJobResponse> getSeekerCompleteJob(@Body Map<String, String> map);

    @POST("users/completeRequestApi")
    io.reactivex.Observable<CommonResponse> completeRequestApi(@Body Map<String, String> map);

    @POST("users/rateServiceProvider")
    io.reactivex.Observable<CommonResponse> rateServiceProvider(@Body Map<String, String> map);

    @POST("users/rateJobProvider")
    io.reactivex.Observable<CommonResponse> rateJobProvider(@Body Map<String, String> map);

    @POST("users/updateJobStatus")
    io.reactivex.Observable<CommonResponse> updateJobStatus(@Body Map<String, String> map);

    @POST("users/editJobRequest")
    io.reactivex.Observable<CommonResponse> editJobRequest(@Body RequestBody file);


    /* Store Section APIs */

    @POST("store/addStore")
    io.reactivex.Observable<CommonResponse> addStoreApi(@Body RequestBody file);

    @POST("store/changeAvailabilityStatus")
    io.reactivex.Observable<CommonResponse> changeAvailabilityStatus(@Body Map<String, String> map);

    @POST("store/editStore")
    io.reactivex.Observable<CommonResponse> editStore(@Body RequestBody file);

    @POST("store/deleteStore")
    io.reactivex.Observable<CommonResponse> deleteStoreApi(@Body Map<String, String> map);

    @POST("store/getStores")
    io.reactivex.Observable<StoreListResponse> getStores(@Body Map<String, String> map);

    @POST("store/categoryListing")
    io.reactivex.Observable<AdminCategoryResponse> adminCategoryListing(@Body Map<String, String> map);

    @POST("store/getStoreCategory")
    io.reactivex.Observable<StoreCategoryResponse> getStoreCategory(@Body Map<String, String> map);

    @POST("store/addCategory")
    io.reactivex.Observable<CommonResponse> addCategory(@Body RequestBody file);

    @POST("store/selectCategoryFromList")
    io.reactivex.Observable<CommonResponse> selectCategoryFromList(@Body Map<String, String> map);

    @POST("store/deleteCategory")
    io.reactivex.Observable<CommonResponse> deleteCategory(@Body Map<String, String> map);

    @POST("store/addProduct")
    io.reactivex.Observable<CommonResponse> addProductApi(@Body RequestBody file);

    @POST("store/editProduct")
    io.reactivex.Observable<CommonResponse> editProductApi(@Body RequestBody file);

    @POST("store/storeCategoryProducts")
    io.reactivex.Observable<StoreProductListResponse> storeCategoryProducts(@Body Map<String, String> map);

    @POST("store/deleteProduct")
    io.reactivex.Observable<CommonResponse> deleteProductApi(@Body Map<String, String> map);

    @POST("store/cloneStore")
    io.reactivex.Observable<CommonResponse> cloneStore(@Body Map<String, String> map);

    @POST("store/getStoreCalender")
    io.reactivex.Observable<StoreCalendarResponse> getStoreCalender(@Body Map<String, String> map);

    @POST("store/setStoreCalender")
    io.reactivex.Observable<CommonResponse> setStoreCalender(@Body JsonObject jsonObject);

    @POST("store/saveAgent")
    io.reactivex.Observable<CommonResponse> saveAgent(@Body RequestBody file);

    @POST("store/listAgent")
    io.reactivex.Observable<AgentListResponse> listAgent(@Body Map<String, String> map);

    @POST("store/getDetailsOfAgentByEmail")
    io.reactivex.Observable<AgentDetailResponse> getDetailsOfAgentByEmail(@Body Map<String, String> map);

    @POST("store/getStoresNearBy")
    io.reactivex.Observable<CommonResponse> getStoresNearBy(@Body Map<String, String> map);

    @POST("store/searchStoreByName")
    io.reactivex.Observable<SearchStoreListResponse> searchStoreByName(@Body Map<String, String> map);

    @POST("store/topicSearch")
    io.reactivex.Observable<TopicsResponse> topicSearch(@Body Map<String, String> map);


    @POST("store/similarProducts")
    io.reactivex.Observable<SimilarProductResponse> similarProducts(@Body Map<String, String> map);

    @POST("store/addUpdateProductToCart")
    io.reactivex.Observable<CommonResponse> addUpdateProductToCart(@Body Map<String, String> map);

    @POST("store/getCart")
    io.reactivex.Observable<CartListResponse> getCart(@Body Map<String, String> map);

    @POST("store/createOrder")
    io.reactivex.Observable<CommonResponse> createOrder(@Body Map<String, String> map);

    @POST("store/customerGetStoreCategories")
    io.reactivex.Observable<StoreCategoryResponse> customerGetStoreCategories(@Body Map<String, String> map);

    @POST("store/customerGetStoreProductByCategory")
    io.reactivex.Observable<StoreProductListResponse> customerGetStoreProductByCategory(@Body Map<String, String> map);

    @POST("store/customerFavouriteProductList")
    io.reactivex.Observable<StoreProductListResponse> customerFavouriteProductList(@Body Map<String, String> map);

    @POST("store/customerSaveFavouriteProduct")
    io.reactivex.Observable<CommonResponse> customerSaveFavouriteProduct(@Body Map<String, String> map);

    @POST("store/customerRemoveFavouriteProduct")
    io.reactivex.Observable<CommonResponse> customerRemoveFavouriteProduct(@Body Map<String, String> map);

    @POST("store/getOrderByCustomerId")
    io.reactivex.Observable<StoreOrderListResponse> getOrderByCustomerId(@Body Map<String, String> map);

    @POST("store/getOrderByStoreId")
    io.reactivex.Observable<StoreOrderListResponse> getOrderByStoreId(@Body Map<String, String> map);

    @POST("store/changeOrderStatus")
    io.reactivex.Observable<CommonResponse> changeOrderStatus(@Body Map<String, String> map);

    @POST("store/consolidateAllStoreOrders")
    io.reactivex.Observable<StoreOrderListResponse> consolidateAllStoreOrders(@Body Map<String, String> map);

    @POST("friend/getCombineFriendRecord")
    io.reactivex.Observable<CombineFriendResponse> getCombineFriendRecord(@Body Map<String, String> map);

   /* @POST("friend/getFriendList")
    io.reactivex.Observable<MyFriendListResponse> getFriendListNetwork(@Body Map<String, String> map);*/

    @POST("friend/getAllFriendsList")
    io.reactivex.Observable<MyFriendListResponse> getFriendListNetwork(@Body Map<String, String> map);

    @POST("store/getCategory")
    io.reactivex.Observable<StoreCategoryResponse> getCategory(@Body Map<String, String> map);


    @POST("store/getStoreParentCategories")
    io.reactivex.Observable<StoreCategoryResponse> getStoreParentCategories(@Body Map<String, String> map);

    @POST("store/getAllChildCategoryProducts")
    io.reactivex.Observable<StoreProductListResponse> getAllChildCategoryProducts(@Body Map<String, String> map);

    @POST("store/getBarCodeProductInfo")
    io.reactivex.Observable<SkuProductResponse> getBarCodeProductInfo(@Body Map<String, String> map);

    @POST("store/rateOrder")
    io.reactivex.Observable<CommonResponse> rateOrder(@Body Map<String, String> map);


    @POST("store/getStoreById")
    io.reactivex.Observable<SearchStoreListResponse> getStoreById(@Body Map<String, String> map);


    @POST("store/otherCategories")
    io.reactivex.Observable<OtherCategoryResponse> otherCategories(@Body Map<String, String> map);


    @POST("post/uploadResume")
    io.reactivex.Observable<CommonResponse> uploadResume(@Body RequestBody file);

    @POST("post/uploadCoverLetter")
    io.reactivex.Observable<CommonResponse> uploadCoverLetter(@Body RequestBody file);


    @POST("post/followlist")
    io.reactivex.Observable<FollowListResponse> followersList(@Body Map<String, String> map);

    @POST("post/followinglist")
    io.reactivex.Observable<FollowListResponse> followingsList(@Body Map<String, String> map);


    @POST("post/follow")
    io.reactivex.Observable<CommonResponse> follow(@Body Map<String, String> map);

    @POST("post/unfollow")
    io.reactivex.Observable<CommonResponse> unFollow(@Body Map<String, String> map);


    @POST("users/setProfilePrivate")
    io.reactivex.Observable<CommonResponse> setProfilePrivate(@Body Map<String, String> map);

    @POST("post/followAccepted")
    io.reactivex.Observable<CommonResponse> followAccepted(@Body Map<String, String> map);

    @POST("post/followRejected")
    io.reactivex.Observable<CommonResponse> followRejected(@Body Map<String, String> map);

    @POST("users/feedback")
    io.reactivex.Observable<CommonResponse> feedback(@Body Map<String, String> map);

    @POST("users/findByDisplayName")
    io.reactivex.Observable<SearchDisplayResponse> findByDisplayName(@Body Map<String, String> map);

    @POST("post/sharePost")
    io.reactivex.Observable<SharePostResponse> sharePost(@Body Map<String, String> map);

    @POST("post/reTweetPost")
    io.reactivex.Observable<SharePostResponse> reTweetPost(@Body Map<String, String> map);

    @POST("post/deleteReTweetPost")
    io.reactivex.Observable<SharePostResponse> deleteReTweetPost(@Body Map<String, String> map);

    @POST("post/postDetails")
    io.reactivex.Observable<CommonResponse> postDetails(@Body Map<String, String> map);

    @POST("users/removeAttachments")
    io.reactivex.Observable<CommonResponse> removeAttachments(@Body Map<String, String> map);

    @POST("users/getserviceprovider")
    io.reactivex.Observable<ServiceProvidersResponse> serviceProviderList(@Body Map<String, String> map);

    @POST("post/addAcademic")
    io.reactivex.Observable<CommonResponse> addAcademic(@Body Map<String, String> map);

    @POST("post/deleteAcademic")
    io.reactivex.Observable<CommonResponse> deleteAcademic(@Body Map<String, String> map);

    @POST("users/getcancelAppliedjobList")
    io.reactivex.Observable<SimpleResponse> getcancelAppliedjobList(@Body Map<String, String> map);

    @POST("users/deleteChat")
    io.reactivex.Observable<ResponseModel> deleteChat(@Body Map<String, String> map);

    @POST("users/blockChat")
    io.reactivex.Observable<ResponseModel> blockChat(@Body Map<String, String> map);

    @POST("users/reportUserReason")
    io.reactivex.Observable<ResponseModel> reportUserReason(@Body Map<String, String> map);

    @POST("users/reportUser")
    io.reactivex.Observable<ResponseModel> reportUser(@Body Map<String, String> map);

    @POST("users/muteChatNotification")
    io.reactivex.Observable<ResponseModel> muteChatNotification(@Body Map<String, String> map);

    @POST("users/checkOtherBlockYou")
    io.reactivex.Observable<ResponseModel> checkOtherBlockYou(@Body Map<String, String> map);

    @POST("users/callHistorySubmit")
    io.reactivex.Observable<ResponseModel> callHistorySubmit(@Body Map<String, String> map);

    @POST("friend/readMessage")
    io.reactivex.Observable<ResponseModel> readMessage(@Body Map<String, String> map);

    @POST("users/checkContacts")
    io.reactivex.Observable<ResponseModel> checkContacts(@Body Map<String, String> map);

    @POST("users/inviteContacts")
    io.reactivex.Observable<ResponseModel> inviteContacts(@Body Map<String, String> map);

    @POST("users/changeAvailablityStatus")
    io.reactivex.Observable<ResponseModel> changeAvailablityStatus(@Body Map<String, String> map);


}
