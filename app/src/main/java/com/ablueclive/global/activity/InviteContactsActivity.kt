package com.ablueclive.global.activity

import AppUtils
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ablueclive.R
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.global.fragment.InviteContactsFragment
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.EditTextChangeListener
import kotlinx.android.synthetic.main.invite_contact_activity.*
import kotlinx.android.synthetic.main.search_layout.*


class InviteContactsActivity : AppCompatActivity() {


    companion object {
        var textListener: EditTextChangeListener? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.invite_contact_activity)
        init()
    }

    private fun init() {
        tvTitle.text = getString(R.string.invite_contacts)
        ivBack.setOnClickListener {
            onBackPressed()
        }

        AppUtils.addFragment(this, InviteContactsFragment.newInstance(Bundle()), R.id.mContainer)

        searchLayout.visibility = View.GONE
        ivSearch.setOnClickListener {
            ivBack.visibility = View.GONE
            ivSearch.visibility = View.GONE
            searchLayout.visibility = View.VISIBLE
        }

        ivCross.visibility = View.VISIBLE
        ivCross.setOnClickListener {
            ivSearchTop.text = null
            searchLayout.visibility = View.GONE
            ivSearch.visibility = View.VISIBLE
            ivBack.visibility = View.VISIBLE
            textListener?.onTextChange("")
        }


        ivSearchTop.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                // ivCross.visibility = if (s.toString().isEmpty()) View.GONE else View.VISIBLE
                if (s.toString().isNotEmpty()) {
                    textListener?.onTextChange(s.toString())
                }
            }

        })

    }


    override fun onBackPressed() {
        if (!BMSPrefs.getBoolean(this, Constants.ARE_INVITED)) {
            BMSPrefs.putBoolean(this, Constants.ARE_INVITED, true)
            CommonMethod.callActivity(this, Intent(this, MainActivity::class.java))
            finishAffinity()
        } else {
            super.onBackPressed()
        }
    }
}