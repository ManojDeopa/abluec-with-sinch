package com.ablueclive.global.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.SimpleResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.appliedJob.AppliedJobFragment
import com.ablueclive.jobSection.newJobFragment.NewJobDetailResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.fragments.OrderDetailFragment
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.search_work_detail_fragment.*


class SearchWorkDetailFragment : Fragment() {


    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""


    companion object {
        val apiParam = HashMap<String, String>()
        var isApplied = false
        fun newInstance(bundle: Bundle): SearchWorkDetailFragment {
            val fragment = SearchWorkDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_work_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getJobDetail()
    }

    private fun getJobDetail() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        apiParam["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getJobDetail(apiParam).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<NewJobDetailResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: NewJobDetailResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            response.responseData?.getjobDetails?.let { loadData(it) }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }

    @SuppressLint("SetTextI18n")
    private fun loadData(it: NewJobDetailResponse.GetjobDetails) {

        var imageUrl = Constants.IMAGE_STORE_URL
        val imageData = it.jobImage
        if (!imageData.isNullOrEmpty()) {
            imageUrl += imageData[0].name.toString()
        }
        CommonMethod.loadImageResize(imageUrl, ivImage)

        tvJobTitle.text = it.jobTitle
        tvAddress.text = it.complete_address


        val minPrice = if (it.minPrice != null) it.minPrice else "0"
        val maxPrice = if (it.maxPrice != null) it.maxPrice else "0"

        //tvPrice.text = Constants.CURRENCY + minPrice + " - " + Constants.CURRENCY + maxPrice
        tvDuration.text = CommonMethod.getTimesAgo(it.createdAt)
        tvJobDescription.text = it.jobDesc


        btnSave.setOnClickListener { v ->
            requestData(0, it.id.toString() + ":" + "")
        }

        if (it.already_applied == 1) {
            btnRequest.text = getString(R.string.applied)
            btnRequest.setBackgroundResource(R.color.gray)
        } else {
            btnRequest.setOnClickListener { v ->
                val title = getString(R.string.apply)
                val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
                alert.setTitle(title)
                alert.setMessage(getString(R.string.before_apply_text))
                alert.setCancelable(false)
                alert.setPositiveButton(getString(R.string.agree)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    requestData(1, it.id.toString() + ":" + it.customerId.toString())
                }

                alert.setNegativeButton(getString(R.string.disagree)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                }
                alert.show()


            }
        }

        if (isApplied) {
            btnCancel.visibility = View.VISIBLE
            btnCancel.setOnClickListener { v ->
                removeJob(it.id.toString())
            }
        } else {
            applyButtonLayout.visibility = View.VISIBLE
        }
    }

    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


    private fun requestData(position: Int, id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = separated[0]

        var api = apiInterface.saveJobForLater(param)

        if (position == 1) {
            param["job_provider_id"] = separated[1]
            api = apiInterface.jobRequestApi(param)
        }

        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {

                        ProgressD.hide()

                        try {
                            if (position == 1) {
                                showAlert(requireActivity(), response.responseData?.jobRequestApi)
                            } else {
                                showAlert(requireActivity(), response.responseData?.saveJobForLater)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(context: Context, text: String?) {
        val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            activity?.finish()
        }
        alert.show()
    }

    private fun removeJob(jobId: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = java.util.HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = jobId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getcancelAppliedjobList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SimpleResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SimpleResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                CommonMethod.showToastShort("Applied job deleted", requireContext())
                                AppliedJobFragment.shoulRefresh = true
                                requireActivity().finish()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}