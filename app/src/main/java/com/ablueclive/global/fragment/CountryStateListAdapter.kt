package com.ablueclive.global.fragment

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.CountryStateListResponse
import kotlinx.android.synthetic.main.country_state_item.view.*

class CountryStateListAdapter(var list: List<CountryStateListResponse.CountryState>, var editText: AppCompatEditText, var dialog: Dialog) : RecyclerView.Adapter<CountryStateListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {

                if (editText.id == R.id.etCountry) {
                    UpdateAddressFragment.country_id = list[absoluteAdapterPosition].id.toString()
                }

                editText.setText(list[absoluteAdapterPosition].name)
                dialog.dismiss()

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.country_state_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvName.text = list[position].name
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun filterList(filterList: MutableList<CountryStateListResponse.CountryState>) {
        list = filterList
        notifyDataSetChanged()
    }

}
