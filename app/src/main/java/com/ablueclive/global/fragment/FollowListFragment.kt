package com.ablueclive.global.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.adapter.FollowListAdapter
import com.ablueclive.global.response.FollowListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.progress_horizontal.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class FollowListFragment(var title: String) : Fragment(), RecyclerViewItemClick {


    lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var followAdapter: FollowListAdapter
    private var followList = ArrayList<FollowListResponse.FollowUserList>()


    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    var isLoading = false
    var searchText = ""
    var pageCount = 1
    var rowCount = 10

    companion object {
        var user_profileId = ""
        var totalCount = "0"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        followListApi()
    }


    private fun init() {

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        followAdapter = FollowListAdapter(this, followList)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = followAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    followListApi()
                }
            }
        })

    }


    private fun followListApi() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = user_profileId
        param["page"] = pageCount.toString()
        param["row_count"] = rowCount.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.followersList(param)
        if (title == getString(R.string.followings)) {
            apiCall = apiInterface.followingsList(param)
        }
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<FollowListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: FollowListResponse) {
                        try {
                            if (pbHeader != null) pbHeader.visibility = View.GONE
                            if (response.responseStatus == 1) {
                                tvHeaderText.visibility = View.VISIBLE
                                val list = if (title == getString(R.string.followers)) response.responseData?.followUserList
                                else response.responseData?.followingUserList
                                if (!list.isNullOrEmpty()) {
                                    followList.addAll(list)
                                    followAdapter.notifyDataSetChanged()
                                    isLoading = false
                                    pageCount++
                                }
                                // val count = followList.size
                                tvHeaderText.text = "$totalCount $title"
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        if (pbHeader != null) pbHeader.visibility = View.GONE
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {

        if (text == "follow") {
            followUnFollowApi(true, followList[position].user?.id.toString(), position)
        }

        if (text == "unfollow") {
            followUnFollowApi(false, followList[position].user?.id.toString(), position)
        }


    }

    private fun followUnFollowApi(isTrue: Boolean, friendId: String, position: Int) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), context)
            return
        }

        ProgressD.show(requireContext(), "")

        val param = java.util.HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(context, Constants._ID)
        param["session_token"] = session_token
        param["to_user_id"] = friendId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.follow(param)
        if (!isTrue) {
            apiCall = apiInterface.unFollow(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                when (isTrue) {
                                    true -> {
                                        followList[position].already_followed = "1"
                                        if (followList[position].user?.is_private == 1) {
                                            followList[position].already_followed = "2"
                                        }
                                    }
                                    false -> {
                                        followList[position].already_followed = "0"
                                    }
                                }

                                followAdapter.notifyDataSetChanged()
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}