package com.ablueclive.global.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.JobsListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.job_list_item.view.*

class JobListAdapter(var listener: RecyclerViewItemClick, var list: List<JobsListResponse.GetjobUserList>, var isApplied: Boolean) : RecyclerView.Adapter<JobListAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            if (isApplied) {
                itemView.ivCancel.visibility = View.VISIBLE
                itemView.ivCancel.setOnClickListener {
                    listener.onRecyclerItemClick("delete", absoluteAdapterPosition)
                }
            }

            itemView.setOnClickListener {
                SearchWorkDetailFragment.apiParam["service_id"] = list[absoluteAdapterPosition].id.toString()
                SearchWorkDetailFragment.apiParam["status"] = list[absoluteAdapterPosition].status.toString()
                SearchWorkDetailFragment.isApplied = isApplied
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.search_work_detail))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.job_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var imageUrl = Constants.IMAGE_STORE_URL
        if (!list[position].jobImage.isNullOrEmpty()) {
            imageUrl += (list[position].jobImage?.get(0) ?: "")
        }
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)

        holder.itemView.tvTitle.text = list[position].cat
        holder.itemView.tvText.text = list[position].jobTitle
        holder.itemView.tvName.text = list[position].customerName


        val minPrice = if (list[position].minPrice != null) list[position].minPrice else "0"
        val maxPrice = if (list[position].maxPrice != null) list[position].maxPrice else "0"

        // holder.itemView.tvAmount.text = "$CURRENCY$minPrice-$CURRENCY$maxPrice"

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(filterList: List<JobsListResponse.GetjobUserList>) {
        list = filterList
        notifyDataSetChanged()
    }

}
