package com.ablueclive.global.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.ablueclive.storeSection.fragments.SearchStoreFragment
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import com.ablueclive.utils.EditTextChangeListener
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.job_post_tab_fragment.tabs
import kotlinx.android.synthetic.main.job_post_tab_fragment.viewpager
import kotlinx.android.synthetic.main.search_layout.*
import kotlinx.android.synthetic.main.search_tab_fragment.*


/**
 * A simple [Fragment] subclass.
 */
class SearchTabFragment : Fragment() {


    private lateinit var titleList: List<String>
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""
    val noData = ""

    companion object {
        var textListener: EditTextChangeListener? = null
        var searchText = ""
        lateinit var edtSearch: EditText
        lateinit var pbHeader: ProgressBar

        fun newInstance(bundle: Bundle): SearchTabFragment {
            val fragment = SearchTabFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_tab_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet
        searchText = noData
        pbHeader = progressBar


        edtSearch = ivSearchTop
        ivSearchTop.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchText = s.toString()
                if (searchText.length < 3) {
                    searchText = noData
                }
                ivCross.visibility = if (searchText.isEmpty()) View.GONE else View.VISIBLE
                textListener?.onTextChange(searchText)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        ivCross.setOnClickListener {
            ivSearchTop.text = null
            searchText = noData
            textListener?.onTextChange("")
        }

        addTabViewPager()

    }


    private fun addTabViewPager() {
        titleList = listOf(
                getString(R.string.peoples),
                getString(R.string.jobs),
                getString(R.string.products),
                getString(R.string.topics),
                getString(R.string.pages),
                getString(R.string.stores),
                getString(R.string.brands)
        )
        val adapter = ViewPagerAdapter(requireActivity())
        viewpager.adapter = adapter
        TabLayoutMediator(tabs, viewpager) { tab, position ->
            tab.text = titleList[position]
        }.attach()

        when (Constants.SEARCH_TYPE) {

            getString(R.string.store) -> {
                viewpager.currentItem = 5
            }

            getString(R.string.jobs) -> {
                viewpager.currentItem = 1
            }

            getString(R.string.products) -> {
                viewpager.currentItem = 2
            }
        }
    }


    internal inner class ViewPagerAdapter(fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {

        override fun createFragment(position: Int): Fragment {

            return when (position) {
                0 -> {
                    SearchPeopleFragment()
                }
                1 -> {
                    SearchJobFragment()
                }
                2 -> {
                    SearchProductFragment("")
                }
                3 -> {
                    TopicSearchFragment()
                }
                4 -> {
                    BlankFragment()
                }
                5 -> {
                    SearchStoreFragment()
                }
                else -> {
                    BlankFragment()
                }
            }
        }

        override fun getItemCount(): Int {
            return titleList.size
        }
    }

}