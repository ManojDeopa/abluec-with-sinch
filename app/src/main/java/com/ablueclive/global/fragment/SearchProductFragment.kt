package com.ablueclive.global.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.ScannedBarcodeActivity
import com.ablueclive.global.fragment.SearchTabFragment.Companion.pbHeader
import com.ablueclive.global.fragment.SearchTabFragment.Companion.textListener
import com.ablueclive.global.response.ProductModel
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.ProductsListsAdapter
import com.ablueclive.storeSection.fragments.SearchStoreDetailFragment
import com.ablueclive.storeSection.response.StoreProductListResponse
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import kotlinx.android.synthetic.main.search_product_fragment.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set

class SearchProductFragment(var categoryId: String) : Fragment(),
        RecyclerViewItemClick,
        EditTextChangeListener {


    private lateinit var compositeDisposable: CompositeDisposable

    var productList = ArrayList<ProductModel>()
    private lateinit var productListAdapter: ProductsListsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager


    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""


    var pageCount = 1
    var rowCount = 30
    var searchText = ""

    var lat = 0.0
    var lng = 0.0

    private var isLoading = false

    private val AUTOCOMPLETE_REQUEST_CODE = 10
    var isInitial = true

    companion object {
        var storeId = ""
        var shouldRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_product_fragment, container, false)
    }


    override fun onResume() {
        super.onResume()

        if (ScannedBarcodeActivity.SCAN_RESULT.isNotEmpty()) {
            SearchTabFragment.searchText = ScannedBarcodeActivity.SCAN_RESULT
            SearchTabFragment.edtSearch.setText(ScannedBarcodeActivity.SCAN_RESULT)
            requestData()
            ScannedBarcodeActivity.SCAN_RESULT = ""
        }

        if (shouldRefresh) {
            shouldRefresh = false
            requestData()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }


    private fun init() {

        compositeDisposable = CompositeDisposable()
        isInitial = true
        textListener = this
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet


        var selectedLoc = BMSPrefs.getString(requireContext(), Constants.SELECTED_LOCATION)
        if (selectedLoc.isNotEmpty()) {
            lat = BMSPrefs.getString(requireContext(), Constants.SELECTED_LAT).toDouble()
            lng = BMSPrefs.getString(requireContext(), Constants.SELECTED_LNG).toDouble()
        } else {
            val gpsTracker = GPSTracker(requireContext())
            lat = gpsTracker.latitude
            lng = gpsTracker.longitude
            selectedLoc = getAddress(LatLng(lat, lng))
        }


        tvChangeAddress.text = selectedLoc

        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }

        tvChangeAddress.visibility = View.VISIBLE
        tvChangeAddress.setOnClickListener {
            onSearchAddress()
        }

        productListAdapter = ProductsListsAdapter(productList, this@SearchProductFragment)
        linearLayoutManager = LinearLayoutManager(requireContext())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = productListAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    searchProductByNameApi()
                }
            }
        })

        ivSearchProduct.setOnClickListener {
            startActivity(Intent(requireContext(), ScannedBarcodeActivity::class.java))
        }

        apiCallOnTextChange(SearchTabFragment.searchText)
        // requestData()
    }

    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("SearchProductFragment-", "Place: " + place.name + ", " + place.id + ", " + place.address)
                    val address = place.address
                    tvChangeAddress.text = address

                    lat = place.latLng?.latitude!!
                    lng = place.latLng?.longitude!!
                    isInitial = true

                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LOCATION, address)
                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LAT, lat.toString())
                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LNG, lng.toString())

                    apiCallOnTextChange(searchText)
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    //CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }
    }

    private fun requestData() {
        if (categoryId.isEmpty()) {
            apiCallOnTextChange(SearchTabFragment.searchText)
        } else {
            storeCategoryProducts()
        }
    }


    override fun onRecyclerItemClick(text: String, position: Int) {

        when (position) {
            0 -> {
                addUpdateProductToCart(text)
            }
            1 -> {
                addRemoveFavourite(text)
            }
        }


    }

    private fun apiCallOnTextChange(text: String) {

        compositeDisposable.clear()

        productList.clear()
        productListAdapter.notifyDataSetChanged()

        if (tvHeaderText != null) tvHeaderText.visibility = View.GONE
        /*if (!isInitial) {
            if (text.isEmpty()) return
        }*/
        if (text.isEmpty()) return
        searchText = text
        pageCount = 1
        searchProductByNameApi()
        isInitial = false
    }


    private fun searchProductByNameApi() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(context, Constants._ID)
        param["search_txt"] = searchText
        param["page"] = pageCount.toString()
        param["row_count"] = rowCount.toString()
        param["lat"] = lat.toString()
        param["long"] = lng.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val disposable = apiInterface.searchProductByName(param).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribeWith(
                object : DisposableObserver<StoreProductListResponse?>() {

                    override fun onNext(response: StoreProductListResponse) {

                        try {
                            if (activity != null) {
                                pbHeader.visibility = View.GONE
                                if (response.responseStatus == 1) {
                                    tvHeaderText.visibility = View.VISIBLE
                                    val list = response.responseData?.productList
                                    if (!list.isNullOrEmpty()) {
                                        if (pageCount < 2) {
                                            productList.clear()
                                        }
                                        productList.addAll(list)
                                        isLoading = false
                                        pageCount++
                                    }
                                    val count = productList.size
                                    tvHeaderText.text = "$count Results Founds"
                                    productListAdapter.notifyDataSetChanged()
                                    emptyParentLayout.visibility = if (count == 0) View.VISIBLE else View.GONE

                                } else {
                                    tvHeaderText.visibility = View.GONE
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        pbHeader.visibility = View.GONE
                        println(e.message)
                    }

                    override fun onComplete() {}
                })

        compositeDisposable.add(disposable)
    }


    override fun onTextChange(text: String) {
        apiCallOnTextChange(text)
    }


    private fun storeCategoryProducts() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = Constants.searchStoreData.storeInfo?.id.toString()
        param["category_id"] = categoryId

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.customerGetStoreProductByCategory(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreProductListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: StoreProductListResponse) {
                        ProgressD.hide()
                        try {
                            val list = response.responseData?.storeProducts
                            if (!list.isNullOrEmpty()) {
                                productList.clear()
                                productList.addAll(list)
                                productListAdapter.notifyDataSetChanged()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun addUpdateProductToCart(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val separated = text.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["store_id"] = storeId
        param["product_id"] = separated[0]
        param["qty"] = separated[1]


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addUpdateProductToCart(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            CommonMethod.showToastlong(response.responseMsg, requireContext())
                            if (categoryId.isEmpty()) {
                                SearchStoreDetailFragment.shouldCartListRefresh = true
                            } else {
                                SearchStoreDetailFragment.parentPage.getCartList()
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun addRemoveFavourite(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val separated = text.split(":".toRegex()).toTypedArray()
        val tag = separated[1]

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = separated[0]

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.customerRemoveFavouriteProduct(param)
        if (tag == "uncheck") {
            apiCall = apiInterface.customerSaveFavouriteProduct(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun getAddress(latLng: LatLng?): String {
        var addressText = ""
        try {
            val addresses: List<Address>
            val geoCoder = Geocoder(requireContext(), Locale.getDefault())
            addresses = geoCoder.getFromLocation(latLng?.latitude!!, latLng.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                addressText = (address)

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return addressText
    }

}