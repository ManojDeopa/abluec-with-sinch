package com.ablueclive.global.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_academic_fragment.*
import kotlinx.android.synthetic.main.year_picker_dialog.view.*
import java.util.*

class AddAcademicFragment : Fragment() {


    private var isPursuing = "0"
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        fun newInstance(bundle: Bundle): AddAcademicFragment {
            val fragment = AddAcademicFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_academic_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        doOnClicks()

    }

    private fun doOnClicks() {

        cbPursuing.setOnCheckedChangeListener { buttonView, isChecked ->
            isPursuing = if (isChecked) "1" else "0"

            val isVisible = if (isChecked) View.GONE else View.VISIBLE

            tvHeaderEndYear.visibility = isVisible
            etEndYear.visibility = isVisible
        }

        etStartYear.setOnClickListener {
            createDialogWithoutDateField(etStartYear)
        }

        etEndYear.setOnClickListener {

            if (etStartYear.text.toString().isEmpty()) {
                CommonMethod.showToastlong(getString(R.string.please_select) + " " + getString(R.string.start_year), requireContext())
            } else {
                createDialogWithoutDateField(etEndYear)
            }
        }

        btnSubmit.setOnClickListener {
            addAcademic()
        }
    }

    private fun init() {
        session_token = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(requireActivity())
        isInternetPresent = connectionDetector.isConnectingToInternet
        resetFocus()
    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    fun showAlert(text: String?) {
        val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etTitle.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter) + " " + getString(R.string.title_degree), requireContext())
                false
            }

            etCollege.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter) + " " + getString(R.string.school_college_name), requireContext())
                false
            }

            etStartYear.text.toString().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_select) + " " + getString(R.string.start_year), requireContext())
                false
            }

            !cbPursuing.isChecked -> {
                var isSelected = true
                if (etEndYear.text.toString().isEmpty()) {
                    CommonMethod.showToastlong(getString(R.string.please_select) + " " + getString(R.string.end_year), requireContext())
                    isSelected = false
                }
                isSelected
            }

            else -> true
        }
    }


    private fun addAcademic() {


        if (!isValidate()) return

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["title"] = etTitle.text.toString()
        param["college_or_school_name"] = etCollege.text.toString()
        param["start_year"] = etStartYear.text.toString()
        param["end_year"] = etEndYear.text.toString()
        param["persuing"] = isPursuing


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addAcademic(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                ProfileActivity.shouldRefresh = true
                                requireActivity().finish()
                            }
                            CommonMethod.showToastShort(response.responseMsg, requireActivity())
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun createDialogWithoutDateField(editText: AppCompatEditText) {

        val alertDialog: AlertDialog?
        val builder = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        val inflater = requireActivity().layoutInflater

        val cal = Calendar.getInstance()

        val dialog = inflater.inflate(R.layout.year_picker_dialog, null)
        val monthPicker = dialog.findViewById(R.id.picker_month) as NumberPicker
        val yearPicker = dialog.findViewById(R.id.picker_year) as NumberPicker


        var headerText = getString(R.string.start_year)
        val year = cal.get(Calendar.YEAR)
        var minYear = 1950
        var maxYear = year

        if (editText == etEndYear) {
            headerText = getString(R.string.end_year)
            minYear = etStartYear.text.toString().toInt()
            maxYear = minYear + 10
        }

        dialog.tvHeader.text = headerText

        monthPicker.minValue = 1
        monthPicker.maxValue = 12
        monthPicker.value = cal.get(Calendar.MONTH) + 1


        yearPicker.minValue = minYear
        yearPicker.maxValue = maxYear
        yearPicker.value = year

        builder.setView(dialog).setPositiveButton(getString(R.string.ok)) { dialogInterface, which ->
            val value = yearPicker.value.toString()
            editText.setText(value)
            dialogInterface.cancel()
        }

        builder.setNegativeButton(getString(R.string.cancel)) { dialogInterface, which ->
            dialogInterface.cancel()
        }

        alertDialog = builder.create()
        alertDialog.setCancelable(true)
        alertDialog.show()
    }

}