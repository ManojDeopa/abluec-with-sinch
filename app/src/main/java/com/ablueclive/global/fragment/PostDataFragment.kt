package com.ablueclive.global.fragment

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.adddPost.AddPostActvity
import com.ablueclive.activity.cheersList.CheersListActivity
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.dialog.ReportPostDialog
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.profile.MyProfileResponse
import com.ablueclive.activity.profile.ProfileDataModel
import com.ablueclive.activity.profile.ProfileDataPresenter
import com.ablueclive.activity.profile.ProfiledataContract
import com.ablueclive.activity.search_friend.AddFriendContract
import com.ablueclive.activity.search_friend.AddFriendPresenter
import com.ablueclive.activity.userProfile.DeletePostContract
import com.ablueclive.activity.userProfile.DeletePostPresenter
import com.ablueclive.activity.userProfile.PostLikeContract
import com.ablueclive.activity.userProfile.PostLikePresenter
import com.ablueclive.adapter.GetAllFeedAdapter
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.listener.*
import com.ablueclive.modelClass.PostData
import com.ablueclive.modelClass.Response_data
import com.ablueclive.modelClass.SharePostResponse
import com.ablueclive.modelClass.ShareUsersPost
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.ablueclive.utils.Constants.*
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.progress_horizontal.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.util.*


class PostDataFragment() :
        Fragment(),
        onReportPostListener,
        onDeletePostListener,
        DeletePostContract.View,
        onLikePostListener,
        PostLikeContract.View,
        onCommentPostListener,
        onProductItemListener,
        onSharePostListener,
        onFriendProfileListener,
        onEditPostListener,
        onClickForDetailsListener,
        AddFriendContract.View,
        ProfiledataContract.View, OnSeeMoreClickListener {

    lateinit var getAllFeedAdapter: GetAllFeedAdapter


    private var session_token = ""
    private var time_zone = ""
    private var user_id: String = ""
    private var user_name = ""
    private var device_token = ""
    private var share_count = ""
    private var post_ids = ""
    private var is_first = ""
    var type = ""


    private var progressDialog: ProgressDialog? = null
    private val pageNo = 1
    private var item_position = 0
    private var isInternetPresent = false


    var pageCount = 1
    private var isLoading = false
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var connectionDetector: ConnectionDetector

    private val postDataArrayList = ArrayList<PostData>()

    companion object {

        @SuppressLint("StaticFieldLeak")
        var scrollView: NestedScrollView? = null

        private const val TAG = "PostDataFragment"
        fun newInstance(bundle: Bundle, type: String): PostDataFragment {
            val fragment = PostDataFragment()
            fragment.arguments = bundle
            fragment.type = type
            ProfileDataModel.TYPE = type
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.post_data_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
    }


    private fun findView() {
        device_token = BMSPrefs.getString(requireContext(), Constants.DEVICE_TOKEN)
        is_first = BMSPrefs.getString(requireContext(), Constants.IS_FIRST)
        user_name = BMSPrefs.getString(requireContext(), Constants.USER_NAME)
        session_token = BMSPrefs.getString(requireContext(), SESSION_TOKEN)
        user_id = BMSPrefs.getString(requireContext(), Constants._ID)

        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet
        time_zone = TimeZone.getDefault().id

        getAllFeedAdapter = GetAllFeedAdapter(requireActivity(), postDataArrayList, this, user_id,
                this, this, this,
                this, this, this)
        linearLayoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = getAllFeedAdapter
        }

        if (type == TYPE_M || type == TYPE_F) {
            scrollView?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                    if (scrollY > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                        isLoading = true
                        getProfileData()
                    }
                }
            })
        }

        if (type == TYPE_P) {
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                    if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                        isLoading = true
                        getProfileData()
                    }
                }
            })
        }

        getProfileData()
    }


    private fun getProfileData() {
        if (isInternetPresent) {
            requestProfileParam()
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
        }
    }

    private fun requestProfileParam() {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["post_time_zone"] = TimeZone.getDefault().id
        params["page"] = pageCount.toString()
        if (type == TYPE_F) {
            params["user_profile_id"] = BMSPrefs.getString(requireContext(), FRIEND_ID)
            params["display_name"] = FriendProfileActivity.display_name
        }
        Log.d("requestProfileParam", params.toString())
        val profileDataPresenter = ProfileDataPresenter(this)
        profileDataPresenter.requestProfileData(params)
    }


    private fun deletePostRequest(post_id: String) {
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["post_id"] = post_id
        Log.d("delPostParam", params.toString())
        val deletePostPresenter = DeletePostPresenter(this)
        deletePostPresenter.requestDeletePost(params)
    }

    private fun likePostRequest(postId: String, item_position: Int) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["postId"] = postId
        params["user_name"] = user_name
        Log.d("likePostParam", params.toString())
        val postLikePresenter = PostLikePresenter(this)
        postLikePresenter.requestPostLike(params)
    }


    //set All feed adapter
    private fun setUserFeedAdapter(postDataList: List<MyProfileResponse.PostDatum>?) {

        try {
            if (!postDataList.isNullOrEmpty()) {

                postDataList.forEach {
                    val postData = PostData()
                    postData.totalCountLike = it.totalCountLike?.toString()
                    postData._id = it.id
                    postData.post_user_name = it.postUserName
                    postData.post_time_status = it.postTimeStatus
                    postData.post_content = it.postContent
                    postData.post_user_profile_image = it.postUserProfileImage
                    postData.status = it.status?.toString()
                    postData.post_user_like_status = it.postUserLikeStatus
                    postData.post_reports_status = it.postReportsStatus?.toString()
                    postData.totalComments = it.totalComments?.toString()
                    postData.user_id = it.userId
                    postData.post_title = it.postTitle
                    postData.share_count = it.shareCount?.toString()
                    postData.hasTagNameUnsedForNow = null
                    postData.comments = it.comments
                    postData.like_users = it.likeUsers
                    postData.images = it.images
                    postData.shareUserPost = it.retweet_user_post
                    postData.shareUrl = it.shareUrl
                    postData.rePostUser = it.repostUser
                    postData.repost_content = it.repost_content
                    postDataArrayList.add(postData)
                }

                if (postDataArrayList.isNotEmpty()) {
                    isLoading = false
                    pageCount++
                }

                if (pageCount == 1) {
                    getAllFeedAdapter.notifyDataSetChanged()
                } else {
                    getAllFeedAdapter.notifyItemRangeInserted(getAllFeedAdapter.itemCount, postDataArrayList.size)
                }

                CommonMethod.isEmptyView(false, requireContext(), "")

            } else {

                if (pageCount == 1) {
                    CommonMethod.isEmptyView(true, requireContext(), "No Posts.")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun showProgress() {
        try {
            if (showLoader) {
                show(requireActivity(), "")
            } else {
                pbHeader.visibility = View.VISIBLE
            }
            showLoader = true
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun hideProgress() {
        try {
            if (pbHeader.visibility == View.VISIBLE) {
                pbHeader.visibility = View.GONE
            }
            hide()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setDataToViews(responseModel: MyProfileResponse?) {
        try {
            val postData = responseModel?.response_data?.postData
            setUserFeedAdapter(postData)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun addFreindResponse(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status == "1" -> {
                    CommonMethod.showToastShort(response_msg, requireActivity())
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireActivity())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    //post like
    override fun setPostLike(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {

        try {
            when {
                response_status.equals("1", ignoreCase = true) -> {
                    val postData = PostData()
                    postData.totalCountLike = response_data.totalCountLike
                    postData._id = postDataArrayList[item_position]._id
                    postData.post_user_name = postDataArrayList[item_position].post_user_name
                    postData.post_time_status = postDataArrayList[item_position].post_time_status
                    postData.post_content = postDataArrayList[item_position].post_content
                    postData.post_user_profile_image = postDataArrayList[item_position].post_user_profile_image
                    postData.images = postDataArrayList[item_position].images
                    postData.hasTagNameUnsedForNow = postDataArrayList[item_position].hasTagNameUnsedForNow
                    postData.status = response_data.status
                    postData.post_user_like_status = response_data.status
                    postData.like_users = postDataArrayList[item_position].like_users
                    postData.post_reports_status = postDataArrayList[item_position].post_reports_status
                    postData.comments = postDataArrayList[item_position].comments
                    postData.totalComments = postDataArrayList[item_position].totalComments
                    postData.share_count = postDataArrayList[item_position].share_count
                    postData.user_id = postDataArrayList[item_position].user_id
                    postData.post_title = postDataArrayList[item_position].post_title
                    postData.store_id = postDataArrayList[item_position].store_id
                    postDataArrayList[item_position] = postData
                    getAllFeedAdapter.notifyItemChanged(item_position)
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // delete post
    override fun setDeletePost(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status == "1" -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }


    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, requireContext())
        BMSPrefs.putString(requireContext(), Constants._ID, "")
        BMSPrefs.putString(requireContext(), Constants.isStoreCreated, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(requireContext(), Intent(requireContext(), LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    // report post
    override fun onReportPostClick(post_id: String, pos: Int) {
        val reportPostDialog = ReportPostDialog(requireActivity(), post_id, postDataArrayList, pos, getAllFeedAdapter)
        reportPostDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    // delete post
    override fun onDelPostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            deletePostRequest(post_id)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // post like method
    override fun onLikePostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            item_position = position
            likePostRequest(post_id, item_position)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // get all cmnt
    override fun onCmntPostClick(post_id: String, pos: Int) {
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, post_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), CommentActivity::class.java))
    }

    override fun onProductItemClick(feed_id: String) {
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, feed_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), CheersListActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    // share post
    override fun onSharePostClick(post_id: String, pos: Int, type: String) {
        item_position = pos
        sharePost(post_id, type)
    }


    override fun onFrndProfileClick(friend_id: String, position: Int) {
        BMSPrefs.putString(requireActivity(), FRIEND_ID, friend_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), FriendProfileActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onAddFriendClick(friend_id: String, position: Int) {
        sendFriendRequestParam(friend_id)
    }

    private fun sendFriendRequestParam(friend_id: String) {
        val params = HashMap<String, String>()
        params["add_friend_id"] = friend_id
        params["session_token"] = session_token
        Log.d("sendFriendReqPAram", params.toString())
        val addFriendPresenter = AddFriendPresenter(this)
        addFriendPresenter.requestAddFriend(params)
    }

    // api
    private fun sharePost(post_id: String, type: String) {
        val hashMap = HashMap<String, String>()
        hashMap["post_id"] = post_id
        hashMap["session_token"] = session_token
        hashMap["post_time_zone"] = TimeZone.getDefault().id
        Log.d("shareParam", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.reTweetPost(hashMap)
        if (type == getString(R.string.un_do_wave)) {
            apiCall = apiInterface.deleteReTweetPost(hashMap)
        }
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SharePostResponse?> {
                    override fun onSubscribe(d: Disposable) {
                        progressDialog = CommonMethod.showProgressDialog(progressDialog, requireContext(), getString(R.string.please_wait))
                    }

                    override fun onNext(response: SharePostResponse) {
                        try {
                            CommonMethod.hideProgressDialog(progressDialog)
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.data
                                if (!list.isNullOrEmpty()) {

                                    val shareUserList = list[0].shareUserPost
                                    if (!shareUserList.isNullOrEmpty()) {
                                        val newList = arrayListOf<ShareUsersPost>()
                                        shareUserList.forEach {
                                            newList.add(it)
                                        }
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = newList
                                    } else {
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = null
                                    }
                                }
                                getAllFeedAdapter.postDataArrayList[item_position].isRetweeted = type != getString(R.string.un_do_wave)
                                getAllFeedAdapter.notifyItemChanged(item_position)
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        CommonMethod.hideProgressDialog(progressDialog)
                        Log.d(TAG, "--" + e.message!!)
                    }

                    override fun onComplete() {}

                })
    }

    // edit post
    override fun onEditPostClick(post_id: String, position: Int) {
        AddPostActvity.data = postDataArrayList[position]
        val intent = Intent(requireActivity(), AddPostActvity::class.java)
        startActivity(intent)
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onClickForDetails(postData: PostData) {

    }


    override fun onSeeMoreClick(text: String, position: Int) {

    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            type = ""
            recyclerView.clearOnScrollListeners()
            scrollView = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}