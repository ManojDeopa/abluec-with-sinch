package com.ablueclive.global.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.AppGlobalTask
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.adapter.FavSProviderAdapter
import com.ablueclive.global.response.FavSProvidersResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*


class FavSProvidersFragment : Fragment(), RecyclerViewItemClick {

    private lateinit var favAdapter: FavSProviderAdapter
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""


    companion object{
        fun newInstance(bundle: Bundle): FavSProvidersFragment {
            val fragment = FavSProvidersFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }


    override fun onResume() {
        super.onResume()
        init()
    }


    private fun getFavProvidersList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getFavServiceProvider(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<FavSProvidersResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: FavSProvidersResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()

                        try {
                            val list = response.responseData?.favServiceProvider
                            if (list.isNullOrEmpty()) {
                                CommonMethod.isEmptyView(true, requireContext(), getString(R.string.no_data_found))
                            } else {
                                CommonMethod.isEmptyView(false, requireContext(), "")

                                favAdapter = FavSProviderAdapter(list, this@FavSProvidersFragment)

                                recyclerView.apply {
                                    layoutManager = LinearLayoutManager(requireActivity())
                                    adapter = favAdapter
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet

        getFavProvidersList()
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        deleteFavProvider(text)
    }


    private fun deleteFavProvider(id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["fav_provider_id"] = id

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.removeFavServiceProvider(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        AppGlobalTask({ getFavProvidersList() }, requireContext()).showAlert(response.responseData?.removeFavProvider)
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }


}