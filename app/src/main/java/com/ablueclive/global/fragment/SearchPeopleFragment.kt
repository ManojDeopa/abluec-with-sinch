package com.ablueclive.global.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.search_friend.AddFriendContract
import com.ablueclive.activity.search_friend.AddFriendPresenter
import com.ablueclive.activity.search_friend.UnFriendContract
import com.ablueclive.activity.search_friend.UnFriendPresenter
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.adapter.PeoplesAdapter
import com.ablueclive.global.fragment.SearchTabFragment.Companion.pbHeader
import com.ablueclive.global.fragment.SearchTabFragment.Companion.textListener
import com.ablueclive.global.response.SearchListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class SearchPeopleFragment() : Fragment(),
        RecyclerViewItemClick,
        AddFriendContract.View,
        UnFriendContract.View,
        EditTextChangeListener {


    lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var peoplesAdapter: PeoplesAdapter
    private var searchFriendList = ArrayList<SearchListResponse.SearchFriendDatum>()


    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    var isLoading = false
    var searchText = ""
    var pageCount = 1
    var rowCount = 30

    companion object {
        var shouldRefresh = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }


    override fun onResume() {
        super.onResume()
        if (shouldRefresh == 2) {
            shouldRefresh = 0
            init()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }


    private fun init() {
        textListener = this
        shouldRefresh = 1
        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet

        peoplesAdapter = PeoplesAdapter(this@SearchPeopleFragment, searchFriendList)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = peoplesAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    searchUserListsApi()
                }
            }
        })

        apiCallOnTextChange(SearchTabFragment.searchText)

    }

    private fun apiCallOnTextChange(text: String) {
        searchFriendList.clear()
        peoplesAdapter.notifyDataSetChanged()
        if (tvHeaderText != null) tvHeaderText.visibility = View.GONE
        if (text.isEmpty()) return
        pageCount = 1
        searchText = text
        searchUserListsApi()
    }


    override fun onRecyclerItemClick(text: String, position: Int) {

        if (text == "follow") {
            followUnFollowApi(true, searchFriendList[position].id.toString(), position)
            // sendFriendRequestParam(searchFriendList[position].id.toString())
        }

        if (text == "unfollow") {
            followUnFollowApi(false, searchFriendList[position].id.toString(), position)
            //  unFriendRequestParam(searchFriendList[position].id.toString())
        }
    }

    override fun showProgress() {
        ProgressD.show(requireActivity(), "")
    }

    override fun hideProgress() {
        ProgressD.hide()
    }

    override fun unFriendResponse(response_status: String?, response_msg: String?, response_invalid: String?) {
        CommonMethod.showAlert(requireContext(), response_msg)
    }

    override fun addFreindResponse(response_status: String?, response_msg: String?, response_invalid: String?) {
        CommonMethod.showAlert(requireContext(), response_msg)
    }

    override fun onResponseFailure(throwable: Throwable?) {
        println(throwable)
    }


    private fun sendFriendRequestParam(friend_id: String) {
        val params = java.util.HashMap<String, String>()
        params["add_friend_id"] = friend_id
        params["session_token"] = session_token
        Log.d("sendFriendRequestParam", params.toString())
        val addFriendPresenter = AddFriendPresenter(this)
        addFriendPresenter.requestAddFriend(params)
    }

    private fun unFriendRequestParam(friend_id: String) {
        val params = java.util.HashMap<String, String>()
        params["un_friend_id"] = friend_id
        params["session_token"] = session_token
        Log.d("unFriendRequestParam", params.toString())
        val unFriendPresenter = UnFriendPresenter(this)
        unFriendPresenter.requestUnFriend(params)
    }


    private fun searchUserListsApi() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["search_name"] = searchText
        param["page"] = pageCount.toString()
        param["row_count"] = rowCount.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.searchUserListsApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SearchListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SearchListResponse) {
                        try {
                            if (activity != null) {
                                pbHeader.visibility = View.GONE
                                if (response.responseStatus == 1) {
                                    tvHeaderText?.visibility = View.VISIBLE
                                    val list = response.responseData?.searchFriendData
                                    searchFriendList.clear()
                                    if (!list.isNullOrEmpty()) {
                                        // if (pageCount == 1) searchFriendList.clear()
                                        searchFriendList.addAll(list)
                                        // unComment below lines when pagination will implement from backend.
                                        // isLoading = false
                                        // pageCount++
                                    }

                                    val count = searchFriendList.size
                                    tvHeaderText.text = "$count Results Founds"
                                    peoplesAdapter.notifyDataSetChanged()
                                    emptyParentLayout.visibility = if (count == 0) View.VISIBLE else View.GONE

                                } else {
                                    tvHeaderText.visibility = View.GONE
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        pbHeader.visibility = View.GONE
                        println(e.message)
                    }
                })
    }

    override fun onTextChange(text: String) {
        apiCallOnTextChange(text)
    }


    private fun followUnFollowApi(isTrue: Boolean, friendId: String, position: Int) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = java.util.HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["session_token"] = session_token.toString()
        param["to_user_id"] = friendId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.follow(param)
        if (!isTrue) {
            apiCall = apiInterface.unFollow(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                when (isTrue) {
                                    true -> {
                                        searchFriendList[position].already_followed = "1"
                                        if (searchFriendList[position].is_private == 1) {
                                            searchFriendList[position].already_followed = "2"
                                        }
                                    }
                                    false -> {
                                        searchFriendList[position].already_followed = "0"
                                    }
                                }
                                peoplesAdapter.notifyDataSetChanged()
                            }else{
                                CommonMethod.showToastShort(response.responseMsg,requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}