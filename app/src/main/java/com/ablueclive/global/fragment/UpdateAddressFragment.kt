package com.ablueclive.global.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.profile.MyProfileResponse
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.response.CountryStateListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.HeaderInterceptor
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_with_edittext.*
import kotlinx.android.synthetic.main.update_address_fragment.*
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

class UpdateAddressFragment : Fragment() {

    private var addressLat: Float? = null
    private var addressLng: Float? = null

    private val AUTOCOMPLETE_REQUEST_CODE = 10
    var data = Constants.myAddress

    companion object {
        var country_id = ""
        var isChangeAddress = false
        var isAddressUpdated = false


        fun newInstance(bundle: Bundle): UpdateAddressFragment {
            val fragment = UpdateAddressFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.update_address_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        doOnClicks()

    }


    private fun doOnClicks() {

        btnUpdate.setOnClickListener {
            updateAddress()
        }


        etAddress.setOnClickListener {
            resetFocus()
            onSearchAddress()
        }

        etCountry.setOnClickListener {
            getCountries()
        }

        etState.setOnClickListener {
            getStateFromCountry()
        }

    }

    private fun init() {


        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }
        resetFocus()

        if (data != null) {
            nullCheck(data.flatno, etFlatNo)
            nullCheck(data.street, etStreet)
            nullCheck(data.country, etCountry)
            nullCheck(data.state, etState)
            nullCheck(data.city, etCity)
            nullCheck(data.zip, etZipCode)
            nullCheck(data.completeAddress, etAddress)
            addressLat = data.lat
            addressLng = data._long
        }

        country_id = ""
    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) return

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("AddAgentFragment-", "Place: " + place.name + ", " + place.id + ", " + place.address)
                    val address = place.address

                    addressLat = place.latLng?.latitude?.toFloat()
                    addressLng = place.latLng?.longitude?.toFloat()
                    etAddress.setText(address)

                    getAllAddress(place.latLng)
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage!! + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    // CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }


    }

    private fun getAllAddress(latLng: LatLng?) {
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(requireContext(), Locale.getDefault())
            addresses = geocoder.getFromLocation(latLng?.latitude!!, latLng.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                val city: String = addresses[0].locality
                val state: String = addresses[0].adminArea
                val country: String = addresses[0].countryName
                val postalCode: String = "" + addresses[0].postalCode
                val knownName: String = addresses[0].featureName

                etStreet.setText(knownName)
                etCountry.setText(country)
                etState.setText(state)
                etCity.setText(city)
                etZipCode.setText(postalCode)

                println("$address $city $country $state $postalCode $knownName")

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }


    fun showAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etAddress.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please select address", requireContext())
                return false
            }

            etStreet.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please enter street", requireContext())
                false
            }
            etCountry.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please select country", requireContext())
                false
            }

            etState.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please select state", requireContext())
                return false
            }

            etCity.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please enter city", requireContext())
                false
            }

            etZipCode.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong("Please enter zip code", requireContext())
                return false
            }


            else -> true
        }
    }

    private fun updateAddress() {

        if (!isValidate()) {
            return
        }

        if (isChangeAddress) {
            val data = MyProfileResponse.Address()
            data.flatno = etFlatNo.text.toString()
            data.flatno = etFlatNo.text.toString()
            data.street = etStreet.text.toString()
            data.country = etCountry.text.toString()
            data.state = etState.text.toString()
            data.city = etCity.text.toString()
            data.zip = etZipCode.text.toString()
            data.completeAddress = etAddress.text.toString()
            data.lat = addressLat
            data._long = addressLng
            Constants.myAddress = data
            isAddressUpdated = true
            requireActivity().finish()
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["flatno"] = etFlatNo.text.toString()
        param["street"] = etStreet.text.toString()
        param["country"] = etCountry.text.toString()
        param["state"] = etState.text.toString()
        param["city"] = etCity.text.toString()
        param["zip"] = etZipCode.text.toString()
        param["complete_address"] = etAddress.text.toString()
        param["lat"] = addressLat.toString()
        param["long"] = addressLng.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.saveAddress(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                showAlert(response.responseData?.address)
                            } else {
                                CommonMethod.showToastlong(response.responseData?.address, requireActivity())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun getCountries() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val params = java.util.HashMap<String, String>()
        params["token"] = HeaderInterceptor.X_TOKEN

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getCountries(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CountryStateListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CountryStateListResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {
                                etState.text = null
                                etState.hint = getString(R.string.select)
                                showCountryStateList(response.responseData?.countries!!, etCountry)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun getStateFromCountry() {

        if (country_id.isEmpty()) {
            CommonMethod.showToastlong("Please Select Country First", requireContext())
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val params = java.util.HashMap<String, String>()
        params["country_id"] = country_id

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getStateFromCountry(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CountryStateListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CountryStateListResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {
                                showCountryStateList(response.responseData?.state!!, etState)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun nullCheck(value: String?, editText: AppCompatEditText) {
        if (!value.isNullOrEmpty()) {
            editText.setText(value)
        }

    }


    private fun showCountryStateList(list: List<CountryStateListResponse.CountryState>, editText: AppCompatEditText) {


        val dialog = Dialog(requireActivity(), R.style.alertDialogTheme)
        /* dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)*/
        dialog.setContentView(R.layout.recycler_with_edittext)


        val mAdapter = CountryStateListAdapter(list, editText, dialog)

        dialog.rvList.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }



        dialog.ivSearchTop.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString(), mAdapter, list)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        dialog.show()


    }

    fun filter(text: String, adapter: CountryStateListAdapter, list: List<CountryStateListResponse.CountryState>) {
        val filterList = mutableListOf<CountryStateListResponse.CountryState>()
        list.forEach {
            if (it.name.toLowerCase(Locale.getDefault()).contains(text.toLowerCase(Locale.getDefault()))) {
                filterList.add(it)
            }
        }
        adapter.filterList(filterList)
    }

    override fun onStop() {
        super.onStop()
        isChangeAddress = false
    }

}