package com.ablueclive.global.fragment

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.global.activity.InviteContactsActivity
import com.ablueclive.global.adapter.InviteContactsAdapter
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.ContactModel
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.invite_contacts_fragment.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set

class InviteContactsFragment() : Fragment(), EditTextChangeListener {


    private lateinit var inviteContactsAdapter: InviteContactsAdapter
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""
    private val REQUEST_READ_CONTACTS = 777


    private var contactsList = ArrayList<ContactModel>()

    companion object {
        fun newInstance(bundle: Bundle): InviteContactsFragment {
            val fragment = InviteContactsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.invite_contacts_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {

        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet


        InviteContactsActivity.textListener = this

        checkPermissionOrGetContacts()

        inviteContactsAdapter = InviteContactsAdapter(contactsList)
        rvContactList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = inviteContactsAdapter
        }

        cbSelectAll.setOnCheckedChangeListener { buttonView, isChecked ->
            inviteContactsAdapter.list.forEach {
                it.isChecked = isChecked
            }
            inviteContactsAdapter.notifyDataSetChanged()
        }

        btnInvite.setOnClickListener {

            val selectedList = arrayListOf<String>()
            inviteContactsAdapter.list.forEach {
                if (it.isChecked && it.already_registered == 0 && it.already_invited == 0) {
                    selectedList.add(it.mobile_number.trim())
                }
            }
            val selectedNumbers = getStringFromList(selectedList)
            if (selectedNumbers.isNotEmpty()) {
                inviteContacts(selectedNumbers)
            } else {
                CommonMethod.showToastShort(getString(R.string.no_contact_selected), requireActivity())
            }
        }

        tvSkip.setOnClickListener {
            if (!BMSPrefs.getBoolean(requireContext(), Constants.ARE_INVITED)) {
                BMSPrefs.putBoolean(requireContext(), Constants.ARE_INVITED, true)
                CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), MainActivity::class.java))
                requireActivity().finishAffinity()
            } else {
                requireActivity().finish()
            }
        }

        rootLayout.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            rootLayout.getWindowVisibleDisplayFrame(r)
            val screenHeight: Int = rootLayout.rootView.height
            val keypadHeight: Int = screenHeight - r.bottom
            if (keypadHeight > screenHeight * 0.15) {
                btnInvite.visibility = View.GONE
                tvSkip.visibility = View.GONE
            } else {
                btnInvite.visibility = View.VISIBLE
                tvSkip.visibility = View.VISIBLE
            }
        }
    }

    private fun getStringFromList(selectedList: ArrayList<String>): String {
        return selectedList.toString().replace("[", "").replace("]", "").replace(", ", ",")
    }

    private fun checkPermissionOrGetContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                fetchContacts()
            } else {
                requestPermission()
            }
        } else {
            fetchContacts()
        }
    }


    private fun requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.READ_CONTACTS)) {
            showMessageOKCancel { _: DialogInterface?, i: Int ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivityForResult(intent, 101)
            }

        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_CONTACTS), REQUEST_READ_CONTACTS)
        }

    }


    private fun showMessageOKCancel(okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
                .setCancelable(false)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.allow_contact_permission_txt))
                .setPositiveButton(getString(R.string.ok), okListener)
                .create()
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_READ_CONTACTS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchContacts()
                } else {
                    CommonMethod.showToastlong("Permission Denied ! Please Allow Read Contact Permission", requireActivity())
                }
            }
        }
    }


    private fun fetchContacts() {
        ProgressD.show(requireContext(), getString(R.string.loading_wait))
        Thread {
            val numberList = ArrayList<String>()
            val nameList = ArrayList<String>()
            val contentResolver = requireActivity().contentResolver
            val cursor: Cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)!!
            if (cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val id: String = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        val cursorInfo: Cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(id), null)!!

                        while (cursorInfo.moveToNext()) {
                            val phoneNo = cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ", "")
                            val displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                            //if (validCellPhone(phoneNo)) {
                            numberList.add(phoneNo.trim())
                            nameList.add(displayName.trim())
                            // }
                        }
                        cursorInfo.close()
                    }
                }
                cursor.close()
            }

            requireActivity().runOnUiThread {
                checkContacts(getStringFromList(numberList), getStringFromList(nameList))
            }
        }.start()
    }

    private fun validCellPhone(number: String): Boolean {
        return Patterns.PHONE.matcher(number).matches()
    }


    private fun checkContacts(contact_numbers: String, contact_names: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["contact_numbers"] = contact_numbers
        param["contact_names"] = contact_names

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.checkContacts(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()

                        try {
                            if (response.response_status == "1") {

                                if (response.response_data.contactModels != null) {
                                    contactsList = response.response_data.contactModels.distinctBy {
                                        it.mobile_number
                                    } as ArrayList<ContactModel>
                                    contactsList.sortBy { it.name.toString() }
                                    inviteContactsAdapter.updateList(contactsList)
                                    selectAllLayout.visibility = View.VISIBLE
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun inviteContacts(contact_numbers: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")
        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["contact_numbers"] = contact_numbers

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.inviteContacts(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        try {
                            if (response.response_status == "1") {
                                CommonMethod.showToastlong("Invitation sent successfully", requireActivity())
                                BMSPrefs.putBoolean(requireContext(), Constants.ARE_INVITED, true)
                                CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), MainActivity::class.java))
                                requireActivity().finishAffinity()
                            } else {
                                CommonMethod.showToastlong(response.response_msg, requireActivity())
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101) {
            checkPermissionOrGetContacts()
        }
    }

    override fun onTextChange(text: String) {
        if (text.isNotEmpty()) {
            val filterList = mutableListOf<ContactModel>()
            contactsList.forEach {

                val query1 = it.name.toString().toLowerCase(Locale.getDefault()).contains(text.toLowerCase(Locale.getDefault()))
                val query2 = it.mobile_number.toString().toLowerCase(Locale.getDefault()).contains(text.toLowerCase(Locale.getDefault()))

                if (query1 || query2) {
                    filterList.add(it)
                }
            }
            inviteContactsAdapter.updateList(filterList)
            selectAllLayout.visibility = if (filterList.isEmpty()) View.GONE else View.VISIBLE
            tvNoResults.visibility = if (filterList.isEmpty()) View.VISIBLE else View.GONE

        } else {
            cbSelectAll.isChecked = false
            tvNoResults.visibility = View.GONE
            inviteContactsAdapter.updateList(contactsList)
        }

    }

}