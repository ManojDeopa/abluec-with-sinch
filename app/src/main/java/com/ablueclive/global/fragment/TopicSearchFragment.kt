package com.ablueclive.global.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.global.fragment.SearchTabFragment.Companion.pbHeader
import com.ablueclive.global.response.TopicsResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.TopicsListAdapter
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.topic_search_layout.*
import java.util.*


class TopicSearchFragment : Fragment(), EditTextChangeListener {


    private lateinit var topicsItemListAdapter: TopicsListAdapter
    private var topicsItemList = ArrayList<TopicsResponse.Item>()

    private var isInternetPresent = false
    private lateinit var connectionDetector: ConnectionDetector
    private var session_token = ""

    var pageCount = 1
    var rowCount = 10
    var searchText = ""

    lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.topic_search_layout, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }


    private fun init() {

        SearchTabFragment.textListener = this
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet


        topicsItemListAdapter = TopicsListAdapter(topicsItemList)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = topicsItemListAdapter
        }


        if (SearchTabFragment.searchText.isNotEmpty()) {
            apiCallOnTextChange(SearchTabFragment.searchText)
        }
    }


    override fun onTextChange(text: String) {
        apiCallOnTextChange(text)
    }

    private fun apiCallOnTextChange(text: String) {
        pageCount = 1
        searchText = text
        getTopicsList()
    }


    private fun getTopicsList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["topic"] = searchText
        param["startIndex"] = pageCount.toString()
        param["count"] = rowCount.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.topicSearch(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<TopicsResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: TopicsResponse) {
                        if(activity!=null){
                            pbHeader.visibility = View.GONE
                            if (response.responseStatus == 1) {
                                bottomLayout.visibility = View.VISIBLE
                                tvHeaderText.visibility = View.VISIBLE
                                val list = response.responseData?.topicSearch?.items
                                if (!list.isNullOrEmpty()) {
                                    topicsItemList.clear()
                                    topicsItemList.addAll(list)
                                }
                                val searchInfo: TopicsResponse.SearchInformation = response.responseData?.topicSearch?.searchInformation!!
                                tvHeaderText.text = "About " + searchInfo.formattedTotalResults + " results(" + searchInfo.formattedSearchTime + "seconds)"
                                topicsItemListAdapter.notifyDataSetChanged()

                                getQuery(response.responseData?.topicSearch?.queries)

                            } else {
                                bottomLayout.visibility = View.GONE
                                tvHeaderText.visibility = View.GONE
                                topicsItemList.clear()
                                topicsItemListAdapter.notifyDataSetChanged()
                            }
                        }

                    }

                    override fun onError(e: Throwable) {
                        pbHeader.visibility = View.GONE
                        println(e.message)
                    }
                })
    }

    private fun getQuery(queries: TopicsResponse.Queries?) {


        val previous = queries?.previousPage
        if (!previous.isNullOrEmpty()) {
            tvPrevious.visibility = View.VISIBLE
            tvPrevious.setOnClickListener {
                pageCount = previous[0].startIndex!!
                getTopicsList()
            }
        } else {
            tvPrevious.visibility = View.GONE
        }


        val next = queries?.nextPage
        if (!next.isNullOrEmpty()) {
            tvNext.visibility = View.VISIBLE
            tvNext.setOnClickListener {
                pageCount = next[0].startIndex!!
                getTopicsList()
            }
        } else {
            tvNext.visibility = View.GONE
        }

    }

}