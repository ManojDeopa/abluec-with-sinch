package com.ablueclive.global.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.modelClass.ContactModel
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.invite_contacts_item.view.*
import java.util.*
import kotlin.collections.ArrayList


class InviteContactsAdapter(var list: List<ContactModel>) : RecyclerView.Adapter<InviteContactsAdapter.ViewHolder>() {

    lateinit var context: Context
    private var colors: MutableList<String> = ArrayList()


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.invite_contacts_item, parent, false)
        colors.add("#5E97F6")
        colors.add("#9CCC65")
        colors.add("#FF8A65")
        colors.add("#9E9E9E")
        colors.add("#9FA8DA")
        colors.add("#90A4AE")
        colors.add("#AED581")
        colors.add("#F6BF26")
        colors.add("#FFA726")
        colors.add("#4DD0E1")
        colors.add("#BA68C8")
        colors.add("#A1887F")
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvName.text = list[position].name.trim()
        holder.itemView.tvMobile.text = list[position].mobile_number.trim()
        val image = list[position].profile_pic
        if (image.isNotEmpty()) {
            holder.itemView.ivThumb.visibility = View.VISIBLE
            holder.itemView.tvThumb.visibility = View.GONE
            CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + image, holder.itemView.ivThumb)
        } else {
            holder.itemView.ivThumb.visibility = View.GONE
            holder.itemView.tvThumb.visibility = View.VISIBLE
            val test = list[position].name.trim()
            val r = Random()
            val i1 = r.nextInt(11 - 0) + 0
            val draw = GradientDrawable()
            draw.shape = GradientDrawable.OVAL
            draw.setColor(Color.parseColor(colors[i1]))
            val firstText = test.substring(0, 1)
            holder.itemView.tvThumb.background = draw
            holder.itemView.tvThumb.text = firstText
        }


        val status = getAlreadyString(list[position].already_registered, list[position].already_invited)
        if (status.isEmpty()) {
            holder.itemView.cbCheck.isChecked = list[position].isChecked
            holder.itemView.cbCheck.visibility = View.VISIBLE
            holder.itemView.tvAlready.visibility = View.GONE
            holder.itemView.cbCheck.setOnCheckedChangeListener { buttonView, isChecked ->
                list[position].isChecked = isChecked
            }
        } else {
            holder.itemView.cbCheck.isChecked = false
            holder.itemView.cbCheck.visibility = View.GONE
            holder.itemView.tvAlready.visibility = View.VISIBLE
            holder.itemView.tvAlready.text = status
        }

        holder.itemView.setOnClickListener {
            if (status == context.getString(R.string.already_registered)) {
                BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].user_id)
                callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
        }

    }

    private fun getAlreadyString(alreadyRegistered: Int, alreadyInvited: Int): String {
        var str = ""
        if (alreadyRegistered == 1 && alreadyInvited == 1) {
            str = context.getString(R.string.already_invited)
        } else if (alreadyInvited == 1) {
            str = context.getString(R.string.already_invited)
        } else if (alreadyRegistered == 1) {
            str = context.getString(R.string.already_registered)
        }
        return str
    }


    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.cbCheck.setOnCheckedChangeListener(null);
        super.onViewRecycled(holder)
    }

    fun updateList(filterList: List<ContactModel>) {
        list = filterList
        notifyDataSetChanged()
    }

}
