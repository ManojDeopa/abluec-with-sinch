package com.ablueclive.global.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.store_detail_menu_item.view.*

class SettingsListAdapter(var context: FragmentActivity, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<SettingsListAdapter.ViewHolder>() {

    private var listTitle: List<String> = arrayListOf()
    private var listIcon: List<Int> = arrayListOf()


    init {

        listTitle = listOf(
                context.getString(R.string.wall),
                /*context.getString(R.string.favourite_service_provider),*/
                /*context.getString(R.string.my_orders),*/
                context.getString(R.string.notifications),
               /* context.getString(R.string.payment),*/
                context.getString(R.string.share_abluec),
                context.getString(R.string.feedback),
                /*context.getString(R.string.logout)*/
        )

        listIcon = listOf(R.drawable.ic_wall,
                /*R.drawable.favorite,*/
               /* R.drawable.ic_my_orders,*/
                R.drawable.ic_notifications,
               /* R.drawable.payments,*/
                R.drawable.share,
                R.drawable.feedback
                /*R.drawable.new_logout*/)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                listener.onRecyclerItemClick(listTitle[absoluteAdapterPosition], absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.settings_list_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.ivIcon.setImageResource(listIcon[position])
        holder.itemView.tvText.text = listTitle[position]
        holder.itemView.tvText.isAllCaps = listTitle[position] != context.getString(R.string.share_abluec)

    }

    override fun getItemCount(): Int {
        return listTitle.size
    }

}
