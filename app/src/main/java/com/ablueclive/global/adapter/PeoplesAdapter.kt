package com.ablueclive.global.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.global.response.SearchListResponse
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.peoples_list_item.view.*

class PeoplesAdapter(var listener: RecyclerViewItemClick, var list: List<SearchListResponse.SearchFriendDatum>) : RecyclerView.Adapter<PeoplesAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.setOnClickListener {
                BMSPrefs.putString(context, Constants.FRIEND_ID, list[absoluteAdapterPosition].id.toString())
                CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.peoples_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val imageUrl = Constants.PROFILE_IMAGE_URL + list[position].profileImage
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)
        holder.itemView.tvName.text = list[position].name

        val mutualF = list[position].mutualFriendsCount

        if (mutualF != null && mutualF > 0) {
            holder.itemView.tvMutualFriends.visibility = View.VISIBLE
            holder.itemView.tvMutualFriends.text = list[position].mutualFriendsCount.toString() + " Mutual Friends"
        } else {
            holder.itemView.tvMutualFriends.visibility = View.GONE
        }

        when (list[position].already_followed) {

            "0" -> {
                holder.itemView.tvAdd.setBackgroundResource(R.drawable.blue_oval_border)
                holder.itemView.tvAdd.text = context.getString(R.string.follow)
                holder.itemView.tvAdd.setOnClickListener { v: View? ->
                    listener.onRecyclerItemClick("follow", position)
                }
            }
            "1" -> {
                holder.itemView.tvAdd.setBackgroundResource(R.drawable.bg_light_gray)
                holder.itemView.tvAdd.text = context.getString(R.string.unFollow)
                holder.itemView.tvAdd.setOnClickListener { v: View? ->

                    val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                    alert.setTitle(context.getString(R.string.app_name))
                    alert.setMessage(context.getString(R.string.sure_to_un_follow))
                    alert.setCancelable(false)
                    alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                        listener.onRecyclerItemClick("unfollow", position)
                    }

                    alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                    }
                    alert.show()
                }
            }
            "2" -> {
                holder.itemView.tvAdd.setBackgroundResource(R.drawable.bg_light_gray)
                holder.itemView.tvAdd.text = context.getString(R.string.follow_requested)
                holder.itemView.tvAdd.setOnClickListener {

                }
            }
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


}
