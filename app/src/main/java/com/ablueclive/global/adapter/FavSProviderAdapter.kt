package com.ablueclive.global.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.global.response.FavSProvidersResponse
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.fav_providers_item.view.*


class FavSProviderAdapter(private var list: List<FavSProvidersResponse.FavServiceProvider>, var itemClickListener: RecyclerViewItemClick) : RecyclerView.Adapter<FavSProviderAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.ivDots.setOnClickListener {
                val popup = PopupMenu(context, itemView.ivDots)
                popup.inflate(R.menu.delete_option_menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menuDelete -> {
                            itemClickListener.onRecyclerItemClick(list[absoluteAdapterPosition].providerData?.providerId.toString(), absoluteAdapterPosition)
                        }
                    }
                    false
                }
                popup.show()
            }

            itemView.setOnClickListener {
                BMSPrefs.putString(context, Constants.FRIEND_ID, list[absoluteAdapterPosition].providerData?.providerId)
                CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.fav_providers_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTitle.text = list[position].providerData?.providerName
        val imageUrl = Constants.PROFILE_IMAGE_URL + list[position].providerData?.profileImage
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)

    }


}
