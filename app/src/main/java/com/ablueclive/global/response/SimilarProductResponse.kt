package com.ablueclive.global.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SimilarProductResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    class ResponseData {
        @SerializedName("similar_products")
        @Expose
        var similerProducts: List<ProductModel>? = null
    }
}