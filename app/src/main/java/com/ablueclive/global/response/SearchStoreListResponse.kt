package com.ablueclive.global.response

import com.ablueclive.storeSection.response.StoreCalender
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SearchStoreListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("near_buy_stores")
        @Expose
        var nearBuyStores: List<NearBuyStore>? = null
    }

    class NearBuyStore {
        @SerializedName("store_info")
        @Expose
        var storeInfo: StoreInfo? = null

        @SerializedName("category_listing")
        @Expose
        var categoryListing: List<CategoryListing>? = null

        @SerializedName("featured_products")
        @Expose
        var productModels: List<ProductModel>? = null

        @SerializedName("popular_products")
        @Expose
        var popularProducts: List<ProductModel>? = null

        @SerializedName("agent_list")
        @Expose
        var agentList: List<AgentList>? = null

        @SerializedName("store_calender")
        @Expose
        var storeCalender: List<StoreCalender>? = null
    }

    class StoreInfo {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("store_name")
        @Expose
        var storeName: String? = null

        @SerializedName("store_contact_number")
        @Expose
        var storeContactNumber: String? = null


        @SerializedName("store_description")
        @Expose
        var storeDescription: String? = null

        @SerializedName("store_address")
        @Expose
        var storeAddress: String? = null

        @SerializedName("store_complete_address")
        @Expose
        var storeCompleteAddress: String? = null

        @SerializedName("store_latitude")
        @Expose
        var storeLatitude: String? = null

        @SerializedName("store_longitude")
        @Expose
        var storeLongitude: String? = null

        @SerializedName("store_image")
        @Expose
        var storeImage: String? = null

        @SerializedName("images")
        @Expose
        var images: List<Image>? = null

        @SerializedName("store_logo")
        @Expose
        var storeLogo: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("isAvailable")
        @Expose
        var isAvailable: Int? = null

        @SerializedName("service_time")
        @Expose
        var serviceTime: String? = null

        @SerializedName("service_time_unit")
        @Expose
        var serviceTimeUnit: String? = null

        @SerializedName("distance")
        @Expose
        var distance: Float? = null

        @SerializedName("google_distance")
        @Expose
        var google_distance: String? = null


        @SerializedName("tbl_store_order_rating")
        @Expose
        var orderRating: OrderRating? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

    }


    class AgentList {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null
    }

    class Image {
        @SerializedName("ImageName")
        @Expose
        var imageName: String? = null

        @SerializedName("ImageType")
        @Expose
        var imageType: String? = null

        @SerializedName("thumbImage")
        @Expose
        var thumbImage: String? = null
    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }


    class CategoryListing {
        @SerializedName("_id")
        @Expose
        var id: Any = ""

        @SerializedName("parent_id")
        @Expose
        var parentId: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null
    }


}