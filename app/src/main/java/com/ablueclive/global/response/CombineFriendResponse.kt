package com.ablueclive.global.response

import com.ablueclive.modelClass.GetFriendListData
import com.ablueclive.modelClass.PeopleUknow


class CombineFriendResponse {


    var response_status: Int? = null

    var response_msg: String? = null

    var response_data: ResponseData? = null

    var response_invalid: Int? = null

    class ResponseData {

        var getCombineFriendRecord: GetCombineFriendRecord? = null
    }

    class GetCombineFriendRecord {

        var getFriendListData: List<GetFriendListData>? = null

        var RequestToFollowList: List<GetFriendListData>? = null

        var PeopleUknow: List<PeopleUknow>? = null
    }


}