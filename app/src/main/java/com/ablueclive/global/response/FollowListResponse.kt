package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FollowListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null


    class ResponseData {

        @SerializedName("followingUserList")
        @Expose
        var followingUserList: List<FollowUserList>? = null

        @SerializedName("followUserList")
        @Expose
        var followUserList: List<FollowUserList>? = null

        @SerializedName("total_rows")
        @Expose
        var totalRows: Int? = null
    }

    class FollowUserList {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("to_user_id")
        @Expose
        var toUserId: String? = null

        @SerializedName("add_on")
        @Expose
        var addOn: String? = null

        @SerializedName("user")
        @Expose
        var user: User? = null

        @SerializedName("already_followed")
        @Expose
        var already_followed: String? = null
    }

    class User {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("device_id")
        @Expose
        var deviceId: Any? = null

        @SerializedName("device_token")
        @Expose
        var deviceToken: String? = null

        @SerializedName("device_type")
        @Expose
        var deviceType: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("language")
        @Expose
        var language: String? = null

        @SerializedName("verification_otp")
        @Expose
        var verificationOtp: String? = null

        @SerializedName("rating")
        @Expose
        var rating: Int? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("skype")
        @Expose
        var skype: Any? = null

        @SerializedName("about")
        @Expose
        var about: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("debug_mode")
        @Expose
        var debugMode: Int? = null

        @SerializedName("hash")
        @Expose
        var hash: String? = null

        @SerializedName("isStoreCreated")
        @Expose
        var isStoreCreated: Int? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("resume")
        @Expose
        var resume: String? = null

        @SerializedName("cover_letter")
        @Expose
        var coverLetter: String? = null

        @SerializedName("is_private")
        @Expose
        var is_private: Int? = null


    }

}