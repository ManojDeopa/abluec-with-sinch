package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Image {

    @SerializedName("ImageName")
    @Expose
    var imageName: String? = null

    @SerializedName("ImageType")
    @Expose
    var imageType: String? = null

    @SerializedName("thumbImage")
    @Expose
    var thumbImage: String? = null
}