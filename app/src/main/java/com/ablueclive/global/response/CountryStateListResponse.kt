package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class CountryStateListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("countries")
        @Expose
        var countries: List<CountryState>? = null

        @SerializedName("state")
        @Expose
        var state: List<CountryState>? = null
    }

    class CountryState {
        @SerializedName("id")
        @Expose
        var id: Int? = null

        @SerializedName("name")
        @Expose
        var name: String = ""

        @SerializedName("iso3")
        @Expose
        var iso3: String? = null

        @SerializedName("iso2")
        @Expose
        var iso2: String? = null

        @SerializedName("phone_code")
        @Expose
        var phoneCode: String? = null

        @SerializedName("capital")
        @Expose
        var capital: String? = null

        @SerializedName("currency")
        @Expose
        var currency: String? = null

        @SerializedName("native")
        @Expose
        var _native: String? = null

        @SerializedName("emoji")
        @Expose
        var emoji: String? = null

        @SerializedName("emojiU")
        @Expose
        var emojiU: String? = null
    }

}