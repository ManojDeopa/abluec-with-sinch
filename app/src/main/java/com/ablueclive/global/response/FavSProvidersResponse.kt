package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FavSProvidersResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("favServiceProvider")
        @Expose
        var favServiceProvider: List<FavServiceProvider>? = null
    }

    class FavServiceProvider {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("provider_data")
        @Expose
        var providerData: ProviderData? = null
    }

    class ProviderData {
        @SerializedName("provider_id")
        @Expose
        var providerId: String? = null

        @SerializedName("provider_name")
        @Expose
        var providerName: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null
    }

}