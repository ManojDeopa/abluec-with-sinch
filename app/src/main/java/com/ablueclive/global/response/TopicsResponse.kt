package com.ablueclive.global.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TopicsResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    class ResponseData {
        @SerializedName("topicSearch")
        @Expose
        var topicSearch: TopicSearch? = null
    }


    class TopicSearch {

        @SerializedName("queries")
        @Expose
        var queries: Queries? = null

        @SerializedName("searchInformation")
        @Expose
        var searchInformation: SearchInformation? = null

        @SerializedName("items")
        @Expose
        var items: List<Item>? = null
    }

    class SearchInformation {
        @SerializedName("searchTime")
        @Expose
        var searchTime: Float? = null

        @SerializedName("formattedSearchTime")
        @Expose
        var formattedSearchTime: String? = null

        @SerializedName("totalResults")
        @Expose
        var totalResults: String? = null

        @SerializedName("formattedTotalResults")
        @Expose
        var formattedTotalResults: String? = null
    }


    class Request {
        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("totalResults")
        @Expose
        var totalResults: String? = null

        @SerializedName("searchTerms")
        @Expose
        var searchTerms: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("startIndex")
        @Expose
        var startIndex: Int? = null

        @SerializedName("inputEncoding")
        @Expose
        var inputEncoding: String? = null

        @SerializedName("outputEncoding")
        @Expose
        var outputEncoding: String? = null

        @SerializedName("safe")
        @Expose
        var safe: String? = null

        @SerializedName("cx")
        @Expose
        var cx: String? = null
    }

    class PreviousPage {
        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("totalResults")
        @Expose
        var totalResults: String? = null

        @SerializedName("searchTerms")
        @Expose
        var searchTerms: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("startIndex")
        @Expose
        var startIndex: Int? = null

        @SerializedName("inputEncoding")
        @Expose
        var inputEncoding: String? = null

        @SerializedName("outputEncoding")
        @Expose
        var outputEncoding: String? = null

        @SerializedName("safe")
        @Expose
        var safe: String? = null

        @SerializedName("cx")
        @Expose
        var cx: String? = null
    }


    class NextPage {
        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("totalResults")
        @Expose
        var totalResults: String? = null

        @SerializedName("searchTerms")
        @Expose
        var searchTerms: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("startIndex")
        @Expose
        var startIndex: Int? = null

        @SerializedName("inputEncoding")
        @Expose
        var inputEncoding: String? = null

        @SerializedName("outputEncoding")
        @Expose
        var outputEncoding: String? = null

        @SerializedName("safe")
        @Expose
        var safe: String? = null

        @SerializedName("cx")
        @Expose
        var cx: String? = null
    }

    class Queries {
        @SerializedName("previousPage")
        @Expose
        var previousPage: List<PreviousPage>? = null

        @SerializedName("request")
        @Expose
        var request: List<Request>? = null

        @SerializedName("nextPage")
        @Expose
        var nextPage: List<NextPage>? = null
    }

    class Item {
        @SerializedName("kind")
        @Expose
        var kind: String? = null

        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("htmlTitle")
        @Expose
        var htmlTitle: String? = null

        @SerializedName("link")
        @Expose
        var link: String? = null

        @SerializedName("displayLink")
        @Expose
        var displayLink: String? = null

        @SerializedName("snippet")
        @Expose
        var snippet: String? = null

        @SerializedName("htmlSnippet")
        @Expose
        var htmlSnippet: String? = null

        @SerializedName("cacheId")
        @Expose
        var cacheId: String? = null

        @SerializedName("formattedUrl")
        @Expose
        var formattedUrl: String? = null

        @SerializedName("htmlFormattedUrl")
        @Expose
        var htmlFormattedUrl: String? = null

        @SerializedName("pagemap")
        @Expose
        var pageMap: Pagemap? = null

    }

    class Pagemap {
        @SerializedName("cse_thumbnail")
        @Expose
        var cseThumbnail: List<CseThumbnail>? = null
    }

    class CseThumbnail {
        @SerializedName("src")
        @Expose
        var src: String? = null

        @SerializedName("width")
        @Expose
        var width: String? = null

        @SerializedName("height")
        @Expose
        var height: String? = null
    }

}