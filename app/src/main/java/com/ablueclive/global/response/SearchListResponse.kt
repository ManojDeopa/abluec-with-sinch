package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SearchListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("searchFriendData")
        @Expose
        var searchFriendData: List<SearchFriendDatum>? = null

        @SerializedName("searchProductData")
        @Expose
        var searchProductData: List<SearchProductDatum>? = null

        @SerializedName("searchStoredData")
        @Expose
        var searchStoredData: List<SearchStoredDatum>? = null
    }

    class SearchFriendDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mutual_friends_count")
        @Expose
        var mutualFriendsCount: Int? = null

        @SerializedName("checkstatus")
        @Expose
        var checkstatus: Int? = null

        @SerializedName("is_friend")
        @Expose
        var is_friend: Int? = null

        @SerializedName("need_to_respond")
        @Expose
        var need_to_respond: Int? = null

        @SerializedName("already_send_request")
        @Expose
        var already_send_request: Int? = null

        @SerializedName("already_followed")
        @Expose
        var already_followed: String? = null


        @SerializedName("is_private")
        @Expose
        var is_private: Int? = null


    }

    class SearchProductDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null
    }

    class SearchStoredDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("store_name")
        @Expose
        var storeName: String? = null

        @SerializedName("store_image")
        @Expose
        var storeImage: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null
    }

}