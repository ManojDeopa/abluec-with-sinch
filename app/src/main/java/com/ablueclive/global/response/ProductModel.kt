package com.ablueclive.global.response

import com.ablueclive.storeSection.response.StoreOrderListResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductModel {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("iso_schedule_from_date")
    @Expose
    var isoScheduleFromDate: String? = null

    @SerializedName("time_zone")
    @Expose
    var timeZone: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: String? = null

    @SerializedName("product_description")
    @Expose
    var productDescription: String? = null

    @SerializedName("product_price")
    @Expose
    var productPrice: Any? = null

    @SerializedName("delivery_time")
    @Expose
    var deliveryTime: String? = null

    @SerializedName("product_category")
    @Expose
    var productCategory: Any = ""

    @SerializedName("sku")
    @Expose
    var sku: String? = null

    @SerializedName("product_title")
    @Expose
    var productTitle: String? = null

    @SerializedName("is_shipping_available")
    @Expose
    var isShippingAvailable: Int? = null

    @SerializedName("iso_schedule_to_date")
    @Expose
    var isoScheduleToDate: String? = null

    @SerializedName("is_featured")
    @Expose
    var isFeatured: Int? = null

    @SerializedName("product_sale_price")
    @Expose
    var productSalePrice: Any? = null

    @SerializedName("unit")
    @Expose
    var unit: String? = null

    @SerializedName("store_id")
    @Expose
    var storeId: String? = null

    @SerializedName("schedule_to_date")
    @Expose
    var scheduleToDate: String? = null

    @SerializedName("is_taxable")
    @Expose
    var isTaxable: Int? = null

    @SerializedName("schedule_from_date")
    @Expose
    var scheduleFromDate: String? = null

    @SerializedName("stock")
    @Expose
    var stock: Int? = null

    @SerializedName("images")
    @Expose
    var images: List<String>? = null

    @SerializedName("product_quantity")
    @Expose
    var productQuantity: Any? = null

    @SerializedName("utc_schedule_from_date")
    @Expose
    var utcScheduleFromDate: String? = null

    @SerializedName("utc_schedule_to_date")
    @Expose
    var utcScheduleToDate: String? = null

    @SerializedName("cart")
    @Expose
    var cart: Cart? = null

    @SerializedName("product_favourite")
    @Expose
    var productFavourite: ProductFavourite? = null

    @SerializedName("_created_at")
    @Expose
    var createdAt: String? = null

    @SerializedName("loc")
    @Expose
    var loc: SearchStoreListResponse.Loc? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("distance")
    @Expose
    var distance: Float? = null

    @SerializedName("google_distance")
    @Expose
    var google_distance: String? = null


    @SerializedName("store")
    @Expose
    var store: StoreOrderListResponse.Store? = null


    class Cart {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("qty")
        @Expose
        var qty: Int? = null
    }

    class ProductFavourite {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null
    }

}


