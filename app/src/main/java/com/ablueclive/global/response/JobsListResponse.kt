package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class JobsListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("getjobUserList")
        @Expose
        var getjobUserList: List<GetjobUserList>? = null

        @SerializedName("getAppliedjobList")
        @Expose
        var getAppliedjobList: List<GetjobUserList>? = null


    }

    class GetjobUserList {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("job_title")
        @Expose
        var jobTitle: String? = null

        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null

        @SerializedName("service_id")
        @Expose
        var serviceId: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("job_image")
        @Expose
        var jobImage: List<String>? = null

        @SerializedName("minPrice")
        @Expose
        var minPrice: Any? = null

        @SerializedName("maxPrice")
        @Expose
        var maxPrice: Any? = null

        @SerializedName("cat")
        @Expose
        var cat: String? = null

        @SerializedName("customer_name")
        @Expose
        var customerName: String? = null
    }

}