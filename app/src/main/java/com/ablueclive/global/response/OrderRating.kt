package com.ablueclive.global.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class OrderRating {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("averageRating")
    @Expose
    var averageRating: Any? = null

    @SerializedName("RatingCount")
    @Expose
    var ratingCount: Int? = null

}
