package com.ablueclive.modelClass;

public class DataAllImages
{
    private String image_name;

    private String post_id;

    private String user_id;

    private String thumb_Image;

    private String _id;

    private String created_date;

    private String Image_type;

    public String getImage_name ()
    {
        return image_name;
    }

    public void setImage_name (String image_name)
    {
        this.image_name = image_name;
    }

    public String getPost_id ()
    {
        return post_id;
    }

    public void setPost_id (String post_id)
    {
        this.post_id = post_id;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getThumb_Image ()
    {
        return thumb_Image;
    }

    public void setThumb_Image (String thumb_Image)
    {
        this.thumb_Image = thumb_Image;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getCreated_date ()
    {
        return created_date;
    }

    public void setCreated_date (String created_date)
    {
        this.created_date = created_date;
    }

    public String getImage_type ()
    {
        return Image_type;
    }

    public void setImage_type (String Image_type)
    {
        this.Image_type = Image_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image_name = "+image_name+", post_id = "+post_id+", user_id = "+user_id+", thumb_Image = "+thumb_Image+", _id = "+_id+", created_date = "+created_date+", Image_type = "+Image_type+"]";
    }
}