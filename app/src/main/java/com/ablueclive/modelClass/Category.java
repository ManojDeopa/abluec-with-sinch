package com.ablueclive.modelClass;

public class Category
{
    private String name;

    private String _id;

    private String updatedon;

    private String addedon;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getUpdatedon ()
    {
        return updatedon;
    }

    public void setUpdatedon (String updatedon)
    {
        this.updatedon = updatedon;
    }

    public String getAddedon ()
    {
        return addedon;
    }

    public void setAddedon (String addedon)
    {
        this.addedon = addedon;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", _id = "+_id+", updatedon = "+updatedon+", addedon = "+addedon+"]";
    }
}
			
			