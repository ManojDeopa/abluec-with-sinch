package com.ablueclive.modelClass;

import java.io.Serializable;

public class HashTag  implements Serializable {
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String _id;
    private String name;
}
