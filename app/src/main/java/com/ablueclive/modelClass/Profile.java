package com.ablueclive.modelClass;

public class Profile {

    private String debug_mode;

    private String isStoreCreated;

    public String getFaceBookId() {
        return faceBookId;
    }

    public void setFaceBookId(String faceBookId) {
        this.faceBookId = faceBookId;
    }

    private String faceBookId;

    public String getIsStoreCreated() {
        return isStoreCreated;
    }

    public void setIsStoreCreated(String isStoreCreated) {
        this.isStoreCreated = isStoreCreated;
    }

    private String online_status;

    private String mobile;

    private String rating;

    private String count;

    private String _created_at;

    private String wallet_balance;

    private String language;

    private String session_token;

    private String country_code;

    private String stripe_customer_id;

    private String verification_otp;

    private String ser_chn;

    private String pub_chn;

    private String _updated_at;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    private String name;

    private String display_name = "";

    private String is_stripe_connected;

    private String _id;

    private String store_id;

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }

    private String email;

    private String hash;

    private String background_image;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    private String status;

    private String profile_image;

    public String getDebug_mode() {
        return debug_mode;
    }

    public void setDebug_mode(String debug_mode) {
        this.debug_mode = debug_mode;
    }

    public String getOnline_status() {
        return online_status;
    }

    public void setOnline_status(String online_status) {
        this.online_status = online_status;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String get_created_at() {
        return _created_at;
    }

    public void set_created_at(String _created_at) {
        this._created_at = _created_at;
    }

    public String getWallet_balance() {
        return wallet_balance;
    }

    public void setWallet_balance(String wallet_balance) {
        this.wallet_balance = wallet_balance;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSession_token() {
        return session_token;
    }

    public void setSession_token(String session_token) {
        this.session_token = session_token;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getStripe_customer_id() {
        return stripe_customer_id;
    }

    public void setStripe_customer_id(String stripe_customer_id) {
        this.stripe_customer_id = stripe_customer_id;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }

    public String getSer_chn() {
        return ser_chn;
    }

    public void setSer_chn(String ser_chn) {
        this.ser_chn = ser_chn;
    }

    public String getPub_chn() {
        return pub_chn;
    }

    public void setPub_chn(String pub_chn) {
        this.pub_chn = pub_chn;
    }

    public String get_updated_at() {
        return _updated_at;
    }

    public void set_updated_at(String _updated_at) {
        this._updated_at = _updated_at;
    }

    public String getName() {
        return name;
    }

    public String getDisplay_name() {
        return display_name;
    }


    public String getIs_stripe_connected() {
        return is_stripe_connected;
    }

    public void setIs_stripe_connected(String is_stripe_connected) {
        this.is_stripe_connected = is_stripe_connected;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [debug_mode = " + debug_mode + ", online_status = " + online_status + ", mobile = " + mobile + ", rating = " + rating + ", count = " + count + ", _created_at = " + _created_at + ", wallet_balance = " + wallet_balance + ", language = " + language + ", session_token = " + session_token + ", country_code = " + country_code + ", stripe_customer_id = " + stripe_customer_id + ", verification_otp = " + verification_otp + ", ser_chn = " + ser_chn + ", pub_chn = " + pub_chn + ", _updated_at = " + _updated_at + ", name = " + name + ", is_stripe_connected = " + is_stripe_connected + ", _id = " + _id + ", email = " + email + ", hash = " + hash + ", status = " + status + "]";
    }
}