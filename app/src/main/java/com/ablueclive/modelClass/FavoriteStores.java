package com.ablueclive.modelClass;

public class FavoriteStores
{
    private String store_name;

    private String store_id;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    private String store_image;

    public String getStore_name ()
    {
        return store_name;
    }

    public void setStore_name (String store_name)
    {
        this.store_name = store_name;
    }



    public String getStore_image ()
    {
        return store_image;
    }

    public void setStore_image (String store_image)
    {
        this.store_image = store_image;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [store_name = "+store_name+", store_id = "+store_id+", store_image = "+store_image+"]";
    }
}
	