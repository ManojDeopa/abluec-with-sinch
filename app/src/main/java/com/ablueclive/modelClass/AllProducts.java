package com.ablueclive.modelClass;

import java.util.ArrayList;

public class AllProducts {
    private String category_name;

    private String category_id;

    private ArrayList<Category_products> category_products;

    public ArrayList<Category_products> getCategory_products() {
        return category_products;
    }

    public void setCategory_products(ArrayList<Category_products> category_products) {
        this.category_products = category_products;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }


    @Override
    public String toString() {
        return "ClassPojo [category_name = " + category_name + ", category_id = " + category_id + ", category_products = " + category_products + "]";
    }
}
	