package com.ablueclive.modelClass;

import java.util.ArrayList;

public class FavoriteProducts
{

    private ArrayList<String>images;
    private String _id;

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    private String product_price;

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }


    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [images = "+images+", _id = "+_id+"]";
    }

}
	