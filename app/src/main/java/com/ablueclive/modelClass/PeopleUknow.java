package com.ablueclive.modelClass;

public class PeopleUknow {

    public String already_send_request="";

    private String profile_image;

    public String mutual_friends_count="0";

    private String name;

    private String _id;

    public String getProfile_image() {
        return profile_image;
    }

    public String getName() {
        return name;
    }

    public String get_id() {
        return _id;
    }



    @Override
    public String toString() {
        return "ClassPojo [profile_image = " + profile_image + ", mutual_friends_count = " + mutual_friends_count + ", name = " + name + ", _id = " + _id + "]";
    }
}
