package com.ablueclive.modelClass;

public class Reply_comments
{
    private String profile_image;

    private String reply_id;

    private String user_id;

    private String user_name;

    private String reply_user_comment;

    private String comment_timestamp;

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    private String post_user_id;

    private String comment_id;

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getReply_id ()
    {
        return reply_id;
    }

    public void setReply_id (String reply_id)
    {
        this.reply_id = reply_id;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getReply_user_comment ()
    {
        return reply_user_comment;
    }

    public void setReply_user_comment (String reply_user_comment)
    {
        this.reply_user_comment = reply_user_comment;
    }

    public String getComment_timestamp ()
    {
        return comment_timestamp;
    }

    public void setComment_timestamp (String comment_timestamp)
    {
        this.comment_timestamp = comment_timestamp;
    }

    public String getPost_user_id ()
    {
        return post_user_id;
    }

    public void setPost_user_id (String post_user_id)
    {
        this.post_user_id = post_user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profile_image = "+profile_image+", reply_id = "+reply_id+", user_id = "+user_id+", user_name = "+user_name+", reply_user_comment = "+reply_user_comment+", comment_timestamp = "+comment_timestamp+", post_user_id = "+post_user_id+"]";
    }
}
		