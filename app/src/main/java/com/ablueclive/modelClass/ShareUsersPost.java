package com.ablueclive.modelClass;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShareUsersPost implements Serializable {


    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("post_content")
    @Expose
    public String post_content = "";

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email = "";

    @SerializedName("profile_image")
    @Expose
    public String profile_image = "";

    @SerializedName("retweet_date")
    @Expose
    public String posted_date = "";


    @NonNull
    @Override
    public String toString() {
        return "ClassPojo [user_id = " + user_id + ", name = " + name + "]";
    }
}