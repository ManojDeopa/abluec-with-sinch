package com.ablueclive.modelClass;

import java.util.ArrayList;

public class Response_data {

    public Integer is_user_chat_block_by_you = 0;

    public Integer is_user_chat_block_by_other = 0;

    public Integer is_chat_notification_mute = 0;

    public Integer unread_msg_count = 0;

    public Integer availability_status = 1;

    public String version_code_android = "0";

    private String OTP;

    private String session_token;

    private Profile profile;

    private Store_info store_info;

    private Loc loc;

    public String getStore_logo() {
        return store_logo;
    }

    public void setStore_logo(String store_logo) {
        this.store_logo = store_logo;
    }

    private String store_logo;
    private String _id;
    private Product_details product_details;
    private ArrayList<FavoriteProducts> favoriteProducts;
    private ArrayList<FavoriteStores> favoriteStores;
    private ArrayList<Product_category_listing> product_category_listing;
    private ArrayList<All_stores> all_stores;
    private ArrayList<DataAllImages> dataAllImages;
    private ArrayList<AllProducts> allProducts;
    private ArrayList<ReplyData> replyData;
    private ArrayList<CheerList> CheerList;

    private ArrayList<PeopleUknow> PeopleUknow;

    private ArrayList<ContactModel> contacts;

    public ArrayList<ContactModel> getContactModels() {
        return contacts;
    }


    public ArrayList<ReportUserReasonData> getReasons() {
        return reasons;
    }

    private ArrayList<ReportUserReasonData> reasons;

    public ArrayList<com.ablueclive.modelClass.PeopleUknow> getPeopleUknow() {
        return PeopleUknow;
    }

    public void setPeopleUknow(ArrayList<com.ablueclive.modelClass.PeopleUknow> peopleUknow) {
        PeopleUknow = peopleUknow;
    }

    public GetUserInfo getGetUserInfo() {
        return getUserInfo;
    }

    public void setGetUserInfo(GetUserInfo getUserInfo) {
        this.getUserInfo = getUserInfo;
    }

    private GetUserInfo getUserInfo;

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    private String title;
    private String isFav;

    public ArrayList<SearchProductData> getSearchProductData() {
        return searchProductData;
    }

    public void setSearchProductData(ArrayList<SearchProductData> searchProductData) {
        this.searchProductData = searchProductData;
    }

    public ArrayList<SearchStoredData> getSearchStoredData() {
        return searchStoredData;
    }

    public void setSearchStoredData(ArrayList<SearchStoredData> searchStoredData) {
        this.searchStoredData = searchStoredData;
    }

    private ArrayList<SearchProductData> searchProductData;
    private ArrayList<SearchStoredData> searchStoredData;


    public ArrayList<NotificationData> getNotificationData() {
        return notificationData;
    }

    public void setNotificationData(ArrayList<NotificationData> notificationData) {
        this.notificationData = notificationData;
    }

    private ArrayList<NotificationData> notificationData;

    public ArrayList<com.ablueclive.modelClass.CheerList> getCheerList() {
        return CheerList;
    }

    public void setCheerList(ArrayList<com.ablueclive.modelClass.CheerList> cheerList) {
        CheerList = cheerList;
    }

    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ArrayList<ReplyData> getReplyData() {
        return replyData;
    }

    public void setReplyData(ArrayList<ReplyData> replyData) {
        this.replyData = replyData;
    }

    public ArrayList<AllComments> getAllComments() {
        return allComments;
    }

    public void setAllComments(ArrayList<AllComments> allComments) {
        this.allComments = allComments;
    }

    private ArrayList<AllComments> allComments;

    private ArrayList<GetFriendData> getFriendData;

    private ArrayList<SearchFriendData> searchFriendData;

    private ArrayList<GetFriendListData> getFriendListData;
    private ArrayList<PostData> postData;

    private ArrayList<GetAllFriendsList> getAllFriendsList;

    public ArrayList<GetAllFriendsList> getGetAllFriendsList() {
        return getAllFriendsList;
    }

    public void setGetAllFriendsList(ArrayList<GetAllFriendsList> getAllFriendsList) {
        this.getAllFriendsList = getAllFriendsList;
    }

    public ArrayList<DataAllImages> getDataAllImages() {
        return dataAllImages;
    }

    public void setDataAllImages(ArrayList<DataAllImages> dataAllImages) {
        this.dataAllImages = dataAllImages;
    }

    public ArrayList<GetFriendData> getGetFriendData() {
        return getFriendData;
    }

    public void setGetFriendData(ArrayList<GetFriendData> getFriendData) {
        this.getFriendData = getFriendData;
    }


    private String page_name;

    private String totalCountLike;

    private String status;

    public String getTotalCountLike() {
        return totalCountLike;
    }

    public void setTotalCountLike(String totalCountLike) {
        this.totalCountLike = totalCountLike;
    }


    public ArrayList<PostData> getPostData() {
        return postData;
    }

    public void setPostData(ArrayList<PostData> postData) {
        this.postData = postData;
    }


    private String addedon;

    private String desc;

    private String debug_mode;

    private String device_id;

    private String online_status;

    private String mobile;

    private String rating;

    private String count;

    private String _created_at;

    private String device_type;

    private String language;

    private String isStoreCreated;

    private String background_image;

    private String country_code;

    private String profile_image;

    private String verification_otp;

    private String device_token;

    private String _updated_at;

    private String name;

    private String email;

    private String hash;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    private String about;
    private String skype;

    public ArrayList<SellarList> getSellarList() {
        return sellarList;
    }

    public void setSellarList(ArrayList<SellarList> sellarList) {
        this.sellarList = sellarList;
    }

    private ArrayList<SellarList> sellarList;

    public ArrayList<Msg> getMsg() {
        return msg;
    }

    public void setMsg(ArrayList<Msg> msg) {
        this.msg = msg;
    }

    private ArrayList<Msg> msg;

    public String getDebug_mode() {
        return debug_mode;
    }

    public void setDebug_mode(String debug_mode) {
        this.debug_mode = debug_mode;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getOnline_status() {
        return online_status;
    }

    public void setOnline_status(String online_status) {
        this.online_status = online_status;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String get_created_at() {
        return _created_at;
    }

    public void set_created_at(String _created_at) {
        this._created_at = _created_at;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIsStoreCreated() {
        return isStoreCreated;
    }

    public void setIsStoreCreated(String isStoreCreated) {
        this.isStoreCreated = isStoreCreated;
    }

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String get_updated_at() {
        return _updated_at;
    }

    public void set_updated_at(String _updated_at) {
        this._updated_at = _updated_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getPage_name() {
        return page_name;
    }

    public void setPage_name(String page_name) {
        this.page_name = page_name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddedon() {
        return addedon;
    }

    public void setAddedon(String addedon) {
        this.addedon = addedon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public ArrayList<AllProducts> getAllProducts() {
        return allProducts;
    }

    public void setAllProducts(ArrayList<AllProducts> allProducts) {
        this.allProducts = allProducts;
    }

    public ArrayList<FavoriteProducts> getFavoriteProducts() {
        return favoriteProducts;
    }

    public void setFavoriteProducts(ArrayList<FavoriteProducts> favoriteProducts) {
        this.favoriteProducts = favoriteProducts;
    }

    public ArrayList<FavoriteStores> getFavoriteStores() {
        return favoriteStores;
    }

    public void setFavoriteStores(ArrayList<FavoriteStores> favoriteStores) {
        this.favoriteStores = favoriteStores;
    }

    public ArrayList<All_stores> getAll_stores() {
        return all_stores;
    }

    public void setAll_stores(ArrayList<All_stores> all_stores) {
        this.all_stores = all_stores;
    }

    public Product_details getProduct_details() {
        return product_details;
    }

    public void setProduct_details(Product_details product_details) {
        this.product_details = product_details;
    }


    public ArrayList<Product_category_listing> getProduct_category_listing() {
        return product_category_listing;
    }

    public void setProduct_category_listing(ArrayList<Product_category_listing> product_category_listing) {
        this.product_category_listing = product_category_listing;
    }

    public ArrayList<Products> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products> products) {
        this.products = products;
    }

    private ArrayList<Products> products;

    public Store_info getStore_info() {
        return store_info;
    }

    public void setStore_info(Store_info store_info) {
        this.store_info = store_info;
    }


    public Loc getLoc() {
        return loc;
    }

    public void setLoc(Loc loc) {
        this.loc = loc;
    }


    public String getSession_token() {
        return session_token;
    }

    public void setSession_token(String session_token) {
        this.session_token = session_token;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ArrayList<GetFriendListData> getGetFriendListData() {
        return getFriendListData;
    }

    public void setGetFriendListData(ArrayList<GetFriendListData> getFriendListData) {
        this.getFriendListData = getFriendListData;
    }

    public ArrayList<SearchFriendData> getSearchFriendData() {
        return searchFriendData;
    }

    public void setSearchFriendData(ArrayList<SearchFriendData> searchFriendData) {
        this.searchFriendData = searchFriendData;
    }

    @Override
    public String toString() {
        return "ClassPojo [session_token = " + session_token + ", profile = " + profile + ",OTP = " + OTP + "]";
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}