package com.ablueclive.modelClass;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class PostData implements Serializable {

    private String post_title;
    private String store_id;
    private int store_status;
    public String post_user_display_name = "";
    private String post_user_name;
    private String status;
    private String totalComments = "";
    private ArrayList<Comments> comments;
    private ArrayList<ShareUsersPost> retweet_user_post;
    private ArrayList<RePostUser> repostUser;
    private ArrayList<HashTag> /*hasTagName*/hasTagNameUnsedForNow = null;
    private ArrayList<Images> images;
    private String post_reports_status;
    private ArrayList<Like_users> like_users;
    private String post_user_profile_image;
    private String post_UTC_Date;
    private String post_user_like_status;
    private String totalCountLike;
    private String post_content="";
    private String user_id;
    private String _id;
    private String post_time_status;
    private String share_count;
    private String shareUrl = "";
    private boolean isRetweeted = false;
    private String repost_content = "";

    public String getRepost_content() {
        return repost_content;
    }

    public void setRepost_content(String repost_content) {
        this.repost_content = repost_content;
    }


    public ArrayList<RePostUser> getRePostUser() {
        return repostUser;
    }

    public void setRePostUser(ArrayList<RePostUser> rePostUser) {
        this.repostUser = rePostUser;
    }

    public boolean isRetweeted() {
        return isRetweeted;
    }

    public void setRetweeted(boolean retweeted) {
        isRetweeted = retweeted;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public int getStore_status() {
        return store_status;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }


    public ArrayList<HashTag> getHasTagNameUnsedForNow() {
        return hasTagNameUnsedForNow;
    }

    public void setHasTagNameUnsedForNow(ArrayList<HashTag> hasTagNameUnsedForNow) {
        this.hasTagNameUnsedForNow = hasTagNameUnsedForNow;
    }


    public String getPost_reports_status() {
        return post_reports_status;
    }

    public void setPost_reports_status(String post_reports_status) {
        this.post_reports_status = post_reports_status;
    }

    public ArrayList<Comments> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comments> comments) {
        this.comments = comments;
    }


    public ArrayList<Like_users> getLike_users() {
        return like_users;
    }

    public void setLike_users(ArrayList<Like_users> like_users) {
        this.like_users = like_users;
    }


    public ArrayList<Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }


    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }


    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_user_name() {
        return post_user_name;
    }

    public void setPost_user_name(String post_user_name) {
        this.post_user_name = post_user_name;
    }


    public String getPost_user_profile_image() {
        return post_user_profile_image;
    }

    public void setPost_user_profile_image(String post_user_profile_image) {
        this.post_user_profile_image = post_user_profile_image;
    }

    public String getPost_user_like_status() {
        return post_user_like_status;
    }

    public void setPost_user_like_status(String post_user_like_status) {
        this.post_user_like_status = post_user_like_status;
    }

    public String getTotalCountLike() {
        return totalCountLike;
    }

    public void setTotalCountLike(String totalCountLike) {
        this.totalCountLike = totalCountLike;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPost_time_status() {
        return post_time_status;
    }

    public void setPost_time_status(String post_time_status) {
        this.post_time_status = post_time_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public ArrayList<ShareUsersPost> getShareUserPost() {
        return retweet_user_post;
    }

    public void setShareUserPost(ArrayList<ShareUsersPost> list) {
        this.retweet_user_post = list;
    }

    @NonNull
    @Override
    public String toString() {
        return "ClassPojo [post_title = " + post_title + ", post_user_name = " + post_user_name + ", images = " + images + ", like_users = " + like_users + ", post_user_profile_image = " + post_user_profile_image + ", post_UTC_Date = " + post_UTC_Date + ", post_user_like_status = " + post_user_like_status + ", totalCountLike = " + totalCountLike + ", post_content = " + post_content + ", user_id = " + user_id + ", _id = " + _id + ", post_time_status = " + post_time_status + ", status = " + status + ",hasTagName = " + hasTagNameUnsedForNow + "]";
    }
}
			
			