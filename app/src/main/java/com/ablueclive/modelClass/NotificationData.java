package com.ablueclive.modelClass;

public class NotificationData
{
    private String profile_image;

    private String post_comment;

    private String name;

    private String _id;

    private String to_user_id;
    private String from_user_id;

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    private String post_id;

    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getNType() {
        return NType;
    }

    public void setNType(String NType) {
        this.NType = NType;
    }

    private String NType;

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getPost_comment ()
    {
        return post_comment;
    }

    public void setPost_comment (String post_comment)
    {
        this.post_comment = post_comment;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profile_image = "+profile_image+", post_comment = "+post_comment+", name = "+name+", _id = "+_id+"]";
    }
}
