package com.ablueclive.modelClass;

public class SellarList
{
    private String profile_image;

    private String name;

    private String last_message;

    private String _id;

    private String status;

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLast_message ()
    {
        return last_message;
    }

    public void setLast_message (String last_message)
    {
        this.last_message = last_message;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profile_image = "+profile_image+", name = "+name+", last_message = "+last_message+", _id = "+_id+", status = "+status+"]";
    }
}
			
			