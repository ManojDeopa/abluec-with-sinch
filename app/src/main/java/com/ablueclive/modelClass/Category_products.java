package com.ablueclive.modelClass;

import java.util.ArrayList;

public class Category_products
{


    private ArrayList<String>images;

    private String _id;

    private String product_price;

    private String isFav;

    private String product_title;



    public String get_id ()
    {
        return _id;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getProduct_price ()
    {
        return product_price;
    }

    public void setProduct_price (String product_price)
    {
        this.product_price = product_price;
    }

    public String getIsFav ()
    {
        return isFav;
    }

    public void setIsFav (String isFav)
    {
        this.isFav = isFav;
    }

    public String getProduct_title ()
    {
        return product_title;
    }

    public void setProduct_title (String product_title)
    {
        this.product_title = product_title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [images = "+images+", _id = "+_id+", product_price = "+product_price+", isFav = "+isFav+", product_title = "+product_title+"]";
    }
}
			
			