package com.ablueclive.modelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Images implements Serializable {

    @SerializedName("ImageName")
    @Expose
    private String ImageName;

    @SerializedName("thumbImage")
    @Expose
    private String thumbImage;

    @SerializedName("ImageType")
    @Expose
    private String ImageType;

    String type;
    String media_path;
    String thumbpath;
    String source;
    String type_edit;
    private boolean isServer;

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String ImageName) {
        this.ImageName = ImageName;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getImageType() {
        return ImageType;
    }

    public void setImageType(String ImageType) {
        this.ImageType = ImageType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia_path() {
        return media_path;
    }

    public void setMedia_path(String media_path) {
        this.media_path = media_path;
    }

    public String getThumbpath() {
        return thumbpath;
    }

    public void setThumbpath(String thumbpath) {
        this.thumbpath = thumbpath;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getType_edit() {
        return type_edit;
    }

    public void setType_edit(String type_edit) {
        this.type_edit = type_edit;
    }

    public boolean isServer() {
        return isServer;
    }

    public void setServer(boolean server) {
        isServer = server;
    }

    @Override
    public String toString() {
        return "ClassPojo [ImageName = " + ImageName + ", thumbImage = " + thumbImage + ", ImageType = " + ImageType + "]";
    }
}
			
			