package com.ablueclive.modelClass;

public class Msg
{
    private String share_count;

    private String _id;

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [share_count = "+share_count+", _id = "+_id+"]";
    }
}
	