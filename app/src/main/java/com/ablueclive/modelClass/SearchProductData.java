package com.ablueclive.modelClass;

public class SearchProductData
{
    private String profile_image;

    private String name;

    private String _id;

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profile_image = "+profile_image+", name = "+name+", _id = "+_id+"]";
    }
}