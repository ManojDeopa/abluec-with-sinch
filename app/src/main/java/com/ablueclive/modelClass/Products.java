package com.ablueclive.modelClass;

import java.util.ArrayList;

public class Products
{
    private String store_id;

    private String session_token;

    private ArrayList<String>images;

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    private String user_id;

    private String _id;

    private String product_price;

    private String product_description;

    private String product_title;

    private String product_category;

    public String getStore_id ()
    {
        return store_id;
    }

    public void setStore_id (String store_id)
    {
        this.store_id = store_id;
    }

    public String getSession_token ()
    {
        return session_token;
    }

    public void setSession_token (String session_token)
    {
        this.session_token = session_token;
    }



    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getProduct_price ()
    {
        return product_price;
    }

    public void setProduct_price (String product_price)
    {
        this.product_price = product_price;
    }

    public String getProduct_description ()
    {
        return product_description;
    }

    public void setProduct_description (String product_description)
    {
        this.product_description = product_description;
    }

    public String getProduct_title ()
    {
        return product_title;
    }

    public void setProduct_title (String product_title)
    {
        this.product_title = product_title;
    }

    public String getProduct_category ()
    {
        return product_category;
    }

    public void setProduct_category (String product_category)
    {
        this.product_category = product_category;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [store_id = "+store_id+", session_token = "+session_token+", images = "+images+", user_id = "+user_id+", _id = "+_id+", product_price = "+product_price+", product_description = "+product_description+", product_title = "+product_title+", product_category = "+product_category+"]";
    }
}