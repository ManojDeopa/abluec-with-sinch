package com.ablueclive.modelClass;

import android.os.Parcel;
import android.os.Parcelable;

public class GetFriendData implements Parcelable {

    private String profile_image;

    private String name;

    private String _id;

    protected GetFriendData(Parcel in) {
        profile_image = in.readString();
        name = in.readString();
        _id = in.readString();
    }

    public static final Creator<GetFriendData> CREATOR = new Creator<GetFriendData>() {
        @Override
        public GetFriendData createFromParcel(Parcel in) {
            return new GetFriendData(in);
        }

        @Override
        public GetFriendData[] newArray(int size) {
            return new GetFriendData[size];
        }
    };

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [profile_image = "+profile_image+", name = "+name+", _id = "+_id+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(profile_image);
        dest.writeString(name);
        dest.writeString(_id);
    }
}