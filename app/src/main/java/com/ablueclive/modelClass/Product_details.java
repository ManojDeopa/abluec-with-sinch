package com.ablueclive.modelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Product_details implements Parcelable
{
    private String store_id;
    private String product_price;

    private String product_quantity;

    protected Product_details(Parcel in) {
        store_id = in.readString();
        product_price = in.readString();
        product_quantity = in.readString();
        delivery_time = in.readString();
        images = in.createStringArrayList();
        no_of_days_ago = in.readString();
        user_id = in.readString();
        _created_at = in.readString();
        _id = in.readString();
        isFav = in.readString();
        brand = in.readString();
        product_description = in.readString();
        product_title = in.readString();
        product_category = in.readString();
    }

    public static final Creator<Product_details> CREATOR = new Creator<Product_details>() {
        @Override
        public Product_details createFromParcel(Parcel in) {
            return new Product_details(in);
        }

        @Override
        public Product_details[] newArray(int size) {
            return new Product_details[size];
        }
    };

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    private String delivery_time;

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    private ArrayList<String> images;

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    private String no_of_days_ago;

    private String user_id;

    private String _created_at;

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    private String _id;

    private String isFav;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    private String brand;

    private String product_description;

    private String product_title;

    private String product_category;

    public String getStore_id ()
    {
        return store_id;
    }

    public void setStore_id (String store_id)
    {
        this.store_id = store_id;
    }



    public String getNo_of_days_ago ()
    {
        return no_of_days_ago;
    }

    public void setNo_of_days_ago (String no_of_days_ago)
    {
        this.no_of_days_ago = no_of_days_ago;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String get_created_at ()
    {
        return _created_at;
    }

    public void set_created_at (String _created_at)
    {
        this._created_at = _created_at;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getProduct_description ()
    {
        return product_description;
    }

    public void setProduct_description (String product_description)
    {
        this.product_description = product_description;
    }

    public String getProduct_title ()
    {
        return product_title;
    }

    public void setProduct_title (String product_title)
    {
        this.product_title = product_title;
    }

    public String getProduct_category ()
    {
        return product_category;
    }

    public void setProduct_category (String product_category)
    {
        this.product_category = product_category;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [store_id = "+store_id+", images = "+images+", no_of_days_ago = "+no_of_days_ago+", user_id = "+user_id+", _created_at = "+_created_at+", _id = "+_id+", product_description = "+product_description+", product_title = "+product_title+", product_category = "+product_category+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_id);
        dest.writeString(product_price);
        dest.writeString(product_quantity);
        dest.writeString(delivery_time);
        dest.writeStringList(images);
        dest.writeString(no_of_days_ago);
        dest.writeString(user_id);
        dest.writeString(_created_at);
        dest.writeString(_id);
        dest.writeString(isFav);
        dest.writeString(brand);
        dest.writeString(product_description);
        dest.writeString(product_title);
        dest.writeString(product_category);
    }
}