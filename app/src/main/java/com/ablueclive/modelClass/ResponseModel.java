package com.ablueclive.modelClass;

public class ResponseModel {


    private Response_data response_data = null;


    private String response_status = "";


    private final Integer response_invalid = 0;


    private String response_msg = "";


    public Response_data getResponse_data() {
        return response_data;
    }

    public void setResponse_data(Response_data response_data) {
        this.response_data = response_data;
    }

    public String getResponse_status() {
        return response_status;
    }

    public void setResponse_status(String response_status) {
        this.response_status = response_status;
    }

    public String getResponse_invalid() {
        return String.valueOf(response_invalid);
    }


    public String getResponse_msg() {
        return response_msg;
    }

    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }

}
