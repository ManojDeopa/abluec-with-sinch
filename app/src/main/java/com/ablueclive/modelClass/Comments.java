package com.ablueclive.modelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Comments implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("reply_comments")
    @Expose
    private ArrayList<Reply_comments> reply_comments;

    @SerializedName("user_name")
    @Expose
    private String user_name;

    @SerializedName("comment_timestamp")
    @Expose
    private String comment_timestamp;

    @SerializedName("comment_id")
    @Expose
    private String comment_id;


    @SerializedName("profile_image")
    @Expose
    private String profile_image;

    @SerializedName("user_comment")
    @Expose
    private String user_comment;

    @SerializedName("post_user_id")
    @Expose
    public String postId="";

    public Comments() {
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }


    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getUser_comment() {
        return user_comment;
    }


    @Override
    public String toString() {
        return "ClassPojo [user_id = " + user_id + ", reply_comments = " + reply_comments + ", user_name = " + user_name + ", comment_timestamp = " + comment_timestamp + ", comment_id = " + comment_id + ", user_comment = " + user_comment + "]";
    }
}