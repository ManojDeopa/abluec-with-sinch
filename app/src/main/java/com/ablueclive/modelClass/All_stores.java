package com.ablueclive.modelClass;

public class All_stores
{
    private String store_name;

    private String _id;

    private String store_image;

    public String getStore_name ()
    {
        return store_name;
    }

    public void setStore_name (String store_name)
    {
        this.store_name = store_name;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getStore_image ()
    {
        return store_image;
    }

    public void setStore_image (String store_image)
    {
        this.store_image = store_image;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [store_name = "+store_name+", _id = "+_id+", store_image = "+store_image+"]";
    }
}