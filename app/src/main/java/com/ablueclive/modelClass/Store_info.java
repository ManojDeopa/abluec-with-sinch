package com.ablueclive.modelClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Store_info implements Parcelable {
    private String store_latitude;

    public ArrayList<ImageStoreData> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageStoreData> images) {
        this.images = images;
    }

    private ArrayList<ImageStoreData> images= new ArrayList<>();



    private String store_longitude;

    private String store_email;

    private String store_city;

    public String getStore_logo() {
        return store_logo;
    }

    public void setStore_logo(String store_logo) {
        this.store_logo = store_logo;
    }

    private String store_state;

    private String store_logo;

    public String getStore_city() {
        return store_city;
    }

    public void setStore_city(String store_city) {
        this.store_city = store_city;
    }

    public String getStore_state() {
        return store_state;
    }

    public void setStore_state(String store_state) {
        this.store_state = store_state;
    }

    private String store_name;

    private String store_address;

    private String _id;

    private String store_description;

    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String store_image;
    private Loc loc;

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    private String isFav;

    public Loc getLoc() {
        return loc;
    }

    public void setLoc(Loc loc) {
        this.loc = loc;
    }

    public static Creator<Store_info> getCREATOR() {
        return CREATOR;
    }

    protected Store_info(Parcel in) {
        store_latitude = in.readString();
        store_longitude = in.readString();
        store_email = in.readString();
        store_name = in.readString();
        store_address = in.readString();
        _id = in.readString();
        store_description = in.readString();
        store_image = in.readString();
        store_phone = in.readString();
        store_logo = in.readString();
    }

    public static final Creator<Store_info> CREATOR = new Creator<Store_info>() {
        @Override
        public Store_info createFromParcel(Parcel in) {
            return new Store_info(in);
        }

        @Override
        public Store_info[] newArray(int size) {
            return new Store_info[size];
        }
    };

    public String getStore_phone() {
        return store_phone;
    }

    public void setStore_phone(String store_phone) {
        this.store_phone = store_phone;
    }

    private String store_phone;

    public String getStore_latitude() {
        return store_latitude;
    }

    public void setStore_latitude(String store_latitude) {
        this.store_latitude = store_latitude;
    }

    public String getStore_longitude() {
        return store_longitude;
    }

    public void setStore_longitude(String store_longitude) {
        this.store_longitude = store_longitude;
    }

    public String getStore_email() {
        return store_email;
    }

    public void setStore_email(String store_email) {
        this.store_email = store_email;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_address() {
        return store_address;
    }

    public void setStore_address(String store_address) {
        this.store_address = store_address;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStore_description() {
        return store_description;
    }

    public void setStore_description(String store_description) {
        this.store_description = store_description;
    }

    public String getStore_image() {
        return store_image;
    }

    public void setStore_image(String store_image) {
        this.store_image = store_image;
    }

    @Override
    public String toString() {
        return "ClassPojo [store_latitude = " + store_latitude + ", store_longitude = " + store_longitude + ", store_email = " + store_email + ", store_name = " + store_name + ", store_address = " + store_address + ", _id = " + _id + ", store_description = " + store_description + ", store_image = " + store_image + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_latitude);
        dest.writeString(store_longitude);
        dest.writeString(store_email);
        dest.writeString(store_name);
        dest.writeString(store_address);
        dest.writeString(_id);
        dest.writeString(store_description);
        dest.writeString(store_image);
        dest.writeString(store_phone);
        dest.writeString(store_logo);
    }
}
			
			