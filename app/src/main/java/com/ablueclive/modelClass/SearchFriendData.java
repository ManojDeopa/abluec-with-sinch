package com.ablueclive.modelClass;

public class SearchFriendData
{
    private String profile_image;

    private String name;

    private String checkstatus;

    private String _id;

    private String already_followed;

    public String getAlready_followed() {
        return already_followed;
    }

    public void setAlready_followed(String already_followed) {
        this.already_followed = already_followed;
    }

    public String getMutual_friends_count() {
        return mutual_friends_count;
    }

    public void setMutual_friends_count(String mutual_friends_count) {
        this.mutual_friends_count = mutual_friends_count;
    }

    private String mutual_friends_count;

    public String getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (String profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCheckstatus ()
    {
        return checkstatus;
    }

    public void setCheckstatus (String checkstatus)
    {
        this.checkstatus = checkstatus;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return name;
    }
}