package com.ablueclive.modelClass

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SharePostResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    class ResponseData {
        @SerializedName("data")
        @Expose
        var data: List<Datum>? = null
    }


    class Datum {
        @SerializedName("retweet_user_post")
        @Expose
        var shareUserPost: List<ShareUsersPost>? = null
    }


}