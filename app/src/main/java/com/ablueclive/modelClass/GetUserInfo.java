package com.ablueclive.modelClass;

public class GetUserInfo {

    private String debug_mode;

    private String thumb_profile_image;

    private String rating;

    private String _created_at;

    public String getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(String friendStatus) {
        this.friendStatus = friendStatus;
    }

    private String friendStatus;


    private String language;

    private String session_token;

    private String profile_image;

    private String verification_otp;

    private String pub_chn;

    private String _updated_at;

    private String email;

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }

    private String online_status;

    private String background_image;
    private String mobile;

    private String count;

    private String wallet_balance;

    private String isStoreCreated;

    private String country_code;

    private String ser_chn;


    private String name;

    private String _id;

    private String hash;

    private String status;

    public String getDebug_mode() {
        return debug_mode;
    }

    public void setDebug_mode(String debug_mode) {
        this.debug_mode = debug_mode;
    }

    public String getThumb_profile_image() {
        return thumb_profile_image;
    }

    public void setThumb_profile_image(String thumb_profile_image) {
        this.thumb_profile_image = thumb_profile_image;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String get_created_at() {
        return _created_at;
    }

    public void set_created_at(String _created_at) {
        this._created_at = _created_at;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSession_token() {
        return session_token;
    }

    public void setSession_token(String session_token) {
        this.session_token = session_token;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }

    public String getPub_chn() {
        return pub_chn;
    }

    public void setPub_chn(String pub_chn) {
        this.pub_chn = pub_chn;
    }

    public String get_updated_at() {
        return _updated_at;
    }

    public void set_updated_at(String _updated_at) {
        this._updated_at = _updated_at;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getOnline_status() {
        return online_status;
    }

    public void setOnline_status(String online_status) {
        this.online_status = online_status;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getWallet_balance() {
        return wallet_balance;
    }

    public void setWallet_balance(String wallet_balance) {
        this.wallet_balance = wallet_balance;
    }

    public String getIsStoreCreated() {
        return isStoreCreated;
    }

    public void setIsStoreCreated(String isStoreCreated) {
        this.isStoreCreated = isStoreCreated;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getSer_chn() {
        return ser_chn;
    }

    public void setSer_chn(String ser_chn) {
        this.ser_chn = ser_chn;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
			
			