package com.ablueclive.modelClass;

import java.util.ArrayList;

public class AllComments {
    private String profile_image;

    private String user_id;

    public ArrayList<Reply_comments> getReply_comments() {
        return reply_comments;
    }

    public void setReply_comments(ArrayList<Reply_comments> reply_comments) {
        this.reply_comments = reply_comments;
    }

    private ArrayList<Reply_comments> reply_comments;

    private String user_name;

    private String comment_timestamp;

    public String postId;

    private String comment_id;

    public String getPost_user_id() {
        return post_user_id;
    }

    public void setPost_user_id(String post_user_id) {
        this.post_user_id = post_user_id;
    }

    private String user_comment;

    private String post_user_id;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getComment_timestamp() {
        return comment_timestamp;
    }

    public void setComment_timestamp(String comment_timestamp) {
        this.comment_timestamp = comment_timestamp;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getUser_comment() {
        return user_comment;
    }

    public void setUser_comment(String user_comment) {
        this.user_comment = user_comment;
    }

    @Override
    public String toString() {
        return "ClassPojo [profile_image = " + profile_image + ", user_id = " + user_id + ", user_name = " + user_name + ", comment_timestamp = " + comment_timestamp + ", postId = " + postId + ", comment_id = " + comment_id + ", user_comment = " + user_comment + "]";
    }
}
			
			