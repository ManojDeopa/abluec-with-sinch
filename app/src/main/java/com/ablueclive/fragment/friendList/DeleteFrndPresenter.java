package com.ablueclive.fragment.friendList;

import java.util.HashMap;

public class DeleteFrndPresenter implements DeleteFrndContract.Presenter, DeleteFrndContract.Model.OnFinishedListener {
    private DeleteFrndContract.View delFrndView;
    private DeleteFrndContract.Model delFrndModel;

    public DeleteFrndPresenter(DeleteFrndContract.View delFrndView) {
        this.delFrndView = delFrndView;
        this.delFrndModel = new DeleteFrndModel();
    }


    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (delFrndView != null) {
            delFrndView.hideProgress();
            delFrndView.setDataDeleteFriend(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (delFrndView != null) {
            delFrndView.hideProgress();
            delFrndView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        delFrndView = null;
    }

    @Override
    public void requestDeleteFriend(HashMap<String, String> param) {
        if (delFrndView != null) {
            delFrndView.showProgress();
            delFrndModel.deleteFriend(this, param);
        }

    }
}
