package com.ablueclive.fragment.friendList;
import java.util.HashMap;

public interface ConfirmFriendContract
{
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void confirmFriend(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataConfirmFriend(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestConfirmFriend(HashMap<String, String> param);
    }
}
