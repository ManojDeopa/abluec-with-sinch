package com.ablueclive.fragment.friendList;
import java.util.HashMap;

public class ConfirmFriendPresenter implements ConfirmFriendContract.Presenter, ConfirmFriendContract.Model.OnFinishedListener {
    private ConfirmFriendContract.View conFrndView;
    private ConfirmFriendContract.Model conFrndModel;

    public ConfirmFriendPresenter(ConfirmFriendContract.View conFrndView) {
        this.conFrndView = conFrndView;
        this.conFrndModel = new ConfirmFriendModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (conFrndView != null) {
            conFrndView.hideProgress();
            conFrndView.setDataConfirmFriend(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (conFrndView != null) {
            conFrndView.hideProgress();
            conFrndView.onResponseFailure(t);
        }


    }

    @Override
    public void onDestroy() {
        conFrndView = null;
    }

    @Override
    public void requestConfirmFriend(HashMap<String, String> param) {
        if (conFrndView != null) {
            conFrndView.showProgress();
            conFrndModel.confirmFriend(this, param);
        }

    }
}
