package com.ablueclive.fragment.friendList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.adapter.FriendListAdapter;
import com.ablueclive.listener.onProductItemListener;
import com.ablueclive.listener.onStoreItemListener;
import com.ablueclive.modelClass.GetFriendListData;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendListFragment extends Fragment implements FriendListContract.View, onStoreItemListener, ConfirmFriendContract.View, onProductItemListener, DeleteFrndContract.View {
    private View view;
    private RecyclerView recycler_friend_list;
    private String session_token = "";
    private TextView text_no_item;
    private FriendListAdapter friendListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private FriendListPresenter friendListPresenter;
    private static final String TAG = "FriendListFragment";
    private ArrayList<GetFriendListData> getFriendListDataArrayList = new ArrayList<>();
    private int pageNo = 1, total_count;
    private boolean isLoading = false;
    private ConfirmFriendPresenter confirmFriendPresenter;
    private DeleteFrndPresenter deleteFrndPresenter;

    public FriendListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_friend_list, container, false);
        findView();
        if (isInternetPresent) {
            friendListParam(pageNo);
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), getContext());
        }
        return view;
    }

    // initialize object here...
    private void findView() {
        session_token = BMSPrefs.getString(getContext(), Constants.SESSION_TOKEN);
        connectionDetector = new ConnectionDetector(getContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        recycler_friend_list = view.findViewById(R.id.recycler_friend_list);
        text_no_item = view.findViewById(R.id.text_no_item);
        BMSPrefs.putString(getContext(), "status", "");
        setListAdapter();
        setListeners();
    }

    private void setListeners() {
        recycler_friend_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true;
                    total_count = totalItemCount;
                    if (isInternetPresent) {
                        friendListParam(pageNo);
                    } else {
                        CommonMethod.showToastlong(getString(R.string.internet_toast), getContext());

                    }

                }
            }
        });
    }


    //adapter
    private void setListAdapter() {
        // friendListAdapter = new FriendListAdapter(getContext(), getFriendListDataArrayList, this, this);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_friend_list.setLayoutManager(linearLayoutManager);
        recycler_friend_list.setAdapter(friendListAdapter);
    }

    // friend request param
    private void friendListParam(int pageNo) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("page", "" + pageNo);
        params.put("session_token", session_token);
        Log.d("friendListParam", params.toString());
        friendListPresenter = new FriendListPresenter(this);
        friendListPresenter.requestGetFriendList(params);
    }

    // friend request param
    private void confirmFriendParam(String confirm_friend_id) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("confirm_friend_id", confirm_friend_id);
        params.put("session_token", session_token);
        Log.d("conFrndParam", params.toString());
        confirmFriendPresenter = new ConfirmFriendPresenter(this);
        confirmFriendPresenter.requestConfirmFriend(params);
    }

    // delete request param
    private void delFriendParam(String delete_friend_id) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("delete_friend_id", delete_friend_id);
        params.put("session_token", session_token);
        Log.d("delFrndParam", params.toString());
        deleteFrndPresenter = new DeleteFrndPresenter(this);
        deleteFrndPresenter.requestDeleteFriend(params);
    }


    @Override
    public void showProgress() {
        ProgressD.Companion.show(getContext(), "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    // delete friend response
    @Override
    public void setDataDeleteFriend(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getContext());
        }
    }

    // confirm friend response
    @Override
    public void setDataConfirmFriend(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getContext());
        }
    }


    // get friend list response
    @Override
    public void setDataToViews(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            isLoading = false;
            getFriendListDataArrayList = response_data.getGetFriendListData();
            if (getFriendListDataArrayList.size() > 0) {
//                text_no_item.setVisibility(View.GONE);
//                recycler_friend_list.setVisibility(View.VISIBLE);
                friendListAdapter.addAll(getFriendListDataArrayList);
                pageNo++;
            } else {
                if (pageNo == 1) {
                    text_no_item.setVisibility(View.VISIBLE);
                } else {
                    text_no_item.setVisibility(View.GONE);
                    CommonMethod.showToastShort(getString(R.string.no_more_data), getContext());
                }


            }

        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }


    // logout method
    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getContext());
        BMSPrefs.putString(getContext(), Constants._ID, "");
        BMSPrefs.putString(getContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getContext(), Constants.PROFILE_IMAGE, "");
        BMSPrefs.putString(getContext(), Constants.PROFILE_IMAGE_BACK, "");
        CommonMethod.callActivityFinish(getContext(), new Intent(getContext(), LoginActivity.class));
        getActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        getActivity().finish();
    }


    // adapter listener(add/confirm frnd button)
    @Override
    public void onStoreItemClick(String store_id, String user_name, int pos, String mobile) {
        BMSPrefs.putString(getContext(), "status", "");
        if (isInternetPresent) {
            confirmFriendParam(store_id);
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), getContext());
        }
    }


    // adapter listener(Delete frnd button)
    @Override
    public void onProductItemClick(String product_id) {
        BMSPrefs.putString(getContext(), "status", "");
        if (isInternetPresent) {
            delFriendParam(product_id);
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), getContext());
        }
    }
}
