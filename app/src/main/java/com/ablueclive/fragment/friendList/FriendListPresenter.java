package com.ablueclive.fragment.friendList;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class FriendListPresenter implements FriendListContract.Presenter, FriendListContract.Model.OnFinishedListener {
    private FriendListContract.View frndListView;
    private FriendListContract.Model frndListModel;

    public FriendListPresenter(FriendListContract.View frndListView) {
        this.frndListView = frndListView;
        this.frndListModel = new FriendListModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (frndListView != null) {
            frndListView.hideProgress();
            frndListView.setDataToViews(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (frndListView != null) {
            frndListView.hideProgress();
            frndListView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        frndListView = null;
    }

    @Override
    public void requestGetFriendList(HashMap<String, String> param) {
        if (frndListView != null) {
            frndListView.showProgress();
            frndListModel.getFriendList(this, param);
        }

    }
}
