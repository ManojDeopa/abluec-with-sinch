package com.ablueclive.fragment.allFriendListChat;

import java.util.HashMap;

public class ReadUnreadPresenter implements ReadUnreadContract.Presenter, ReadUnreadContract.Model.OnFinishedListener {
    private ReadUnreadContract.View readStatusView;
    private ReadUnreadContract.Model readStatusModel;

    public ReadUnreadPresenter(ReadUnreadContract.View readStatusView) {
        this.readStatusView = readStatusView;
        this.readStatusModel = new ReadUnreadModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (readStatusView != null) {
            //readStatusView.hideProgress();
            readStatusView.setReadStatus(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        readStatusView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        readStatusView = null;
    }

    @Override
    public void requestReadStatus(HashMap<String, String> param) {
        readStatusModel.readStatus(this, param);
    }
}
