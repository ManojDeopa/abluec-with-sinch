package com.ablueclive.fragment.allFriendListChat

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.allFriendList.AllFrndContract
import com.ablueclive.activity.allFriendList.AllFrndPresenter
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.adapter.MessagingListAdapter
import com.ablueclive.fcmClass.MyFirebaseMessagingService
import com.ablueclive.modelClass.GetAllFriendsList
import com.ablueclive.modelClass.Response_data
import com.ablueclive.network.SeeAllFriend
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.callActivityFinish
import com.ablueclive.utils.CommonMethod.showToastShort
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.fragment_all_friend_list.*
import kotlinx.android.synthetic.main.progress_horizontal.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class MessagingListFragment : Fragment(), AllFrndContract.View {


    private var session_token = ""
    private lateinit var messagingListAdapter: MessagingListAdapter
    private var isInternetPresent = false
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var pageNo = 1
    private var getAllFriendsListArrayList = ArrayList<GetAllFriendsList>()
    var TAG = MessagingListFragment::class.java.simpleName

    companion object {
        var CHAT_DATABASE = "FriendChat"
        fun newInstance(bundle: Bundle): MessagingListFragment {
            val fragment = MessagingListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_all_friend_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
    }

    // initialize object
    private fun findView() {
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        val connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        setFriendAdapter()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction(Constants.NOTIFICATION_ACTION_FILTER)
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(receiver, filter)
        callMessagingList()
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(receiver)
    }

    private val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action != null && action == Constants.NOTIFICATION_ACTION_FILTER) {
                val type = intent.getStringExtra("type")
                if (type == MyFirebaseMessagingService.STATUS_CHAT_MESSAGE) {
                    callMessagingList()
                }
            }
        }
    }

    private fun callMessagingList() {
        if (isInternetPresent) {
            messagingListAdapter.clearAll()
            requestAllFrndList(1)
        } else {
            showToastShort(getString(R.string.internet_toast), context)
        }
    }

    private fun setListeners() {
        recycler_all_friend.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (getAllFriendsListArrayList.size >= 10) {
                    val visibleItemCount = linearLayoutManager.childCount
                    val totalItemCount = linearLayoutManager.itemCount
                    val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                    if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                        isLoading = true
                        requestAllFrndList(pageNo)
                    }
                }
            }
        })
        ivAddChat.setOnClickListener { v: View? ->
            callActivity(requireActivity(), Intent(requireActivity(), SeeAllFriend::class.java))
        }
    }

    //set friend adapter
    private fun setFriendAdapter() {
        messagingListAdapter = MessagingListAdapter(requireContext(), getAllFriendsListArrayList)
        recycler_all_friend.layoutManager = linearLayoutManager
        recycler_all_friend.adapter = messagingListAdapter
    }

    // allFrnd api param
    private fun requestAllFrndList(pageNo: Int) {
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["page"] = "" + pageNo
        params["type"] = "chat"
        Log.d("requestAllFrndList : ", params.toString())
        val allFrndPresenter = AllFrndPresenter(this)
        allFrndPresenter.requestAllFrnd(params)
    }

    override fun showProgress() {
        if (pbHeader != null)
            pbHeader.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if (pbHeader != null)
            pbHeader.visibility = View.GONE
    }

    override fun setDataToRecyclerView(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            if (response_status.equals("1", ignoreCase = true)) {
                isLoading = false
                getAllFriendsListArrayList = response_data.getAllFriendsList
                if (getAllFriendsListArrayList.size > 0) {
                    text_no_item.visibility = View.GONE
                    recycler_all_friend.visibility = View.VISIBLE
                } else {
                    text_no_item.visibility = View.VISIBLE
                    recycler_all_friend.visibility = View.GONE
                }
                if (getAllFriendsListArrayList.size > 0) {
                    messagingListAdapter.addAll(getAllFriendsListArrayList)
                    pageNo++
                }
            } else if (response_invalid.equals("1", ignoreCase = true)) {
                logout(response_msg)
            } else {
                showToastShort(response_msg, context)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
       println(throwable)
    }

    // logout method
    private fun logout(response_msg: String) {
        showToastShort(response_msg, context)
        BMSPrefs.putString(context, Constants._ID, "")
        BMSPrefs.putString(context, Constants.isStoreCreated, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE_BACK, "")
        callActivityFinish(requireActivity(), Intent(context, LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }


}