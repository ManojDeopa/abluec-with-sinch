package com.ablueclive.fragment.allFriendListChat;

import java.util.HashMap;

public interface ReadUnreadContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void readStatus(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {
//        void showProgress();
//
//        void hideProgress();

        void setReadStatus(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestReadStatus(HashMap<String, String> param);

    }
}
