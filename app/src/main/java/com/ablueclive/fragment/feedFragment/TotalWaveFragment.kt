package com.ablueclive.fragment.feedFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.FeedbackFragment
import com.ablueclive.modelClass.ShareUsersPost
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.util.*

class TotalWaveFragment() : Fragment() {


    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        lateinit var waveList: ArrayList<ShareUsersPost>
        fun newInstance(bundle: Bundle): TotalWaveFragment {
            val fragment = TotalWaveFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {
        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet

        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter=TotalWaveAdapter(waveList)
        }

    }

}