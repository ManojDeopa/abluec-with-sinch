package com.ablueclive.fragment.feedFragment

import AppUtils
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.adddPost.AddPostActvity
import com.ablueclive.activity.cheersList.CheersListActivity
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.dialog.ReportPostDialog
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.mainActivity.MainActivity.Companion.mainActivity
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.activity.search_friend.AddFriendContract
import com.ablueclive.activity.search_friend.AddFriendPresenter
import com.ablueclive.activity.userProfile.DeletePostContract
import com.ablueclive.activity.userProfile.DeletePostPresenter
import com.ablueclive.activity.userProfile.PostLikeContract
import com.ablueclive.activity.userProfile.PostLikePresenter
import com.ablueclive.adapter.FriendListAdapter
import com.ablueclive.adapter.GetAllFeedAdapter
import com.ablueclive.common.CommonResponse
import com.ablueclive.fcmClass.MyFirebaseMessagingService
import com.ablueclive.fragment.friendList.ConfirmFriendContract
import com.ablueclive.fragment.friendList.ConfirmFriendPresenter
import com.ablueclive.fragment.friendList.DeleteFrndContract
import com.ablueclive.fragment.friendList.DeleteFrndPresenter
import com.ablueclive.global.response.CombineFriendResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.listener.*
import com.ablueclive.modelClass.*
import com.ablueclive.network.PeopleYouKnowAdapter
import com.ablueclive.network.`interface`.PeopleUMayKnowListener
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.Constants.showLoader
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_feed.*
import java.util.*


class FeedFragment : Fragment(),
        AllFeedContract.View,
        onReportPostListener,
        onDeletePostListener,
        DeletePostContract.View,
        onLikePostListener,
        PostLikeContract.View,
        onCommentPostListener,
        View.OnClickListener,
        onProductItemListener,
        onSharePostListener,
        CombinedFriendContract.View,
        onFriendProfileListener,
        onEditPostListener,
        onClickForDetailsListener,
        AddFriendContract.View,
        CustomRecyclerViewItemClick,
        ConfirmFriendContract.View,
        DeleteFrndContract.View,
        OnSeeMoreClickListener,
        PeopleUMayKnowListener,
        FCMEventListener {


    private var version = 0
    private var fcmStatus = ""
    lateinit var getAllFeedAdapter: GetAllFeedAdapter

    private var session_token = ""
    private var time_zone = ""
    private var user_id: String = ""
    private var user_name = ""
    private var device_token = ""
    private var share_count = ""
    private var post_ids = ""
    private var is_first = ""

    private var pageNoFeedList = 1
    private var item_position = 0
    private var isFeedLoading = false
    private var isInternetPresent = false


    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var connectionDetector: ConnectionDetector

    private val postDataArrayList = ArrayList<PostData>()
    private var peopleUKnowArrayList = ArrayList<PeopleUknow>()
    private var getFriendReqDataList = ArrayList<GetFriendListData>()
    private var requestToFollowList = ArrayList<GetFriendListData>()

    lateinit var friendReqListAdapter: FriendListAdapter


    companion object {
        val TAG = "FeedFragment"
        var shouldDataRefresh = false

        fun newInstance(bundle: Bundle): FeedFragment {
            val fragment = FeedFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
    }


    private fun findView() {
        shouldDataRefresh = false
        device_token = BMSPrefs.getString(requireContext(), Constants.DEVICE_TOKEN)
        is_first = BMSPrefs.getString(requireContext(), Constants.IS_FIRST)
        user_name = BMSPrefs.getString(requireContext(), Constants.USER_NAME)
        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        user_id = BMSPrefs.getString(requireContext(), Constants._ID)

        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet
        time_zone = TimeZone.getDefault().id

        img_add_post.setOnClickListener(this)
        mainActivity.fcmListener = this

        val appInfo = requireActivity().packageManager.getPackageInfo(requireActivity().packageName, 0)
        version = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            appInfo.longVersionCode.toInt()
        } else {
            appInfo.versionCode
        }

        simpleSwipeRefreshLayout.setOnRefreshListener {
            // shouldDataRefresh = true
            // onResume()
            mainActivity.bottomLayout.selectedItemId = R.id.nav_home
        }

        setUserFeedAdapter()

        setListeners()

        callInitialApis()
    }

    override fun onResume() {
        super.onResume()
        loadProfileImage()
        if (shouldDataRefresh) {
            showLoader = true
            shouldDataRefresh = false
            // callInitialApis()
            mainActivity.bottomLayout.selectedItemId = R.id.nav_home
        }
    }

    private fun callInitialApis() {
        if (isInternetPresent) {
            postDataArrayList.clear()
            pageNoFeedList = 1
            getAllFeedRequest()
            getCombinedFriendRecord()
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    private fun loadProfileImage() {
        val profileImage = Constants.PROFILE_IMAGE_URL + BMSPrefs.getString(requireActivity(), Constants.PROFILE_IMAGE)
        CommonMethod.loadImageCircle(profileImage, mainActivity.ivUserProfile)
    }

    @SuppressLint("NewApi")
    private fun setListeners() {

        scrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->

            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (scrollY > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isFeedLoading) {
                    isFeedLoading = true
                    showLoader = false
                    getAllFeedRequest()
                }
            }
        })
    }

    // all feed api response
    override fun setDataToRecyclerView(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {

        try {
            if (activity != null) {
                simpleSwipeRefreshLayout.isRefreshing = false
                if (pbHeader != null) pbHeader.visibility = View.GONE
                when {
                    response_status == "1" -> {

                        if (fcmStatus == MyFirebaseMessagingService.STATUS_CHAT_MESSAGE) {
                            mainActivity.unread_msg_count = response_data.unread_msg_count
                            mainActivity.updateChatCount()
                            return
                        }

                        if (response_data.version_code_android.isNotEmpty()) {
                            val storeVersion: Float = response_data.version_code_android.toFloat()
                            Log.e(TAG, "version: ${version.toFloat()} , storeVersion: $storeVersion")
                            if (version.toFloat() < storeVersion) {
                                showVersionAlert()
                            }
                        }

                        mainActivity.unread_msg_count = response_data.unread_msg_count
                        mainActivity.updateChatCount()

                        if (pageNoFeedList == 1) {
                            ProfileActivity.isAvailable = response_data.availability_status == 1
                            mainActivity.updateToggle()
                        }

                        val postDataList = response_data.postData
                        if (postDataList != null && postDataList.size > 0) {
                            postDataList.forEach {
                                postDataArrayList.add(it)
                            }
                            if (pageNoFeedList == 1) {
                                getAllFeedAdapter.notifyDataSetChanged()
                            } else {
                                getAllFeedAdapter.notifyItemRangeInserted(getAllFeedAdapter.itemCount, postDataArrayList.size)
                            }
                            pageNoFeedList++
                            isFeedLoading = false

                        } else {
                            showEmptyView()
                        }
                    }
                    response_invalid == "1" -> {
                        logout(response_msg)
                    }
                    else -> {
                        CommonMethod.showToastShort(response_msg, requireContext())
                    }
                }


                if (BMSPrefs.getString(requireContext(), "IS_SIGN_UP") == "1") {
                    BMSPrefs.putString(requireContext(), "IS_SIGN_UP", "0")
                    /* Handler(Looper.getMainLooper()).postDelayed({
                         if (activity != null) {
                             noPostLayout.visibility = View.VISIBLE
                         }
                     }, 5000)*/
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    private fun showEmptyView() {
        if (requestToFollowList.isEmpty() && getFriendReqDataList.isEmpty() && postDataArrayList.isEmpty()) {
            noPostLayout.visibility = View.VISIBLE
        } else {
            noPostLayout.visibility = View.GONE
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }


    private fun getAllFeedRequest() {
        if (pbHeader != null) {
            pbHeader.visibility = View.VISIBLE
        }
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["page"] = pageNoFeedList.toString()
        params["post_time_zone"] = time_zone
        params["android_version"] = version.toString()
        Log.d("getAllFeedRequest", params.toString())
        val allFeedPresenter = AllFeedPresenter(this)
        allFeedPresenter.requestAllUserFeed(params)
    }

    private fun getCombinedFriendRecord() {
        val gpsTracker = GPSTracker(requireContext())
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["lat"] = gpsTracker.latitude.toString()
        params["long"] = gpsTracker.longitude.toString()
        Log.d("getCombinedFriendRecord", params.toString())
        val peopleKnowPresenter = CombinedFriendPresenter(this)
        peopleKnowPresenter.requestCombinedFriendRecord(params)
    }

    private fun deletePostRequest(post_id: String) {
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["post_id"] = post_id
        Log.d("deletePostRequest", params.toString())
        val deletePostPresenter = DeletePostPresenter(this)
        deletePostPresenter.requestDeletePost(params)
    }

    private fun likePostRequest(postId: String, item_position: Int) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["postId"] = postId
        params["user_name"] = user_name
        Log.d("likePostRequest", params.toString())
        val postLikePresenter = PostLikePresenter(this)
        postLikePresenter.requestPostLike(params)
    }

    private fun confirmFriendParam(confirm_friend_id: String) {
        val params = HashMap<String, String>()
        params["confirm_friend_id"] = confirm_friend_id
        params["session_token"] = session_token
        Log.d("confirmFriendParam", params.toString())
        val confirmFriendPresenter = ConfirmFriendPresenter(this)
        confirmFriendPresenter.requestConfirmFriend(params)
    }

    private fun delFriendParam(delete_friend_id: String) {
        val params = HashMap<String, String>()
        params["delete_friend_id"] = delete_friend_id
        params["session_token"] = session_token
        Log.d("delFriendParam", params.toString())
        val deleteFrndPresenter = DeleteFrndPresenter(this)
        deleteFrndPresenter.requestDeleteFriend(params)
    }

    private fun sendFriendRequestParam(friend_id: String) {
        val params = HashMap<String, String>()
        params["add_friend_id"] = friend_id
        params["session_token"] = session_token
        Log.d("sendFriendRequestParam", params.toString())
        val addFriendPresenter = AddFriendPresenter(this)
        addFriendPresenter.requestAddFriend(params)
    }

    //set All feed adapter
    private fun setUserFeedAdapter() {
        getAllFeedAdapter = GetAllFeedAdapter(requireActivity(), postDataArrayList, this, user_id,
                this, this, this,
                this, this, this)
        linearLayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)

        recycler_all_feed.apply {
            layoutManager = linearLayoutManager
            adapter = getAllFeedAdapter
        }
    }

    private fun setPeopleAdapter() {
        // NetworkPeopleYouKnowAdapter
        val peopleKnowAdapter = PeopleYouKnowAdapter(2, peopleUKnowArrayList, this)
        recycler_people.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            adapter = peopleKnowAdapter
        }
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_add_post -> {
                mainActivity.bottomLayout.selectedItemId = R.id.nav_add_post
            }
        }
    }

    override fun showProgress() {

        if (showLoader) {
            show(requireActivity(), "")
        }
        showLoader = true
        fcmStatus = ""

    }

    override fun hideProgress() {
        hide()
    }

    override fun setDataDeleteFriend(response_status: String?, response_msg: String?, response_invalid: String?) {
        CommonMethod.showAlert(requireContext(), response_msg)
    }


    override fun setDataConfirmFriend(response_status: String?, response_msg: String?, response_invalid: String?) {
        // CommonMethod.showAlert(requireContext(), response_msg)
    }


    override fun addFreindResponse(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status == "1" -> {
                    // CommonMethod.showAlert(requireActivity(), response_msg)
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireActivity())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun combinedFriendResponse(response: CombineFriendResponse) {
        try {
            simpleSwipeRefreshLayout.isRefreshing = false
            if (response.response_status == 1) {
                peopleUKnowArrayList = response.response_data?.getCombineFriendRecord?.PeopleUknow as ArrayList<PeopleUknow>
                if (peopleUKnowArrayList.size > 0) {
                    text_know?.visibility = View.VISIBLE
                    setPeopleAdapter()
                } else {
                    text_know?.visibility = View.GONE
                }

                getFriendReqDataList = response.response_data?.getCombineFriendRecord?.getFriendListData as ArrayList<GetFriendListData>
                if (getFriendReqDataList.size > 0) {
                    setFriendListAdapter(true)
                } else {
                    tvInvitedText.visibility = View.GONE
                }

                requestToFollowList = response.response_data?.getCombineFriendRecord?.RequestToFollowList as ArrayList<GetFriendListData>
                if (requestToFollowList.size > 0) {
                    setFriendListAdapter(false)
                } else {
                    tvFollowedText.visibility = View.GONE
                }

            } else if (response.response_invalid == 1) {
                logout(response.response_msg!!)
            } else {
                CommonMethod.showToastShort(response.response_msg, requireContext())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setFriendListAdapter(isAddFreind: Boolean) {
        when (isAddFreind) {
            true -> {

                tvInvitedText.visibility = View.VISIBLE
                tvInvitedText.text = getFriendReqDataList.size.toString() + " " + getString(R.string.invited_you_to_connect)
                val friendReqListAdapter = requireContext().let { FriendListAdapter(it, getFriendReqDataList, this, isAddFreind, tvInvitedText) }
                recycler_friend_request.apply {
                    layoutManager = LinearLayoutManager(requireActivity())
                    adapter = friendReqListAdapter
                }
            }

            false -> {
                tvFollowedText.visibility = View.VISIBLE
                tvFollowedText.text = requestToFollowList.size.toString() + " " + getString(R.string.followed_you_to_connect)
                friendReqListAdapter = requireContext().let { FriendListAdapter(it, requestToFollowList, this, isAddFreind, tvFollowedText) }
                recycler_followed_request.apply {
                    layoutManager = LinearLayoutManager(requireActivity())
                    adapter = friendReqListAdapter
                }
            }

        }

    }

    //post like
    override fun setPostLike(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            when {
                response_status == "1" -> {
                    // CommonMethod.showToastShort(response_msg, requireContext())
                    getAllFeedAdapter.setLikeCount(null, response_data.status, response_data.totalCountLike)
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // delete post
    override fun setDeletePost(response_status: String, response_msg: String, response_invalid: String) {
        try {
            if (response_status == "1") {
                CommonMethod.showToastShort(response_msg, requireContext())
                if (postDataArrayList.size == 0) {
                    showEmptyView()
                }
            } else if (response_invalid == "1") {
                logout(response_msg)
            } else {
                CommonMethod.showToastShort(response_msg, requireContext())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, requireContext())
        BMSPrefs.putString(requireContext(), Constants._ID, "")
        BMSPrefs.putString(requireContext(), Constants.isStoreCreated, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(requireContext(), Intent(requireContext(), LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    // report post
    override fun onReportPostClick(post_id: String, pos: Int) {
        val reportPostDialog = ReportPostDialog(requireActivity(), post_id, postDataArrayList, pos, getAllFeedAdapter)
        reportPostDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    // delete post
    override fun onDelPostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            deletePostRequest(post_id)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // post like method
    override fun onLikePostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            item_position = position
            likePostRequest(post_id, item_position)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // get all cmnt
    override fun onCmntPostClick(post_id: String, pos: Int) {
        shouldDataRefresh = true
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, post_id)
        callActivity(requireActivity(), Intent(requireActivity(), CommentActivity::class.java))
    }

    override fun onProductItemClick(feed_id: String) {
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, feed_id)
        callActivity(requireActivity(), Intent(requireActivity(), CheersListActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    // share post
    override fun onSharePostClick(post_id: String, pos: Int, type: String) {
        item_position = pos
        sharePost(post_id, type)
    }


    override fun onFrndProfileClick(friend_id: String, position: Int) {
        BMSPrefs.putString(requireActivity(), Constants.FRIEND_ID, friend_id)
        callActivity(requireActivity(), Intent(requireActivity(), FriendProfileActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onAddFriendClick(friend_id: String, position: Int) {

        if (position == 0) {
            cancelRequest(friend_id)
        }

        if (position == 1) {
            sendFriendRequestParam(friend_id)
        }
    }


    private fun cancelRequest(id: Any?) {
        show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["un_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.UnFriendRequest(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        hide()
                        try {
                            if (response.response_status == "1") {
                                CommonMethod.showToastShort("Request Cancelled Successfully!", requireContext())
                            } else {
                                CommonMethod.showToastShort(response.response_msg, requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun sharePost(post_id: String, type: String) {
        // show(requireActivity(), "")
        val hashMap = HashMap<String, String>()
        hashMap["post_id"] = post_id
        hashMap["session_token"] = session_token
        hashMap["post_time_zone"] = TimeZone.getDefault().id
        Log.d("sharePost", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)


        var apiCall = apiInterface.reTweetPost(hashMap)
        if (type == getString(R.string.un_do_wave)) {
            apiCall = apiInterface.deleteReTweetPost(hashMap)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SharePostResponse?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(response: SharePostResponse) {
                        // hide()
                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.data
                                if (!list.isNullOrEmpty()) {
                                    val shareUserList = list[0].shareUserPost
                                    if (!shareUserList.isNullOrEmpty()) {
                                        val newList = arrayListOf<ShareUsersPost>()
                                        shareUserList.forEach {
                                            newList.add(it)
                                        }
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = newList
                                    } else {
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = null
                                    }
                                }

                                getAllFeedAdapter.postDataArrayList[item_position].isRetweeted = type != getString(R.string.un_do_wave)
                                getAllFeedAdapter.notifyItemChanged(item_position)
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireContext())
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        // hide()
                        Log.d(TAG, e.message!!)
                    }

                    override fun onComplete() {}

                })
    }

    // edit post
    override fun onEditPostClick(post_id: String, position: Int) {
        AddPostActvity.data = postDataArrayList[position]
        val intent = Intent(requireActivity(), AddPostActvity::class.java)
        startActivity(intent)
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onClickForDetails(postData: PostData) {

    }


    override fun onSeeMoreClick(text: String, position: Int) {

        /* try {
             if (text == "collapse") {
                 val y = recycler_all_feed.getChildAt(position).y.toInt()
                 scrollView.post {
                     scrollView.fling(0)
                     scrollView.smoothScrollTo(0, y)
                 }
             }
         } catch (e: Exception) {
             e.printStackTrace()
         }*/
    }

    override fun onRecyclerItemClick(isTrue: Boolean, text: String, position: Int) {
        try {
            val separated = text.split(":".toRegex()).toTypedArray()
            when (isTrue) {
                true -> {
                    val id = separated[1]
                    if (text == separated[0]) {
                        delFriendParam(id)
                    } else {
                        confirmFriendParam(id)
                    }

                }
                false -> {
                    val id = separated[1]
                    if (text == separated[0]) {
                        followUnFollowApi(false, id, 0)
                    } else {
                        followUnFollowApi(true, id, 0)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun followUnFollowApi(isAcceptFolow: Boolean, userId: String, from: Int) {

        if (!connectionDetector.isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = userId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.followAccepted(param)
        if (!isAcceptFolow) {
            apiCall = apiInterface.followRejected(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            friendReqListAdapter.notifyDataSetChanged()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun followUser(friendID: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["session_token"] = session_token
        param["to_user_id"] = friendID


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        val apiCall = apiInterface.follow(param)

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                /*BMSPrefs.putString(requireContext(), Constants.FRIEND_ID, friendID)
                                callActivity(requireContext(), Intent(requireContext(), FriendProfileActivity::class.java))*/
                                callInitialApis()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    override fun addFriend(get: PeopleUknow, pos: Int) {
        followUser(get._id)
    }

    override fun cancelRequest(get: PeopleUknow, pos: Int) {
        // followUnFollowApi(false,get._id,1)
    }


    private fun showVersionAlert() {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.version_dialog)
        dialog.show()
        dialog.findViewById<View>(R.id.tvDone).setOnClickListener { v: View? ->
            dialog.dismiss()
            AppUtils.callBrowserIntent(requireContext(), Constants.PLAY_STORE_LINK + requireActivity().packageName)
            requireActivity().finishAffinity()
        }
    }

    override fun onDataReceived(type: String, str: String, int: Int) {
        fcmStatus = type
        showLoader = false
        getAllFeedRequest()
    }


}