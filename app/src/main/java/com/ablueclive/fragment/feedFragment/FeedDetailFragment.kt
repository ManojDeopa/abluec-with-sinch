package com.ablueclive.fragment.feedFragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.adddPost.AddPostActvity
import com.ablueclive.activity.cheersList.CheersListActivity
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.dialog.ReportPostDialog
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.userProfile.DeletePostContract
import com.ablueclive.activity.userProfile.DeletePostPresenter
import com.ablueclive.activity.userProfile.PostLikeContract
import com.ablueclive.activity.userProfile.PostLikePresenter
import com.ablueclive.adapter.FeedDetailAdapter
import com.ablueclive.common.CommonContainerActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.listener.*
import com.ablueclive.modelClass.PostData
import com.ablueclive.modelClass.Response_data
import com.ablueclive.modelClass.SharePostResponse
import com.ablueclive.modelClass.ShareUsersPost
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.showToastlong
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import com.bumptech.glide.Glide
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_feed_detail.*
import java.util.*


class FeedDetailFragment(var shouldHide: Boolean) : Fragment(),
        AllFeedContract.View,
        onReportPostListener,
        onDeletePostListener,
        DeletePostContract.View,
        onLikePostListener,
        PostLikeContract.View,
        onCommentPostListener,
        View.OnClickListener,
        onProductItemListener,
        onSharePostListener,
        onFriendProfileListener,
        onEditPostListener,
        onClickForDetailsListener,
        CustomRecyclerViewItemClick,
        OnSeeMoreClickListener {


    private var session_token = ""
    private var time_zone = ""
    private var user_id: String = ""
    private var user_name = ""
    private var device_token = ""
    private var is_first = ""
    private var item_position = 0
    private var isInternetPresent = false
    private var post_id = ""


    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var connectionDetector: ConnectionDetector

    private val postDataArrayList = ArrayList<PostData>()
    lateinit var getAllFeedAdapter: FeedDetailAdapter


    companion object {
        val TAG = "FeedDetailFragment"
        var shouldDataRefresh = false
        fun newInstance(): FeedDetailFragment {
            return FeedDetailFragment(true)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feed_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
    }

    override fun onResume() {
        super.onResume()
        postDetails()
    }


    private fun findView() {
        shouldDataRefresh = false
        device_token = BMSPrefs.getString(requireContext(), Constants.DEVICE_TOKEN)
        is_first = BMSPrefs.getString(requireContext(), Constants.IS_FIRST)
        user_name = BMSPrefs.getString(requireContext(), Constants.USER_NAME)
        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        user_id = BMSPrefs.getString(requireContext(), Constants._ID)
        post_id = BMSPrefs.getString(requireContext(), Constants.FEED_POST_ID)

        connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet
        time_zone = TimeZone.getDefault().id

        rootLayout.requestFocus()

        if (shouldHide) {
            if (CommonContainerActivity.feedDetailTitle.isNotEmpty()) {
                Glide.with(requireActivity().applicationContext)
                        .load(Constants.PROFILE_IMAGE_URL + BMSPrefs.getString(requireActivity(), Constants.PROFILE_IMAGE))
                        .skipMemoryCache(true)
                        .error(R.drawable.user_profile)
                        .into(ivImage)

                edtLayout.visibility = View.VISIBLE
                btnPost.visibility = View.VISIBLE
                btnPost.setOnClickListener {
                    wavePost()
                }
            }
        }

    }


    // all feed api response
    override fun setDataToRecyclerView(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
    }


    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }


    private fun deletePostRequest(post_id: String) {
        val params = HashMap<String, String>()
        params["session_token"] = session_token
        params["post_id"] = post_id
        Log.d("deletePostRequest", params.toString())
        val deletePostPresenter = DeletePostPresenter(this)
        deletePostPresenter.requestDeletePost(params)
    }

    private fun likePostRequest(postId: String, item_position: Int) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["postId"] = postId
        params["user_name"] = user_name
        Log.d("likePostRequest", params.toString())
        val postLikePresenter = PostLikePresenter(this)
        postLikePresenter.requestPostLike(params)
    }


    private fun postDetails() {

        if (!isInternetPresent) {
            showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["post_time_zone"] = time_zone
        param["post_id"] = post_id


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.postDetails(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.postData
                                if (!list.isNullOrEmpty()) {
                                    postDataArrayList.clear()
                                    postDataArrayList.addAll(list)
                                    setUserFeedAdapter()
                                } else {
                                    CommonMethod.showToastShort(getString(R.string.no_data_found), requireActivity())
                                    requireActivity().finish()
                                }
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireActivity())
                                requireActivity().finish()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

    //set All feed adapter
    private fun setUserFeedAdapter() {

        getAllFeedAdapter = FeedDetailAdapter(requireActivity(), postDataArrayList, this, user_id,
                this, this, this, this,
                this, this, shouldHide)
        linearLayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)

        recycler_all_feed.apply {
            layoutManager = linearLayoutManager
            adapter = getAllFeedAdapter
        }

    }


    override fun onClick(v: View) {
    }

    override fun showProgress() {

        if (Constants.showLoader) {
            show(requireActivity(), "")
        }
        Constants.showLoader = true

    }

    override fun hideProgress() {
        hide()
    }


    //post like
    override fun setPostLike(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            when {
                response_status == "1" -> {
                    FeedFragment.shouldDataRefresh = true
                    getAllFeedAdapter.setLikeCount(null, response_data.status, response_data.totalCountLike)
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // delete post
    override fun setDeletePost(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status == "1" -> {
                    FeedFragment.shouldDataRefresh = true
                    CommonMethod.showToastShort(response_msg, requireContext())
                    requireActivity().finish()
                }
                response_invalid == "1" -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, requireContext())
        BMSPrefs.putString(requireContext(), Constants._ID, "")
        BMSPrefs.putString(requireContext(), Constants.isStoreCreated, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(requireContext(), Intent(requireContext(), LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    // report post
    override fun onReportPostClick(post_id: String, pos: Int) {
        val reportPostDialog = ReportPostDialog(requireActivity(), post_id, postDataArrayList, pos)
        reportPostDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    // delete post
    override fun onDelPostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            deletePostRequest(post_id)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // post like method
    override fun onLikePostClick(post_id: String, position: Int) {
        if (isInternetPresent) {
            item_position = position
            likePostRequest(post_id, item_position)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // get all cmnt
    override fun onCmntPostClick(post_id: String, pos: Int) {
        shouldDataRefresh = true
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, post_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), CommentActivity::class.java))
    }

    override fun onProductItemClick(feed_id: String) {
        BMSPrefs.putString(requireActivity(), Constants.FEED_POST_ID, feed_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), CheersListActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    // share post
    override fun onSharePostClick(post_id: String, pos: Int, type: String) {
        item_position = pos
        sharePost(post_id, type)
    }


    override fun onFrndProfileClick(friend_id: String, position: Int) {
        BMSPrefs.putString(requireActivity(), Constants.FRIEND_ID, friend_id)
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), FriendProfileActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onAddFriendClick(friend_id: String, position: Int) {
    }


    private fun sharePost(post_id: String, type: String) {
        show(requireActivity(), "")
        val hashMap = HashMap<String, String>()
        hashMap["post_id"] = post_id
        hashMap["session_token"] = session_token
        hashMap["post_time_zone"] = time_zone
        Log.d("sharePost", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.reTweetPost(hashMap)
        if (type == getString(R.string.un_do_wave)) {
            apiCall = apiInterface.deleteReTweetPost(hashMap)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SharePostResponse?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(response: SharePostResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.data
                                if (!list.isNullOrEmpty()) {

                                    val shareUserList = list[0].shareUserPost
                                    if (!shareUserList.isNullOrEmpty()) {
                                        val newList = arrayListOf<ShareUsersPost>()
                                        shareUserList.forEach {
                                            newList.add(it)
                                        }
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = newList
                                    } else {
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = null
                                    }
                                }
                                getAllFeedAdapter.postDataArrayList[item_position].isRetweeted = type != getString(R.string.un_do_wave)
                                getAllFeedAdapter.notifyItemChanged(item_position)
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        Log.d(TAG, "--" + e.message!!)
                    }

                    override fun onComplete() {
                    }

                })
    }

    // edit post
    override fun onEditPostClick(post_id: String, position: Int) {
        AddPostActvity.data = postDataArrayList[position]
        val intent = Intent(requireActivity(), AddPostActvity::class.java)
        startActivity(intent)
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onClickForDetails(postData: PostData) {

    }


    override fun onRecyclerItemClick(isTrue: Boolean, text: String, position: Int) {

    }


    override fun onSeeMoreClick(text: String, position: Int) {

    }


    private fun wavePost() {
        show(requireActivity(), "")
        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["post_id"] = post_id
        param["post_content"] = etMessage.text.toString().trim()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.reTweetPost(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SharePostResponse> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onNext(response: SharePostResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                requireActivity().finish()
                                FeedFragment.shouldDataRefresh = true
                            } else {
                                showToastlong(response.responseMsg, requireActivity())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        Log.d(TAG, "--" + e.message)
                    }

                    override fun onComplete() {}
                })
    }


}