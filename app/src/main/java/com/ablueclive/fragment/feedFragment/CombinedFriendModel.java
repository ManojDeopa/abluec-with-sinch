package com.ablueclive.fragment.feedFragment;

import android.util.Log;

import com.ablueclive.global.response.CombineFriendResponse;
import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.networkClass.RetrofitClient;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class CombinedFriendModel implements CombinedFriendContract.Model {
    private static final String TAG = "CombinedFriendModel";


    @Override
    public void combinedFriend(OnFinishedListener onFinishedListener, HashMap<String, String> param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.getCombineFriendRecord(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CombineFriendResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CombineFriendResponse responseModel) {
                        if (responseModel != null) {
                            onFinishedListener.onFinished(responseModel);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            Log.d(TAG, "" + e.getMessage());
                            onFinishedListener.onFailure(e);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
