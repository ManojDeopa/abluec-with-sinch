package com.ablueclive.fragment.feedFragment;
import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class AllFeedPresenter implements AllFeedContract.Presenter, AllFeedContract.Model.OnFinishedListener {
    private AllFeedContract.View allFeedView;
    private AllFeedContract.Model allFeedModel;

    public AllFeedPresenter(AllFeedContract.View allFeedView) {
        this.allFeedView = allFeedView;
        this.allFeedModel = new AllFeedModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (allFeedView != null) {
            allFeedView.hideProgress();
            allFeedView.setDataToRecyclerView(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (allFeedView != null) {
            allFeedView.hideProgress();
            allFeedView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        allFeedView = null;
    }

    @Override
    public void requestAllUserFeed(HashMap<String, String> param) {
        if (allFeedView != null) {
            allFeedView.showProgress();
            allFeedModel.allUserFeed(this, param);
        }

    }
}
