package com.ablueclive.fragment.feedFragment;

import com.ablueclive.global.response.CombineFriendResponse;

import java.util.HashMap;

public class CombinedFriendPresenter implements CombinedFriendContract.Presenter, CombinedFriendContract.Model.OnFinishedListener {
    private CombinedFriendContract.View combinedFriendView;
    private CombinedFriendContract.Model combinedFriendModel;

    public CombinedFriendPresenter(CombinedFriendContract.View combinedFriendView) {
        this.combinedFriendView = combinedFriendView;
        this.combinedFriendModel = new CombinedFriendModel();
    }


    @Override
    public void onFinished(CombineFriendResponse response) {
        if (combinedFriendView != null) {
            combinedFriendView.combinedFriendResponse(response);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (combinedFriendView != null)
            combinedFriendView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        combinedFriendView = null;
    }

    @Override
    public void requestCombinedFriendRecord(HashMap<String, String> param) {
        if (combinedFriendView != null)
            combinedFriendModel.combinedFriend(this, param);
    }
}
