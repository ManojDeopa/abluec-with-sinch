package com.ablueclive.fragment.feedFragment;

import com.ablueclive.global.response.CombineFriendResponse;

import java.util.HashMap;

public interface CombinedFriendContract {

    interface Model {

        interface OnFinishedListener {

            void onFinished(CombineFriendResponse response);

            void onFailure(Throwable t);
        }

        void combinedFriend(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void combinedFriendResponse(CombineFriendResponse response);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {

        void onDestroy();

        void requestCombinedFriendRecord(HashMap<String, String> param);

    }
}
