package com.ablueclive.fragment.feedFragment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class FeedListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class Comment {
        @SerializedName("comment_id")
        @Expose
        var commentId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("user_name")
        @Expose
        var userName: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("user_comment")
        @Expose
        var userComment: String? = null

        @SerializedName("post_user_id")
        @Expose
        var postUserId: String? = null

        @SerializedName("comment_timestamp")
        @Expose
        var commentTimestamp: String? = null

        @SerializedName("reply_comments")
        @Expose
        var replyComments: List<Any>? = null
    }

    class HasTagName {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null
    }

    class Image {
        @SerializedName("ImageName")
        @Expose
        var imageName: String? = null

        @SerializedName("ImageType")
        @Expose
        var imageType: String? = null

        @SerializedName("thumbImage")
        @Expose
        var thumbImage: String? = null
    }

    class LikeUser {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("post_id")
        @Expose
        var postId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("user_name")
        @Expose
        var userName: String? = null

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null
    }

    class PostDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("post_title")
        @Expose
        var postTitle: String? = null

        @SerializedName("post_content")
        @Expose
        var postContent: String? = null

        @SerializedName("images")
        @Expose
        var images: List<Image>? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("post_UTC_Date")
        @Expose
        var postUTCDate: String? = null

        @SerializedName("store_status")
        @Expose
        var storeStatus: Int? = null

        @SerializedName("store_id")
        @Expose
        var storeId: String? = null

        @SerializedName("post_user_name")
        @Expose
        var postUserName: String? = null

        @SerializedName("post_user_profile_image")
        @Expose
        var postUserProfileImage: String? = null

        @SerializedName("post_user_profile_thumb_image")
        @Expose
        var postUserProfileThumbImage: String? = null

        @SerializedName("totalCountLike")
        @Expose
        var totalCountLike: Int? = null

        @SerializedName("like_users")
        @Expose
        var likeUsers: List<LikeUser>? = null

        @SerializedName("comments")
        @Expose
        var comments: List<Comment>? = null

        @SerializedName("post_user_like_status")
        @Expose
        var postUserLikeStatus: String? = null

        @SerializedName("post_reports_status")
        @Expose
        var postReportsStatus: Int? = null

        @SerializedName("post_time_status")
        @Expose
        var postTimeStatus: String? = null

        @SerializedName("totalComments")
        @Expose
        var totalComments: Int? = null

        @SerializedName("hasTag")
        @Expose
        var hasTag: List<String>? = null

        @SerializedName("share_count")
        @Expose
        var shareCount: Int? = null

        @SerializedName("share_user")
        @Expose
        var shareUser: List<String>? = null

        @SerializedName("hasTagName")
        @Expose
        var hasTagName: List<HasTagName>? = null
    }

    class ResponseData {
        @SerializedName("postData")
        @Expose
        var postData: List<PostDatum>? = null
    }

}