package com.ablueclive.fragment.feedFragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.modelClass.ShareUsersPost
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.peoples_list_item.view.*
import java.util.*

class TotalWaveAdapter(var list: ArrayList<ShareUsersPost>) : RecyclerView.Adapter<TotalWaveAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.tvAdd.visibility = View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.peoples_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val imageUrl = Constants.PROFILE_IMAGE_URL + list[position].profile_image
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)
        holder.itemView.tvName.text = list[position].name

        holder.itemView.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].user_id.toString())
            CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }

    }


    override fun getItemCount(): Int {
        return list.size
    }

}
