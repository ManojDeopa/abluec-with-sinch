package com.ablueclive.fragment.menu_fragment;

import java.util.HashMap;

public interface LogoutContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg);

            void onFailure(Throwable t);
        }

        void getLogoutRequest(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestLogoutRequest(HashMap<String, String> param);
    }
}
