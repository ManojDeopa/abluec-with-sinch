package com.ablueclive.fragment.menu_fragment;

import java.util.HashMap;

public class LogoutPresenter implements LogoutContract.Presenter, LogoutContract.Model.OnFinishedListener {
    private LogoutContract.View logoutView;
    private LogoutContract.Model logoutModel;

    public LogoutPresenter(LogoutContract.View logoutView) {
        this.logoutView = logoutView;
        this.logoutModel = new LogoutModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg) {
        if (logoutView != null) {
            logoutView.hideProgress();
            logoutView.setDataToViews(response_status, response_msg);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (logoutView != null) {
            logoutView.hideProgress();
            logoutView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        logoutView = null;
    }

    @Override
    public void requestLogoutRequest(HashMap<String, String> param) {
        if (logoutView != null) {
            logoutView.showProgress();
            logoutModel.getLogoutRequest(this, param);
        }

    }
}
