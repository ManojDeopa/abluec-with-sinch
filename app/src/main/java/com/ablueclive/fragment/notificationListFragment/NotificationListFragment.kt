package com.ablueclive.fragment.notificationListFragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.adapter.NoticiationListAdapter
import com.ablueclive.common.CommonContainerActivity.Companion.feedDetailTitle
import com.ablueclive.listener.onNotificationItemListener
import com.ablueclive.modelClass.NotificationData
import com.ablueclive.modelClass.Response_data
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.callCommonContainer
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.fragment_notification_list.*
import java.util.*


class NotificationListFragment : Fragment(), NotificationListContract.View, onNotificationItemListener {
    lateinit var linearLayoutManager: LinearLayoutManager
    private var session_token = ""
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    lateinit var noticiationListAdapter: NoticiationListAdapter
    private var notificationDataArrayList = ArrayList<NotificationData>()
    private var isLoading = false
    private var pageNo = 1
    private var total_count = 0
    lateinit var intent: Intent

    companion object {

        fun newInstance(bundle: Bundle): NotificationListFragment {
            val fragment = NotificationListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView()

        if (isInternetPresent) {
            requestNotificationList(pageNo)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), context)
        }
    }

    // initialize object...
    private fun findView() {
        linearLayoutManager = LinearLayoutManager(context)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        setNotificationAdapter()
        setListeners()
    }

    // pagination code
    private fun setListeners() {
        recycler_all_notification.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    total_count = totalItemCount
                    requestNotificationList(pageNo)
                }
            }
        })
    }

    //notification list api param
    private fun requestNotificationList(pageNo: Int) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["page"] = "" + pageNo
        Log.d("CheersListParams", params.toString())
        val notificationListPresenter = NotificationListPresenter(this)
        notificationListPresenter.requestNotificationList(params)
    }

    //set notification adapter
    private fun setNotificationAdapter() {
        noticiationListAdapter = NoticiationListAdapter(context, notificationDataArrayList, this)
        recycler_all_notification.layoutManager = linearLayoutManager
        recycler_all_notification.adapter = noticiationListAdapter
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun setDataToRecyclerView(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            if (response_status == "1") {
                isLoading = false
                notificationDataArrayList = response_data.notificationData
                if (notificationDataArrayList.size > 0) {
                    noticiationListAdapter.addAll(notificationDataArrayList)
                    pageNo++
                } else {
                    if (pageNo == 1) {
                        text_no_item.visibility = View.VISIBLE
                    } else {
                        CommonMethod.showToastShort(getString(R.string.no_more_data), context)
                    }
                }
            } else if (response_invalid == "1") {
                logout(response_msg)
            } else {
                CommonMethod.showToastShort(response_msg, context)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
        throwable.printStackTrace()
    }

    // logout method
    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, context)
        BMSPrefs.putString(context, Constants._ID, "")
        BMSPrefs.putString(context, Constants.isStoreCreated, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(context!!, Intent(context, LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    // listener
    override fun onNotificationClick(NType: String, post_id: String, mobile: String, name: String, to_user_id: String, from_user_id: String, user_image: String) {
        if (NType == "1001" || NType == "1002") {
            intent = Intent(context, CommentActivity::class.java)
            BMSPrefs.putString(context, Constants.FEED_POST_ID, post_id)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        } else if (NType == "1003") {
            BMSPrefs.putString(context, Constants.TO_ID, from_user_id)
            BMSPrefs.putString(context, Constants.CHAT_USER_NAME, name)
            BMSPrefs.putString(context, Constants.FRIEND_MOBILE, mobile)
            BMSPrefs.putString(context, Constants.CHAT_USER_IMAGE, user_image)
            intent = Intent(context, StartChatActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        } else if (NType == "2001") {
            feedDetailTitle = name
            BMSPrefs.putString(context, Constants.FEED_POST_ID, post_id)
            callCommonContainer(context as Activity, getString(R.string.feed_details))
        } else {
            BMSPrefs.putString(context, Constants.FRIEND_ID, from_user_id)
            callActivity(context!!, Intent(context, FriendProfileActivity::class.java))
        }
    }

}