package com.ablueclive.fragment.notificationListFragment;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class NotificationListPresenter implements NotificationListContract.Presenter, NotificationListContract.Model.OnFinishedListener {
    private NotificationListContract.View notificationListView;
    private final NotificationListContract.Model notificationListModel;

    public NotificationListPresenter(NotificationListContract.View notificationListView) {
        this.notificationListView = notificationListView;
        this.notificationListModel = new NotificationListModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (notificationListView != null) {
            notificationListView.hideProgress();
            notificationListView.setDataToRecyclerView(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (notificationListView != null) {
            notificationListView.hideProgress();
            notificationListView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        notificationListView = null;
    }

    @Override
    public void requestNotificationList(HashMap<String, String> param) {
        if (notificationListView != null) {
            notificationListView.showProgress();
            notificationListModel.notificationList(this, param);
        }

    }
}
