package com.ablueclive.storeSection.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.recycler_add_layout.view.*
import kotlinx.android.synthetic.main.store_category_list_item.view.*


class StoreCategoryListAdapter(var list: List<StoreCategoryResponse.StoreCategory>, var recyclerViewItemClick: RecyclerViewItemClick) : RecyclerView.Adapter<StoreCategoryListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position == list.size) {
            holder.itemView.addLayout.setOnClickListener {
                recyclerViewItemClick.onRecyclerItemClick(context.getString(R.string.add_category), 1)
            }
        } else {
            holder.itemView.tvText.text = list[position].name
            CommonMethod.loadImage(Constants.IMAGE_STORE_URL + list[position].catImage, holder.itemView.ivImage)

            holder.itemView.ivClose.setOnClickListener {
                recyclerViewItemClick.onRecyclerItemClick(list[position]._id.toString(), 0)
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size) {
            R.layout.recycler_add_layout
        } else {
            R.layout.store_category_list_item
        }
    }

    fun filterList(filterList: List<StoreCategoryResponse.StoreCategory>) {
        list = filterList
        notifyDataSetChanged()
    }
}
