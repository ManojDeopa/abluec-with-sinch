package com.ablueclive.storeSection.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.OtherCategoryResponse
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.country_state_item.view.*

class OtherCategoryListAdapter(var list: List<OtherCategoryResponse.OtherCategory>,var listener: RecyclerViewItemClick) : RecyclerView.Adapter<OtherCategoryListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                listener.onRecyclerItemClick("CategoryItemClick",absoluteAdapterPosition)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.country_state_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvName.text = list[position].name
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
