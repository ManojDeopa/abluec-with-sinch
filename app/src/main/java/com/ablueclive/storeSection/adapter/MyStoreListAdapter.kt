package com.ablueclive.storeSection.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.storeSection.fragments.ManageStoreFragment
import com.ablueclive.storeSection.fragments.StoreDetailFragment
import com.ablueclive.storeSection.response.StoreListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.my_stores_item.view.*

class MyStoreListAdapter(var list: List<StoreListResponse.StoreList>) : RecyclerView.Adapter<MyStoreListAdapter.ViewHolder>() {

    lateinit var context: Context


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                StoreContainerActivity.selectedStoreName = list[absoluteAdapterPosition].storeName.toString()
                Constants.myStoreResponse = list[absoluteAdapterPosition]
                StoreDetailFragment.storeId = list[absoluteAdapterPosition].id.toString()
                CommonMethod.callStoreContainer(context as Activity, context.getString(R.string.store_detail))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.my_stores_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + list[position].storeImage, holder.itemView.imgStore)
        holder.itemView.tvStoreName.text = list[position].storeName

        var isStoreOn = false
        val cList = list[position].storeCalender
        if (!cList.isNullOrEmpty()) {
            cList.forEach {
                if (ManageStoreFragment.currentDayId == it.day.toString()) {
                    isStoreOn = true
                }
            }
        } else {
            list[position].isOpen = 0
            holder.itemView.ivActiveDot.setColorFilter(ContextCompat.getColor(context, R.color.grayCommon))
        }


        if (isStoreOn) {
            if (list[position].isAvailable == 0) {
                holder.itemView.ivActiveDot.setColorFilter(ContextCompat.getColor(context, R.color.grayCommon))
                list[position].isOpen = 0
            } else {
                holder.itemView.ivActiveDot.setColorFilter(ContextCompat.getColor(context, R.color.green))
                list[position].isOpen = 1
            }
        } else {
            holder.itemView.ivActiveDot.setColorFilter(ContextCompat.getColor(context, R.color.grayCommon))
            list[position].isOpen = 0
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun filterList(filterList: List<StoreListResponse.StoreList>) {
        list = filterList
        notifyDataSetChanged()
    }

}
