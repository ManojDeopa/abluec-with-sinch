package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.ProductModel
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.storeSection.fragments.ProductDetailsFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.store_product_item.view.*
import java.util.*

class StoreItemAdapter(val listener: RecyclerViewItemClick, val title: String) : RecyclerView.Adapter<StoreItemAdapter.ViewHolder>() {

    lateinit var categoryList: ArrayList<SearchStoreListResponse.CategoryListing>
    lateinit var featureProductModelList: ArrayList<ProductModel>
    lateinit var popularProductModelList: ArrayList<ProductModel>
    var context: Context = Constants.currentContext


    constructor(list: ArrayList<SearchStoreListResponse.CategoryListing>, listener: RecyclerViewItemClick, title: String) : this(listener, title) {
        categoryList = list
    }

    constructor(listener: RecyclerViewItemClick, list: ArrayList<ProductModel>, title: String) : this(listener, title) {
        featureProductModelList = list
    }


    constructor(listener: RecyclerViewItemClick, title: String, list: ArrayList<ProductModel>) : this(listener, title) {
        popularProductModelList = list
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.setOnClickListener {

                if (title == context.getString(R.string.featured_products_text)) {
                    Constants.sProductData = featureProductModelList[absoluteAdapterPosition]
                }

                if (title == context.getString(R.string.popular_products)) {
                    Constants.sProductData = popularProductModelList[absoluteAdapterPosition]
                }

                if (ProductDetailsFragment.isSimilar) {
                    ProductDetailsFragment.isSimilar = false
                    val activity: FragmentActivity = context as FragmentActivity
                    activity.finish()
                }
                CommonMethod.callCommonContainer(context as Activity, title)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        Constants.currentContext = null
        val view = LayoutInflater.from(context).inflate(R.layout.store_product_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var imageUrl = Constants.IMAGE_STORE_URL
        var text = ""

        when (title) {

            context.getString(R.string.categories) -> {
                imageUrl += categoryList[position].catImage
                text = categoryList[position].name.toString()
            }

            context.getString(R.string.featured_products_text) -> {

                val img = featureProductModelList[position].images
                if (!img.isNullOrEmpty()) {
                    imageUrl += img[0]
                }
                text = featureProductModelList[position].productTitle.toString()

                holder.itemView.tvPrice.visibility = View.VISIBLE
                holder.itemView.tvPrice.text = Constants.CURRENCY + featureProductModelList[position].productPrice


                val isFav = featureProductModelList[position].productFavourite

                if (isFav != null) {
                    holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_checked)
                    holder.itemView.ivFav.tag = "check"
                } else {
                    holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_unchecked)
                    holder.itemView.ivFav.tag = "uncheck"
                }

                holder.itemView.ivFav.setOnClickListener {
                    listener.onRecyclerItemClick(featureProductModelList[position].id.toString() + ":" + holder.itemView.ivFav.tag.toString(), position)
                    updateFavIcon(holder.itemView.ivFav)
                }
            }

            context.getString(R.string.popular_products) -> {
                val img = popularProductModelList[position].images
                if (!img.isNullOrEmpty()) {
                    imageUrl += img[0]
                }

                text = popularProductModelList[position].productTitle.toString()
                holder.itemView.tvPrice.text = Constants.CURRENCY + popularProductModelList[position].productPrice

                val salePrice = if (popularProductModelList[position].productSalePrice == null) "0" else popularProductModelList[position].productSalePrice.toString()
                holder.itemView.tvProductSalePrice.apply {
                    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    text = Constants.CURRENCY + salePrice
                }

                val isFav = popularProductModelList[position].productFavourite
                if (isFav != null) {
                    holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_checked)
                    holder.itemView.ivFav.tag = "check"
                } else {
                    holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_unchecked)
                    holder.itemView.ivFav.tag = "uncheck"
                }

                holder.itemView.ivFav.setOnClickListener {
                    listener.onRecyclerItemClick(popularProductModelList[position].id.toString() + ":" + holder.itemView.ivFav.tag.toString(), position)
                    updateFavIcon(holder.itemView.ivFav)
                }
            }
        }

        CommonMethod.loadImageResize(imageUrl, holder.itemView.ivProduct)
        holder.itemView.tvName.text = text
    }


    private fun updateFavIcon(ivFav: AppCompatImageView) {

        val tag = ivFav.tag
        if (tag == "uncheck") {
            ivFav.setImageResource(R.drawable.ic_heart_checked)
            ivFav.tag = "check"
        } else {
            ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            ivFav.tag = "uncheck"
        }
    }

    override fun getItemCount(): Int {

        return when (title) {

            context.getString(R.string.categories) -> {
                categoryList.size
            }

            context.getString(R.string.featured_products_text) -> {
                featureProductModelList.size
            }

            context.getString(R.string.popular_products) -> {
                popularProductModelList.size
            }

            else -> {
                0
            }

        }

    }

}
