package com.ablueclive.storeSection.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.AgentListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.agent_list_item.view.*

class AgentListAdapter(var list: List<AgentListResponse.Agent>) : RecyclerView.Adapter<AgentListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.agent_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageCircle(Constants.IMAGE_STORE_URL + list[position].profileImage, holder.itemView.ivImage)
        holder.itemView.tvName.text = list[position].name
        val mobile = list[position].mobile
        holder.itemView.tvPhoneNo.text = mobile

        holder.itemView.tvPhoneNo.setOnClickListener {
            if (!mobile.isNullOrEmpty()) {
                CommonMethod.callIntent(mobile, context)
            }
        }
    }

    fun filterList(filterList: List<AgentListResponse.Agent>) {
        list = filterList
        notifyDataSetChanged()
    }

}
