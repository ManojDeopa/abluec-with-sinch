package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.storeSection.fragments.StoreDetailListFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.GPSTracker
import kotlinx.android.synthetic.main.store_list_item.view.*


class SearchStoreListAdapter(var list: List<SearchStoreListResponse.NearBuyStore>) : RecyclerView.Adapter<SearchStoreListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.setOnClickListener {
                StoreDetailListFragment.storeId = list[absoluteAdapterPosition].storeInfo?.id.toString()
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.search_store_detail))
            }

            itemView.ivLocationStore.setOnClickListener {
                try {

                    val lng = list[absoluteAdapterPosition].storeInfo?.storeLongitude
                    val lat = list[absoluteAdapterPosition].storeInfo?.storeLatitude

                    val gpsTracker = GPSTracker(context)
                    val currentLat = gpsTracker.latitude
                    val currentLng = gpsTracker.longitude
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=$currentLat,$currentLng&daddr=$lat,$lng"))
                    context.startActivity(intent)


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.store_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position].storeInfo
        holder.itemView.tvStoreName.text = data?.storeName
        holder.itemView.tvStoreAddress.text = data?.storeAddress


        val serviceTime = if (data?.serviceTime.isNullOrEmpty()) "0" else data?.serviceTime
        val serviceTimeUnit = if (data?.serviceTimeUnit.isNullOrEmpty()) "Minutes" else data?.serviceTimeUnit

        holder.itemView.tvServiceTime.text = "Service time : $serviceTime $serviceTimeUnit"

        /*val df = DecimalFormat("##.##")
        val distance = if (data?.distance != null) df.format(data.distance) else 0.0*/
        val distance = if (data?.google_distance != null) data.google_distance else "-1"
        

        holder.itemView.tvDistance.visibility = if (distance == "-1") View.GONE else View.VISIBLE
        holder.itemView.tvDistance.text = "Distance : $distance "


        CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + data?.storeImage, holder.itemView.ivStoreImage)
        holder.itemView.ivActiveDot.visibility = if (data?.isAvailable == 1) View.VISIBLE else View.GONE

        val orderRating = data?.orderRating
        if (orderRating != null) {
            val averageRating = orderRating.averageRating.toString()
            holder.itemView.tvRatingCount.text = averageRating
        } else {
            holder.itemView.tvRatingCount.text = "0"
        }
    }

    fun updateList(searchStoreList: List<SearchStoreListResponse.NearBuyStore>) {
        list = searchStoreList
        notifyDataSetChanged()
    }

}
