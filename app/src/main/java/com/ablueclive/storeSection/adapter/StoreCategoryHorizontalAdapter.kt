package com.ablueclive.storeSection.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.category_item_horizontal.view.*


class StoreCategoryHorizontalAdapter(var list: List<StoreCategoryResponse.StoreCategory>, var recyclerViewItemClick: RecyclerViewItemClick) : RecyclerView.Adapter<StoreCategoryHorizontalAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.tvTitle.setOnClickListener {
                list.forEach {
                    it.isSelected = false
                }
                list[absoluteAdapterPosition].isSelected = true
                notifyDataSetChanged()

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.category_item_horizontal, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTitle.text = list[position].name
        if (list[position].isSelected) {
            StoreContainerActivity.selectedCategoryId = list[position]._id.toString()
            StoreContainerActivity.selectedCatName = list[position].name.toString()
            holder.itemView.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.itemLayout.setBackgroundResource(R.drawable.blue_button_border)
            val id = if (list[position]._id.isNullOrEmpty()) list[position]._id.toString() else list[position]._id.toString()
            recyclerViewItemClick.onRecyclerItemClick(id, 5)
        } else {
            holder.itemView.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.iconColor))
            holder.itemView.itemLayout.setBackgroundResource(R.drawable.app_bg_border)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
