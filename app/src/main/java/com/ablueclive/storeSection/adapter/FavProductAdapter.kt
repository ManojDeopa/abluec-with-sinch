package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.fragments.FavProductListFragment
import com.ablueclive.storeSection.fragments.ProductDetailsFragment
import com.ablueclive.storeSection.response.StoreProductListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.add_remove_count.view.*
import kotlinx.android.synthetic.main.products_lists_item.view.*


class FavProductAdapter(var list: MutableList<StoreProductListResponse.FavProduct>, var recyclerViewItemClick: RecyclerViewItemClick) : RecyclerView.Adapter<FavProductAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.addCountLayout.visibility = View.VISIBLE

            itemView.setOnClickListener {

                if (itemView.tvCount.text.toString().toInt() > 0) {
                    ProductDetailsFragment.shouldUpdateCounts = true
                }

                ProductDetailsFragment.isFav = itemView.ivFav.tag == "check"
                ProductDetailsFragment.count = itemView.tvCount.text.toString()

                Constants.sProductData = list[absoluteAdapterPosition].product
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.popular_products))
            }

            itemView.ivRemove.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count - 1, absoluteAdapterPosition)
            }


            itemView.ivAdd.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count + 1, absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.products_lists_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvTitle.text = list[position].product?.productTitle
        holder.itemView.tvDescription.text = list[position].product?.productDescription
        holder.itemView.tvPrice.text = Constants.CURRENCY + list[position].product?.productPrice.toString()


        val salePrice = if (list[position].product?.productSalePrice == null) "0" else list[position].product?.productSalePrice.toString()
        holder.itemView.tvSalePrice.visibility = if (salePrice == "0") View.GONE else View.VISIBLE
        holder.itemView.tvSalePrice.apply {
            paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            text = Constants.CURRENCY + salePrice
        }

        val unit = if (list[position].product?.unit.isNullOrEmpty()) "Items" else list[position].product?.unit
        holder.itemView.tvAvail.text = "Available :" + list[position].product?.stock + " $unit"


        var imgURL = Constants.IMAGE_STORE_URL
        val img = list[position].product?.images
        if (!img.isNullOrEmpty()) {
            imgURL += img[0]
        }
        CommonMethod.loadImage(imgURL, holder.itemView.ivProductImage)


        holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_checked)
        holder.itemView.ivFav.tag = "check"
        holder.itemView.ivFav.setOnClickListener {
            val tag = holder.itemView.ivFav.tag
            recyclerViewItemClick.onRecyclerItemClick(list[position].productId.toString() + ":" + tag.toString(), 1)
            list.removeAt(position)
            notifyItemRemoved(position)
        }

        val isCart = list[position].cart
        if (isCart != null) {
            holder.itemView.addRemoveContent.visibility = View.VISIBLE
            holder.itemView.ivAddBlue.visibility = View.GONE
            holder.itemView.tvCount.text = isCart.qty.toString()
        } else {
            holder.itemView.addRemoveContent.visibility = View.GONE
            holder.itemView.ivAddBlue.visibility = View.VISIBLE
            holder.itemView.ivAddBlue.setOnClickListener {
                holder.itemView.addRemoveContent.visibility = View.VISIBLE
                holder.itemView.ivAddBlue.visibility = View.GONE
                updateCounts(holder.itemView.tvCount, 1, position)
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun updateCounts(tvCount: AppCompatTextView, i: Int, pos: Int) {
        FavProductListFragment.storeId = list[pos].product?.storeId.toString()
        var count = 0
        if (i > 0) {
            count = i
        }
        tvCount.text = count.toString()
        recyclerViewItemClick.onRecyclerItemClick(list[pos].productId.toString() + ":" + count.toString(), 0)

    }


}
