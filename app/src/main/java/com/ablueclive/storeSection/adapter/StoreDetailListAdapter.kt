package com.ablueclive.storeSection.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.ProductModel
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.Constants.currentContext
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.recycler_view.view.*
import kotlinx.android.synthetic.main.store_detail_list_item.view.*
import java.util.*

class StoreDetailListAdapter(var list: List<String>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<StoreDetailListAdapter.ViewHolder>() {

    var categoryList = ArrayList<SearchStoreListResponse.CategoryListing>()
    var featureProductList = ArrayList<ProductModel>()
    var popularProductList = ArrayList<ProductModel>()


    companion object {
        lateinit var context: Context
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        currentContext = context
        val view = LayoutInflater.from(context).inflate(R.layout.store_detail_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title = list[position]
        holder.itemView.tvTitle.text = title

        when (title) {

            context.getString(R.string.categories) -> {
                val list = Constants.searchStoreData.categoryListing
                if (list.isNullOrEmpty()) {
                    holder.itemView.tvTitle.text = "$title\n(No Categories)"
                } else {
                    holder.itemView.ivMore.visibility = View.VISIBLE

                    holder.itemView.ivMore.setOnClickListener {
                        CommonMethod.callCommonContainer(context as Activity, title)
                    }

                    categoryList.clear()
                    categoryList.addAll(list)
                    holder.itemView.rvCommon.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = StoreItemAdapter(categoryList, listener, title)
                    }
                }
            }

            context.getString(R.string.featured_products_text) -> {

                val list = Constants.searchStoreData.productModels
                if (list.isNullOrEmpty()) {
                    holder.itemView.tvTitle.text = "$title\n(No Products)"
                } else {
                    featureProductList.clear()
                    featureProductList.addAll(list)
                    holder.itemView.rvCommon.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = StoreItemAdapter(listener, featureProductList, title)
                    }
                }
            }

            context.getString(R.string.popular_products) -> {

                val list = Constants.searchStoreData.popularProducts
                if (list.isNullOrEmpty()) {
                    holder.itemView.tvTitle.text = "$title\n(No Products)"
                } else {
                    popularProductList.clear()
                    popularProductList.addAll(list)
                    holder.itemView.rvCommon.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = StoreItemAdapter(listener, title, popularProductList)
                    }
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

}
