package com.ablueclive.storeSection.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.utils.CommonMethod
import kotlinx.android.synthetic.main.store_detail_menu_item.view.*

class StoreDetailMenuAdapter(var context: FragmentActivity) : RecyclerView.Adapter<StoreDetailMenuAdapter.ViewHolder>() {

    private var listTitle: List<String> = arrayListOf()
    private var listIcon: List<Int> = arrayListOf()


    init {

        listTitle = listOf(context.getString(R.string.add_product),
                context.getString(R.string.manage_products),
                context.getString(R.string.store_calendar),
                context.getString(R.string.manage_agent),
                context.getString(R.string.order_completed),
                context.getString(R.string.manage_orders))

        listIcon = listOf(R.drawable.manage_categories,
                R.drawable.manage_products,
                R.drawable.store_calendar,
                R.drawable.manage_agent,
                R.drawable.manage_products,
                R.drawable.manage_categories)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {

                /* disable two options for now */
                if (listTitle[absoluteAdapterPosition] == context.getString(R.string.manage_orders)
                        || listTitle[absoluteAdapterPosition] == context.getString(R.string.order_completed)){
                    return@setOnClickListener
                }





                if (listTitle[absoluteAdapterPosition] == context.getString(R.string.manage_orders)) {
                    StoreContainerActivity.consolidate = false
                }
                CommonMethod.callStoreContainer(context, listTitle[absoluteAdapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.store_detail_menu_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.ivIcon.setImageResource(listIcon[position])
        holder.itemView.tvText.text = listTitle[position]
    }

    override fun getItemCount(): Int {
        return listTitle.size
    }

}
