package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.StoreCalender
import kotlinx.android.synthetic.main.store_calendar_item.view.*
import java.util.*


class StoreCalendarAdapter(var list: ArrayList<StoreCalender>) : RecyclerView.Adapter<StoreCalendarAdapter.ViewHolder>() {


    lateinit var context: Context


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.tvFrom.setOnClickListener {
                openTimePicker(itemView.tvFrom, absoluteAdapterPosition)
            }

            itemView.tvTo.setOnClickListener {
                openTimePicker(itemView.tvTo, absoluteAdapterPosition)
            }

        }

    }

    @SuppressLint("SetTextI18n")
    private fun openTimePicker(textView: AppCompatTextView, pos: Int) {
        val mCurrentTime: Calendar = Calendar.getInstance()
        val hour: Int = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute: Int = mCurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(context, R.style.DatePickerDialogThemes, { timePicker, selectedHour, selectedMinute ->

            val time = getFormat(selectedHour) + ":" + getFormat(selectedMinute)
            textView.text = time
            if (textView.id == R.id.tvFrom) {
                list[pos].fromTime = time
            } else if (textView.id == R.id.tvTo) {
                list[pos].toTime = time
            }

            if (isValidated(list[pos].toTime, list[pos].fromTime)) {
                list[pos].day = pos
                notifyItemChanged(pos)
            }


        }, hour, minute, true) //Yes 24 hour time
        mTimePicker.show()
    }

    private fun getFormat(selected: Int): String {
        if (selected < 10) {
            return "0$selected"
        }
        return "" + selected
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.store_calendar_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]

        val v = holder.itemView
        v.btnDay.text = data.dayName

        if (isValidated(data.toTime, data.fromTime)) {
            data.isValidated = true
            v.btnDay.setTextColor(ContextCompat.getColor(context, R.color.white))
            v.btnDay.setBackgroundResource(R.drawable.blue_button_border)
            v.tvFrom.text = data.fromTime
            v.tvTo.text = data.toTime
        } else {
            data.isValidated = false
            v.btnDay.setTextColor(ContextCompat.getColor(context, R.color.grayDark))
            v.btnDay.setBackgroundResource(R.drawable.gray_non_active_border)
            v.tvFrom.text = null
            v.tvTo.text = null
            v.tvFrom.hint = context.getString(R.string.from)
            v.tvTo.hint = context.getString(R.string.to)
        }

        holder.itemView.tvReset.setOnClickListener {
            data.toTime = null
            data.fromTime = null
            notifyItemChanged(position)
        }


    }


    private fun isValidated(toDate: String?, fromDate: String?): Boolean {
        return !toDate.isNullOrEmpty() && !fromDate.isNullOrEmpty()
    }

}
