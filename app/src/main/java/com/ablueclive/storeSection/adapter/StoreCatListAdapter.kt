package com.ablueclive.storeSection.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.storeSection.fragments.AddProductFragment
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.store_cat_item.view.*


class StoreCatListAdapter(private var list: List<StoreCategoryResponse.StoreCategory>) : RecyclerView.Adapter<StoreCatListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {

                StoreContainerActivity.addActivities(context as Activity)
                StoreContainerActivity.selectedCategoryId = list[absoluteAdapterPosition]._id.toString()
                StoreContainerActivity.selectedCatName = list[absoluteAdapterPosition].name.toString()

                if (list[absoluteAdapterPosition].isHavingChild == 1) {
                    CommonMethod.callStoreContainer(context as Activity, context.getString(R.string.select_category))
                } else {
                    if (StoreContainerActivity.isEdit) {
                        AddProductFragment.shouldNotify=true
                        StoreContainerActivity.killActivities()
                        (context as Activity).finish()
                    } else {
                        CommonMethod.callStoreContainer(context as Activity, context.getString(R.string.add_product))
                    }
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.store_cat_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTitle.text = list[position].name
        CommonMethod.loadImage(Constants.IMAGE_STORE_URL + list[position].catImage, holder.itemView.ivImage)
    }
}
