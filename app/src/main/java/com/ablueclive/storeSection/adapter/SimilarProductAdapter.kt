package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.ProductModel
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.similar_product_item.view.*
import java.util.*

class SimilarProductAdapter(val listener: RecyclerViewItemClick, var list: ArrayList<ProductModel>) : RecyclerView.Adapter<SimilarProductAdapter.ViewHolder>() {

    var context: Context = Constants.currentContext


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.setOnClickListener {
                Constants.sProductData = list[absoluteAdapterPosition]
                val activity: FragmentActivity = context as FragmentActivity
                activity.finish()
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.popular_products))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        Constants.currentContext = null
        val view = LayoutInflater.from(context).inflate(R.layout.similar_product_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var imageUrl = Constants.IMAGE_STORE_URL
        val img = list[position].images
        if (!img.isNullOrEmpty()) {
            imageUrl += img[0]
        }
        CommonMethod.loadImageResize(imageUrl, holder.itemView.ivProduct)
        holder.itemView.tvName.text = list[position].productTitle
        holder.itemView.tvPrice.text = Constants.CURRENCY + list[position].productPrice
        val salePrice = if (list[position].productSalePrice == null) "0" else list[position].productSalePrice.toString()
        holder.itemView.tvProductSalePrice.apply {
            paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            text = Constants.CURRENCY + salePrice
        }

        val isFav = list[position].productFavourite
        if (isFav != null) {
            holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_checked)
            holder.itemView.ivFav.tag = "check"
        } else {
            holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            holder.itemView.ivFav.tag = "uncheck"
        }

        holder.itemView.ivFav.setOnClickListener {
            listener.onRecyclerItemClick(list[position].id.toString() + ":" + holder.itemView.ivFav.tag.toString(), position)
            updateFavIcon(holder.itemView.ivFav)
        }


    }


    private fun updateFavIcon(ivFav: AppCompatImageView) {

        val tag = ivFav.tag
        if (tag == "uncheck") {
            ivFav.setImageResource(R.drawable.ic_heart_checked)
            ivFav.tag = "check"
        } else {
            ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            ivFav.tag = "uncheck"
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
