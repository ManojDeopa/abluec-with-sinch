package com.ablueclive.storeSection.adapter

import AppUtils
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.fragments.OrderDetailFragment
import com.ablueclive.storeSection.response.StoreOrderListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.customaer_order_list_item.view.*


class CustomerOrderListAdepter(var list: MutableList<StoreOrderListResponse.OrderInfo>, var listener: RecyclerViewItemClick, var title: String) : RecyclerView.Adapter<CustomerOrderListAdepter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            if (title == context.getString(R.string.requested)) {
                itemView.btnCancelOrder.visibility = View.VISIBLE
            }

            itemView.setOnClickListener {
                OrderDetailFragment.data = list[absoluteAdapterPosition]
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.order_details))
            }

            itemView.btnCancelOrder.setOnClickListener {

                val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                alert.setTitle(context.getString(R.string.app_name))
                alert.setMessage(context.getString(R.string.cancel_order_alert))
                alert.setCancelable(false)
                alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    listener.onRecyclerItemClick(list[absoluteAdapterPosition].id.toString(), 0)
                    list.removeAt(absoluteAdapterPosition)
                    notifyItemRemoved(absoluteAdapterPosition)
                }

                alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                }
                alert.show()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.customaer_order_list_item, parent, false)
        return ViewHolder(view)

    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val v = holder.itemView
        val data = list[position]

        var storeImage = Constants.IMAGE_STORE_URL
        val imageObj = data.store?.images
        if (!imageObj.isNullOrEmpty()) {
            storeImage += imageObj[0].imageName.toString()
        }
        CommonMethod.loadImage(storeImage, v.ivImage)

        v.tvStoreName.text = data.store?.storeName

        v.tvStoreContactNo.text = data.store?.storeContactNumber
        v.tvStoreContactNo.setOnClickListener {
            CommonMethod.callIntent(data.store?.storeContactNumber.toString(), context)
        }

        v.tvAddress.text = data.store?.storeCompleteAddress

        v.tvOrderDate.text = AppUtils.convertToDateTime(data.createdAt)

        val mContainer = v.itemsContainer
        mContainer.removeAllViews()
        val items = data.product
        if (!items.isNullOrEmpty()) {
            var price = 0.0
            items.forEach {
                val textView = TextView(context)
                textView.text = it.qty.toString() + "x " + it.productTitle
                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT)
                textView.layoutParams = params
                mContainer.addView(textView)
                price += it.totalPrice.toString().toDouble()
            }
            v.tvTotalPrice.text = Constants.CURRENCY + price.toString()
        }

        var orderStatus = ""
        var colorInt = 0

        when (list[position].status) {

            1 -> {
                orderStatus = context.getString(R.string.pending_text)
                colorInt = ContextCompat.getColor(context, R.color.grayDark)
            }

            2 -> {
                orderStatus = context.getString(R.string.processed_text)
                colorInt = ContextCompat.getColor(context, R.color.yellow)
            }

            3 -> {
                orderStatus = context.getString(R.string.completed)
                colorInt = ContextCompat.getColor(context, R.color.green)
            }

            4 -> {
                orderStatus = context.getString(R.string.cancelled)
                colorInt = ContextCompat.getColor(context, R.color.google_plus)
            }

        }

        v.tvStatus.text = orderStatus
        v.tvStatus.setTextColor(colorInt)

    }

    override fun getItemCount(): Int {
        return list.size
    }
}
