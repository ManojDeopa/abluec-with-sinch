package com.ablueclive.storeSection.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.StoreCalender
import kotlinx.android.synthetic.main.buyer_store_calendar_item.view.*


class BuyerStoreCalendarAdapter(var list: List<StoreCalender>) : RecyclerView.Adapter<BuyerStoreCalendarAdapter.ViewHolder>() {

    val dayList = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.buyer_store_calendar_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvDay.text = /*getDayName(list[position].day)*/list[position].dayName
        var timing = "Closed"
        var colorInt = ContextCompat.getColor(context, R.color.google_plus)
        if (!list[position].toTime.isNullOrEmpty()) {
            timing = list[position].toTime + " - " + list[position].fromTime
            colorInt = ContextCompat.getColor(context, R.color.grayDark)
        }
        holder.itemView.tvTime.text = timing
        holder.itemView.tvTime.setTextColor(colorInt)

    }

    private fun getDayName(day: Int?): String {
        if (day != null) {
            return dayList[day]
        }
        return ""
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
