package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.fragment.SearchProductFragment
import com.ablueclive.global.response.ProductModel
import com.ablueclive.storeSection.fragments.ProductDetailsFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.add_remove_count.view.*
import kotlinx.android.synthetic.main.products_lists_item.view.*


class ProductsListsAdapter(var list: List<ProductModel>, var recyclerViewItemClick: RecyclerViewItemClick) : RecyclerView.Adapter<ProductsListsAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.tvDistance.visibility = View.VISIBLE

            itemView.setOnClickListener {
                ProductDetailsFragment.isFav = itemView.ivFav.tag == "check"
                ProductDetailsFragment.count = itemView.tvCount.text.toString()
                Constants.sProductData = list[absoluteAdapterPosition]
                CommonMethod.callCommonContainer(context as Activity, context.getString(R.string.popular_products))
            }

            itemView.ivRemove.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count - 1, absoluteAdapterPosition)
            }

            itemView.ivAdd.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count + 1, absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.products_lists_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvTitle.text = list[position].productTitle
        holder.itemView.tvDescription.text = list[position].productDescription


        val productPrice: String = if (list[position].productPrice == null) "0" else list[position].productPrice.toString()
        holder.itemView.tvPrice.text = Constants.CURRENCY + productPrice

        val salePrice = if (list[position].productSalePrice == null) "0" else list[position].productSalePrice.toString()
        holder.itemView.tvSalePrice.visibility = if (salePrice == "0") View.GONE else View.VISIBLE
        holder.itemView.tvSalePrice.apply {
            paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            text = Constants.CURRENCY + salePrice
        }


        /*val df = DecimalFormat("##.##")
        val distance = if (list[position].distance != null) df.format(list[position].distance) else 0.0*/
        val distance = if (list[position].google_distance != null) list[position].google_distance else "-1"
        val storeName = list[position].store?.storeName

        if (distance == "-1") {
            holder.itemView.tvDistance.text = "$storeName"
        } else {
            holder.itemView.tvDistance.text = "$storeName ($distance)"
        }

        val unit = if (list[position].unit.isNullOrEmpty()) "Items" else list[position].unit
        holder.itemView.tvAvail.text = "Available :" + list[position].stock + " $unit"


        var imgURL = Constants.IMAGE_STORE_URL
        val img = list[position].images
        if (!img.isNullOrEmpty()) {
            imgURL += img[0]
        }
        CommonMethod.loadImage(imgURL, holder.itemView.ivProductImage)

        val isFavourite = list[position].productFavourite
        if (isFavourite != null) {
            holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_checked)
            holder.itemView.ivFav.tag = "check"
        } else {
            holder.itemView.ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            holder.itemView.ivFav.tag = "uncheck"
        }

        val isCart = list[position].cart
        if (isCart != null) {
            holder.itemView.addRemoveContent.visibility = View.VISIBLE
            holder.itemView.ivAddBlue.visibility = View.GONE
            holder.itemView.tvCount.text = isCart.qty.toString()
        } else {
            holder.itemView.addRemoveContent.visibility = View.GONE
            holder.itemView.ivAddBlue.visibility = View.VISIBLE
            holder.itemView.ivAddBlue.setOnClickListener {
                holder.itemView.addRemoveContent.visibility = View.VISIBLE
                holder.itemView.ivAddBlue.visibility = View.GONE
                updateCounts(holder.itemView.tvCount, 1, position)
            }
        }

        holder.itemView.ivFav.setOnClickListener {
            val tag = holder.itemView.ivFav.tag
            recyclerViewItemClick.onRecyclerItemClick(list[position].id.toString() + ":" + tag.toString(), 1)
            updateFavIcon(holder.itemView.ivFav)
        }

    }

    private fun updateFavIcon(ivFav: AppCompatImageView) {

        val tag = ivFav.tag
        if (tag == "uncheck") {
            ivFav.setImageResource(R.drawable.ic_heart_checked)
            ivFav.tag = "check"
        } else {
            ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            ivFav.tag = "uncheck"
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun updateCounts(tvCount: AppCompatTextView, i: Int, pos: Int) {
        var count = 0
        if (i > 0) {
            count = i
        }
        tvCount.text = count.toString()
        SearchProductFragment.storeId = list[pos].storeId.toString()
        recyclerViewItemClick.onRecyclerItemClick(list[pos].id.toString() + ":" + count.toString(), 0)

    }


    fun filterList(filterList: List<ProductModel>) {
        list = filterList
        notifyDataSetChanged()
    }
}
