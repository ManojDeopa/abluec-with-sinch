package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.CartListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.add_remove_count.view.*
import kotlinx.android.synthetic.main.cart_list_item.view.*
import java.util.*

class CartItemListAdapter(var list: ArrayList<CartListResponse.Item>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<CartItemListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.ivRemove.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count - 1, absoluteAdapterPosition, list[absoluteAdapterPosition].product?.stock)
            }

            itemView.ivAdd.setOnClickListener {
                val count = itemView.tvCount.text.toString().toInt()
                updateCounts(itemView.tvCount, count + 1, absoluteAdapterPosition, list[absoluteAdapterPosition].product?.stock)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.cart_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvProductName.text = list[position].product?.productTitle
        holder.itemView.tvStoreName.text = "(" + list[position].store?.storeName + ")"
        val productPrice = if (list[position].product == null) "0" else list[position].product?.productPrice
        holder.itemView.tvPrice.text = Constants.CURRENCY + productPrice
        holder.itemView.tvCount.text = list[position].qty.toString()

    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun updateCounts(tvCount: AppCompatTextView, i: Int, pos: Int, stock: Int?) {


        if (stock != null) {
            if (stock < i) {
                CommonMethod.showToastlong(context.getString(R.string.stock_limit_exceed_text), context)
                return
            }
        }

        var count = 0
        if (i > 0) {
            count = i
        }
        tvCount.text = count.toString()
        list[pos].qty = count
        listener.onRecyclerItemClick(list[pos].productId.toString(), count)

    }

}
