package com.ablueclive.storeSection.adapter

import AppUtils
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.fragments.StoreOrderDetailFragment
import com.ablueclive.storeSection.response.StoreOrderListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.store_order_list_item.view.*


class StoreOrderListAdepter(var list: MutableList<StoreOrderListResponse.OrderInfo>, var listener: RecyclerViewItemClick, var title: String) : RecyclerView.Adapter<StoreOrderListAdepter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            if (title == context.getString(R.string.requested)) {

                itemView.btnCancelOrder.visibility = View.VISIBLE
                itemView.btnAcceptOrder.visibility = View.VISIBLE

                itemView.btnCancelOrder.setOnClickListener {

                    val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                    alert.setTitle(context.getString(R.string.app_name))
                    alert.setMessage(context.getString(R.string.cancel_order_alert))
                    alert.setCancelable(false)
                    alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                        listener.onRecyclerItemClick(list[absoluteAdapterPosition].id.toString(), 5)
                        list.removeAt(absoluteAdapterPosition)
                        notifyItemRemoved(absoluteAdapterPosition)
                    }

                    alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                    }
                    alert.show()


                }

                itemView.btnAcceptOrder.setOnClickListener {

                    val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                    alert.setTitle(context.getString(R.string.app_name))
                    alert.setMessage(context.getString(R.string.accept_order_alert))
                    alert.setCancelable(false)
                    alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                        listener.onRecyclerItemClick(list[absoluteAdapterPosition].id.toString(), 2)
                        list.removeAt(absoluteAdapterPosition)
                        notifyItemRemoved(absoluteAdapterPosition)
                    }

                    alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                    }
                    alert.show()

                }
            }

            if (title == context.getString(R.string.in_process)) {
                itemView.btnComplete.visibility = View.VISIBLE
                itemView.btnComplete.setOnClickListener {

                    val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                    alert.setTitle(context.getString(R.string.app_name))
                    alert.setMessage(context.getString(R.string.complete_order_alert))
                    alert.setCancelable(false)
                    alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                        listener.onRecyclerItemClick(list[absoluteAdapterPosition].id.toString(), 3)
                        list.removeAt(absoluteAdapterPosition)
                        notifyItemRemoved(absoluteAdapterPosition)
                    }

                    alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                        dialog.dismiss()
                    }
                    alert.show()

                }
            }


            itemView.setOnClickListener {
                StoreOrderDetailFragment.title = title
                StoreOrderDetailFragment.data = list[absoluteAdapterPosition]
                CommonMethod.callStoreContainer(context as Activity, context.getString(R.string.order_details))
            }

            itemView.tvPhoneNo.setOnClickListener {
                list[absoluteAdapterPosition].customer?.mobile?.let { it1 -> CommonMethod.callIntent(it1, context) }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.store_order_list_item, parent, false)
        return ViewHolder(view)

    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val v = holder.itemView
        val data = list[position]


        v.tvStoreName.text = data.store?.storeName
        v.tvAddress.text = data.address?.completeAddress
        v.tvOrderDate.text = AppUtils.convertToDateTime(data.createdAt)

        v.tvName.text = data.customer?.name
        v.tvPhoneNo.text = data.customer?.mobile

        v.tvPhoneNo.setOnClickListener {
            CommonMethod.callIntent(data.customer?.mobile.toString(), context)
        }


        val mContainer = v.itemsContainer
        mContainer.removeAllViews()
        val items = data.product
        if (!items.isNullOrEmpty()) {
            var price = 0.0
            items.forEach {
                val textView = TextView(context)
                textView.text = it.qty.toString() + "X " + it.productTitle
                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT)
                textView.layoutParams = params
                mContainer.addView(textView)
                price += it.totalPrice.toString().toDouble()
            }
            v.tvTotalPrice.text = Constants.CURRENCY + price.toString()
        }

        var orderStatus = ""
        var colorInt = 0
        when (list[position].status) {

            3 -> {
                orderStatus = context.getString(R.string.completed)
                colorInt = ContextCompat.getColor(context, R.color.green)
                v.tvStatus.visibility = View.VISIBLE
            }

            4 -> {
                orderStatus = context.getString(R.string.cancelled)
                colorInt = ContextCompat.getColor(context, R.color.google_plus)
                v.tvStatus.visibility = View.VISIBLE
            }
        }

        v.tvStatus.text = orderStatus
        v.tvStatus.setTextColor(colorInt)

    }

    override fun getItemCount(): Int {
        return list.size
    }
}
