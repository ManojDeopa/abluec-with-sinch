package com.ablueclive.storeSection.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.storeSection.response.StoreProductListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.makeTextViewResizable
import com.ablueclive.utils.Constants
import com.ablueclive.utils.OnSeeMoreClickListener
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.store_product_list_item.view.*


class CategoryProductAdapter(var list: MutableList<StoreProductListResponse.ProductListing>, var recyclerViewItemClick: RecyclerViewItemClick, var onSeeMoreClick: OnSeeMoreClickListener) : RecyclerView.Adapter<CategoryProductAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(viewType, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvTitle.text = list[position].productTitle
        holder.itemView.tvPrice.text = Constants.CURRENCY + list[position].productPrice.toString()

        holder.itemView.tvDescription.text = list[position].productDescription
        if (holder.itemView.tvDescription.text.length > 250) {
            makeTextViewResizable(holder.itemView.tvDescription, 3, " view more", true, onSeeMoreClick, position)
        }

        val unit = if (list[position].unit.isNullOrEmpty()) "Items" else list[position].unit
        holder.itemView.tvAvail.text = "Available :" + list[position].stock + " $unit"


        var imgURL = Constants.IMAGE_STORE_URL
        val img = list[position].images
        if (!img.isNullOrEmpty()) {
            imgURL = Constants.IMAGE_STORE_URL + img[0]
        }
        CommonMethod.loadImage(imgURL, holder.itemView.ivProductImage)
        holder.itemView.ivCross.setOnClickListener {


            val alert = androidx.appcompat.app.AlertDialog.Builder(context, R.style.alertDialogTheme)
            alert.setTitle(context.getString(R.string.app_name))
            alert.setMessage(context.getString(R.string.delete_product_alert))
            alert.setCancelable(false)
            alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
                recyclerViewItemClick.onRecyclerItemClick(list[position].id.toString(), 0)
                list.removeAt(position)
                notifyItemRemoved(position)
            }

            alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
            }
            alert.show()
        }

        holder.itemView.tvEdit.setOnClickListener {
            Constants.productData = list[position]
            CommonMethod.callStoreContainer(context as Activity, context.getString(R.string.edit_product))
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.store_product_list_item
    }
    
}
