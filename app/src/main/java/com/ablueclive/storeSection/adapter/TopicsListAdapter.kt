package com.ablueclive.storeSection.adapter

import AppUtils
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.response.TopicsResponse
import com.ablueclive.utils.CommonMethod
import kotlinx.android.synthetic.main.topics_list_item.view.*
import java.util.*


class TopicsListAdapter(var list: ArrayList<TopicsResponse.Item>) : RecyclerView.Adapter<TopicsListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                AppUtils.callBrowserIntent(context, list[absoluteAdapterPosition].link.toString())
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.topics_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val v = holder.itemView
        val data = list[position]
        v.tvTitle.text = data.title
        v.tvUrl.text = data.displayLink
        v.tvDescription.text = data.snippet

        if (!data.pageMap?.cseThumbnail.isNullOrEmpty()) {
            CommonMethod.loadImageResize(data.pageMap?.cseThumbnail?.get(0)?.src, v.ivImage)
        }
    }


}
