package com.ablueclive.storeSection.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonContainerActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.fragment.AddExperienceFragment
import com.ablueclive.global.fragment.UpdateAddressFragment.Companion.isAddressUpdated
import com.ablueclive.global.fragment.UpdateAddressFragment.Companion.isChangeAddress
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.CartItemListAdapter
import com.ablueclive.storeSection.response.CartListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.check_out_fragment.*
import kotlinx.android.synthetic.main.recycler_view.*
import kotlin.collections.set

class CheckOutFragment : Fragment(), RecyclerViewItemClick {


    private var totalPrice: Double = 0.0
    private lateinit var cartItemListAdapter: CartItemListAdapter
    var cartItemsList = ArrayList<CartListResponse.Item>()
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        lateinit var data: CartListResponse.CartProducts
        fun newInstance(bundle: Bundle): CheckOutFragment {
            val fragment = CheckOutFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    var flatNo = ""
    var street = ""
    var city = ""
    var state = ""
    var zip = ""
    var country = ""
    var lat = ""
    var lng = ""
    var complete_address = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.check_out_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        init()
    }


    private fun init() {

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        cartItemsList.clear()
        cartItemsList.addAll(data.items!!)


        if (cartItemsList.isNullOrEmpty()) {
            ivEmptyCart.visibility = View.VISIBLE
            ivEmptyCart.setImageResource(R.drawable.empty_cart)
        } else {
            contentLayout.visibility = View.VISIBLE
        }
        cartItemListAdapter = CartItemListAdapter(cartItemsList, this@CheckOutFragment)
        rvCommon.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = cartItemListAdapter
        }

        var address = data.userInfo?.address
        if (isAddressUpdated) {
            address = Constants.myAddress
            isAddressUpdated = false
        }

        if (address != null) {
            flatNo = address.flatno.toString()
            street = address.street.toString()
            city = address.city.toString()
            state = address.state.toString()
            zip = address.zip.toString()
            country = address.country.toString()
            lat = address.lat.toString()
            lng = address._long.toString()
            complete_address = address.completeAddress.toString()
            tvAddress.text = "$flatNo,$street,$city,$state,$country,$zip,$complete_address"
            tvChangeAddress.text = getString(R.string.change_address)
        } else {
            tvAddress.hint = "NA"
            tvChangeAddress.text = getString(R.string.select_address)
        }


        setTotalPrice()

        tvChangeAddress.setOnClickListener {
            isChangeAddress = true
            Constants.myAddress = address
            CommonMethod.callCommonContainer(requireActivity(), getString(R.string.update_address))
        }

        btnCheckout.setOnClickListener {
            createOrder()
        }

    }

    private fun setTotalPrice() {
        try {
            totalPrice = 0.0
            cartItemListAdapter.list.forEach {
                val price = if (it.product?.productPrice == null) "0" else it.product?.productPrice.toString()
                val quantity = if (it.qty == null) 0 else it.qty!!
                val priceWithQty = price.toDouble() * quantity
                totalPrice += priceWithQty
            }
            tvTotalPrice.text = "Total: ${Constants.CURRENCY}" + totalPrice.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun createOrder() {

        if (totalPrice == 0.0) {
            CommonMethod.showToastlong("Invalid Product Price", requireContext())
            return
        }

        if (complete_address.isEmpty()) {
            CommonMethod.showToastlong(getString(R.string.valid_address), requireContext())
            return
        }

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)

        param["flatno"] = flatNo
        param["street"] = street
        param["city"] = city
        param["state"] = state
        param["zip"] = zip
        param["country"] = country

        param["lat"] = lat
        param["long"] = lng
        param["complete_address"] = complete_address
        param["instruction"] = etInstruction.text.toString().trim()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.createOrder(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {

                                /* ivEmptyCart.visibility = View.VISIBLE
                                 ivEmptyCart.setImageResource(R.drawable.thank_u_for_order)
                                 contentLayout.visibility = View.GONE

                                 Handler().postDelayed({
                                      resetAll()
                                  }, 2000)*/

                                val alert = androidx.appcompat.app.AlertDialog.Builder(requireContext(), R.style.alertDialogTheme)
                                alert.setTitle(getString(R.string.app_name))
                                alert.setMessage("Thank you for order !")
                                alert.setCancelable(false)
                                alert.setPositiveButton("SHOP MORE") { dialog: DialogInterface, which: Int ->
                                    dialog.dismiss()
                                    resetAll()
                                }
                                alert.show()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun resetAll() {
        CommonContainerActivity.killActivities()
        // parentPage.getCartList()
        requireActivity().finish()
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        updateCount(text, position.toString())
    }


    private fun updateCount(id: String, qty: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = id
        param["qty"] = qty

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addUpdateProductToCart(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()

                        if (id == ProductDetailsFragment.productId) {
                            ProductDetailsFragment.count = qty
                        }
                        setTotalPrice()
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}