package com.ablueclive.storeSection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreOrderListAdepter
import com.ablueclive.storeSection.response.StoreOrderListResponse
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.lang.Exception
import kotlin.collections.set

class StoreOrderListFragment(var title: String) : Fragment(), RecyclerViewItemClick {

    lateinit var apiCall: Observable<StoreOrderListResponse>
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        var shouldRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_view_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (StoreContainerActivity.isFromContainer) {
            init()
        }
    }

    private fun init() {
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet
        getOrderList()
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            shouldRefresh = false
            getOrderList()
        }
    }


    private fun getOrderList() {

        var status = "1"

        if (title == getString(R.string.in_process)) {
            status = "2"
        }

        val orderCompleted = if (StoreContainerActivity.isFromContainer) getString(R.string.order_completed) else getString(R.string.completed)
        if (title == orderCompleted) {
            status = "3"
        }

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["status"] = status

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        if (StoreContainerActivity.consolidate) {
            param["page"] = "1"
            param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
            apiCall = apiInterface.consolidateAllStoreOrders(param)
        } else {
            param["store_id"] = Constants.myStoreResponse.id.toString()
            apiCall = apiInterface.getOrderByStoreId(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreOrderListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: StoreOrderListResponse) {
                        ProgressD.hide()

                       try {
                           if (response.responseStatus == 1) {
                               val list = response.responseData?.orderInfo

                               if (list.isNullOrEmpty()) {
                                   var msg = "No $title Orders"
                                   if (title == getString(R.string.order_completed)) {
                                       msg = "No $title"
                                   }
                                   CommonMethod.isEmptyView(true, requireContext(), msg)
                               } else {
                                   CommonMethod.isEmptyView(false, requireContext(), "")
                                   recyclerView.apply {
                                       layoutManager = LinearLayoutManager(requireActivity())
                                       adapter = StoreOrderListAdepter(list as MutableList<StoreOrderListResponse.OrderInfo>, this@StoreOrderListFragment, title)
                                   }
                               }
                           }
                       }catch (e:Exception) {
                           e.printStackTrace()
                       }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun changeOrderStatus(text: String, status: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["order_id"] = text
        param["status"] = status

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeOrderStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onStop() {
        super.onStop()
        StoreContainerActivity.isFromContainer = false
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        changeOrderStatus(text, position.toString())
    }
}