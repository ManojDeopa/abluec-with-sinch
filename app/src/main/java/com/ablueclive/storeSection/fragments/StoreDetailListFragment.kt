package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.BuyerStoreCalendarAdapter
import com.ablueclive.storeSection.adapter.StoreDetailListAdapter
import com.ablueclive.storeSection.response.StoreCalender
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_detail_list_fragment.*
import kotlin.collections.set

class StoreDetailListFragment() : Fragment(), RecyclerViewItemClick {


    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        var storeId = ""
    }

    lateinit var storeData: SearchStoreListResponse.NearBuyStore

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.store_detail_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    @SuppressLint("SetTextI18n")
    private fun init() {

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        getStoreById()

    }

    private fun unusedForNow() {
        val list = listOf(getString(R.string.categories), getString(R.string.featured_products_text), getString(R.string.popular_products))
        rvStoreList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = StoreDetailListAdapter(list, this@StoreDetailListFragment)
        }
    }


    override fun onRecyclerItemClick(text: String, position: Int) {
        addRemoveFavourite(text)
    }


    private fun addRemoveFavourite(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val separated = text.split(":".toRegex()).toTypedArray()
        val tag = separated[1]

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = separated[0]

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.customerRemoveFavouriteProduct(param)
        if (tag == "uncheck") {
            apiCall = apiInterface.customerSaveFavouriteProduct(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun getStoreById() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["store_id"] = storeId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val apiCall = apiInterface.getStoreById(param)
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SearchStoreListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SearchStoreListResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {

                                val data = response.responseData?.nearBuyStores
                                if (!data.isNullOrEmpty()) {
                                    storeData = data[0]

                                    CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + storeData.storeInfo?.storeImage, ivStrImg)

                                    SearchStoreDetailFragment.parentPage.tvHeader.text = /*storeData.storeInfo?.storeName*/"Store"

                                    tvStoreName.text = storeData.storeInfo?.storeName

                                    tvAddress.text = storeData.storeInfo?.storeAddress

                                    tvAddress.setOnClickListener {
                                        try {

                                            val lng = storeData.storeInfo?.storeLongitude
                                            val lat = storeData.storeInfo?.storeLatitude

                                            val gpsTracker = GPSTracker(context)
                                            val currentLat = gpsTracker.latitude
                                            val currentLng = gpsTracker.longitude
                                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=$currentLat,$currentLng&daddr=$lat,$lng"))
                                            startActivity(intent)


                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                    }

                                    tvStoreContactNo.text = storeData.storeInfo?.storeContactNumber
                                    tvStoreContactNo.setOnClickListener {
                                        CommonMethod.callIntent(storeData.storeInfo?.storeContactNumber.toString(), requireContext())
                                    }


                                    val orderRating = storeData.storeInfo?.orderRating
                                    if (orderRating != null) {
                                        val averageRating = orderRating.averageRating.toString()
                                        tvStars.text = averageRating
                                        tvRatings.text = orderRating.ratingCount.toString() + " " + " Ratings"
                                    }

                                    val serviceTime = if (storeData.storeInfo?.serviceTime.isNullOrEmpty()) "0" else storeData.storeInfo?.serviceTime
                                    val serviceTimeUnit = if (storeData.storeInfo?.serviceTimeUnit.isNullOrEmpty()) "Minutes" else storeData.storeInfo?.serviceTimeUnit
                                    tvDuration.text = "$serviceTime $serviceTimeUnit"
                                    tvDescription.text = storeData.storeInfo?.storeDescription

                                    /*val calendarList = storeData.storeCalender
                                    if (!calendarList.isNullOrEmpty()) {
                                        rvStoreList.apply {
                                            layoutManager = LinearLayoutManager(requireContext())
                                            adapter = BuyerStoreCalendarAdapter(calendarList)
                                        }
                                    }*/


                                    val dayList = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
                                    val list = storeData.storeCalender
                                    val calendarList = arrayListOf<StoreCalender>()

                                    for (i in dayList.indices) {
                                        val calData = StoreCalender()
                                        calData.dayName = dayList[i]
                                        calendarList.add(calData)
                                    }

                                    if (!list.isNullOrEmpty()) {
                                        list.forEach {
                                            it.dayName = dayList[it.day]
                                            calendarList[it.day] = it
                                        }
                                    }

                                    rvStoreList.apply {
                                        layoutManager = LinearLayoutManager(requireContext())
                                        adapter = BuyerStoreCalendarAdapter(calendarList)
                                    }

                                }

                            }
                        } catch (e: Exception) {
                        e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}