package com.ablueclive.storeSection.fragments

import AppUtils
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.fragment.CountryStateListAdapter
import com.ablueclive.global.fragment.UpdateAddressFragment
import com.ablueclive.global.response.CountryStateListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.HeaderInterceptor
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.simplecropimage.CropImage
import com.ablueclive.storeSection.response.AgentDetailResponse
import com.ablueclive.utils.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_agent_fragment.*
import kotlinx.android.synthetic.main.recycler_with_edittext.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.util.*
import kotlin.collections.HashMap

class AddAgentFragment : Fragment() {

    var TYPE = "TYPE"
    val TYPE_SERVICE_AREA = "TYPE_SERVICE_AREA"
    val TYPE_ADDRESS = "TYPE_ADDRESS"

    private var serviceAreaLat: Double? = null
    private var serviceAreaLng: Double? = null

    private var addressLat: Float? = null
    private var addressLng: Float? = null

    private val AUTOCOMPLETE_REQUEST_CODE = 10
    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12
    private val REQUEST_CODE_CROP_IMAGE = 13

    var countryCode = Constants.CURRENT_COUNTRY_CODE
    var agentId = ""


    private var file: Uri? = null
    var path = ""

    val storeData = Constants.myStoreResponse


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_agent_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
        doOnClicks()
    }

    private fun doOnClicks() {

        ivAddPhoto.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }

        btnSave.setOnClickListener {
            callAddAgentApi()
        }

        etServiceArea.setOnClickListener {
            TYPE = TYPE_SERVICE_AREA
            resetFocus()
            onSearchAddress()
        }

        etAddress.setOnClickListener {
            TYPE = TYPE_ADDRESS
            resetFocus()
            onSearchAddress()
        }

        ivUploadLogo.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }

        btnCancel.setOnClickListener {
            requireActivity().finish()
        }

        btnArrow.setOnClickListener {
            getDetailsOfAgentByEmail()
        }

        etAgentEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                btnArrow.isClickable = AppUtils.isValidEmaillId(s.toString())
                val color = if (!AppUtils.isValidEmaillId(s.toString())) ContextCompat.getColor(requireContext(), R.color.view_color) else ContextCompat.getColor(requireContext(), R.color.colorPrimary)
                btnArrow.setBackgroundColor(color)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        etCountry.setOnClickListener {
            getCountries()
        }

        etState.setOnClickListener {
            getStateFromCountry()
        }

    }

    private fun init() {

        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }
        resetFocus()

        UpdateAddressFragment.country_id = ""
    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    private fun onAddLogoClick() {

        val alertDialog = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun startCropImage(mFileTemp: File) {
        val intent = Intent(requireActivity(), CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 4)
        intent.putExtra(CropImage.ASPECT_Y, 4)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) return

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("AddAgentFragment-", "Place: " + place.name + ", " + place.id + ", " + place.address)
                    val address = place.address
                    if (TYPE == TYPE_SERVICE_AREA) {
                        serviceAreaLat = place.latLng?.latitude
                        serviceAreaLng = place.latLng?.longitude
                        etServiceArea.setText(address)
                    } else {
                        addressLat = place.latLng?.latitude?.toFloat()
                        addressLng = place.latLng?.longitude?.toFloat()
                        etAddress.setText(address)
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    // CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null) {
                try {
                    /*  val file = File(CommonMethods.getRealPathFromURI(requireActivity(), data.data))
                  startCropImage(file)*/
                    val imagePath = CommonMethods.getRealPathFromURI(requireActivity(), data.data)
                    path = imagePath
                    val bitmap = BitmapFactory.decodeFile(imagePath)
                    ivUploadLogo.setImageBitmap(bitmap)
                    tvUploadLogo.visibility = View.GONE
                    ivAddPhoto.visibility = View.VISIBLE
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                Log.d("TAG", "you don`t pic any image")
            }
        }


        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                startCropImage(File(CommonMethods.getRealPathFromURI(requireActivity(), file)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        if (requestCode == REQUEST_CODE_CROP_IMAGE) {
            try {
                val imagePath = data!!.getStringExtra(CropImage.IMAGE_PATH) ?: return
                path = imagePath
                val bitmap = BitmapFactory.decodeFile(imagePath)
                ivUploadLogo.setImageBitmap(bitmap)
                tvUploadLogo.visibility = View.GONE
                ivAddPhoto.visibility = View.VISIBLE
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureFromGallery()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun callAddAgentApi() {

        if (!isValidate()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")


        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("store_id", storeData.id.toString())
        builder.addFormDataPart("session_token", BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN))
        builder.addFormDataPart("email", etAgentEmail.text.toString())
        builder.addFormDataPart("name", etAgentName.text.toString())
        builder.addFormDataPart("mobile", etAgentPhone.text.toString())
        builder.addFormDataPart("service_area_lat", serviceAreaLat.toString())
        builder.addFormDataPart("service_area_long", serviceAreaLng.toString())
        builder.addFormDataPart("service_area_address", etServiceArea.text.toString().trim())
        builder.addFormDataPart("password", etPassword.text.toString())

        builder.addFormDataPart("city", etCity.text.toString())
        builder.addFormDataPart("state", etState.text.toString())
        builder.addFormDataPart("zip", etZipCode.text.toString())
        builder.addFormDataPart("country", etCountry.text.toString())

        builder.addFormDataPart("flatno", etFlatNo.text.toString())
        builder.addFormDataPart("street", etStreet.text.toString())
        builder.addFormDataPart("complete_address", etAddress.text.toString())
        builder.addFormDataPart("lat", addressLat.toString())
        builder.addFormDataPart("long", addressLng.toString())

        builder.addFormDataPart("country_code", countryCode)
        builder.addFormDataPart("agent_id", agentId)


        if (path.isNotEmpty()) {
            val imageFile = File(path)
            builder.addFormDataPart("profile_image", imageFile.name, imageFile.asRequestBody("image/jpg".toMediaTypeOrNull()))
        }

        val requestBody = builder.build()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val apiCall = apiInterface.saveAgent(requestBody)


        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            showAlert(response.responseMsg)
                        } else {
                            CommonMethod.showToastlong(response.responseMsg, requireActivity())
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etAgentEmail.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_agent_email_validation), requireContext())
                false
            }

            !AppUtils.isValidEmaillId(etAgentEmail.text.toString()) -> {
                CommonMethod.showToastlong(getString(R.string.enter_valid_email), requireContext())
                false
            }

            etAgentName.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_agent_name_validation), requireContext())
                false
            }
            etAgentPhone.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_agent_phone_validation), requireContext())
                false
            }

            etServiceArea.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.select_service_area), requireContext())
                return false
            }

            else -> true
        }
    }

    private fun getDetailsOfAgentByEmail() {

        if (etAgentEmail.text.toString().trim().isEmpty()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = storeData.id.toString()
        param["email"] = etAgentEmail.text.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getDetailsOfAgentByEmail(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<AgentDetailResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: AgentDetailResponse) {
                        ProgressD.hide()

                        if (response.responseStatus == 1) {
                            contentLayout.visibility = View.VISIBLE
                            imgLayout.visibility = View.VISIBLE
                            btnArrow.visibility = View.GONE
                            etLoginId.setText(etAgentEmail.text.toString())
                            response.responseData?.let { fillAgentData(it) }
                        } else {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun fillAgentData(data: AgentDetailResponse.ResponseData) {

        val agentData = data.agentInfo!!
        etAgentName.setText(agentData.name)
        etAgentPhone.setText(agentData.mobile)
        agentId = agentData.id

        if (agentData.profileImage.trim().isNotEmpty()) {
            ivAddPhoto.visibility = View.VISIBLE
            tvUploadLogo.visibility = View.GONE
            CommonMethod.loadImageResize(Constants.PROFILE_IMAGE_URL + agentData.profileImage, ivUploadLogo)

            val addressData = agentData.address
            if (addressData != null) {
                nullCheck(addressData.flatno, etFlatNo)
                nullCheck(addressData.street, etStreet)
                nullCheck(addressData.country, etCountry)
                nullCheck(addressData.state, etState)
                nullCheck(addressData.city, etCity)
                nullCheck(addressData.zip, etZipCode)
                nullCheck(addressData.completeAddress, etAddress)
                addressLat = addressData.lat
                addressLng = addressData._long
            }
        }

    }

    private fun nullCheck(value: String, editText: AppCompatEditText) {
        if (value.isNotEmpty()) {
            editText.setText(value)
        }

    }


    private fun getCountries() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val params = java.util.HashMap<String, String>()
        params["token"] = HeaderInterceptor.X_TOKEN

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getCountries(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CountryStateListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CountryStateListResponse) {
                        ProgressD.hide()

                        if (response.responseStatus == 1) {
                            etState.text = null
                            etState.hint = getString(R.string.select)
                            showCountryStateList(response.responseData?.countries!!, etCountry)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun getStateFromCountry() {

        if (UpdateAddressFragment.country_id.isEmpty()) {
            CommonMethod.showToastlong("Please Select Country First", requireContext())
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val params = java.util.HashMap<String, String>()
        params["country_id"] = UpdateAddressFragment.country_id

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getStateFromCountry(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CountryStateListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CountryStateListResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {
                                showCountryStateList(response.responseData?.state!!, etState)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun showCountryStateList(list: List<CountryStateListResponse.CountryState>, editText: AppCompatEditText) {


        val dialog = Dialog(requireActivity(), R.style.alertDialogTheme)
        /* dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)*/
        dialog.setContentView(R.layout.recycler_with_edittext)


        val mAdapter = CountryStateListAdapter(list, editText, dialog)

        dialog.rvList.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }



        dialog.ivSearchTop.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString(), mAdapter, list)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        dialog.show()


    }

    fun filter(text: String, adapter: CountryStateListAdapter, list: List<CountryStateListResponse.CountryState>) {
        val filterList = mutableListOf<CountryStateListResponse.CountryState>()
        list.forEach {
            if (it.name.toLowerCase(Locale.getDefault()).contains(text.toLowerCase(Locale.getDefault()))) {
                filterList.add(it)
            }
        }
        adapter.filterList(filterList)
    }

}