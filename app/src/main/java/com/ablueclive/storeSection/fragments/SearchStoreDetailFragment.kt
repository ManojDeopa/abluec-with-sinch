package com.ablueclive.storeSection.fragments

import AppUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.response.CartListResponse
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.popupMenuIconTextList
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.common_search_header.*
import kotlinx.android.synthetic.main.search_store_detail_fragment.*
import kotlin.collections.set


class SearchStoreDetailFragment(var title: String) : Fragment() {

    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""
    var user_id = ""
    lateinit var cartCount: AppCompatTextView
    lateinit var rLCart: RelativeLayout
    lateinit var tvHeader: TextView

    companion object {
        lateinit var parentPage: SearchStoreDetailFragment
        var shouldCartListRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_store_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getCartList()
    }

    override fun onResume() {
        super.onResume()
        if (shouldCartListRefresh) {
            shouldCartListRefresh = false
            getCartList()
        }
    }


    private fun init() {
        tvHeader = tvTitle
        parentPage = this
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        user_id = BMSPrefs.getString(context, Constants._ID)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet
        cartCount = tvCartCount
        rLCart = ivCart

        ivDots.visibility = View.VISIBLE
        // ivCart.visibility = View.VISIBLE
        ivBack.setOnClickListener {
            requireActivity().finish()
        }

        ivDots.setOnClickListener {
            // showListPopupWindow(it)
            onMenuDotsClick()
        }

        ivSearch.setOnClickListener {
            Constants.SEARCH_TYPE = getString(R.string.products)
            CommonMethod.callCommonContainer(requireActivity(), getString(R.string.search_all))
        }

        when (title) {

            getString(R.string.search_store_detail) -> {
                AppUtils.addFragment(requireActivity(), StoreDetailListFragment(), R.id.storeDetailContainer)
            }

            getString(R.string.categories) -> {
                AppUtils.addFragment(requireActivity(), StoreCategoryTabFragment(), R.id.storeDetailContainer)
            }

            getString(R.string.featured_products_text) -> {
                AppUtils.addFragment(requireActivity(), ProductDetailsFragment(), R.id.storeDetailContainer)
            }

            getString(R.string.popular_products) -> {
                AppUtils.addFragment(requireActivity(), ProductDetailsFragment(), R.id.storeDetailContainer)
            }

        }

    }


    private fun onMenuDotsClick() {

        val textList = arrayOf(getString(R.string.your_order), getString(R.string.favourite_product))
        val iconList = arrayOf(R.drawable.manage_products, R.drawable.ic_heart_checked)
        popupMenuIconTextList(requireContext(), ivDots, textList, iconList, object : MenuItemClickListener {
            override fun onMenuItemClick(text: String, position: Int) {

                if (position == 0) {
                    // CommonMethod.callCommonContainer(requireActivity(), getString(R.string.your_order))
                } else {
                    CommonMethod.callCommonContainer(requireActivity(), getString(R.string.favourite_product))
                }

            }
        })
    }


    fun getCartList() {

        return

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader?.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = user_id

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getCart(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CartListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CartListResponse) {
                        pbHeader?.visibility = View.GONE
                        if (response.responseStatus == 1) {
                            val list = response.responseData?.cartProducts?.items
                            if (!list.isNullOrEmpty()) {
                                cartCount.visibility = View.VISIBLE
                                cartCount.text = list.size.toString()
                            } else {
                                cartCount.visibility = View.GONE
                            }

                            rLCart.setOnClickListener {
                                CommonContainerActivity.addActivities(requireActivity())
                                CheckOutFragment.data = response.responseData?.cartProducts!!
                                CommonMethod.callCommonContainer(requireActivity(), getString(R.string.check_out))
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        pbHeader?.visibility = View.GONE
                        println(e.message)
                    }
                })
    }


}