package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreDetailMenuAdapter
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.clone_store_dialog.*
import kotlinx.android.synthetic.main.clone_store_dialog.view.*
import kotlinx.android.synthetic.main.store_detail_fragment.*
import java.util.*
import kotlin.collections.HashMap


class StoreDetailFragment : Fragment() {

    val data = Constants.myStoreResponse
    var copyCategory = "1"
    var copyProducts = "1"
    var copyStoreCalendar = "0"
    var storeNameCopy = ""

    companion object {
        var storeId = ""
        var shouldRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.store_detail_fragment, container, false)

    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            shouldRefresh = false
            getStoreById()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        renderData()
    }

    private fun renderData() {

        CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + data?.storeImage, ivStoreImage)

        btnDeleteStore.setOnClickListener {
            showDeleteAlert()
        }

        tvEditStore.setOnClickListener {
            CommonMethod.callStoreContainer(requireActivity(), getString(R.string.edit_store))
        }

        btnCloneStore.setOnClickListener {
            showCloneStoreDialog()
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = StoreDetailMenuAdapter(requireActivity())
        }


        var isStoreOn = false
        val cList = data?.storeCalender
        if (!cList.isNullOrEmpty()) {
            cList.forEach {
                if (ManageStoreFragment.currentDayId == it.day.toString()) {
                    isStoreOn = true
                }
            }
        }

        if (isStoreOn) {
            switchForSales.visibility = View.VISIBLE
            switchForSales.isChecked = data.isOpen != 0
            switchForSales.text = if (switchForSales.isChecked) getString(R.string.store_is_open) else getString(R.string.store_is_close)

            switchForSales.setOnCheckedChangeListener { buttonView, isChecked ->
                val status = if (isChecked) "1" else "0"
                switchForSales.text = if (isChecked) getString(R.string.store_is_open) else getString(R.string.store_is_close)
                changeAvailabilityStatus(status)
            }
        }
    }

    private fun showDeleteAlert() {
        val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.delete_store))
        alert.setMessage(getString(R.string.delete_store_alert_text))
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
            deleteStore()
            dialog.dismiss()
        }

        alert.setNegativeButton(getString(R.string.no)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        alert.show()
    }


    private fun showCloneStoreDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup: ViewGroup = requireActivity().findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(requireContext()).inflate(R.layout.clone_store_dialog, viewGroup, false)
        val builder = AlertDialog.Builder(requireContext())
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setCancelable(false)
        alertDialog.show()

        CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + data.storeImage, dialogView.ivImg)
        dialogView.tvStoreName.text = data.storeName

        storeNameCopy = data.storeName.toString() + "_Copy"

        dialogView.btnDone.setOnClickListener {
            cloneStore(alertDialog)
        }

        dialogView.ivCross.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.switchCopyCategory.setOnCheckedChangeListener { buttonView, isChecked ->
            copyCategory = if (isChecked) "1" else "0"
        }

        alertDialog.switchCopyProducts.setOnCheckedChangeListener { buttonView, isChecked ->
            copyProducts = if (isChecked) "1" else "0"
        }

        alertDialog.switchCopyCalendar.setOnCheckedChangeListener { buttonView, isChecked ->
            copyStoreCalendar = if (isChecked) "1" else "0"
        }

    }


    private fun cloneStore(alertDialog: AlertDialog) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = data.id.toString()

        param["copy_category"] = copyCategory
        param["copy_products"] = copyProducts
        param["copy_calender"] = copyStoreCalendar
        param["store_name"] = storeNameCopy

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.cloneStore(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            alertDialog.dismiss()
                            showAlert(response.responseMsg)
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun changeAvailabilityStatus(status: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = data.id.toString()
        param["available_status"] = status

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeAvailabilityStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            val str = response.responseMsg.toString()
                            val msg = str.substring(0, 1).toUpperCase(Locale.getDefault()) + str.substring(1)
                            CommonMethod.showAlert(requireContext(), msg)
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun deleteStore() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = data.id.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteStoreApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            showAlert(response.responseMsg)
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(text: String?) {
        val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }


    private fun getStoreById() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["store_id"] = storeId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val apiCall = apiInterface.getStoreById(param)
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SearchStoreListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SearchStoreListResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                val store = response.responseData?.nearBuyStores
                                if (!store.isNullOrEmpty()) {
                                    val storeData = store[0]
                                    data.storeImage = storeData.storeInfo?.storeImage
                                    data.storeName = storeData.storeInfo?.storeName
                                    data.storeContactNumber = storeData.storeInfo?.storeContactNumber
                                    data.storeAddress = storeData.storeInfo?.storeAddress
                                    data.storeCompleteAddress = storeData.storeInfo?.storeCompleteAddress
                                    data.serviceTime = storeData.storeInfo?.serviceTime
                                    data.serviceTimeUnit = storeData.storeInfo?.serviceTimeUnit
                                    data.storeDescription = storeData.storeInfo?.storeDescription
                                    data.loc?.coordinates = storeData.storeInfo?.loc?.coordinates
                                    data.storeCalender = storeData.storeCalender
                                    data.isOpen = storeData.storeInfo?.isAvailable
                                    renderData()
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}
