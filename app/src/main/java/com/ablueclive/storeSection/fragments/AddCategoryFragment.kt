package com.ablueclive.storeSection.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.response.AdminCategoryResponse
import com.ablueclive.utils.*
import com.ablueclive.simplecropimage.CropImage
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_category_fragment.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

class AddCategoryFragment : Fragment() {


    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12
    private val REQUEST_CODE_CROP_IMAGE = 13


    private var file: Uri? = null
    var path = ""
    var catParentId = ""
    var catName = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_category_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        doOnClicks()

        resetFocus()

    }

    private fun doOnClicks() {


        btnAddNow.setOnClickListener {

            if (etSelectCategory.isEnabled) {
                selectCategoryFromList()
            } else {
                callAddStoreApi()
            }

        }

        etSelectCategory.setOnClickListener {
            onCatSelect()
            resetFocus()
            getAdminCategory()
        }

        ivUploadImage.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }

        tvAddOtherCat.setOnClickListener {
            onOtherCatSelect()
        }


        etCategoryName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                etSelectCategory.isEnabled = s.toString().isEmpty()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private fun onOtherCatSelect() {
        etCategoryName.requestFocus()
        etCategoryName.visibility = View.VISIBLE
        etSelectCategory.text = null
        etSelectCategory.hint = getString(R.string.select_category)
    }


    private fun onCatSelect() {
        etCategoryName.visibility = View.GONE
        etCategoryName.hint = getString(R.string.category_name)
    }


    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    private fun onAddLogoClick() {

        val alertDialog = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun startCropImage(mFileTemp: File) {
        val intent = Intent(requireActivity(), CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 4)
        intent.putExtra(CropImage.ASPECT_Y, 4)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) return


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null) {
                val file = File(CommonMethods.getRealPathFromURI(requireActivity(), data.data))
                startCropImage(file)
            } else {
                Log.d("TAG", "you don`t pic any image")
            }
        }


        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                startCropImage(File(CommonMethods.getRealPathFromURI(requireActivity(), file)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        if (requestCode == REQUEST_CODE_CROP_IMAGE) {
            val imagePath = data!!.getStringExtra(CropImage.IMAGE_PATH) ?: return
            path = imagePath
            val bitmap = BitmapFactory.decodeFile(imagePath)
            ivUploadImage.setImageBitmap(bitmap)
            tvUploadImage.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureFromGallery()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun getAdminCategory() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.adminCategoryListing(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<AdminCategoryResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: AdminCategoryResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            response.responseData?.categoryListing?.let { showCategoryList(it) }
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun showCategoryList(categoryListing: List<AdminCategoryResponse.CategoryListing>) {

        val nameList = arrayListOf<String>()
        categoryListing.forEach {
            nameList.add(it.name.toString())
        }

        val builder: androidx.appcompat.app.AlertDialog.Builder = androidx.appcompat.app.AlertDialog.Builder(requireActivity())
        val list = nameList.toTypedArray()
        val checkedItem = list.indexOf(catName)
        builder.setSingleChoiceItems(list, checkedItem) { dialog, which ->
            catName = list[which]
            catParentId = categoryListing[which].id.toString()
        }
        builder.setPositiveButton("OK") { dialog, which ->
            etSelectCategory.setText(catName)
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel", null)
        val dialog: androidx.appcompat.app.AlertDialog = builder.create()
        dialog.show()
    }


    private fun selectCategoryFromList() {

        if (!isValidate()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["store_id"] = Constants.myStoreResponse.id.toString()
        param["category_id"] = catParentId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.selectCategoryFromList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            showAlert(response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun callAddStoreApi() {

        if (!isValidate()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")


        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN))
        builder.addFormDataPart("user_id", BMSPrefs.getString(requireActivity(), Constants._ID))
        builder.addFormDataPart("store_id", Constants.myStoreResponse.id.toString())
        builder.addFormDataPart("parent_id", "0")
        if (etSelectCategory.isEnabled) {
            builder.addFormDataPart("name", etSelectCategory.text.toString())
        } else {
            builder.addFormDataPart("name", etCategoryName.text.toString())
        }

        if (path.isNotEmpty()) {
            val imageFile = File(path)
            builder.addFormDataPart("cat_image", imageFile.name, imageFile.asRequestBody("image/jpg".toMediaTypeOrNull()))
        }

        val requestBody = builder.build()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val apiCall = apiInterface.addCategory(requestBody)


        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            showAlert(response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        if (etSelectCategory.isEnabled) {
            if (etSelectCategory.text.toString().trim().isEmpty()) {
                CommonMethod.showToastlong(getString(R.string.select_category_validation), requireContext())
                return false
            }
        } else {
            if (etCategoryName.text.toString().trim().isEmpty()) {
                CommonMethod.showToastlong(getString(R.string.enter_category_name_validation), requireContext())
                return false
            }
        }

        return true

    }
}