package com.ablueclive.storeSection.fragments

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.simplecropimage.CropImage
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_store_fragment.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.util.*

class AddStoreFragment : Fragment() {

    private var storeLat: Double? = null
    private var storeLng: Double? = null

    private val AUTOCOMPLETE_REQUEST_CODE = 10
    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12
    private val REQUEST_CODE_CROP_IMAGE = 13

    var selectedUnit = ""


    private var file: Uri? = null
    var path = ""

    val storeData = Constants.myStoreResponse


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_store_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        doOnClicks()

    }

    private fun doOnClicks() {

        ivAddPhoto.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }

        btnSubmit.setOnClickListener {
            callAddStoreApi()
        }

        etAddress.setOnClickListener {
            resetFocus()
            onSearchAddress()
        }

        etServiceTimeUnit.setOnClickListener {
            selectTimeUnitList()
        }

        ivUploadLogo.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }
    }

    private fun init() {

        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }

        resetFocus()

        if (StoreContainerActivity.isEdit) {
            tvUploadLogo.visibility = View.GONE
            ivAddPhoto.visibility = View.VISIBLE
            CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + storeData.storeImage, ivUploadLogo)

            nullCheck(storeData.storeName, etStoreName)
            nullCheck(storeData.storeContactNumber, etStorePhoneNo)
            nullCheck(storeData.storeAddress, etAddress)
            nullCheck(storeData.storeCompleteAddress, etCompleteAddress)

            nullCheck(storeData.serviceTime, etServiceTime)
            nullCheck(storeData.serviceTimeUnit, etServiceTimeUnit)
            nullCheck(storeData.storeDescription, etDescription)

            storeLng = storeData.loc?.coordinates?.get(0)?.toDouble()
            storeLat = storeData.loc?.coordinates?.get(1)?.toDouble()

            btnSubmit.text = getString(R.string.save)
        }
    }

    private fun nullCheck(value: String?, editText: AppCompatEditText?) {
        if (!value.isNullOrEmpty()) {
            editText?.setText(value.toString())
        }

    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    private fun selectTimeUnitList() {
        val builder: androidx.appcompat.app.AlertDialog.Builder = androidx.appcompat.app.AlertDialog.Builder(requireActivity())
        val list = arrayOf("Hours", "Minutes")
        val checkedItem = list.indexOf(selectedUnit)
        builder.setSingleChoiceItems(list, checkedItem) { dialog, which ->
            selectedUnit = list[which]
        }
        builder.setPositiveButton("Ok") { dialog, which ->
            etServiceTimeUnit.setText(selectedUnit)
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel", null);
        val dialog: androidx.appcompat.app.AlertDialog = builder.create()
        dialog.show()
    }

    private fun onAddLogoClick() {

        val alertDialog = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun startCropImage(mFileTemp: File) {
        val intent = Intent(requireActivity(), CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 4)
        intent.putExtra(CropImage.ASPECT_Y, 4)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }


    private fun getAllAddress(latLng: LatLng?) {
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(requireContext(), Locale.getDefault())
            addresses = geocoder.getFromLocation(latLng?.latitude!!, latLng.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                etCompleteAddress.setText(address)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) return

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("AddStoreFragment-", "Place: " + place.name + ", " + place.id + ", " + place.address)

                    storeLat = place.latLng?.latitude
                    storeLng = place.latLng?.longitude

                    val address = place.address
                    etAddress.setText(address)

                    getAllAddress(place.latLng)

                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    // CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null) {
                /*val file = File(CommonMethods.getRealPathFromURI(requireActivity(), data.data))
                startCropImage(file)*/

                try {
                    val imagePath = CommonMethods.getRealPathFromURI(requireActivity(), data.data)
                    path = imagePath
                    val bitmap = BitmapFactory.decodeFile(imagePath)
                    ivUploadLogo.setImageBitmap(bitmap)
                    tvUploadLogo.visibility = View.GONE
                    ivAddPhoto.visibility = View.VISIBLE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Log.d("TAG", "you don`t pic any image")
            }
        }


        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                startCropImage(File(CommonMethods.getRealPathFromURI(requireActivity(), file)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        if (requestCode == REQUEST_CODE_CROP_IMAGE) {
            try {
                val imagePath = data!!.getStringExtra(CropImage.IMAGE_PATH) ?: return
                path = imagePath
                val bitmap = BitmapFactory.decodeFile(imagePath)
                ivUploadLogo.setImageBitmap(bitmap)
                tvUploadLogo.visibility = View.GONE
                ivAddPhoto.visibility = View.VISIBLE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureFromGallery()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun callAddStoreApi() {

        if (!isValidate()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN))
        builder.addFormDataPart("user_id", BMSPrefs.getString(requireActivity(), Constants._ID))
        builder.addFormDataPart("store_name", etStoreName.text.toString())
        builder.addFormDataPart("store_contact_number", etStorePhoneNo.text.toString())
        builder.addFormDataPart("store_description", etDescription.text.toString())
        builder.addFormDataPart("store_address", etAddress.text.toString())
        builder.addFormDataPart("store_complete_address", etCompleteAddress.text.toString())
        builder.addFormDataPart("store_latitude", storeLat.toString())
        builder.addFormDataPart("store_longitude", storeLng.toString())
        builder.addFormDataPart("service_time", etServiceTime.text.toString())
        builder.addFormDataPart("service_time_unit", etServiceTimeUnit.text.toString())



        if (StoreContainerActivity.isEdit) {
            builder.addFormDataPart("store_id", storeData.id.toString())
        }

        if (path.isNotEmpty()) {
            val imageFile = File(path)
            builder.addFormDataPart("store_image", imageFile.name, RequestBody.create("image/jpg".toMediaTypeOrNull(), imageFile))
        }

        val requestBody = builder.build()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.addStoreApi(requestBody)
        if (StoreContainerActivity.isEdit) {
            apiCall = apiInterface.editStore(requestBody)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            if (StoreContainerActivity.isEdit) StoreDetailFragment.shouldRefresh = true
                            showAlert(response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etStoreName.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_store_name_validation), requireContext())
                false
            }

            etStorePhoneNo.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter) + " " + getString(R.string.store_phone_no), requireContext())
                false
            }

            etAddress.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_store_address_validation), requireContext())
                false
            }

            etCompleteAddress.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter) + " " + getString(R.string.complete_address), requireContext())
                false
            }

            /* etServiceTime.text.toString().trim().isEmpty() -> {
                 CommonMethod.showToastlong(getString(R.string.please_enter_service_time), requireContext())
                 false
             }

             etServiceTimeUnit.text.toString().trim().isEmpty() -> {
                 CommonMethod.showToastlong(getString(R.string.please_select_service_time_unit), requireContext())
                 false
             }*/

            etDescription.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_description_validation), requireContext())
                false
            }

            else -> true
        }
    }

}