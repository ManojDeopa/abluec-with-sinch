package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreCalendarAdapter
import com.ablueclive.storeSection.response.StoreCalendarRequest
import com.ablueclive.storeSection.response.StoreCalendarResponse
import com.ablueclive.storeSection.response.StoreCalender
import com.ablueclive.utils.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_calendar_fragment.*
import java.util.*
import kotlin.collections.HashMap


class StoreCalendarFragment : Fragment() {


    lateinit var adapterStoreCalendar: StoreCalendarAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.store_calendar_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getStoreCalendarData()

        btnSave.setOnClickListener {
            setStoreCalender()
        }


    }


    private fun setStoreCalender() {

        val sessionToken = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        val userId = BMSPrefs.getString(requireActivity(), Constants._ID)
        val storeId = Constants.myStoreResponse.id.toString()
        val timeZone = TimeZone.getDefault().id

        val dataList = arrayListOf<StoreCalendarRequest>()

        var isValidated = false

        for (i in adapterStoreCalendar.list.indices) {
            val it = adapterStoreCalendar.list[i]


            val requestModel = StoreCalendarRequest()
            requestModel.store_id = storeId
            requestModel.user_id = userId
            requestModel.day = it.day.toString()

            requestModel.from_time = it.fromTime.toString()
            requestModel.to_time = it.toTime.toString()

            if (!it.isValidated) {
                requestModel.from_time = ""
                requestModel.to_time = ""
            } else {
                isValidated = true
            }

            requestModel.utc_from_time = it.utcFromTime.toString()
            requestModel.utc_to_time = it.utcToTime.toString()
            requestModel.time_zone = timeZone
            dataList.add(requestModel)

        }


        if (!isValidated) {
            //CommonMethod.showToastlong("Please Add Time Slot !", requireContext())
            //return
        }

        val calendarData: String = Gson().toJson(dataList)

        val jsonParser = JsonParser()
        val jsonArray = jsonParser.parse(calendarData).asJsonArray


        val req = JsonObject()
        req.addProperty("session_token", sessionToken)
        req.add("calender_data", jsonArray)

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.setStoreCalender(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            StoreDetailFragment.shouldRefresh = true
                            getStoreCalendarData()
                            CommonMethod.showToastShort(response.responseMsg, requireContext())
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun changeAvailabilityStatus(status: String) {

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = Constants.myStoreResponse.id.toString()
        param["available_status"] = status

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeAvailabilityStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        println(response)
                    }

                    override fun onError(e: Throwable) {
                        println(e.message)
                    }
                })
    }


    private fun getStoreCalendarData() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = Constants.myStoreResponse.id.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getStoreCalender(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreCalendarResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: StoreCalendarResponse) {

                        ProgressD.hide()
                        try {
                            val dayList = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
                            val list = response.responseData?.storeCalender
                            val calendarList = arrayListOf<StoreCalender>()

                            for (i in dayList.indices) {
                                val calData = StoreCalender()
                                calData.dayName = dayList[i]
                                calendarList.add(calData)
                            }

                            if (!list.isNullOrEmpty()) {
                                changeAvailabilityStatus("1")
                                list.forEach {
                                    it.dayName = dayList[it.day]
                                    calendarList[it.day] = it
                                }
                            } else {
                                changeAvailabilityStatus("0")
                            }

                            adapterStoreCalendar = StoreCalendarAdapter(calendarList)
                            recyclerView.apply {
                                layoutManager = LinearLayoutManager(requireActivity())
                                adapter = adapterStoreCalendar
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}