package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.CategoryProductAdapter
import com.ablueclive.storeSection.response.StoreProductListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.product_list.*
import kotlinx.android.synthetic.main.progress_horizontal.view.*
import kotlin.collections.set


class ManageProductListFragment(var categoryId: String) : Fragment(), RecyclerViewItemClick, EditTextChangeListener, OnSeeMoreClickListener {

    companion object {
        var shouldRefresh = false
    }

    lateinit var recyclerViewState: Parcelable

    private var isProductLoading = false
    private lateinit var categoryProductsAdapter: CategoryProductAdapter
    var productListing = ArrayList<StoreProductListResponse.ProductListing>()
    lateinit var linearLayoutManager: LinearLayoutManager
    private var rowCount = 10
    var page = 1
    var searchText = ""
    var storeId = Constants.myStoreResponse.id.toString()

    var lat = 0.0
    var lng = 0.0

    var v = ManageProductsFragment.parentView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.product_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        storeCategoryProducts()
    }


    private fun init() {

        ManageProductsFragment.textListener = this

        val gpsTracker = GPSTracker(requireContext())
        lat = gpsTracker.latitude
        lng = gpsTracker.longitude

        categoryProductsAdapter = CategoryProductAdapter(productListing, this@ManageProductListFragment, this)
        linearLayoutManager = LinearLayoutManager(requireContext())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = categoryProductsAdapter
        }


        /* recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
             override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                 super.onScrolled(recyclerView, dx, dy)
                 val visibleItemCount = linearLayoutManager.childCount
                 val totalItemCount = linearLayoutManager.itemCount
                 val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                 if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isProductLoading) {
                     isProductLoading = true
                     storeCategoryProducts()
                 }
             }
         })*/


        scrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (scrollY > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isProductLoading) {
                    isProductLoading = true
                    storeCategoryProducts()
                }
            }
        })
    }


    private fun storeCategoryProducts() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        v.pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = storeId
        param["category_id"] = categoryId
        param["row_count"] = rowCount.toString()
        param["page"] = page.toString()
        param["search_txt"] = searchText
        param["lat"] = lat.toString()
        param["long"] = lng.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getAllChildCategoryProducts(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreProductListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: StoreProductListResponse) {

                        try {
                            v.pbHeader.visibility = View.GONE
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.productListing
                                if (!list.isNullOrEmpty()) {
                                    productListing.addAll(list)
                                    isProductLoading = false
                                    page++
                                    categoryProductsAdapter.notifyItemRangeInserted(categoryProductsAdapter.itemCount, productListing.size)
                                }
                            }
                        } catch (e: Exception) {
                        e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        v.pbHeader.visibility = View.GONE
                        println(e.message)
                    }
                })
    }


    private fun deleteProduct(productId: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = storeId
        param["product_id"] = productId

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteProductApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        when (position) {
            0 -> {
                deleteProduct(text)
            }

        }
    }

    override fun onTextChange(text: String) {
        apiCallOnTextChange(text)
    }


    private fun apiCallOnTextChange(text: String) {

        if (categoryId.isNotEmpty()) {
            productListing.clear()
            categoryProductsAdapter.notifyDataSetChanged()
            page = 1
            searchText = text
            storeCategoryProducts()
        }
    }

    override fun onSeeMoreClick(text: String, position: Int) {

        if (text == "collapse") {

            if (position == productListing.size - 1) {
                recyclerView.layoutManager?.onRestoreInstanceState(recyclerViewState)
            } else {
                val y = recyclerView.getChildAt(position).y.toInt()
                scrollView.post {
                    scrollView.fling(0)
                    scrollView.smoothScrollTo(0, y)
                }
            }

        } else {
            recyclerViewState = recyclerView.layoutManager?.onSaveInstanceState()!!
        }


    }
}