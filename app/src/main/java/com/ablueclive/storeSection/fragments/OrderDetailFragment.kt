package com.ablueclive.storeSection.fragments

import AppUtils
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.AppGlobalTask
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.response.StoreOrderListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.order_detail_fragment.*
import kotlin.collections.set

class OrderDetailFragment : Fragment() {

    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object {
        lateinit var data: StoreOrderListResponse.OrderInfo
        fun newInstance(bundle: Bundle): OrderDetailFragment {
            val fragment = OrderDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.order_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    @SuppressLint("SetTextI18n")
    private fun init() {

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet


        tvOrderId.text = "Order #${data.custom_order_id}"
        tvOrderTime.text = "Order Time: " + AppUtils.convertToDateTime(data.createdAt)

        var storeImage = Constants.IMAGE_STORE_URL
        val imageObj = data.store?.images
        if (!imageObj.isNullOrEmpty()) {
            storeImage += imageObj[0].imageName.toString()
        }
        CommonMethod.loadImage(storeImage, ivImage)

        tvStoreName.text = data.store?.storeName

        tvAddress.text = data.store?.storeCompleteAddress


        val status = data.status
        var orderStatus = ""
        var colorInt = 0

        when (status) {

            1 -> {
                orderStatus = getString(R.string.pending_text)
                colorInt = ContextCompat.getColor(requireContext(), R.color.grayDark)
            }

            2 -> {
                orderStatus = getString(R.string.processed_text)
                colorInt = ContextCompat.getColor(requireContext(), R.color.yellow)
            }

            3 -> {
                orderStatus = getString(R.string.completed)
                colorInt = ContextCompat.getColor(requireContext(), R.color.green)
            }

            4 -> {
                orderStatus = getString(R.string.cancelled)
                colorInt = ContextCompat.getColor(requireContext(), R.color.google_plus)
            }

        }

        tvOrderStatus.text = orderStatus
        tvOrderStatus.setTextColor(colorInt)


        ivStoreLoc.setOnClickListener {
            try {

                val lng = data.store?.storeLongitude
                val lat = data.store?.storeLatitude

                val gpsTracker = GPSTracker(requireContext())
                val currentLat = gpsTracker.latitude
                val currentLng = gpsTracker.longitude
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=$currentLat,$currentLng&daddr=$lat,$lng"))
                startActivity(intent)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        tvStoreContactNo.text = data.store?.storeContactNumber
        tvStoreContactNo.setOnClickListener {
            CommonMethod.callIntent(data.store?.storeContactNumber.toString(), requireContext())
        }


        val mContainer = itemsContainer
        mContainer.removeAllViews()
        val items = data.product
        if (!items.isNullOrEmpty()) {
            var price = 0.0
            items.forEach {

                val llParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1f)

                val llh = LinearLayout(requireContext())
                llh.orientation = LinearLayout.HORIZONTAL
                llh.layoutParams = llParams

                llh.removeAllViews()

                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1f)

                val textView1 = TextView(context)
                textView1.text = it.qty.toString() + "x " + it.productTitle
                textView1.layoutParams = params

                val textView2 = TextView(context)
                textView2.text = Constants.CURRENCY + it.totalPrice
                textView2.layoutParams = params
                textView2.gravity = Gravity.END

                llh.addView(textView1)
                llh.addView(textView2)

                mContainer.addView(llh)
                price += it.totalPrice.toString().toDouble()
            }
            tvTotalPrice.text = "Total: $$price"
        }


        val flatNo = data.address?.flatno.toString()
        val street = data.address?.street.toString()
        val city = data.address?.city.toString()
        val state = data.address?.state.toString()
        val zip = data.address?.zip.toString()
        val country = data.address?.country.toString()

        val completeAddress = data.address?.completeAddress.toString()
        tvDeliveryAddress.text = "$flatNo,$street,$city,$state,$country,$zip,$completeAddress"


        val instruction = data.instruction
        if (instruction.isNullOrEmpty()) {
            tvInstructionLabel.visibility = View.GONE
            etInstruction.visibility = View.GONE
        } else {
            etInstruction.setText(instruction)
        }


        if (orderStatus == getString(R.string.pending_text)) {
            btnCancel.visibility = View.VISIBLE
            btnCancel.setOnClickListener {
                cancelOrder()
            }
        } else if (orderStatus == getString(R.string.completed) || orderStatus == getString(R.string.cancelled)) {

            val mRating = if (data.rating == null) "0" else data.rating.toString()

            ratingLayout.visibility = View.VISIBLE
            val orderRating = mRating.toFloat()
            ratingBar.rating = orderRating
            ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                if (rating != 0.0f) {
                    rateOrder(rating.toString())
                }
            }
        }
    }

    private fun cancelOrder() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["order_id"] = data.id.toString()
        param["status"] = "4"

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeOrderStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            AppGlobalTask({ finish() }, requireContext()).showAlert(response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun rateOrder(rating: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["order_id"] = data.id.toString()
        param["rating"] = rating
        param["review"] = "$rating Stars"


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.rateOrder(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            AppGlobalTask({ finish() }, requireContext()).showAlert(response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun finish() {
        CustomerOrderListFragment.shouldRefresh = true
        requireActivity().finish()
    }


}