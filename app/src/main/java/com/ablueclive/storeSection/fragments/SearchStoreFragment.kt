package com.ablueclive.storeSection.fragments

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.global.fragment.SearchTabFragment
import com.ablueclive.global.fragment.SearchTabFragment.Companion.pbHeader
import com.ablueclive.global.response.SearchStoreListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.SearchStoreListAdapter
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import java.io.IOException
import java.util.*


class SearchStoreFragment : Fragment(), EditTextChangeListener {


    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var searchStoreListAdapter: SearchStoreListAdapter
    private var searchStoreList = ArrayList<SearchStoreListResponse.NearBuyStore>()
    private var lat: Double = 0.0
    private var lng: Double = 0.0
    private var isInternetPresent = false
    private lateinit var connectionDetector: ConnectionDetector
    private var session_token = ""
    private val AUTOCOMPLETE_REQUEST_CODE = 10

    var pageCount = 1
    var rowCount = 30
    var searchText = ""

    private var isLoading = false
    lateinit var linearLayoutManager: LinearLayoutManager
    var isInitial = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }


    private fun init() {

        compositeDisposable = CompositeDisposable()
        isInitial = true
        SearchTabFragment.textListener = this
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        var selectedLoc = BMSPrefs.getString(requireContext(), Constants.SELECTED_LOCATION)
        if (selectedLoc.isNotEmpty()) {
            lat = BMSPrefs.getString(requireContext(), Constants.SELECTED_LAT).toDouble()
            lng = BMSPrefs.getString(requireContext(), Constants.SELECTED_LNG).toDouble()
        } else {
            val gpsTracker = GPSTracker(requireContext())
            lat = gpsTracker.latitude
            lng = gpsTracker.longitude
            selectedLoc = getAddress(LatLng(lat, lng))
        }

        tvChangeAddress.text = selectedLoc


        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }

        tvChangeAddress.visibility = View.VISIBLE
        tvChangeAddress.setOnClickListener {
            onSearchAddress()
        }

        searchStoreListAdapter = SearchStoreListAdapter(searchStoreList)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = searchStoreListAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    getStoresList()
                }
            }
        })

        apiCallOnTextChange(SearchTabFragment.searchText)

    }


    private fun getAddress(latLng: LatLng?): String {
        var addressText = ""
        try {
            val addresses: List<Address>
            val geoCoder = Geocoder(requireContext(), Locale.getDefault())
            addresses = geoCoder.getFromLocation(latLng?.latitude!!, latLng.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                addressText = (address)

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return addressText
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("SearchStoreFragment-", "Place: " + place.name + ", " + place.id + ", " + place.address)
                    val address = place.address
                    tvChangeAddress.text = address
                    lat = place.latLng?.latitude!!
                    lng = place.latLng?.longitude!!

                    isInitial = true

                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LOCATION, address)
                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LAT, lat.toString())
                    BMSPrefs.putString(requireContext(), Constants.SELECTED_LNG, lng.toString())

                    apiCallOnTextChange(SearchTabFragment.searchText)
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    //CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }
    }


    override fun onTextChange(text: String) {
        apiCallOnTextChange(text)
    }

    private fun apiCallOnTextChange(text: String) {
        compositeDisposable.clear()
        searchStoreList.clear()
        searchStoreListAdapter.notifyDataSetChanged()
        tvHeaderText.visibility = View.GONE
        /*if (!isInitial) {
            if (text.isEmpty()) return
        }*/
        if (text.isEmpty()) return
        pageCount = 1
        searchText = text
        getStoresList()
        isInitial = false
    }


    private fun getStoresList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(context, Constants._ID)
        param["search_txt"] = searchText
        param["lat"] = lat.toString()
        param["long"] = lng.toString()
        param["page"] = pageCount.toString()
        param["row_count"] = rowCount.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        val disposable = apiInterface.searchStoreByName(param).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribeWith(
                object : DisposableObserver<SearchStoreListResponse?>() {
                    override fun onStart() {
                        super.onStart()
                    }

                    override fun onNext(response: SearchStoreListResponse) {

                        if (activity != null) {
                            pbHeader.visibility = View.GONE
                            if (response.responseStatus == 1) {
                                tvHeaderText.visibility = View.VISIBLE
                                val list = response.responseData?.nearBuyStores
                                if (!list.isNullOrEmpty()) {
                                    searchStoreList.clear()
                                    searchStoreList.addAll(list)
                                    isLoading = false
                                    pageCount++
                                }
                                val counts = searchStoreList.size
                                tvHeaderText.text = "$counts Results Founds"
                                searchStoreListAdapter.notifyDataSetChanged()
                                emptyParentLayout.visibility = if (counts == 0) View.VISIBLE else View.GONE
                            } else {
                                tvHeaderText.visibility = View.GONE
                            }
                        }

                    }

                    override fun onError(e: Throwable) {
                        pbHeader.visibility = View.GONE
                        println(e.message)
                    }

                    override fun onComplete() {}
                })

        compositeDisposable.add(disposable)

    }

}