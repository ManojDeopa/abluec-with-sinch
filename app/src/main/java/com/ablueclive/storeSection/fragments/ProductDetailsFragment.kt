package com.ablueclive.storeSection.fragments

import android.app.Activity
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.global.fragment.SearchProductFragment
import com.ablueclive.global.response.ProductModel
import com.ablueclive.global.response.SimilarProductResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.SimilarProductAdapter
import com.ablueclive.storeSection.fragments.SearchStoreDetailFragment.Companion.parentPage
import com.ablueclive.utils.*
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_remove_count.*
import kotlinx.android.synthetic.main.product_details_fragment.*
import kotlinx.android.synthetic.main.recycler_view.*
import kotlinx.android.synthetic.main.store_detail_list_item.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class ProductDetailsFragment : Fragment(), RecyclerViewItemClick {


    private var isFirstTime = true
    private var stock = ""
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    val data = Constants.sProductData
    var similarProductList = ArrayList<ProductModel>()

    companion object {
        var shouldUpdateCounts = false
        var isSimilar = false
        var count = "0"
        var isFav = false
        var productId = ""
        var shouldRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.product_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            shouldRefresh = false
            if (count == "0") {
                isFirstTime = true
            } else {
                shouldUpdateCounts = true
            }
            init()
        }
    }


    private fun init() {

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet


        productId = data.id.toString()

        getSimilarProducts()

        var image = Constants.IMAGE_STORE_URL
        val imgList = data?.images
        if (!imgList.isNullOrEmpty()) {
            image += imgList[0]
        }

        CommonMethod.loadImage(image, ivImage)

        ivImage.setOnClickListener {
            val i = Intent(requireActivity(), ShowFullImage::class.java)
            i.putExtra("ImageUrl", image)
            startActivity(i)
        }

        parentPage.tvHeader.text = data?.productTitle

        tvProductName.text = data?.productTitle

        // val price = CommonMethod.getProductPrice(data.utcScheduleFromDate, data.utcScheduleToDate, data.productSalePrice.toString(), data.productPrice.toString())
        val price = data.productPrice.toString()

        tvProductPrice.text = "Price: " + Constants.CURRENCY + price
        tvDescription.text = data?.productDescription


        val storeName = if (data?.store?.storeName.isNullOrEmpty()) "" else data?.store?.storeName
        tvStoreName.text = storeName

        val salePrice = if (data?.productSalePrice == null) "0" else data.productSalePrice.toString()
        tvProductSalePrice.visibility = if (salePrice == "0") View.GONE else View.VISIBLE
        tvProductSalePrice.apply {
            paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            text = Constants.CURRENCY + salePrice
        }

        stock = if (data?.stock == null) "0" else data.stock.toString()
        val unit = if (data?.unit == null) "items" else data.unit.toString()

        tvAvailableItem.text = "Available:$stock $unit"

        isFavourite()

        tvStoreName.setOnClickListener {
            btnContactSeller.callOnClick()
        }

        btnContactSeller.setOnClickListener {
            StoreDetailListFragment.storeId = data.storeId.toString()
            CommonMethod.callCommonContainer(context as Activity, getString(R.string.search_store_detail))
        }

        // isCart()

    }

    private fun isCart() {


        if (shouldUpdateCounts) {
            shouldUpdateCounts = false
            isFirstTime = false
            tvCount.text = count
        }


        if (isFirstTime) {
            addRemoveContent.visibility = View.GONE
            btnAddToCart.setOnClickListener {
                isFirstTime = false
                tvCount.text = "1"
                addToCart()
                isCart()
            }
        } else {

            addRemoveContent.visibility = View.VISIBLE

            ivRemove.setOnClickListener {
                val addCount = tvCount.text.toString().toInt()
                updateCounts(addCount - 1)
            }

            ivAdd.setOnClickListener {
                val addCount = tvCount.text.toString().toInt()
                updateCounts(addCount + 1)
            }

            btnAddToCart.setOnClickListener {
                addToCart()
            }
        }

    }

    private fun isFavourite() {


        if (isFav) {
            ivFav.setImageResource(R.drawable.ic_heart_checked)
            ivFav.tag = "check"
        }

        ivFav.setOnClickListener {
            val tag = ivFav.tag
            addRemoveFavourite(tag)
        }
    }

    private fun updateCounts(i: Int) {


        if (stock == "0") {
            CommonMethod.showToastlong(getString(R.string.product_out_of_stock), requireContext())
            return
        }

        if (stock.toInt() < i) {
            CommonMethod.showToastlong(getString(R.string.stock_limit_exceed_text), requireContext())
            return
        }

        var count = "0"
        if (i > 0) {
            count = i.toString()
        }

        if (i == 0) {
            isFirstTime = true
            isCart()
        }

        tvCount.text = count

        addToCart()

    }

    private fun addToCart() {


        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = productId
        param["qty"] = tvCount.text.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addUpdateProductToCart(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        if (response.responseStatus == 1) {
                            SearchProductFragment.shouldRefresh = true
                            parentPage.getCartList()
                            CommonMethod.showToastlong(response.responseMsg, requireContext())
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun addRemoveFavourite(tag: Any) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = productId

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.customerRemoveFavouriteProduct(param)
        if (tag == "uncheck") {
            apiCall = apiInterface.customerSaveFavouriteProduct(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                SearchProductFragment.shouldRefresh = true
                                CommonMethod.showAlert(requireContext(), response.responseMsg)
                                updateFavIcon(tag)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

    private fun updateFavIcon(tag: Any) {

        if (tag == "uncheck") {
            ivFav.setImageResource(R.drawable.ic_heart_checked)
            ivFav.tag = "check"
        } else {
            ivFav.setImageResource(R.drawable.ic_heart_unchecked)
            ivFav.tag = "uncheck"
        }
    }


    private fun getSimilarProducts() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        pbHeader.visibility = View.VISIBLE

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["session_token"] = session_token
        param["product_id"] = data.id.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.similarProducts(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SimilarProductResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SimilarProductResponse) {
                       try {
                           pbHeader.visibility = View.GONE
                           if (response.responseStatus == 1) {
                               val list = response.responseData?.similerProducts
                               if (!list.isNullOrEmpty()) {
                                   similarProductList.clear()
                                   similarProductList.addAll(list)
                                   tvTitle.text = getString(R.string.similer_products)
                                   ivMore.setOnClickListener {
                                   }
                                   rvCommon.apply {
                                       isSimilar = true
                                       Constants.currentContext = requireContext()
                                       layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                                       adapter = SimilarProductAdapter(this@ProductDetailsFragment, similarProductList)
                                   }
                               }
                           }
                       }catch (e:Exception){
                           e.printStackTrace()
                       }
                    }

                    override fun onError(e: Throwable) {
                        if (pbHeader!=null) pbHeader.visibility = View.GONE
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        similarProductsItemApiCall(text)
    }


    private fun similarProductsItemApiCall(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        show(requireContext(), "")

        val separated = text.split(":".toRegex()).toTypedArray()
        val tag = separated[1]

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = separated[0]

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.customerRemoveFavouriteProduct(param)
        if (tag == "uncheck") {
            apiCall = apiInterface.customerSaveFavouriteProduct(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                SearchProductFragment.shouldRefresh = true
                                CommonMethod.showAlert(requireContext(), response.responseMsg)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


}