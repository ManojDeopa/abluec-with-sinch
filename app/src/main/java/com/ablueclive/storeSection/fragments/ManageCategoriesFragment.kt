package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ablueclive.R
import com.ablueclive.common.AppGlobalTask
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreCategoryListAdapter
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.common_search_header.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.util.*
import kotlin.collections.HashMap


class ManageCategoriesFragment : Fragment(), RecyclerViewItemClick {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.manage_categories_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }


    private fun init() {
        tvTitle.text = getString(R.string.manage_categories)
        ivBack.setOnClickListener {
            requireActivity().finish()
        }
    }


    override fun onResume() {
        super.onResume()
        getStoreCategoryList
    }


    private val getStoreCategoryList: Unit
        get() {
            if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
                CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
                return
            }

            ProgressD.show(requireContext(), "")

            val param = HashMap<String, String>()
            param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
            param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
            param["store_id"] = Constants.myStoreResponse.id.toString()

            val retrofit = RetrofitClient.getRetrofitClient()
            val apiInterface = retrofit.create(ApiInterface::class.java)
            apiInterface.getStoreCategory(param).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<StoreCategoryResponse?> {

                        override fun onSubscribe(d: Disposable) {}
                        override fun onComplete() {}

                        @SuppressLint("SetTextI18n")
                        override fun onNext(response: StoreCategoryResponse) {
                            println(response.responseMsg)
                            ProgressD.hide()

                            try {
                                val list = response.responseData?.storeCategory
                                val adapterM = StoreCategoryListAdapter(list!!, this@ManageCategoriesFragment)

                                recyclerView.apply {
                                    layoutManager = GridLayoutManager(requireActivity(), 2)
                                    adapter = adapterM
                                }

                                ivSearch.setOnClickListener {
                                    editSearchLayout.visibility = View.VISIBLE
                                    tvTitle.visibility = View.GONE
                                }

                                ivCross.setOnClickListener {
                                    ivSearchTop.setText("")
                                    ivSearchTop.hint = getString(R.string.search_all)
                                    editSearchLayout.visibility = View.GONE
                                    tvTitle.visibility = View.VISIBLE
                                }


                                ivSearchTop.addTextChangedListener(object : TextWatcher {
                                    override fun afterTextChanged(s: Editable?) {

                                        val filterList = mutableListOf<StoreCategoryResponse.StoreCategory>()
                                        list.forEach {
                                            if (it.name.toString().toLowerCase(Locale.getDefault()).contains(s.toString().toLowerCase(Locale.getDefault()))) {
                                                filterList.add(it)
                                            }
                                        }
                                        adapterM.filterList(filterList)
                                    }

                                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                                    }

                                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                    }
                                })
                            } catch (e: Exception) {
                            e.printStackTrace()
                            }


                        }

                        override fun onError(e: Throwable) {
                            ProgressD.hide()
                            println(e.message)
                        }
                    })
        }


    private fun deleteCategory(categoryId: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = Constants.myStoreResponse.id.toString()
        param["category_id"] = categoryId

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteCategory(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            AppGlobalTask(::getStoreCategoryList, requireActivity()).showAlert(response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        if (position == 0) {
            showDeleteAlert(text)
        } else {
            CommonMethod.callStoreContainer(requireActivity(), text)
        }
    }


    private fun showDeleteAlert(text: String) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(getString(R.string.delete_category_alert_text))
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
            deleteCategory(text)
            dialog.dismiss()
        }

        alert.setNegativeButton(getString(R.string.no)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        alert.show()
    }

}