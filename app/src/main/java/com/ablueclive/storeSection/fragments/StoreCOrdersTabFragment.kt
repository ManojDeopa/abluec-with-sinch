package com.ablueclive.storeSection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.search_job_tab_fragment.*

/**
 * A simple [Fragment] subclass.
 */
class StoreCOrdersTabFragment : Fragment() {

    private lateinit var list: List<String>
    companion object{
        fun newInstance(bundle: Bundle): StoreCOrdersTabFragment {
            val fragment = StoreCOrdersTabFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_job_tab_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTabViewPager()
    }


    private fun addTabViewPager() {
        list = listOf(getString(R.string.requested), getString(R.string.in_process), getString(R.string.completed))
        val adapter = ViewPagerAdapter(requireActivity())
        viewpager.adapter = adapter
        TabLayoutMediator(tabs, viewpager) { tab, position ->
            tab.text = list[position]
        }.attach()
    }


    internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {
        @NonNull
        override fun createFragment(position: Int): Fragment {
            return CustomerOrderListFragment(list[position])
        }

        override fun getItemCount(): Int {
            return list.size
        }
    }
}