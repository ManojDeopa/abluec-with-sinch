package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ablueclive.R
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.MyStoreListAdapter
import com.ablueclive.storeSection.response.StoreListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.manage_store_fragment.*
import java.util.*
import kotlin.collections.HashMap


class ManageStoreFragment : Fragment() {


    var list = arrayListOf<StoreListResponse.StoreList>()

    var currentDay: String = CommonMethod.getDateFromTimestamp(System.currentTimeMillis(), "EEEE")

    companion object {
        var currentDayId = "99"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.manage_store_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doOnClick()
    }

    private fun doOnClick() {

        btnManageOrders.setOnClickListener {
            //consolidate = true
            //CommonMethod.callStoreContainer(requireActivity(), getString(R.string.manage_orders))
        }

        btnAddStore.setOnClickListener {
            CommonMethod.callStoreContainer(requireActivity(), getString(R.string.add_store))
        }

        rootLayout.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            rootLayout.getWindowVisibleDisplayFrame(r)
            val screenHeight: Int = rootLayout.rootView.height
            val keypadHeight: Int = screenHeight - r.bottom
            if (keypadHeight > screenHeight * 0.15) {
                btnManageOrders.visibility = View.GONE
                btnAddStore.visibility = View.GONE
            } else {
                if (list.isNotEmpty()) {
                    btnManageOrders.visibility = View.VISIBLE
                }
                btnAddStore.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getStoreList()
    }


    private fun getStoreList() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getStores(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: StoreListResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()


                        try {
                            if (response.responseData?.storeList.isNullOrEmpty()) {
                                headerText.visibility = View.GONE
                                btnManageOrders.visibility = View.GONE
                                noStoresLayout.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            } else {

                                list = response.responseData?.storeList as ArrayList<StoreListResponse.StoreList>
                                val dayNameList = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
                                val dayCountList = listOf("0", "1", "2", "3", "4", "5", "6")
                                for (i in dayNameList.indices) {
                                    if (dayNameList[i] == currentDay) {
                                        currentDayId = dayCountList[i]
                                    }
                                }

                                headerText.visibility = View.VISIBLE
                                btnManageOrders.visibility = View.VISIBLE
                                noStoresLayout.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE

                                val adapterM = MyStoreListAdapter(list)
                                recyclerView.apply {
                                    layoutManager = GridLayoutManager(requireActivity(), 2)
                                    adapter = adapterM
                                }

                                etSearchStore.addTextChangedListener(object : TextWatcher {
                                    override fun afterTextChanged(s: Editable?) {

                                        val filterList = mutableListOf<StoreListResponse.StoreList>()
                                        list.forEach {
                                            if (it.storeName.toString().toLowerCase(Locale.getDefault()).contains(s.toString().toLowerCase(Locale.getDefault()))) {
                                                filterList.add(it)
                                            }
                                        }

                                        tvNoStore.visibility = if (filterList.isEmpty()) View.VISIBLE else View.GONE
                                        headerText.visibility = if (filterList.isEmpty()) View.GONE else View.VISIBLE
                                        adapterM.filterList(filterList)
                                    }

                                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                                    }

                                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                    }
                                })
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }
}