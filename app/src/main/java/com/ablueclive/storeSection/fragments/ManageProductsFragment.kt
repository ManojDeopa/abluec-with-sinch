package com.ablueclive.storeSection.fragments

import AppUtils
import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreCategoryHorizontalAdapter
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.common_search_header.*
import kotlinx.android.synthetic.main.manage_products_fragment.*


class ManageProductsFragment : Fragment(), RecyclerViewItemClick {


    companion object {
        var shouldRefresh = false
        lateinit var parentView: LinearLayoutCompat
        var textListener: EditTextChangeListener? = null
    }

    var storeId = Constants.myStoreResponse.id.toString()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.manage_products_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {

        parentView = parentViewLayout
        getStoreCategoryList

        tvTitle.text = getString(R.string.manage_products)
        ivBack.setOnClickListener {
            requireActivity().finish()
        }

        ivSearch.setOnClickListener {
            editSearchLayout.visibility = View.VISIBLE
            tvTitle.visibility = View.GONE
        }

        ivCross.setOnClickListener {
            ivSearchTop.setText("")
            ivSearchTop.hint = getString(R.string.search_all)
            editSearchLayout.visibility = View.GONE
            tvTitle.visibility = View.VISIBLE
        }

        ivSearchTop.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                textListener?.onTextChange(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            getStoreCategoryList
        }
    }


    private val getStoreCategoryList: Unit
        get() {
            if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
                CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
                return
            }

            ProgressD.show(requireContext(), "")

            val param = HashMap<String, String>()
            param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
            param["store_id"] = storeId

            val retrofit = RetrofitClient.getRetrofitClient()
            val apiInterface = retrofit.create(ApiInterface::class.java)
            apiInterface.getStoreParentCategories(param).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<StoreCategoryResponse?> {

                        override fun onSubscribe(d: Disposable) {}
                        override fun onComplete() {}

                        @SuppressLint("SetTextI18n")
                        override fun onNext(response: StoreCategoryResponse) {
                            println(response.responseMsg)
                            ProgressD.hide()

                            try {
                                val list = response.responseData?.allCategory
                                if (!list.isNullOrEmpty()) {
                                    tvNoData.visibility = View.GONE
                                    rvCategories.visibility = View.VISIBLE

                                    if (shouldRefresh) {
                                        shouldRefresh = false
                                        var isMatch = false
                                        list.forEach {
                                            if (it._id == StoreContainerActivity.selectedCategoryId) {
                                                it.isSelected = true
                                                isMatch = true
                                            }
                                        }
                                        if (!isMatch) {
                                            list[0].isSelected = true
                                        }
                                    } else {
                                        list[0].isSelected = true
                                    }

                                    rvCategories.apply {
                                        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                                        adapter = StoreCategoryHorizontalAdapter(list, this@ManageProductsFragment)
                                    }
                                } else {
                                    tvNoData.visibility = View.VISIBLE
                                    rvCategories.visibility = View.GONE
                                    AppUtils.addFragment(requireActivity(), ManageProductListFragment(""), R.id.productListContainer)
                                }
                            } catch (e: Exception) {
                            e.printStackTrace()
                            }
                        }

                        override fun onError(e: Throwable) {
                            ProgressD.hide()
                            println(e.message)
                        }
                    })
        }


    override fun onRecyclerItemClick(text: String, position: Int) {
        when (position) {
            5 -> {
                AppUtils.replaceFragment(requireActivity(), ManageProductListFragment(text), R.id.productListContainer)
            }
        }
    }
}