package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.AgentListAdapter
import com.ablueclive.storeSection.response.AgentListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.common_search_header.*
import kotlinx.android.synthetic.main.manage_agent_fragment.*
import java.util.*
import kotlin.collections.HashMap


class ManageAgentFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.manage_agent_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHeader()

        btnAddAgent.setOnClickListener {
            CommonMethod.callStoreContainer(requireActivity(), getString(R.string.add_agent))
        }
    }

    override fun onResume() {
        super.onResume()
        getAgentList()
    }

    private fun setHeader() {
        tvTitle.text = getString(R.string.manage_agent)
        ivBack.setOnClickListener {
            requireActivity().finish()
        }
    }


    private fun getAgentList() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["store_id"] = Constants.myStoreResponse.id.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.listAgent(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<AgentListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: AgentListResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {

                                val list = response.responseData?.agents
                                if (list.isNullOrEmpty()) {
                                    tvNoData.visibility = View.VISIBLE
                                } else {

                                    val mAdapter = AgentListAdapter(list)
                                    tvNoData.visibility = View.GONE
                                    recyclerView.apply {
                                        layoutManager = LinearLayoutManager(requireActivity())
                                        adapter = mAdapter
                                    }

                                    ivSearch.setOnClickListener {
                                        editSearchLayout.visibility = View.VISIBLE
                                        tvTitle.visibility = View.GONE
                                    }

                                    ivCross.setOnClickListener {
                                        ivSearchTop.setText("")
                                        ivSearchTop.hint = getString(R.string.search_all)
                                        editSearchLayout.visibility = View.GONE
                                        tvTitle.visibility = View.VISIBLE
                                    }

                                    ivSearchTop.addTextChangedListener(object : TextWatcher {
                                        override fun afterTextChanged(s: Editable?) {

                                            val filterList = mutableListOf<AgentListResponse.Agent>()
                                            list.forEach {
                                                if (it.name.toString().toLowerCase(Locale.getDefault()).contains(s.toString().toLowerCase(Locale.getDefault()))) {
                                                    filterList.add(it)
                                                }
                                            }
                                            mAdapter.filterList(filterList)
                                        }

                                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                                        }

                                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                        }
                                    })
                                }

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}