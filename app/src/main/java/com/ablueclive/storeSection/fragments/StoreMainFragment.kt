package com.ablueclive.storeSection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.store_main_fragment.*

class StoreMainFragment : Fragment() {


    companion object {

        fun newInstance(bundle: Bundle): StoreMainFragment {
            val fragment = StoreMainFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.store_main_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        buyNowLayout.setOnClickListener { v: View? ->
            Constants.SEARCH_TYPE = getString(R.string.products)
            CommonMethod.callCommonContainer(requireActivity(), getString(R.string.search_all))
            //CommonMethod.callStoreContainer(requireActivity(), getString(R.string.buy_now))
        }

        manageStoreLayout.setOnClickListener { v: View? ->
            CommonMethod.callStoreContainer(requireActivity(), getString(R.string.manage_your_store))
        }

    }
}