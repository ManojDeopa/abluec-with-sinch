package com.ablueclive.storeSection.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.FavProductAdapter
import com.ablueclive.storeSection.fragments.SearchStoreDetailFragment.Companion.shouldCartListRefresh
import com.ablueclive.storeSection.response.StoreProductListResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.lang.Exception


class FavProductListFragment : Fragment(), RecyclerViewItemClick {


    companion object {
        var storeId = ""
        fun newInstance(bundle: Bundle): FavProductListFragment {
            val fragment = FavProductListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_view_layout, container, false)
    }


    override fun onResume() {
        super.onResume()
        customerFavouriteProductList()
    }

    private fun customerFavouriteProductList() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)


        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)

        apiInterface.customerFavouriteProductList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<StoreProductListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: StoreProductListResponse) {
                        ProgressD.hide()

                       try {
                           val list = response.responseData?.favProducts
                           if (!list.isNullOrEmpty()) {
                               CommonMethod.isEmptyView(false, requireContext(), "")
                               val adapterM = FavProductAdapter(list as MutableList<StoreProductListResponse.FavProduct>, this@FavProductListFragment)
                               recyclerView.apply {
                                   layoutManager = LinearLayoutManager(requireContext())
                                   adapter = adapterM
                               }
                           } else {
                               CommonMethod.isEmptyView(true, requireContext(), " No " + getString(R.string.favourite_product) + " Found ")
                           }
                       }catch (e:Exception){
                           e.printStackTrace()
                       }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun addUpdateProductToCart(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")


        val separated = text.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["store_id"] = storeId
        param["product_id"] = separated[0]
        param["qty"] = separated[1]


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addUpdateProductToCart(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            CommonMethod.showToastlong(response.responseMsg, requireContext())
                            shouldCartListRefresh = true
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        when (position) {
            0 -> {
                addUpdateProductToCart(text)
            }

            1 -> {
                addRemoveFavourite(text)
            }
        }
    }

    private fun addRemoveFavourite(text: String) {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val separated = text.split(":".toRegex()).toTypedArray()
        val tag = separated[1]

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = BMSPrefs.getString(requireContext(), Constants._ID)
        param["product_id"] = separated[0]

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.customerRemoveFavouriteProduct(param)
        if (tag == "uncheck") {
            apiCall = apiInterface.customerSaveFavouriteProduct(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }
}