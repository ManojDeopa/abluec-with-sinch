package com.ablueclive.storeSection.fragments

import AppUtils
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.ScannedBarcodeActivity
import com.ablueclive.common.StoreContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.simplecropimage.CropImage
import com.ablueclive.storeSection.adapter.OtherCategoryListAdapter
import com.ablueclive.storeSection.response.OtherCategoryResponse
import com.ablueclive.storeSection.response.SkuProductResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.add_product_fragment.*
import kotlinx.android.synthetic.main.add_product_fragment.view.*
import kotlinx.android.synthetic.main.list_dialog_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.util.*
import kotlin.collections.HashMap

class AddProductFragment : Fragment(), RecyclerViewItemClick {

    private lateinit var categoryDialog: Dialog
    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12
    private val REQUEST_CODE_CROP_IMAGE = 13


    private var file: Uri? = null
    var path = ""
    var isTaxable = "0"
    var isShippingAvailable = "0"
    var isFeatured = "1"
    var timeZone = ""
    var selectedUnit = ""

    val isEdit = StoreContainerActivity.isEdit
    val data = Constants.productData

    var categoryList = arrayListOf<OtherCategoryResponse.OtherCategory>()

    companion object {
        var isFromEdit = false
        var catParentId = ""
        var catName = ""
        var shouldNotify = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_product_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        doOnClicks()
    }

    override fun onResume() {
        super.onResume()
        if (shouldNotify) {
            shouldNotify = false
            catParentId = StoreContainerActivity.selectedCategoryId
            catName = StoreContainerActivity.selectedCatName
            etSelectCategory.setText(catName)
        }


        if (ScannedBarcodeActivity.SCAN_RESULT.isNotEmpty()) {
            etSku.setText(ScannedBarcodeActivity.SCAN_RESULT)
            ScannedBarcodeActivity.SCAN_RESULT = ""
            getBarCodeProductInfo()
        }

    }

    fun init() {

        resetFocus()

        catParentId = StoreContainerActivity.selectedCategoryId
        catName = StoreContainerActivity.selectedCatName
        timeZone = TimeZone.getDefault().id

        if (isEdit) {

            tvAddWith.visibility = View.GONE
            contentLayout.visibility = View.VISIBLE
            btnArrow.visibility = View.GONE

            val sku = if (data.sku.isNullOrEmpty()) {
                ""
            } else {
                data.sku
            }
            etSku.setText(sku)

            if (etSku.text.toString().isNotEmpty()) {
                etProductName.isFocusable = false
                etProductName.isClickable = false
                etDescription.isFocusable = false
                etDescription.isClickable = false
                etSku.isClickable = false
                etSku.isFocusable = false
                ivUploadImage.isEnabled = false
            }

            // isValidSku()

            var imgUrl = Constants.IMAGE_STORE_URL
            val prodImage = data.images
            if (!prodImage.isNullOrEmpty()) {
                imgUrl = Constants.IMAGE_STORE_URL + prodImage[0]
            }
            tvUploadImage.visibility = View.GONE
            CommonMethod.loadImageResize(imgUrl, ivUploadImage)


            nullCheck(data.productTitle, etProductName)
            nullCheck(data.productPrice.toString(), etNormalPrice)
            nullCheck(data.stock.toString(), etStock)
            if (data.productSalePrice != null) {
                nullCheck(data.productSalePrice.toString(), etSalePrice)
            }
            nullCheck(data.productDescription, etDescription)
            nullCheck(data.scheduleFromDate, etScheduleFrom)
            nullCheck(data.scheduleToDate, etScheduleTo)
            nullCheck(data.unit, etUnit)

            val timeZn = data.timeZone
            if (!timeZn.isNullOrEmpty()) {
                timeZone = timeZn
            }

            isFeatured = data.isFeatured.toString()
            isTaxable = data.isTaxable.toString()
            isShippingAvailable = data.isShippingAvailable.toString()

            cbFeaturedProduct.isChecked = isFeatured != "0"

            if (isTaxable == "1") {
                rgTaxable.rbTYes.isChecked = true
            } else {
                rgTaxable.rbTNo.isChecked = true
            }

            if (isShippingAvailable == "1") {
                rgShipping.rbSYes.isChecked = true
            } else {
                rgShipping.rbSNo.isChecked = true
            }

            /*  etSelectCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0)
              etSelectCategory.setOnClickListener {
                  // resetFocus()
                  // getCategory()
                  isFromEdit = true
                  StoreContainerActivity.selectedCategoryId = "0"
                  CommonMethod.callStoreContainer(requireActivity(), getString(R.string.select_category))
              }*/

            shouldCategoryVisible(false)
            etSelectCategory.setText(catName)
            btnAddNow.text = getString(R.string.edit_now)
        }

        etSelectCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0)
        etSelectCategory.setOnClickListener {
            getCategories()
        }

    }

    fun shouldCategoryVisible(isVisible: Boolean) {

        if (isVisible) {
            tvCategoryLabel.visibility = View.VISIBLE
            etSelectCategory.visibility = View.VISIBLE

        } else {
            tvCategoryLabel.visibility = View.GONE
            etSelectCategory.visibility = View.GONE
        }
    }

    private fun doOnClicks() {

        /* var manualAdd = true

         var addManually = getString(R.string.add_manually_text)
         addManually = addManually.replace("\n", "<br>").replace("Add Manually", "<font color='#1DA1F2'>Add Manually</font>")

         var addSku = getString(R.string.add_sku_text)
         addSku = addSku.replace("\n", "<br>").replace("Add with Barcode No.", "<font color='#1DA1F2'>Add with Barcode No.</font>")


         tvAddWith.text = HtmlCompat.fromHtml(addManually, HtmlCompat.FROM_HTML_MODE_LEGACY)
         tvAddWith.setOnClickListener {

             if (manualAdd) {
                 manualAdd = false
                 contentLayout.visibility = View.VISIBLE
                 tvLabelSku.visibility = View.GONE
                 layoutSku.visibility = View.GONE
                 tvAddWith.text = HtmlCompat.fromHtml(addSku, HtmlCompat.FROM_HTML_MODE_LEGACY)

             } else {
                 manualAdd = true
                 contentLayout.visibility = View.GONE
                 tvAddWith.text = HtmlCompat.fromHtml(addManually, HtmlCompat.FROM_HTML_MODE_LEGACY)
                 tvLabelSku.visibility = View.VISIBLE
                 layoutSku.visibility = View.VISIBLE
             }

         }*/

        etSku.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                btnArrow.tag = "scan"
                var image = R.drawable.scanner
                if (s.toString().isNotEmpty()) {
                    image = R.drawable.arrow_more
                    btnArrow.tag = "next"
                }
                btnArrow.setImageResource(image)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        btnArrow.setOnClickListener {

            if (btnArrow.tag == "scan") {
                startActivity(Intent(requireContext(), ScannedBarcodeActivity::class.java))
                return@setOnClickListener
            }

            getBarCodeProductInfo()
        }

        btnAddNow.setOnClickListener {
            callAddEditProductApi()
        }


        ivUploadImage.setOnClickListener {
            resetFocus()
            onAddLogoClick()
        }

        rgTaxable.setOnCheckedChangeListener { group, checkedId ->
            val radio: RadioButton = group.findViewById(checkedId)
            isTaxable = if (radio.text.toString() == getString(R.string.yes)) {
                "1"
            } else {
                "0"
            }
        }

        rgShipping.setOnCheckedChangeListener { group, checkedId ->
            val radio: RadioButton = group.findViewById(checkedId)
            isShippingAvailable = if (radio.text.toString() == getString(R.string.yes)) {
                "1"
            } else {
                "0"
            }
        }

        cbFeaturedProduct.setOnCheckedChangeListener { buttonView, isChecked ->

            isFeatured = if (isChecked) {
                "1"
            } else {
                "0"
            }
        }

        etScheduleFrom.setOnClickListener {
            showDatePicker(etScheduleFrom)
        }

        etScheduleTo.setOnClickListener {
            showDatePicker(etScheduleTo)
        }

        tvAddOtherCategory.setOnClickListener {
            CommonMethod.callStoreContainer(requireActivity(), getString(R.string.add_category))
        }

        etUnit.setOnClickListener {
            selectUnitList()
        }

        etDescription.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

    }


    private fun getCategories() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val params = java.util.HashMap<String, String>()
        params["session_token"] = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.otherCategories(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<OtherCategoryResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: OtherCategoryResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {
                                categoryList = response.responseData?.otherCategories as ArrayList<OtherCategoryResponse.OtherCategory>
                                if (!categoryList.isNullOrEmpty()) {
                                    showCategoryListDialogList()
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun showCategoryListDialogList() {


        categoryDialog = Dialog(requireActivity(), R.style.alertDialogTheme)
        /* dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)*/
        categoryDialog.setContentView(R.layout.list_dialog_layout)

        categoryDialog.tvHeader.text = getString(R.string.select_category)


        val mAdapter = OtherCategoryListAdapter(categoryList, this)

        categoryDialog.recyclerView.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        categoryDialog.show()


    }


    private fun nullCheck(value: String?, editText: AppCompatEditText?) {
        if (!value.isNullOrEmpty()) {
            editText?.setText(value.toString())
        }

    }


    fun convertImages(imgUrl: String) {
        ProgressD.show(requireContext(), "")
        Thread {
            try {
                val `in` = URL(imgUrl).openStream()
                val bmp = BitmapFactory.decodeStream(`in`)
                val bytes = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.PNG, 100, bytes)
                val file = File(requireActivity().getExternalFilesDir(null)?.absolutePath + File.separator + System.currentTimeMillis() + "temporary_file.jpg")
                try {
                    val fo = FileOutputStream(file)
                    fo.write(bytes.toByteArray())
                    fo.flush()
                    fo.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                path = file.toString()
                val bitmap = BitmapFactory.decodeFile(path)

                requireActivity().runOnUiThread {
                    ProgressD.hide()
                    ivUploadImage.setImageBitmap(bitmap)
                    tvUploadImage.visibility = View.GONE
                }
            } catch (e: java.lang.Exception) {
                Log.e("Error", "--" + e.message)
                e.printStackTrace()
            }
        }.start()

    }


    private fun selectUnitList() {
        val builder: androidx.appcompat.app.AlertDialog.Builder = androidx.appcompat.app.AlertDialog.Builder(requireActivity())
        val list = arrayOf("Pieces", "Kg", "Gram", "Mg")
        val checkedItem = list.indexOf(selectedUnit)
        builder.setSingleChoiceItems(list, checkedItem) { dialog, which ->
            selectedUnit = list[which]
        }
        builder.setPositiveButton("Ok") { dialog, which ->
            etUnit.setText(selectedUnit)
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel", null)
        val dialog: androidx.appcompat.app.AlertDialog = builder.create()
        dialog.show()
    }


    @SuppressLint("SetTextI18n")
    fun showDatePicker(textView: TextView) {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.DatePickerDialogThemes, { view, year, monthOfYear, dayOfMonth ->

            var month = "" + (monthOfYear + 1)
            if ((monthOfYear + 1) < 10) {
                month = "0" + (monthOfYear + 1)
            }

            textView.text = "$year-$month-$dayOfMonth"

        }, mYear, mMonth, mDay
        )
        datePickerDialog.show()

        /* if (textView == etScheduleFrom) {
             datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
         } else {
             datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
         }*/
    }


    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    private fun onAddLogoClick() {

        val alertDialog = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }


    private fun startCropImage(mFileTemp: File) {
        val intent = Intent(requireActivity(), CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 4)
        intent.putExtra(CropImage.ASPECT_Y, 4)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) return


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null) {
                try {
                    /* val file = File(CommonMethods.getRealPathFromURI(requireActivity(), data.data))
                 startCropImage(file)*/
                    val imagePath = CommonMethods.getRealPathFromURI(requireActivity(), data.data)
                    path = imagePath
                    val bitmap = BitmapFactory.decodeFile(imagePath)
                    ivUploadImage.setImageBitmap(bitmap)
                    tvUploadImage.visibility = View.GONE
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                Log.d("TAG", "you don`t pic any image")
            }
        }


        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                startCropImage(File(CommonMethods.getRealPathFromURI(requireActivity(), file)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        if (requestCode == REQUEST_CODE_CROP_IMAGE) {
            try {
                val imagePath = data!!.getStringExtra(CropImage.IMAGE_PATH) ?: return
                path = imagePath
                val bitmap = BitmapFactory.decodeFile(imagePath)
                ivUploadImage.setImageBitmap(bitmap)
                tvUploadImage.visibility = View.GONE
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }

        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }

        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureFromGallery()
            } else {
                Toast.makeText(requireActivity(), "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun getBarCodeProductInfo() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        param["barcode"] = etSku.text.toString().trim()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getBarCodeProductInfo(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SkuProductResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: SkuProductResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseStatus == 1) {
                                val data = response.responseData?.getBarCodeProductInfo
                                if (data != null) {

                                    contentLayout.visibility = View.VISIBLE
                                    tvAddWith.visibility = View.GONE

                                    etProductName.setText(data.productTitle)
                                    etDescription.setText(data.productDescription)
                                    etNormalPrice.setText(data.productPrice)

                                    btnArrow.visibility = View.GONE

                                    etSku.isFocusable = false
                                    etSku.isClickable = false
                                    etProductName.isFocusable = false
                                    etProductName.isClickable = false
                                    etDescription.isFocusable = false
                                    etDescription.isClickable = false
                                    convertImages(data.images.toString())

                                    val productCategory = data.productCategory.toString()
                                    if (productCategory == "0") {

                                        catParentId = ""
                                        catName = ""
                                        etSelectCategory.hint = getString(R.string.select_category)
                                        etSelectCategory.setText(catName)

                                        shouldCategoryVisible(true)

                                        getCategories()

                                    } else {
                                        catParentId = productCategory
                                        shouldCategoryVisible(false)
                                    }

                                } else {
                                    CommonMethod.showAlert(requireContext(), response.responseMsg)
                                }
                            } else {
                                CommonMethod.showAlert(requireContext(), response.responseMsg)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun callAddEditProductApi() {

        if (!isValidate()) {
            return
        }

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")


        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN))
        builder.addFormDataPart("user_id", BMSPrefs.getString(requireActivity(), Constants._ID))
        builder.addFormDataPart("store_id", Constants.myStoreResponse.id.toString())
        builder.addFormDataPart("product_title", etProductName.text.toString())
        builder.addFormDataPart("product_price", etNormalPrice.text.toString())
        builder.addFormDataPart("product_category", catParentId)
        builder.addFormDataPart("product_description", etDescription.text.toString())

        builder.addFormDataPart("delivery_time", "0")
        builder.addFormDataPart("is_featured", isFeatured)
        builder.addFormDataPart("product_sale_price", etSalePrice.text.toString())
        builder.addFormDataPart("schedule_from_date", etScheduleFrom.text.toString())
        builder.addFormDataPart("schedule_to_date", etScheduleTo.text.toString())

        builder.addFormDataPart("is_taxable", isTaxable)
        builder.addFormDataPart("is_shipping_available", isShippingAvailable)
        builder.addFormDataPart("stock", etStock.text.toString())
        builder.addFormDataPart("unit", etUnit.text.toString())

        // val sku = if (etProductName.isClickable && etProductName.isFocusable) "" else etSku.text.toString()
        builder.addFormDataPart("sku", /*sku*/etSku.text.toString())

        builder.addFormDataPart("time_zone", timeZone)
        builder.addFormDataPart("iso_schedule_from_date", AppUtils.convertDateToISO(etScheduleFrom.text.toString()).toString())
        builder.addFormDataPart("iso_schedule_to_date", AppUtils.convertDateToISO(etScheduleTo.text.toString()).toString())


        if (path.isNotEmpty()) {
            val imageFile = File(path)
            builder.addFormDataPart("product_image", imageFile.name, imageFile.asRequestBody("image/jpg".toMediaTypeOrNull()))
        }

        if (isEdit) {
            builder.addFormDataPart("product_id", data.id.toString())
        }

        val requestBody = builder.build()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.addProductApi(requestBody)
        if (isEdit) {
            apiCall = apiInterface.editProductApi(requestBody)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        if (response.responseStatus == 1) {
                            showSimpleAlert(response.responseMsg)
                        } else {
                            CommonMethod.showAlert(requireContext(), response.responseMsg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showSimpleAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireContext(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            ManageProductsFragment.shouldRefresh = true
            StoreContainerActivity.killActivities()
            requireActivity().finish()
        }
        alert.show()
    }


    fun showAlert(text: String?) {

        var positiveButtonText = getString(R.string.ok)
        var message = text.toString()

        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setCancelable(false)

        if (!isEdit) {
            message = "Product added successfully. Are you want to add another product on same category ? "
            positiveButtonText = getString(R.string.yes)
            alert.setNegativeButton(getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                ManageProductsFragment.shouldRefresh = true
                StoreContainerActivity.killActivities()
                dialog.dismiss()
                requireActivity().finish()
            }
        }

        alert.setMessage(message)
        alert.setPositiveButton(positiveButtonText) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            if (isEdit) {
                ManageProductsFragment.shouldRefresh = true
                StoreContainerActivity.killActivities()
                requireActivity().finish()
            } else {
                CommonMethod.callStoreContainer(requireActivity(), getString(R.string.add_product))
                requireActivity().finish()
            }
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etProductName.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.enter_product_name_validation), requireContext())
                false
            }

            catParentId.isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.select_category_validation), requireContext())
                false
            }

            etNormalPrice.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.select_normal_price_validation), requireContext())
                false
            }


            /*etSalePrice.text.toString().trim().isNotEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.select_sales_price_validation), requireContext())
                false
            }*/

            /* etSalePrice.text.toString().toFloat() > etNormalPrice.text.toString().toFloat() -> {
                 CommonMethod.showToastlong(getString(R.string.sale_maximum_price_validation), requireContext())
                 return false
             }*/

            etSalePrice.text.toString().trim().isNotEmpty() -> {
                if (etSalePrice.text.toString().toFloat() > etNormalPrice.text.toString().toFloat()) {
                    CommonMethod.showToastlong(getString(R.string.sale_maximum_price_validation), requireContext())
                    return false
                }
                true
            }

            etStock.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter) + " " + getString(R.string.stock), requireContext())
                false
            }

            /* etUnit.text.toString().trim().isEmpty() -> {
                 CommonMethod.showToastlong(getString(R.string.please_select) + " " + getString(R.string.stock) + " Unit", requireContext())
                 false
             }*/

            else -> true
        }
    }

    override fun onRecyclerItemClick(text: String, position: Int) {

        if (text == "CategoryItemClick") {
            categoryDialog.dismiss()
            catParentId = categoryList[position].id.toString()
            catName = categoryList[position].name.toString()
            etSelectCategory.setText(catName)
        }
    }


    private fun isValidSku() {

        if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        param["barcode"] = data.sku.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getBarCodeProductInfo(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SkuProductResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: SkuProductResponse) {
                        ProgressD.hide()
                        Log.e("isValidSku", "--" + response.responseMsg)

                        try {
                            if (response.responseStatus == 1) {
                                val data = response.responseData?.getBarCodeProductInfo
                                if (data != null) {
                                    etProductName.isFocusable = false
                                    etProductName.isClickable = false
                                    etDescription.isFocusable = false
                                    etDescription.isClickable = false
                                    etSku.isClickable = false
                                    etSku.isFocusable = false
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }


                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}