package com.ablueclive.storeSection.fragments

import AppUtils
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.global.fragment.SearchProductFragment
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.storeSection.adapter.StoreCategoryHorizontalAdapter
import com.ablueclive.storeSection.response.StoreCategoryResponse
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_category_tab_fragment.*


class StoreCategoryTabFragment : Fragment(), RecyclerViewItemClick {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.store_category_tab_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    private fun init() {
        getStoreCategoryList
    }


    private val getStoreCategoryList: Unit
        get() {
            if (!ConnectionDetector(requireActivity()).isConnectingToInternet) {
                CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
                return
            }

            ProgressD.show(requireContext(), "")

            val param = HashMap<String, String>()
            param["user_id"] = BMSPrefs.getString(requireActivity(), Constants._ID)
            param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
            param["store_id"] = Constants.searchStoreData.storeInfo?.id.toString()

            val retrofit = RetrofitClient.getRetrofitClient()
            val apiInterface = retrofit.create(ApiInterface::class.java)
            apiInterface.customerGetStoreCategories(param).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<StoreCategoryResponse?> {

                        override fun onSubscribe(d: Disposable) {}
                        override fun onComplete() {}

                        @SuppressLint("SetTextI18n")
                        override fun onNext(response: StoreCategoryResponse) {
                            println(response.responseMsg)
                            ProgressD.hide()

                            try {
                                val list = response.responseData?.categoryListing
                                if (!list.isNullOrEmpty()) {

                                    tvNoData.visibility = View.GONE
                                    rvCategories.visibility = View.VISIBLE

                                    list[0].isSelected = true
                                    rvCategories.apply {
                                        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                                        adapter = StoreCategoryHorizontalAdapter(list, this@StoreCategoryTabFragment)
                                    }

                                } else {
                                    tvNoData.visibility = View.VISIBLE
                                    rvCategories.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                            e.printStackTrace()
                            }
                        }

                        override fun onError(e: Throwable) {
                            ProgressD.hide()
                            println(e.message)
                        }
                    })
        }


    override fun onRecyclerItemClick(text: String, position: Int) {
        when (position) {
            5 -> {
                AppUtils.addFragment(requireActivity(), SearchProductFragment(text), R.id.listContainer)
            }
        }
    }

}