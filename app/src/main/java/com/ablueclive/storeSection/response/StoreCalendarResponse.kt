package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class StoreCalendarResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("store_calender")
        @Expose
        var storeCalender: List<StoreCalender>? = null
    }


}