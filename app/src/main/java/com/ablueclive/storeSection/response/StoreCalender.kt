package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class StoreCalender {


    var dayName = ""

    var isValidated = false


    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("day")
    @Expose
    var day: Int = 10

    @SerializedName("store_id")
    @Expose
    var storeId: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: String? = null

    @SerializedName("from_time")
    @Expose
    var fromTime: String? = null

    @SerializedName("time_zone")
    @Expose
    var timeZone: String? = null

    @SerializedName("to_time")
    @Expose
    var toTime: String? = null

    @SerializedName("utc_from_time")
    @Expose
    var utcFromTime: String? = null

    @SerializedName("utc_from_time_hours")
    @Expose
    var utcFromTimeHours: String? = null

    @SerializedName("utc_from_time_min")
    @Expose
    var utcFromTimeMin: String? = null

    @SerializedName("utc_to_time")
    @Expose
    var utcToTime: String? = null

    @SerializedName("utc_to_time_hours")
    @Expose
    var utcToTimeHours: String? = null

    @SerializedName("utc_to_time_min")
    @Expose
    var utcToTimeMin: String? = null


}