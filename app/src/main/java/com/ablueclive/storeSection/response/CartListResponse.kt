package com.ablueclive.storeSection.response

import com.ablueclive.activity.profile.MyProfileResponse
import com.ablueclive.global.response.ProductModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CartListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("cart_products")
        @Expose
        var cartProducts: CartProducts? = null
    }

    class CartProducts {
        @SerializedName("items")
        @Expose
        var items: List<Item>? = null

        @SerializedName("user_info")
        @Expose
        var userInfo: MyProfileResponse.GetprofileInfo? = null
    }

    class Item {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("qty")
        @Expose
        var qty: Int? = null

        @SerializedName("product")
        @Expose
        var product: ProductModel? = null

        @SerializedName("store")
        @Expose
        var store: StoreListResponse.StoreList? = null
    }


}