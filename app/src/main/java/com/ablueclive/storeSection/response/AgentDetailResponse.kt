package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class AgentDetailResponse {

    val noData = "NO_DATA_AVAIL"

    @SerializedName("response_key")
    @Expose
    var response_key: String = ""

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("agent_info")
        @Expose
        var agentInfo: AgentInfo? = null
    }

    class AgentInfo {
        @SerializedName("_id")
        @Expose
        var id: String = ""

        @SerializedName("country_code")
        @Expose
        var countryCode: String = ""

        @SerializedName("email")
        @Expose
        var email: String = ""

        @SerializedName("device_id")
        @Expose
        var deviceId: String = ""

        @SerializedName("device_token")
        @Expose
        var deviceToken: String = ""

        @SerializedName("device_type")
        @Expose
        var deviceType: Int? = null

        @SerializedName("name")
        @Expose
        var name: String = ""

        @SerializedName("mobile")
        @Expose
        var mobile: String = ""

        @SerializedName("language")
        @Expose
        var language: String = ""

        @SerializedName("verification_otp")
        @Expose
        var verificationOtp: String = ""

        @SerializedName("rating")
        @Expose
        var rating: Int = 0

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("skype")
        @Expose
        var skype: Any? = null

        @SerializedName("about")
        @Expose
        var about: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("debug_mode")
        @Expose
        var debugMode: Int? = null

        @SerializedName("hash")
        @Expose
        var hash: String? = null

        @SerializedName("isStoreCreated")
        @Expose
        var isStoreCreated: Int? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String = ""

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String = ""

        @SerializedName("address")
        @Expose
        var address: Address? = null
    }

    class Address {
        @SerializedName("flatno")
        @Expose
        var flatno: String = ""

        @SerializedName("street")
        @Expose
        var street: String = ""

        @SerializedName("city")
        @Expose
        var city: String = ""

        @SerializedName("state")
        @Expose
        var state: String = ""

        @SerializedName("zip")
        @Expose
        var zip: String = ""

        @SerializedName("country")
        @Expose
        var country: String = ""

        @SerializedName("lat")
        @Expose
        var lat: Float? = null

        @SerializedName("long")
        @Expose
        var _long: Float? = null

        @SerializedName("complete_address")
        @Expose
        var completeAddress: String = ""
    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }

}