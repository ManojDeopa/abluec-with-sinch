package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class AdminCategoryResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("category_listing")
        @Expose
        var categoryListing: List<CategoryListing>? = null
    }

    class CategoryListing {
        @SerializedName("_id")
        @Expose
        var id: Any? = null

        @SerializedName("parent_id")
        @Expose
        var parentId: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null
    }

}