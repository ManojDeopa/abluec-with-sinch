package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class GetStoreCatListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null


    class ResponseData {
        @SerializedName("allCategory")
        @Expose
        var allCategory: List<AllCategory>? = null
    }

    class AllCategory {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("parent_id")
        @Expose
        var parentId: String? = null

        @SerializedName("is_having_child")
        @Expose
        var isHavingChild: Int? = null
    }

}