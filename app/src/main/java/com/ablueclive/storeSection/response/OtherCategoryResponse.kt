package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OtherCategoryResponse {
    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null


    class ResponseData {
        @SerializedName("otherCategories")
        @Expose
        var otherCategories: List<OtherCategory>? = null
    }

    class OtherCategory {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("custom_id")
        @Expose
        var customId: String? = null

        @SerializedName("parent_custom_id")
        @Expose
        var parentCustomId: String? = null

        @SerializedName("parent_id")
        @Expose
        var parentId: String? = null
    }

}