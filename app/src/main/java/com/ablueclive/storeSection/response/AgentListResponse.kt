package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class AgentListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("agents")
        @Expose
        var agents: List<Agent>? = null
    }

    class Agent {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null
    }

}