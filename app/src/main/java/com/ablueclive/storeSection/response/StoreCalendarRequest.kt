package com.ablueclive.storeSection.response

class StoreCalendarRequest {

    var store_id = ""
    var user_id = ""
    var day = ""
    var from_time = ""
    var to_time = ""
    var utc_from_time = ""
    var utc_to_time = ""
    var time_zone = ""


}