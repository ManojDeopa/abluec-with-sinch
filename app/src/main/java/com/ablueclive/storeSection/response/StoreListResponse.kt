package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class StoreListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null


    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("store_list")
        @Expose
        var storeList: List<StoreList>? = null
    }

    class StoreList {

        var title = ""

        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("store_name")
        @Expose
        var storeName: String? = null

        @SerializedName("store_contact_number")
        @Expose
        var storeContactNumber: String? = null

        @SerializedName("store_description")
        @Expose
        var storeDescription: String? = null

        @SerializedName("store_address")
        @Expose
        var storeAddress: String? = null

        @SerializedName("store_complete_address")
        @Expose
        var storeCompleteAddress: String? = null

        @SerializedName("store_latitude")
        @Expose
        var storeLatitude: String? = null

        @SerializedName("store_longitude")
        @Expose
        var storeLongitude: String? = null

        @SerializedName("store_image")
        @Expose
        var storeImage: String? = null

        @SerializedName("images")
        @Expose
        var images: List<Image>? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("store_logo")
        @Expose
        var storeLogo: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("service_time")
        @Expose
        var serviceTime: String? = null

        @SerializedName("service_time_unit")
        @Expose
        var serviceTimeUnit: String? = null

        @SerializedName("isAvailable")
        @Expose
        var isAvailable: Int? = null

        @SerializedName("calender")
        @Expose
        var storeCalender: List<StoreCalender>? = null


        var isOpen: Int? = null
    }

    class Image {
        @SerializedName("ImageName")
        @Expose
        var imageName: String? = null

        @SerializedName("ImageType")
        @Expose
        var imageType: String? = null

        @SerializedName("thumbImage")
        @Expose
        var thumbImage: String? = null
    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }


}