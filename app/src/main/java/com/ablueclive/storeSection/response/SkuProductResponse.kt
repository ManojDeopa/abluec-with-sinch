package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SkuProductResponse {


    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null


    class ResponseData {
        @SerializedName("getBarCodeProductInfo")
        @Expose
        var getBarCodeProductInfo: GetBarCodeProductInfo? = null
    }

    class GetBarCodeProductInfo {
        @SerializedName("product_title")
        @Expose
        var productTitle: String? = null

        @SerializedName("product_price")
        @Expose
        var productPrice: String? = null

        @SerializedName("product_description")
        @Expose
        var productDescription: String? = null

        @SerializedName("sku")
        @Expose
        var sku: String? = null

        @SerializedName("images")
        @Expose
        var images: String? = null

        @SerializedName("product_category")
        @Expose
        var productCategory: String? = null


    }

}