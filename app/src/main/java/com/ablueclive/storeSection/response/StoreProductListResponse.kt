package com.ablueclive.storeSection.response

import com.ablueclive.global.response.ProductModel
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class StoreProductListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("product_listing")
        @Expose
        var productListing: List<ProductListing>? = null

        @SerializedName("store_products")
        @Expose
        var storeProducts: List<ProductModel>? = null

        @SerializedName("fav_products")
        @Expose
        var favProducts: List<FavProduct>? = null

        @SerializedName("product_list")
        @Expose
        var productList: List<ProductModel>? = null

    }


    class FavProduct {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null

        @SerializedName("product")
        @Expose
        var product: ProductModel? = null

        @SerializedName("cart")
        @Expose
        var cart: Cart? = null

        @SerializedName("product_favourite")
        @Expose
        var productFavourite: ProductFavourite? = null
    }


    class Cart {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("qty")
        @Expose
        var qty: Int? = null
    }

    class ProductFavourite {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null
    }


    class ProductListing {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("store_id")
        @Expose
        var storeId: String? = null

        @SerializedName("product_title")
        @Expose
        var productTitle: String? = null

        @SerializedName("product_price")
        @Expose
        var productPrice: Float? = null


        @SerializedName("product_description")
        @Expose
        var productDescription: String? = null

        @SerializedName("delivery_time")
        @Expose
        var deliveryTime: String? = null

        @SerializedName("is_featured")
        @Expose
        var isFeatured: Int? = null

        @SerializedName("product_sale_price")
        @Expose
        var productSalePrice: Float? = null

        @SerializedName("schedule_from_date")
        @Expose
        var scheduleFromDate: String? = null

        @SerializedName("schedule_to_date")
        @Expose
        var scheduleToDate: String? = null

        @SerializedName("is_taxable")
        @Expose
        var isTaxable: Int? = null

        @SerializedName("is_shipping_available")
        @Expose
        var isShippingAvailable: Int? = null

        @SerializedName("stock")
        @Expose
        var stock: Int = 0

        @SerializedName("unit")
        @Expose
        var unit: String? = null

        @SerializedName("sku")
        @Expose
        var sku: String? = null

        @SerializedName("time_zone")
        @Expose
        var timeZone: String? = null

        @SerializedName("iso_schedule_from_date")
        @Expose
        var isoScheduleFromDate: String? = null

        @SerializedName("iso_schedule_to_date")
        @Expose
        var isoScheduleToDate: String? = null

        @SerializedName("images")
        @Expose
        var images: List<String>? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("product_quantity")
        @Expose
        var productQuantity: Any? = null

        @SerializedName("utc_schedule_from_date")
        @Expose
        var utcScheduleFromDate: String? = null

        @SerializedName("utc_schedule_to_date")
        @Expose
        var utcScheduleToDate: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("product_category")
        @Expose
        var productCategory: String? = null

        @SerializedName("product_category_name")
        @Expose
        var productCategoryName: String=""

    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }

}