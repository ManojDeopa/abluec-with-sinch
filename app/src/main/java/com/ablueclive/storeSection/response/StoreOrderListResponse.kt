package com.ablueclive.storeSection.response

import com.ablueclive.global.response.Image
import com.ablueclive.global.response.Loc
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class StoreOrderListResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("Order_Info")
        @Expose
        var orderInfo: List<OrderInfo>? = null
    }

    class OrderInfo {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("custom_order_id")
        @Expose
        var custom_order_id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("instruction")
        @Expose
        var instruction: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("store_id")
        @Expose
        var storeId: String? = null

        @SerializedName("address")
        @Expose
        var address: Address? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: String = ""

        @SerializedName("order_total")
        @Expose
        var orderTotal: Int? = null

        @SerializedName("product")
        @Expose
        var product: List<Product>? = null

        @SerializedName("store")
        @Expose
        var store: Store? = null

        @SerializedName("customer")
        @Expose
        var customer: Customer? = null

        @SerializedName("rating")
        @Expose
        var rating: Any? = null

        @SerializedName("review")
        @Expose
        var review: String? = null
    }


    class Product {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("order_id")
        @Expose
        var orderId: String? = null

        @SerializedName("product_id")
        @Expose
        var productId: String? = null

        @SerializedName("qty")
        @Expose
        var qty: Int? = null

        @SerializedName("price")
        @Expose
        var price: Any? = null

        @SerializedName("total_price")
        @Expose
        var totalPrice: Any? = null

        @SerializedName("product_title")
        @Expose
        var productTitle: String? = null

        @SerializedName("images")
        @Expose
        var images: List<String>? = null

        @SerializedName("sku")
        @Expose
        var sku: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: String? = null
    }


    class Store {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("store_longitude")
        @Expose
        var storeLongitude: String? = null

        @SerializedName("store_address")
        @Expose
        var storeAddress: String? = null

        @SerializedName("store_complete_address")
        @Expose
        var storeCompleteAddress: String? = null

        @SerializedName("store_latitude")
        @Expose
        var storeLatitude: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("store_name")
        @Expose
        var storeName: String? = null

        @SerializedName("store_contact_number")
        @Expose
        var storeContactNumber: String? = null

        @SerializedName("store_description")
        @Expose
        var storeDescription: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("store_image")
        @Expose
        var storeImage: String? = null

        @SerializedName("images")
        @Expose
        var images: List<Image>? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("store_logo")
        @Expose
        var storeLogo: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("isAvailable")
        @Expose
        var isAvailable: Int? = null
    }

    class Address {
        @SerializedName("flatno")
        @Expose
        var flatno: String? = null

        @SerializedName("street")
        @Expose
        var street: String? = null

        @SerializedName("city")
        @Expose
        var city: String? = null

        @SerializedName("state")
        @Expose
        var state: String? = null

        @SerializedName("zip")
        @Expose
        var zip: String? = null

        @SerializedName("country")
        @Expose
        var country: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Float? = null

        @SerializedName("long")
        @Expose
        var _long: Float? = null

        @SerializedName("complete_address")
        @Expose
        var completeAddress: String? = null
    }

    class Customer {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("device_id")
        @Expose
        var deviceId: String? = null

        @SerializedName("device_token")
        @Expose
        var deviceToken: String? = null

        @SerializedName("device_type")
        @Expose
        var deviceType: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("language")
        @Expose
        var language: String? = null

        @SerializedName("verification_otp")
        @Expose
        var verificationOtp: String? = null

        @SerializedName("rating")
        @Expose
        var rating: Int? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("skype")
        @Expose
        var skype: String? = null

        @SerializedName("about")
        @Expose
        var about: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("debug_mode")
        @Expose
        var debugMode: Int? = null

        @SerializedName("hash")
        @Expose
        var hash: String? = null

        @SerializedName("isStoreCreated")
        @Expose
        var isStoreCreated: Int? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("address")
        @Expose
        var address: Address? = null
    }


}