package com.ablueclive.storeSection.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class StoreCategoryResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null


    class ResponseData {
        @SerializedName("store_category")
        @Expose
        var storeCategory: List<StoreCategory>? = null

        @SerializedName("category_listing")
        @Expose
        var categoryListing: List<StoreCategory>? = null

        @SerializedName("allCategory")
        @Expose
        var allCategory: List<StoreCategory>? = null

    }

    class StoreCategory {

        var isSelected = false

        @SerializedName("_id")
        @Expose
        var _id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("parent_id")
        @Expose
        var parentId: String? = null

        @SerializedName("is_having_child")
        @Expose
        var isHavingChild: Int? = null

    }

}