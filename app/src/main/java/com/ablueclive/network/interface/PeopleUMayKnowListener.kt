package com.ablueclive.network.`interface`

import com.ablueclive.modelClass.PeopleUknow

interface PeopleUMayKnowListener {

    fun addFriend(get: PeopleUknow,pos: Int)

    fun cancelRequest(get: PeopleUknow,pos : Int)
}