package com.ablueclive.network.`interface`

import com.ablueclive.network.response.MyFriendListResponse

interface MyFriendRemoveListner {

    fun onMyFriendRemove(get: MyFriendListResponse.GetFriendsListData)

    fun onMessageClick(get: MyFriendListResponse.GetFriendsListData)

    fun onCallClick(type: String, get: MyFriendListResponse.GetFriendsListData)


}