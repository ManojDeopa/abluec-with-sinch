package com.ablueclive.network.`interface`

import com.ablueclive.modelClass.GetFriendListData

interface InvitationListner {
    fun removeInvitation(get: GetFriendListData);
    fun acceptInvitation(get: GetFriendListData);
}

interface FollowingListner {
    fun rejectFollowers(get: GetFriendListData);
    fun acceptFollowers(get: GetFriendListData);
}