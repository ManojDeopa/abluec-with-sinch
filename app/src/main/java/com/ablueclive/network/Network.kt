package com.ablueclive.network

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.fragment.friendList.DeleteFrndContract
import com.ablueclive.fragment.friendList.DeleteFrndPresenter
import com.ablueclive.global.activity.InviteContactsActivity
import com.ablueclive.global.response.CombineFriendResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.listener.onFriendProfileListener
import com.ablueclive.modelClass.ContactModel
import com.ablueclive.modelClass.GetFriendListData
import com.ablueclive.modelClass.PeopleUknow
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.network.`interface`.FollowingListner
import com.ablueclive.network.`interface`.InvitationListner
import com.ablueclive.network.`interface`.MyFriendRemoveListner
import com.ablueclive.network.`interface`.PeopleUMayKnowListener
import com.ablueclive.network.response.MyFriendListResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.sinch.OnCallClickListener
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_network.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class Network(var callClickListener: OnCallClickListener) : Fragment(), onFriendProfileListener, MyFriendRemoveListner, InvitationListner, FollowingListner, PeopleUMayKnowListener, DeleteFrndContract.View {

    private lateinit var contactsList: List<ContactModel>
    private var totalFriendCount = 0
    lateinit var connectionDetector: ConnectionDetector
    private var peopleUknowArrayList = ArrayList<PeopleUknow>()
    private var invitationArrayList = ArrayList<GetFriendListData>()
    private var followingUserArrayList = ArrayList<GetFriendListData>()
    private var friendsList = ArrayList<MyFriendListResponse.GetFriendsListData>()

    lateinit var peopleKnowAdapter: PeopleYouKnowAdapter
    lateinit var invitationAdapter: InvitationAdapter
    lateinit var followingAdapter: FollowUserListAdapter
    lateinit var friendAdapter: MyFriendAdapter
    private var isInternetPresent = false

    var TYPE = ""
    val TYPE_CANCEL_REQUEST = "TYPE_CANCEL_REQUEST"
    val TYPE_REMOVE_INVITATION = "TYPE_REMOVE_INVITATION"
    val TYPE_UN_FRIEND = "TYPE_UN_FRIEND"
    val TYPE_EMPTY = ""


    companion object {
        var shouldRefresh = false
        fun newInstance(bundle: Bundle, callClickListener: OnCallClickListener): Network {
            val fragment = Network(callClickListener)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_network, container, false);
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()

    }


    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            shouldRefresh = false
            initView()
        }
    }

    private fun initView() {
        connectionDetector = ConnectionDetector(activity)
        setPeopleAdapter()
        shouldRefresh = false
        if (connectionDetector.isConnectingToInternet) {
            callData()
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), context)
        }

        tvInvite.setOnClickListener {
            CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), InviteContactsActivity::class.java))
        }
    }

    private fun callFriendList() {
        ProgressD.show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["page"] = "1"
        param["type"] = "list"
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getFriendListNetwork(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<MyFriendListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: MyFriendListResponse) {
                        ProgressD.hide()
                        try {
                            when {
                                response.responseStatus == 1 -> {
                                    friendsList.clear()

                                    if (response.responseData?.total_count != null) {
                                        totalFriendCount = response.responseData?.total_count!!
                                    }

                                    response.responseData?.getAllFriendsList?.let { friendsList.addAll(it) }
                                }
                                response.response_invalid == 1 -> {
                                    logout(response.responseMsg.toString())
                                }
                                else -> {
                                    CommonMethod.showToastShort(response.responseMsg, context)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        updateEmptyUi()
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, context)
        BMSPrefs.putString(context, Constants._ID, "")
        BMSPrefs.putString(context, Constants.isStoreCreated, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(context, Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(context!!, Intent(context, LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    private fun updateEmptyUi() {

        try {
            var emptyCount = 0

            if (peopleUknowArrayList.isEmpty()) {
                label.visibility = View.GONE
                emptyCount += 1
            } else {
                label.visibility = View.VISIBLE
            }

            if (friendsList.isEmpty()) {
                label_my_friend.visibility = View.INVISIBLE
                my_friend_list.visibility = View.GONE
                emptyCount += 1
            } else {

                if (totalFriendCount == 0) {
                    totalFriendCount = friendsList.size
                }

                label_my_friend.text = getString(R.string.my_friend) + " (" + totalFriendCount + ")"
                label_my_friend.visibility = View.VISIBLE
                my_friend_list.visibility = View.VISIBLE
                if (friendsList.size >= 4) {
                    friendAdapter.updateListCount(3)
                    seeAllMyFriends.visibility = View.VISIBLE
                } else {
                    seeAllMyFriends.visibility = View.GONE
                    friendAdapter.updateListCount(friendsList.size)
                }
                seeAllMyFriends.setOnClickListener {
                    CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), SeeAllFriend::class.java))
                }
            }


            if (followingUserArrayList.isEmpty()) {
                followoing_list.visibility = View.GONE
                label_following.visibility = View.GONE
            } else {
                followoing_list.visibility = View.VISIBLE
                label_following.visibility = View.VISIBLE
            }


            if (invitationArrayList.isEmpty()) {
                invitation_list.visibility = View.GONE
                label_invitation.visibility = View.GONE
                emptyCount += 1
            } else {
                invitation_list.visibility = View.VISIBLE
                label_invitation.visibility = View.VISIBLE
                if (invitationArrayList.size >= 15) {
                    seeAllInvitation.visibility = View.VISIBLE
                } else {
                    seeAllInvitation.visibility = View.GONE
                }
            }

            if (emptyCount == 3) {
                CommonMethod.showAlert(requireContext(), getString(R.string.no_data_found))
            }

            tvInvite.visibility = View.VISIBLE

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setPeopleAdapter() {
        peopleKnowAdapter = PeopleYouKnowAdapter(1, peopleUknowArrayList, this)
        invitationAdapter = InvitationAdapter(invitationArrayList, this)
        followingAdapter = FollowUserListAdapter(followingUserArrayList, this)
        friendAdapter = MyFriendAdapter(friendsList, this)

        val layoutManager2 = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        val layoutManagerVertical = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        val layoutManagerVertical2 = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        val layoutManagerVertical3 = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

        network_list.apply {
            layoutManager = layoutManager2
            adapter = peopleKnowAdapter
        }

        invitation_list.apply {
            layoutManager = layoutManagerVertical
            adapter = invitationAdapter
        }

        followoing_list.apply {
            layoutManager = layoutManagerVertical3
            adapter = followingAdapter
        }

        my_friend_list.apply {
            layoutManager = layoutManagerVertical2
            adapter = friendAdapter
        }

    }

    private fun callData() {
        ProgressD.show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getCombineFriendRecord(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CombineFriendResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CombineFriendResponse) {
                        ProgressD.hide()
                        if (response.response_status == 1) {

                            peopleUknowArrayList.clear()
                            invitationArrayList.clear()
                            followingUserArrayList.clear()

                            response.response_data?.getCombineFriendRecord?.PeopleUknow?.let { peopleUknowArrayList.addAll(it) }
                            response.response_data?.getCombineFriendRecord?.getFriendListData?.let { invitationArrayList.addAll(it) }
                            response.response_data?.getCombineFriendRecord?.RequestToFollowList?.let { followingUserArrayList.addAll(it) }
                            peopleKnowAdapter.notifyDataSetChanged()
                            invitationAdapter.notifyDataSetChanged()
                            followingAdapter.notifyDataSetChanged()
                        } else {
                            CommonMethod.showToastlong(response.response_msg, requireContext())
                        }

                        callFriendList()

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onAddFriendClick(friend_id: String?, position: Int) {

    }

    override fun onFrndProfileClick(friend_id: String?, position: Int) {

    }

    override fun onMyFriendRemove(get: MyFriendListResponse.GetFriendsListData) {
        TYPE = TYPE_UN_FRIEND
        unFriendApi(get.id)
    }

    override fun onMessageClick(get: MyFriendListResponse.GetFriendsListData) {
        onMessagingClick(get.id.toString(), get.name, get.mobile, get.profile_image)
    }


    override fun onCallClick(type: String, get: MyFriendListResponse.GetFriendsListData) {

        if (type == getString(R.string.voice_call)) {
            callClickListener.onActionVoiceCall(get.id.toString(), get.name.toString(), get.profile_image.toString())
        } else {
            callClickListener.onActionVideoCall(get.id.toString(), get.name.toString(), get.profile_image.toString())
        }
    }

    override fun removeInvitation(get: GetFriendListData) {
        delFriendRequest(get._id)
    }

    override fun acceptInvitation(get: GetFriendListData) {
        acceptInvites(get._id);
    }


    private fun unFriendApi(id: Any?) {
        ProgressD.show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["un_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.UnFriendRequest(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        if (response.response_status == "1") {
                            callFriendList()
                        } else {
                            CommonMethod.showAlert(requireContext(), response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun addFriend(item: Any?) {
        ProgressD.show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["add_friend_id"] = item.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addFriend(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        if (response.response_status == "1") {
                            CommonMethod.showAlert(requireContext(), response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun acceptInvites(id: Any?) {
        ProgressD.show(requireContext(), "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["confirm_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.confirmFriendInList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        if (response.response_status == "1") {
                            CommonMethod.showAlert(requireContext(), response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun delFriendRequest(delete_friend_id: String) {
        val params = java.util.HashMap<String, String>()
        params["delete_friend_id"] = delete_friend_id
        params["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        Log.d("delFriendRequestParam", params.toString())
        val deleteFrndPresenter = DeleteFrndPresenter(this)
        deleteFrndPresenter.requestDeleteFriend(params)
    }


    private fun onMessagingClick(friend_id: String?, user_name: String?, mobile: String?, userImage: String?) {
        BMSPrefs.putString(requireContext(), Constants.TO_ID, friend_id)
        BMSPrefs.putString(requireContext(), Constants.FRIEND_MOBILE, mobile)
        BMSPrefs.putString(requireContext(), Constants.CHAT_USER_NAME, user_name)
        BMSPrefs.putString(requireContext(), Constants.CHAT_USER_IMAGE, userImage)
        CommonMethod.callActivity(requireContext(), Intent(requireContext(), StartChatActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun showProgress() {
        ProgressD.show(requireContext(), "")
    }

    override fun hideProgress() {
        ProgressD.hide()
    }

    override fun setDataDeleteFriend(response_status: String?, response_msg: String?, response_invalid: String?) {
        // CommonMethod.showToastlong(response_msg, requireContext())
    }

    override fun onResponseFailure(throwable: Throwable?) {
        println(throwable)
    }


    public fun followUnFollowApi(isAcceptFolow: Boolean, userId: String) {

        if (!connectionDetector.isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), context)
            return
        }

        ProgressD.show(context!!, "")

        val param = java.util.HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(context, Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["user_id"] = userId


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.followAccepted(param)
        if (!isAcceptFolow) {
            apiCall = apiInterface.followRejected(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                if (followingUserArrayList.size < 1) {
                                    followoing_list.visibility = View.GONE
                                    label_following.visibility = View.GONE
                                }
                                followingAdapter.notifyDataSetChanged()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun rejectFollowers(get: GetFriendListData) {
        followUnFollowApi(false, get._id)
    }

    override fun acceptFollowers(get: GetFriendListData) {
        followUnFollowApi(true, get._id)
    }


    private fun followUser(isTrue: Boolean, friendID: String, pos: Int) {

        if (!connectionDetector.isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), context)
            return
        }

        ProgressD.show(context!!, "")

        val param = java.util.HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(context, Constants._ID)
        param["session_token"] = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        param["to_user_id"] = friendID


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        var apiCall = apiInterface.follow(param)
        if (!isTrue) {
            apiCall = apiInterface.unFollow(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                peopleKnowAdapter.notifyItemChanged(pos)
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, requireContext())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun addFriend(get: PeopleUknow, pos: Int) {
        //addFriend(get._id)
        get.already_send_request = "1"
        followUser(true, get._id, pos)
    }

    override fun cancelRequest(get: PeopleUknow, pos: Int) {
        get.already_send_request = "0"
        followUser(false, get._id, pos)
        //unFriendApi(get._id)
    }

}