package com.ablueclive.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyFriendListResponse {

    @SerializedName("response_invalid")
    @Expose
    var response_invalid: Int = 0

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null


    class ResponseData {
        @SerializedName("getAllFriendsList")
        @Expose
        var getAllFriendsList: List<GetFriendsListData>? = null

        @SerializedName("total_count")
        @Expose
        var total_count: Int? = null

    }

    class GetFriendsListData {

        @SerializedName("_id")
        @Expose
        var id: Any? = null

        @SerializedName("profile_image")
        @Expose
        var profile_image: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("timestamp")
        @Expose
        var timestamp: String? = null

        @SerializedName("read_status")
        @Expose
        var readStatus: Int? = null

        @SerializedName("last_message")
        @Expose
        var lastMessage: String? = null

    }
}