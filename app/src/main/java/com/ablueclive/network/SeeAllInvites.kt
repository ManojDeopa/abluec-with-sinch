package com.ablueclive.network

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ablueclive.R
import com.ablueclive.global.response.CombineFriendResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.GetFriendListData
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.network.`interface`.InvitationListner
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ProgressD
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_network.*
import kotlinx.android.synthetic.main.new_app_header.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class SeeAllInvites : AppCompatActivity(), InvitationListner {

    private var invitationArrayList = ArrayList<GetFriendListData>()
    lateinit var invitationAdapter: InvitationAdapter
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_see_all_friends)


        img_back.setOnClickListener { finish() }
        invitationAdapter = InvitationAdapter(this.invitationArrayList, this)

    }

    override fun removeInvitation(get: GetFriendListData) {

    }

    override fun acceptInvitation(get: GetFriendListData) {
        acceptInvites(get._id);
    }

    private fun acceptInvites(id: Any?) {

        ProgressD.show(this, "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        param["confirm_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.confirmFriendInList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        if (response.response_status.equals("1")) {
                            callData()
                        } else {
                            CommonMethod.showAlert(this@SeeAllInvites, response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun callData() {
        ProgressD.show(this, "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getCombineFriendRecord(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CombineFriendResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: CombineFriendResponse) {
                        ProgressD.hide()
                        if (response.response_status == 1) {
                            invitationArrayList.clear()
                            response.response_data?.getCombineFriendRecord?.getFriendListData?.let { invitationArrayList.addAll(it) }

                            invitationAdapter.notifyDataSetChanged()

                            if (invitationArrayList.isEmpty()) {
                                invitation_list.visibility = View.GONE
                                label_invitation.visibility = View.GONE
                            } else {
                                invitation_list.visibility = View.VISIBLE
                                label_invitation.visibility = View.VISIBLE
                                if (invitationArrayList.size > 4) {
                                    seeAllInvitation.visibility = View.VISIBLE;
                                } else {
                                    seeAllInvitation.visibility = View.GONE;
                                }
                            }

                        } else {
                            CommonMethod.showAlert(this@SeeAllInvites, response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}