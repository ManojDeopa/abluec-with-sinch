package com.ablueclive.network

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.modelClass.GetFriendListData
import com.ablueclive.network.`interface`.FollowingListner
import com.ablueclive.network.`interface`.InvitationListner
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.friendlist_adapter.view.*

class FollowUserListAdapter (var list: MutableList<GetFriendListData>, var listner: FollowingListner) : RecyclerView.Adapter<FollowUserListAdapter.ViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.friendlist_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: FollowUserListAdapter.ViewHolder, position: Int) {
        holder.itemView.text_name.text = list[position].name
        // holder.itemView.text_confirm.text = context.getString(R.string.follow)

        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list.get(position).profile_image, holder.itemView.img_friend)

        holder.itemView.text_confirm.setOnClickListener {
            listner.acceptFollowers(list[position])
            list.removeAt(position)
            notifyItemRemoved(position)
        }

        holder.itemView.text_delete.setOnClickListener {
           listner.rejectFollowers(list[position])
            list.removeAt(position)
            notifyItemRemoved(position)
        }

        holder.itemView.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position]._id)
            CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}