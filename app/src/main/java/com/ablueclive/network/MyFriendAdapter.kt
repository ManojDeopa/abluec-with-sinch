package com.ablueclive.network

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.network.`interface`.MyFriendRemoveListner
import com.ablueclive.network.response.MyFriendListResponse
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.popupMenuTextList
import com.ablueclive.utils.Constants
import com.ablueclive.utils.MenuItemClickListener
import kotlinx.android.synthetic.main.recycler_myfriend.view.*


class MyFriendAdapter(var list: MutableList<MyFriendListResponse.GetFriendsListData>, var listner: MyFriendRemoveListner) : RecyclerView.Adapter<MyFriendAdapter.ViewHolder>() {

    lateinit var context: Context
    var count = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyFriendAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.recycler_myfriend, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return count
    }

    override fun onBindViewHolder(holder: MyFriendAdapter.ViewHolder, position: Int) {
        holder.itemView.text_name.text = list[position].name
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].profile_image, holder.itemView.img_friend)

        holder.itemView.message.setOnClickListener {
            listner.onMessageClick(list[position])
        }

        holder.itemView.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].id.toString())
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }
    }

    public fun updateListCount(i: Int) {
        count = i
        notifyDataSetChanged()

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.remove.setOnClickListener {
                val itemList = arrayOf(context.getString(R.string.voice_call), context.getString(R.string.video_call), context.getString(R.string.remove_friend))
                popupMenuTextList(context, itemView.remove, itemList, object : MenuItemClickListener {
                    override fun onMenuItemClick(text: String, position: Int) {
                        if (text == context.getString(R.string.voice_call) || text == context.getString(R.string.video_call)) {
                            listner.onCallClick(text, list[absoluteAdapterPosition])
                        } else {
                            listner.onMyFriendRemove(list[absoluteAdapterPosition])
                        }
                    }
                })
            }
        }
    }


}