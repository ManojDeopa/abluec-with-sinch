package com.ablueclive.network

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.modelClass.PeopleUknow
import com.ablueclive.network.`interface`.PeopleUMayKnowListener
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.people_know_adapter.view.*


class PeopleYouKnowAdapter(var from: Int, var list: MutableList<PeopleUknow>, var listener: PeopleUMayKnowListener) : RecyclerView.Adapter<PeopleYouKnowAdapter.ViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleYouKnowAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.people_know_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PeopleYouKnowAdapter.ViewHolder, position: Int) {

        Glide.with(context)
                .load(Constants.PROFILE_IMAGE_URL + list[position].profile_image)
                .skipMemoryCache(true)
                .error(R.drawable.user_profile_blue_transp)
                .into(holder.itemView.ivImage)

        holder.itemView.text_user_name.text = list[position].name
        if (list[position].already_send_request == "1") {
//            if (from == 1)
//              //holder.itemView.tvText.setBackgroundColor(Color.RED)
//            else  holder.itemView.tvText.setBackgroundColor(Color.BLUE)

            holder.itemView.tvText.setBackgroundResource(R.drawable.red_button_border)
            holder.itemView.tvText.text = context.getString(R.string.UNFOLLOW)
            holder.itemView.tvText.setOnClickListener {

                val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                alert.setTitle(context.getString(R.string.app_name))
                alert.setMessage(context.getString(R.string.sure_to_un_follow))
                alert.setCancelable(false)
                alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    listener.cancelRequest(list[position], position)
                }

                alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                }
                alert.show()
            }

        } else {
            holder.itemView.tvText.setBackgroundResource(R.drawable.blue_button_border)
            holder.itemView.tvText.isClickable = true
            holder.itemView.tvText.text = context.getString(R.string.follow)
            holder.itemView.tvText.setOnClickListener {
                listener.addFriend(list[position], position)
            }
        }

        val mutualFCount = list[position].mutual_friends_count
        if (mutualFCount == "0") {
            holder.itemView.tvMutualFriends.visibility = View.INVISIBLE
        } else if (mutualFCount == "1") {
            holder.itemView.tvMutualFriends.visibility = View.VISIBLE
            holder.itemView.tvMutualFriends.text = "$mutualFCount Mutual Friend"
        } else {
            holder.itemView.tvMutualFriends.visibility = View.VISIBLE
            holder.itemView.tvMutualFriends.text = "$mutualFCount Mutual Friends"
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                BMSPrefs.putString(context, Constants.FRIEND_ID, list[absoluteAdapterPosition]._id)
                CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
        }
    }
}