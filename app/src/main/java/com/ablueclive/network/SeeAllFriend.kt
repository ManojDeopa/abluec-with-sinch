package com.ablueclive.network

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.network.`interface`.MyFriendRemoveListner
import com.ablueclive.network.response.MyFriendListResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.sinch.BaseActivity
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ProgressD
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.common_activity_container.*
import kotlinx.android.synthetic.main.progress_horizontal.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class SeeAllFriend : BaseActivity(), MyFriendRemoveListner {

    lateinit var friendAdapter: MyFriendAdapter
    private var friendsList = ArrayList<MyFriendListResponse.GetFriendsListData>()
    lateinit var context: Context

    var isLoading = false
    var pageCount = 1
    var rowCount = 15
    lateinit var linearLayoutManager: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_all_friends)
        context = this


        ivBack.setOnClickListener {
            onBackPressed()
        }

        friendAdapter = MyFriendAdapter(friendsList, this)
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = friendAdapter
        }


        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    callFriendList()
                }
            }
        })

        callFriendList()
    }


    override fun onMyFriendRemove(get: MyFriendListResponse.GetFriendsListData) {
        unfriendList(get.id)
    }

    override fun onMessageClick(get: MyFriendListResponse.GetFriendsListData) {
        onMessagingClick(get.id.toString(), get.name, get.mobile, get.profile_image)
    }

    override fun onCallClick(type: String, get: MyFriendListResponse.GetFriendsListData) {
        if (type == getString(R.string.voice_call)) {
            actionVoiceCall(get.id.toString(), get.name.toString(), get.profile_image.toString())
        } else {
            actionVoiceCall(get.id.toString(), get.name.toString(), get.profile_image.toString())
        }
    }


    private fun onMessagingClick(friend_id: String?, user_name: String?, mobile: String?, user_image: String?) {
        BMSPrefs.putString(context, Constants.TO_ID, friend_id)
        BMSPrefs.putString(context, Constants.FRIEND_MOBILE, mobile)
        BMSPrefs.putString(context, Constants.CHAT_USER_NAME, user_name)
        BMSPrefs.putString(context, Constants.CHAT_USER_IMAGE, user_image)
        CommonMethod.callActivity(context, Intent(context, StartChatActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    private fun unfriendList(id: Any?) {

        ProgressD.show(context, "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        param["un_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.UnFriendRequest(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        if (response.response_status == "1") {
                            callFriendList()
                        } else {
                            CommonMethod.showAlert(context, response.response_msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun callFriendList() {
        pbHeader.visibility = VISIBLE
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        param["type"] = "list"
        param["page"] = pageCount.toString()
        param["row_count"] = rowCount.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getFriendListNetwork(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<MyFriendListResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: MyFriendListResponse) {
                        pbHeader.visibility = GONE
                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.getAllFriendsList
                                if (!list.isNullOrEmpty()) {
                                    CommonMethod.isEmptyView(false, context, "")
                                    friendsList.addAll(list)
                                    friendAdapter.updateListCount(friendsList.size)
                                    isLoading = false
                                    pageCount++
                                } else {
                                    CommonMethod.isEmptyView(true, context, getString(R.string.no_friends))
                                }
                            } else {
                                CommonMethod.showToastlong(response.responseMsg, context)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        pbHeader.visibility = GONE
                        println(e.message)
                    }
                })
    }


}