package com.ablueclive.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


public class CommonMethods {

    // The minimum distance to change Updates in meters
    private static final long MINIMUM_UPDATE_DISTANCE = 10;
    // The minimum time between updates in milliseconds
    private static final long MINIMUM_UPDATE_TIME = 1000 * 60 * 1;
    static ProgressDialog dialog;
    protected int daa;


    /**
     * method used to get current time
     *
     * @return date and time both
     */
    public static String getFormattedCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
//        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        String dateTime = formatter.format(date);
        return dateTime;
    }


    public static Bitmap getMarkerBitmapFromView(View customMarkerView, ImageView markerImageView, final Bitmap bitmap) {
        markerImageView.setImageBitmap(bitmap);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


    /**
     * method used to validate ic_email
     *
     * @param email get ic_email
     * @return true or false
     */
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * method used to get age from date of birth
     *
     * @param _month month
     * @param _day   day
     * @param _year  year
     * @return age
     */
    public static int getAge(int _month, int _day, int _year) {

        GregorianCalendar cal = new GregorianCalendar();
        int year, month, date, age;

        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        date = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        age = year - cal.get(Calendar.YEAR);
        if ((month < cal.get(Calendar.MONTH))
                || ((month == cal.get(Calendar.MONTH)) && (date < cal.get(Calendar.DAY_OF_MONTH)))) {
            --age;
        }
        if (age < 0)
            throw new IllegalArgumentException("Age < 0");
        return age;
    }


    public static boolean CheckDates(String startDate, String endDate) {

        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");

        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return b;
    }


    /**
     * method used to change the date format
     *
     * @param date ,inputfrmat,outputformat
     * @return changed date
     */
    public static String formatDate(String date, String initDateFormat, String endDateFormat) {
        String inputPattern = initDateFormat;
        String outputPattern = endDateFormat;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date1 = null;
        String str = null;
        try {
            date1 = inputFormat.parse(date);
            str = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public static String changeCurrentTime(String date) {
        SimpleDateFormat inputFormat11 = new SimpleDateFormat("MMM ,dd yyyy hh:mm a");
        Date fecha = new Date(date);
        DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
        Date date2 = null, date3;
        String str = null;
        try {
            date2 = (Date) formatter.parse(fecha.toString());
            date3 = formatter.parse(date);
            str = inputFormat11.format(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("date22222222" + date2);
        System.out.println("str11" + str);


        return str;
    }

    /**
     * method used to change date format
     *
     * @param date date
     * @return change format date
     */
    public static String changeDateFormat(String date) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String mDate = date.substring(0, date.indexOf(' '));
   /*     System.out.println("Date: " + date.substring(0, date.indexOf(' ')));
        SimpleDateFormat output = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date oneWayTripDate = input.parse(date);  // parse input
            date = output.format(oneWayTripDate);    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        return mDate;
    }

    /**
     * method used to change the font
     *
     * @param context get context
     * @return changed font
     */
    public static Typeface headerFont(Context context) {
        Typeface headerFont = Typeface.createFromAsset(context.getAssets(), "font/OpenSans-Regular.ttf");

        return headerFont;
    }

    /**
     * method used to change the font
     *
     * @param context get context
     * @return changed font
     */
    public static Typeface boldFont(Context context) {
        Typeface headerFont = Typeface.createFromAsset(context.getAssets(), "font/Lato-Bold.ttf");

        return headerFont;
    }

    /**
     * method used to change the font
     *
     * @param context get context
     * @return changed font
     */
    public static Typeface normalText(Context context) {
        Typeface headerFont = Typeface.createFromAsset(context.getAssets(), "font/Lato-Light.ttf");

        return headerFont;
    }

    /**
     * method used to remove a comma from last in string
     *
     * @param str contain comma seprated string
     * @return string
     */
    public static String method(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        Log.d("String is" + " String", str);
        return str;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * method used to check is service started
     *
     * @param serviceClass contain service class
     * @param activity     contain activity object
     * @return true or false
     */
    public static boolean isServiceRunning(Class<?> serviceClass, Activity activity) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * method used to get date and time
     *
     * @return return date and time
     */
    public static String getDateAndTime() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        Log.d("Current Date:- ", formattedDate);
        return formattedDate;
    }

    /**
     * method used to get distance in miles
     *
     * @param srcLat contain begin lat
     * @param srcLng contain begin long
     * @param desLat contain ic_drop lat
     * @param desLng contain ic_drop long
     * @return distance in miles
     */
    public static double distanceBetweenTwoPoint(double srcLat, double srcLng, double desLat, double desLng) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(desLat - srcLat);
        double dLng = Math.toRadians(desLng - srcLng);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(srcLat))
                * Math.cos(Math.toRadians(desLat)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        return dist;
//        double meterConversion = 1609;

//        return (int) (dist * meterConversion);
    }

    /**
     * method used to get distance in kilometers
     *
     * @param distance contain distance
     * @return distance in kilometer
     */
    public static double getDistanceInKilometers(double distance) {
        return distance * 1.60934;
    }

    /**
     * method used to get distance in meters
     *
     * @param distance contain distance
     * @return distance in meter
     */
    public static double getDistanceInMeters(double distance) {
        return distance * 1609.34;
    }

    /**
     * method used to get distance in meters
     *
     * @param distance contain distance
     * @return distance in meter
     */
    public static double metersToMiles(double distance) {
        return distance / 1609.34;
    }

    /**
     * method used to convert date format
     *
     * @param oldDate contain old date
     * @return converted date
     */
    public static String getConvertedDate(String oldDate) {
        Date date = null;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat write = new SimpleDateFormat("dd-MMM-yyyy");
        write.setTimeZone(TimeZone.getDefault());
        try {
            date = parser.parse(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return write.format(date);
    }

    /**
     * method used to convert date format
     *
     * @param oldDate contain old date
     * @return converted date
     */
    public static String getConvertedDateWithTime(String oldDate) {
        Date date = null;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat write = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        write.setTimeZone(TimeZone.getDefault());
        try {
            date = parser.parse(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return write.format(date);
    }


    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

   /* public static void snackBar(@NotNull AddContacts addContacts, @NotNull String s, @NotNull String s1, @Nullable View rootView, @NotNull Unit requestContact, boolean iS_DENY) {





    }*/

    public static String getRealPathFromURI(Context context, Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                /*final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.parseLong(id));

                return getDataColumn(context, contentUri, null, null);*/


                String fileName = getFilePath(context, uri);
                if (fileName != null) {
                    return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                }

                String id = DocumentsContract.getDocumentId(uri);
                if (id.startsWith("raw:")) {
                    id = id.replaceFirst("raw:", "");
                    File file = new File(id);
                    if (file.exists())
                        return id;
                }

                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static String getFilePath(Context context, Uri uri) {

        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

}
