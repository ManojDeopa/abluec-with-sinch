package com.ablueclive.utils

import AppUtils
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Process
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.format.DateUtils
import android.text.method.LinkMovementMethod
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.CommonContainerActivity
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.common.StoreContainerActivity
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_alert.view.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object CommonMethod {


    @JvmStatic
    fun getProductPrice(scheduleFromDate: String?, scheduleToDate: String?, productSalePrice: String?, productPrice: String?): String {

        val salePrice = if (productSalePrice.isNullOrEmpty()) "0.0" else productSalePrice.toString()
        val normalPrice = if (productPrice.isNullOrEmpty()) "0.0" else productPrice.toString()

        return if (!scheduleFromDate.isNullOrEmpty() && !scheduleToDate.isNullOrEmpty()) {
            val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            val currentDate = AppUtils.getCurrentTimeStamp(pattern).toString()
            if ((AppUtils.timeDifferenceInMinutes(scheduleFromDate, currentDate, pattern) >= 0 && AppUtils.timeDifferenceInMinutes(currentDate, scheduleToDate, pattern) >= 0))
                salePrice
            else
                normalPrice
        } else {
            normalPrice
        }
    }

    @JvmStatic
    fun enableDisableViewGroup(viewGroup: ViewGroup, enabled: Boolean) {
        val childCount: Int = viewGroup.childCount
        for (i in 0 until childCount) {
            val view: View = viewGroup.getChildAt(i)
            view.isEnabled = enabled
            if (view is ViewGroup) {
                enableDisableViewGroup(view, enabled)
            }
        }
    }

    @JvmStatic
    fun extractUrl(input: String): String {
        var str = ""
        str = try {
            input
                    .split(" ")
                    .firstOrNull { Patterns.WEB_URL.matcher(it).find() }!!
        } catch (e: Exception) {
            ""
        }
        return str

    }


    @JvmStatic
    fun isValidUrl(url: String): Boolean {

        if (url.startsWith("http://")) {
            return true
        }

        if (url.startsWith("https://")) {
            return true
        }

        if (url.startsWith("www.")) {
            return true
        }

        return false
    }


    @JvmStatic
    fun isValidDisplayName(str: String): Boolean {
        if (str == "") return false
        return str.toLowerCase(Locale.getDefault()).contains("abluec".toLowerCase(Locale.getDefault()))
    }

    @JvmStatic
    fun popupMenuTextList(context: Context, anchorView: View, itemList: Array<String>, menuItemClickListener: MenuItemClickListener) {
        val listPopupWindow = ListPopupWindow(context)
        listPopupWindow.anchorView = anchorView
        listPopupWindow.setDropDownGravity(Gravity.END)
        listPopupWindow.height = ListPopupWindow.WRAP_CONTENT
        val width: Int = context.resources.getDimensionPixelSize(R.dimen.overflow_width)
        listPopupWindow.width = width
        //listPopupWindow.width = 300
        listPopupWindow.setAdapter(ArrayAdapter<Any?>(context, R.layout.menu_list_item, itemList))
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            listPopupWindow.dismiss()
            menuItemClickListener.onMenuItemClick(itemList[position], position)
        }

        listPopupWindow.show()
    }


    @JvmStatic
    fun popupMenuIconTextList(context: Context, anchorView: View, textList: Array<String>, iconList: Array<Int>, menuItemClickListener: MenuItemClickListener) {
        val listPopupWindow = ListPopupWindow(context)
        listPopupWindow.anchorView = anchorView
        listPopupWindow.setDropDownGravity(Gravity.END)
        listPopupWindow.height = ListPopupWindow.WRAP_CONTENT

        val width: Int = context.resources.getDimensionPixelSize(R.dimen.overflow_width)
        listPopupWindow.width = width
        listPopupWindow.setAdapter(MyListAdapter(context as Activity, textList, iconList))
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            listPopupWindow.dismiss()
            menuItemClickListener.onMenuItemClick(textList[position], position)
        }

        listPopupWindow.show()
    }

    @JvmStatic
    fun showProgressDialog(mProgressDialog: ProgressDialog?, context: Context?, title: String?): ProgressDialog {
        var mProgressDialog = mProgressDialog
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog(context)
            mProgressDialog.isIndeterminate = true
            mProgressDialog.setCancelable(false)
        }
        mProgressDialog.setMessage(title)
        mProgressDialog.show()
        return mProgressDialog
    }

    @JvmStatic
    fun hideProgressDialog(mProgressDialog: ProgressDialog?) {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing) {
                mProgressDialog.dismiss()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getDurationForRoute(context: Context?, lat: Double, lng: Double): String {
        val durationPostFixHour = "Hours"
        val durationPostFix = "Minutes"
        val distancePostFix = "Km"
        var duration = "0$durationPostFix"
        var distance = "0$distancePostFix"
        if (lat == 0.0 || lng == 0.0 || lat == 0.0 || lng == 0.0) return "$distance:$duration"
        try {
            val gpsTracker = GPSTracker(context)
            val location1 = Location("")
            location1.latitude = gpsTracker.latitude
            location1.longitude = gpsTracker.longitude
            val location2 = Location("")
            location2.latitude = lat
            location2.longitude = lng
            val distanceInMeter = location1.distanceTo(location2)
            val distanceInKm = location1.distanceTo(location2) / 1000
            val df = DecimalFormat("##.##")
            distance = df.format(distanceInKm.toDouble()) + distancePostFix
            val speedIs10MetersPerMinute = 100
            val estimatedDriveTimeInMinutes = distanceInMeter / speedIs10MetersPerMinute
            val intDuration = estimatedDriveTimeInMinutes.toInt()
            duration = if (intDuration > 60) {
                (intDuration / 60).toString() + durationPostFixHour
            } else {
                intDuration.toString() + durationPostFix
            }
        } catch (e: NumberFormatException) {
            e.printStackTrace()
            return "$distance:$duration"
        }
        return "$distance:$duration"
    }

    @JvmStatic
    fun getTimesAgo(dataDate: String): String {
        if (dataDate.isEmpty()) return ""
        var ago: CharSequence = dataDate
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val time = sdf.parse(dataDate).time
            val now = System.currentTimeMillis()
            ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ago.toString()
    }


    @JvmStatic
    fun getTimesAgoFromTimeStamp(timeStamp: String): String {
        var ago: CharSequence = ""
        try {
            val time = timeStamp.toLong()
            val now = System.currentTimeMillis()
            ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ago.toString()
    }

    fun friendStatus(status: String?): String {
        var isFriend = "Add Friend"
        when (status) {
            "0" -> isFriend = "Add Friend"
            "1" -> isFriend = "Requested"
            "2" -> isFriend = "Unfriend"
            "3" -> isFriend = "Unfriend"
            "4" -> isFriend = "Requested"
        }
        return isFriend
    }

    //**************** it is used for validate email address***************//
    @JvmStatic
    fun isEmailValid(email: String): Boolean {
        /* var isValid = false
         val expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
         //  String expression = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
         val inputStr: CharSequence = email
         val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
         val matcher = pattern.matcher(inputStr)
         if (matcher.matches()) {
             isValid = true
         }*/

        val isValid = Patterns.EMAIL_ADDRESS.matcher(email).matches()
        if (!isValid) {
            if (email.contains("!")) {
                return true
            }
        }
        return isValid
    }

    @JvmStatic
    fun isValidPassword(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        if (!matcher.matches()) {
            if (password.contains("!")) {
                return true
            }
        }
        return matcher.matches()
    }

    fun isValidPhoneNumber(phoneNumber: String?): Boolean {
        val emailPattern = Pattern
                .compile("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$")
        val m = emailPattern.matcher(phoneNumber)
        return m.matches()
    }

    @JvmStatic
    fun isValidName(name: String?): Boolean {
        val namePattern = Pattern.compile("[a-zA-Z][a-zA-Z ]*")
        val m = namePattern.matcher(name)
        return m.matches()
    }

    @JvmStatic
    fun hideKeyBoard(context: Context) {
        val manager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        try {
            manager.hideSoftInputFromWindow((context as Activity).currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun loadImage(imgUrl: String?, ivImage: AppCompatImageView?) {
        try {
            if (imgUrl != null) {
                Log.e("imgUrl -->", imgUrl)
            }
            Picasso.get().load(imgUrl).placeholder(R.drawable.placeholder).into(ivImage)

            /* Glide.with(ivImage.getContext())
                    .load(imgUrl)
                    .skipMemoryCache(true)
                    .error(R.drawable.placeholder)
                    .into(ivImage);*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun loadImageGlide(imgUrl: String, ivImage: ImageView, placeHolder: Int) {
        try {
            if (imgUrl.isNotEmpty()) {
                Log.e("imgUrl -->", imgUrl)
            }
            // Picasso.get().load(imgUrl).placeholder(R.drawable.placeholder).into(ivImage)
            Glide.with(ivImage.context)
                    .asBitmap()
                    .load(imgUrl)
                    .override(500, 300)
                    .centerCrop()
                    .skipMemoryCache(false)
                    .placeholder(placeHolder)
                    .into(ivImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun loadImageResize(imgUrl: String?, ivImage: AppCompatImageView?) {
        try {
            Log.e("imgUrl -->", "" + imgUrl)

            var placeHolder = R.drawable.placeholder
            if (imgUrl?.contains(Constants.PROFILE_IMAGE_URL)!!) {
                placeHolder = R.drawable.user_profile
            }
            Picasso.get().load(imgUrl).resize(250, 250).placeholder(placeHolder).into(ivImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun loadImageCircle(imgUrl: String?, ivImage: AppCompatImageView?) {
        try {
            Log.e("imgUrl -->", "" + imgUrl)

            var placeHolder = R.drawable.place_holder_circle
            if (imgUrl?.contains(Constants.PROFILE_IMAGE_URL)!!) {
                placeHolder = R.drawable.user_profile
            }

            Picasso.get().load(imgUrl).resize(250, 250).placeholder(placeHolder).transform(PicassoCircleTransformation()).into(ivImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun showAlertWithNoButtons(context: Context?, title: String?, msg: String?) {
        AlertDialog.Builder(context)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(msg)
                .show()
    }

    fun alertErrorAndExit(context: Context?, msg: String) {
        val d = AlertDialog.Builder(context)
        d.setCancelable(false)
        d.setTitle("Oppps!")
        d.setMessage("$msg!\nApp will be terminated!")
        d.setPositiveButton("EXIT") { dialog, which ->
            Process.killProcess(Process.myPid()) //exit app (by killing its process)
        }
        d.show()
    }

    fun showAlert(context: Context, title: String?, string: String?) {
        /*val alert = androidx.appcompat.app.AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(title)
        alert.setMessage(string)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
        alert.show()*/


        val act = context as Activity
        val viewGroup: ViewGroup = act.findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(act).inflate(R.layout.custom_alert, viewGroup, false)
        val builder = AlertDialog.Builder(act)
        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.show()


        dialogView.tvTitle.text = title
        dialogView.tvText.text = string

        dialogView.tvOk.setOnClickListener {
            alertDialog.dismiss()
        }

    }

    fun showAlert(context: Context, text: String?) {
        /*val alert = androidx.appcompat.app.AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
        alert.show()*/

        val act = context as Activity
        val viewGroup: ViewGroup = act.findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(act).inflate(R.layout.custom_alert, viewGroup, false)
        val builder = AlertDialog.Builder(act)
        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.show()


        dialogView.tvTitle.text = context.getString(R.string.app_name)
        dialogView.tvText.text = text

        dialogView.tvOk.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    fun callIntent(text: String, context: Context) {
        try {
            if (text.isEmpty()) return
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$text")
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //**************** it is used for make short length toast***************//
    @JvmStatic
    fun showToastShort(text: String?, context: Context?) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    //**************** it is used for make long length toast***************//
    @JvmStatic
    fun showToastlong(text: String?, context: Context?) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    @SuppressLint("ResourceAsColor")
    fun displayMessage(view: View?, toastString: String?) {
//        Snackbar.make(view, toastString, Snackbar.LENGTH_SHORT)
//                .setAction("Action", null).show();
        val snackbar = Snackbar
                .make(view!!, toastString!!, Snackbar.LENGTH_LONG)

        // Changing message text color
        snackbar.setActionTextColor(R.color.app_color)
        val sbView = snackbar.view
        //     sbView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.app_color));
        val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        snackbar.show()
    }

    fun getDate(sDate: String?): Date? {
        return try {
            SimpleDateFormat("yyyy-MM-dd").parse(sDate)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun getTime(sTime: String?): Date? {
        return try {
            SimpleDateFormat("hh:mm aa").parse(sTime)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun getBitmapFromURL(src: String?): Bitmap? {
        return try {
            val connection = URL(src).openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            BitmapFactory.decodeStream(connection.inputStream)
        } catch (e: IOException) {
            null
        }
    }

    fun StringToBitMap(encodedString: String?): Bitmap? {
        return try {
            val encodeByte = Base64.decode(encodedString, 0)
            BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            null
        }
    }

    fun getBase64(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    @JvmStatic
    fun callActivity(context: Context, intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    @JvmStatic
    fun callCommonContainer(activity: Activity, title: String?) {
        val intent = Intent(activity, CommonContainerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(Constants.TITLE, title)
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    fun callJobContainer(activity: Activity, title: String?) {
        val intent = Intent(activity, JobContainerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(Constants.TITLE, title)
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    fun callStoreContainer(activity: Activity, title: String?) {
        val intent = Intent(activity, StoreContainerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(Constants.TITLE, title)
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    @JvmStatic
    fun callActivityFinish(context: Context, intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }

    @JvmStatic
    fun getDateFromTimestamp(timestamp: Long, formate: String?): String {
        val sdf = SimpleDateFormat(formate!!, Locale.getDefault())
        /*sdf.setTimeZone(TimeZone.getTimeZone("GMT"));*/
        val date = Date(timestamp)
        return sdf.format(date)
    }

    fun isEmptyView(b: Boolean, context: Context, string: String?) {
        val commonListLayout = (context as Activity).findViewById<RelativeLayout>(R.id.commonListLayout)
        val recyclerView: RecyclerView = commonListLayout.findViewById(R.id.recyclerView)
        val noJobLayout: LinearLayoutCompat = commonListLayout.findViewById(R.id.emptyParentLayout)
        val tvNoData = noJobLayout.findViewById<TextView>(R.id.tvNoData)
        if (b) {
            recyclerView.visibility = View.GONE
            tvNoData.text = string
            noJobLayout.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            noJobLayout.visibility = View.GONE
        }
    }

    @JvmStatic
    fun makeTextViewResizable(tv: TextView, maxLine: Int, expandText: String, viewMore: Boolean, listener: OnSeeMoreClickListener, pos: Int) {
        if (tv.tag == null) {
            tv.tag = tv.text
        }

        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                try {
                    if (tv.layout == null) return

                    //val obs = tv.viewTreeObserver
                    //obs.removeGlobalOnLayoutListener(this)
                    vto.removeOnGlobalLayoutListener(this)

                    if (maxLine == 0) {
                        val lineEndIndex = tv.layout.getLineEnd(0)
                        val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(addClickablePartTextViewResizable(HtmlCompat.fromHtml(tv.text.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY), tv, maxLine, expandText,
                                viewMore, listener, pos), TextView.BufferType.SPANNABLE)
                    } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                        val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                        val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                                addClickablePartTextViewResizable(HtmlCompat.fromHtml(tv.text.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY), tv, maxLine, expandText,
                                        viewMore, listener, pos), TextView.BufferType.SPANNABLE)
                    } else {
                        val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                        val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                                addClickablePartTextViewResizable(HtmlCompat.fromHtml(tv.text.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY), tv, lineEndIndex, expandText,
                                        viewMore, listener, pos), TextView.BufferType.SPANNABLE)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun addClickablePartTextViewResizable(strSpanned: Spanned, tv: TextView, maxLine: Int, spanableText: String, viewMore: Boolean, listener: OnSeeMoreClickListener, pos: Int): SpannableStringBuilder {
        val str = strSpanned.toString()
        val ssb = SpannableStringBuilder(strSpanned)
        if (str.contains(spanableText)) {
            ssb.setSpan(object : MySpannable(false) {
                override fun onClick(widget: View) {
                    if (viewMore) {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv, -1, "", false, listener, pos)
                        // makeTextViewResizable(tv, -1, " view less", false, listener, pos)
                        listener.onSeeMoreClick("expand", pos)
                    } else {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv, maxLine, " view more", true, listener, pos)
                        listener.onSeeMoreClick("collapse", pos)
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
        }
        return ssb
    }

}