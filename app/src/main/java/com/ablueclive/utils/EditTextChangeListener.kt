package com.ablueclive.utils

interface EditTextChangeListener {

    fun onTextChange(text: String)

}