package com.ablueclive.utils;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.Nullable;

import com.ablueclive.R;
import com.squareup.picasso.Picasso;

public class ShowFullImage extends Activity {

    private ZoomImageView show_full_image;
    private VideoView video_view;
    private int position = 0;
    private MediaController mediaController;
    ProgressBar pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_full_image);

        video_view = findViewById(R.id.video_view);
        show_full_image = findViewById(R.id.ivImage);

        pd = findViewById(R.id.progress_bar);


        if (mediaController == null) {
            mediaController = new MediaController(this);
            mediaController.setAnchorView(video_view);
            video_view.setMediaController(mediaController);
        }

        if (getIntent().getExtras() != null) {

            if (getIntent().getStringExtra("ImageUrl") != null) {
                show_full_image.setVisibility(View.VISIBLE);
                String imageName = getIntent().getStringExtra("ImageUrl");
                pd.setVisibility(View.GONE);
                Picasso.get().load(imageName).placeholder(R.drawable.placeholder_fitxy).into(show_full_image);

            } else {

                String videolink = getIntent().getStringExtra("video_path");
                video_view.setVisibility(View.VISIBLE);

                Uri video = Uri.parse(videolink);
                video_view.setVideoURI(video);
                video_view.requestFocus();
                video_view.setOnPreparedListener(mediaPlayer -> {
                    video_view.seekTo(position);
                    if (position == 0) {
                        pd.setVisibility(View.GONE);
                        video_view.start();
                    }
                    // When video Screen change size.
                    mediaPlayer.setOnVideoSizeChangedListener((mp, width, height) -> {
                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(video_view);
                    });
                });
            }
        }
    }

}

