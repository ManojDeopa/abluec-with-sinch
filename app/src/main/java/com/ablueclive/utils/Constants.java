package com.ablueclive.utils;

import android.content.Context;

import com.ablueclive.activity.profile.MyProfileResponse;
import com.ablueclive.global.response.ProductModel;
import com.ablueclive.global.response.SearchStoreListResponse;
import com.ablueclive.jobSection.newJobFragment.NewJobDetailResponse;
import com.ablueclive.storeSection.response.StoreListResponse;
import com.ablueclive.storeSection.response.StoreProductListResponse;

public class Constants {

    // public static final String ROOT_URL = "https://www.abluec.com/";
    public static final String ROOT_URL = "http://54.212.177.11:8010/";
    public static final String BASE_URL = ROOT_URL + "api/";

    public static final String USER_LICENCE = ROOT_URL + "mobile/user-agreement";
    public static final String TERMS = ROOT_URL + "mobile/terms";
    public static final String PRIVACY = ROOT_URL + "mobile/privacy";

    public static final String IMAGE_STORE_URL = ROOT_URL + "images/store/";
    public static final String PROFILE_IMAGE_URL = ROOT_URL + "images/profile/";
    public static final String COVER_IMAGE_URL = ROOT_URL + "images/cover_photo/";
    public static final String POST_IMAGE_URL = ROOT_URL + "images/post/";
    public static final String IMAGE_CATEGORY_URL = ROOT_URL + "images/categorynew/";

    public static final String ACCESS_TOKEN = "x-access-token";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String DEVICE_ID = "device_id";
    public static final String SESSION_TOKEN = "session_token";
    public static final String _ID = "_id";
    public static final String USER_NAME = "user_names";
    public static final String DISPLAY_NAME = "display_names";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_COUNTRY_CODE = "country_code";
    public static final String isStoreCreated = "isStoreCreated";
    public static final String STORE_ID = "store_id";
    public static final String PRODUCT_ID = "product_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String STATIC_PAGE_NAME = "page_name";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String COVER_IMAGE = "cover_image";
    public static final String PROFILE_IMAGE_BACK = "profile_image_back";
    public static final String FEED_POST_ID = "post_id";
    public static final String CHAT_DATA = "66";
    public static final String NOTIFICATION_SOUND = "notification_sound";
    public static final String SELECTED_TAB = "selected_tab";
    public static final String FRIEND_ID = "friend_profile_id";
    public static final String ids = "ids";
    public static final String FRIEND_MOBILE = "friend_mobile";
    public static final String IS_FIRST = "is_first";
    public static final String USER_ABOUT = "about";
    public static final String USER_SKYPE_ID = "skype";
    public static final String TITLE = "TITLE";
    public static final String SELECTED_LOCATION = "SELECTED_LOCATION";
    public static final String SELECTED_LAT = "SELECTED_LAT";
    public static final String SELECTED_LNG = "SELECTED_LNG";

    public static boolean showLoader = true;


    public static final String POST_RUNNING_JOB = "POST_RUNNING_JOB";
    public static final String POST_COMPLETED_JOB = "POST_COMPLETED_JOB";
    public static final String SEARCH_RUNNING_JOB = "SEARCH_RUNNING_JOB";

    public static final String SEARCH_ACCEPTED_JOB = "SEARCH_ACCEPTED_JOB";

    public static final String NEW_POSTED_JOB = "NEW_POSTED_JOB";

    public static String SEARCH_TYPE = "";

    public static String CANCELLABLE = "CANCELLABLE";
    public static String NOT_CANCELLABLE = "NOT_CANCELLABLE";


    public static final String TYPE_USER = "User";
    public static final String TYPE_PROVIDER = "Provider";
    public static final String CURRENCY = "$";
    public static String CURRENT_COUNTRY_CODE = "US";
    public static String CURRENT_LOCATION_ADDRESS = "Noida";

    public static NewJobDetailResponse.GetjobDetails jobResponse = null;
    public static StoreListResponse.StoreList myStoreResponse = null;
    public static StoreProductListResponse.ProductListing productData = null;

    public static MyProfileResponse.Address myAddress = null;
    public static SearchStoreListResponse.NearBuyStore searchStoreData = null;
    public static ProductModel sProductData = null;
    public static Context currentContext = null;

    public static final String NOTIFICATION_ACTION_FILTER = "com.ablueclive.foregroundNotification";
    public static final String EULA = "EULA";

    public static final String TO_ID = "to_id";
    public static final String MAPURL = "https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=";

    public static final String CHAT_DATABASE_NAME = "FriendChat";
    public static final String USER_DATABASE_NAME = "users";
    public static final String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=";

    public static final String ABLUEC_USER_ID = "60239a5cb51fb522710717c4";
    public static final String PARCELABLE = "parce";
    public static final String CHAT_USER_IMAGE = "chat_user_image";
    public static final String CHAT_USER_NAME = "chat_user_name";

    public static final String ARE_INVITED = "ARE_INVITED";

    public static final String TYPE_M = "MY_PROFILE";
    public static final String TYPE_P = "POST_COMMON";
    public static final String TYPE_F = "FRIEND_PROFILE";


}
