package com.ablueclive.utils

interface FCMEventListener {

    fun onDataReceived(type: String, str: String, int: Int)

}