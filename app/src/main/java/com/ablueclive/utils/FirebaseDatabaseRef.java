package com.ablueclive.utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDatabaseRef {
    private static FirebaseDatabaseRef mInstance;
    private DatabaseReference myRef;

    public static FirebaseDatabaseRef getInstant() {
        if (mInstance == null) {
            mInstance = new FirebaseDatabaseRef();
        }
        return mInstance;

    }


    public DatabaseReference getFirebaseRef() {
        myRef = FirebaseDatabase.getInstance().getReference().child(Constants.CHAT_DATABASE_NAME);
        return myRef;
    }

    public DatabaseReference getFirebaseReference(String refrence) {
        myRef = FirebaseDatabase.getInstance().getReference().child(refrence);
        return myRef;
    }

    public DatabaseReference getFirebaseRefForUid() {
        myRef = FirebaseDatabase.getInstance().getReference();
        return myRef;
    }

    public DatabaseReference getFirebaseRef(String node) {
        myRef = FirebaseDatabase.getInstance().getReference().child(Constants.CHAT_DATABASE_NAME).child(node);
        return myRef;
    }
}
