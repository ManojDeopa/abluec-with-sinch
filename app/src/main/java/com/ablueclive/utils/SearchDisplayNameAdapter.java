package com.ablueclive.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ablueclive.R;
import com.ablueclive.common.SearchDisplayResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SearchDisplayNameAdapter extends ArrayAdapter<SearchDisplayResponse.SearchUserlist> {

    private final Activity context;
    public List<SearchDisplayResponse.SearchUserlist> searchDisplayNameList;


    public SearchDisplayNameAdapter(Activity context, ArrayList<SearchDisplayResponse.SearchUserlist> searchDisplayNameList) {
        super(context, R.layout.search_display_name_item, searchDisplayNameList);
        this.context = context;
        this.searchDisplayNameList = searchDisplayNameList;
    }


    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.search_display_name_item, parent, false);
        }

        SearchDisplayResponse.SearchUserlist data = searchDisplayNameList.get(position);

        TextView tvName = rowView.findViewById(R.id.tvName);
        TextView tvDisplayName = rowView.findViewById(R.id.tvDisplayName);
        ImageView ivImage = rowView.findViewById(R.id.ivImage);

        tvName.setText(data.getName());
        if (data.getDisplayName() != null) {
            if (!data.getDisplayName().contains("@")) {
                tvDisplayName.setText("@" + data.getDisplayName());
            } else {
                tvDisplayName.setText(data.getDisplayName());
            }
        }


        String image = data.getProfileImage();

        if (!TextUtils.isEmpty(image)) {
            Glide.with(context)
                    .load(Constants.PROFILE_IMAGE_URL + image)
                    .skipMemoryCache(true)
                    .error(R.drawable.user_profile)
                    .into(ivImage);
        } else {
            ivImage.setImageResource(R.drawable.user_profile);
        }


        return rowView;

    }

    @Override
    public int getCount() {
        return searchDisplayNameList.size();
    }

    public void updateList(List<SearchDisplayResponse.SearchUserlist> searchDisplayNameList) {
        this.searchDisplayNameList = searchDisplayNameList;
        notifyDataSetChanged();
    }
}
