package com.ablueclive.utils

interface MenuItemClickListener {

    fun onMenuItemClick(text: String, position: Int)

}