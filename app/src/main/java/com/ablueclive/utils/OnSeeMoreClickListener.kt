package com.ablueclive.utils

interface OnSeeMoreClickListener {

    fun onSeeMoreClick(text: String, position: Int)

}