package com.ablueclive.utils

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.View
import com.ablueclive.R
import kotlinx.android.synthetic.main.progress_layout.*
import java.util.*


class ProgressD : Dialog {

    constructor(context: Context) : super(context)

    constructor(context: Context, theme: Int) : super(context, theme)


    companion object {

        private val dialogs: Vector<Dialog> = Vector()
        private lateinit var dialog: ProgressD

        fun show(context: Context, message: String?): ProgressD {
            dialog = ProgressD(context, R.style.ProgressD)
            dialog.setTitle("")
            dialog.setContentView(R.layout.progress_layout)
            dialog.setCancelable(true)
            dialog.message.visibility = if (message.toString().isNotEmpty()) View.VISIBLE else View.GONE
            dialog.message.text = message
            dialogs.add(dialog)
            dialog.window?.attributes?.gravity = Gravity.CENTER
            // dialog.window?.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
            return dialog
        }

        fun hide() {
            try {
                for (dialog in dialogs) if (dialog.isShowing) dialog.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
