package com.ablueclive.utils

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.adapter.FeedMediaAdapterVertical
import com.ablueclive.modelClass.Images
import kotlinx.android.synthetic.main.activity_post_media_list.*

class PostMediaList : AppCompatActivity() {


    companion object {
        var imagesArrayList = ArrayList<Images>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_media_list)

        rvList.apply {
            layoutManager = LinearLayoutManager(this@PostMediaList)
            adapter = FeedMediaAdapterVertical(imagesArrayList)
        }
    }

}