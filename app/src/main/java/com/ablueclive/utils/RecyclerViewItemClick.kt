package com.ablueclive.utils

interface RecyclerViewItemClick {

    fun onRecyclerItemClick(text: String, position: Int)

}

interface CustomRecyclerViewItemClick {

    fun onRecyclerItemClick(isTrue : Boolean, text: String, position: Int)

}