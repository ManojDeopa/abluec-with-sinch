package com.ablueclive.jobSection.newJobFragment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class NewJobDetailResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("getjobDetails")
        @Expose
        var getjobDetails: GetjobDetails? = null

        @SerializedName("users")
        @Expose
        var users: List<User>? = null
    }

    class GetjobDetails {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("job_title")
        @Expose
        var jobTitle: String? = null

        @SerializedName("job_desc")
        @Expose
        var jobDesc: String? = null

        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null

        @SerializedName("service_id")
        @Expose
        var serviceId: String? = null

        @SerializedName("sub_category_id")
        @Expose
        var subCategoryId: String? = null

        @SerializedName("serviceProviderIds")
        @Expose
        var serviceProviderIds: List<String>? = null

        @SerializedName("jobType")
        @Expose
        var jobType: Any? = null

        @SerializedName("lat")
        @Expose
        var lat: String? = null

        @SerializedName("long")
        @Expose
        var _long: String? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String = ""

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("appointment_date")
        @Expose
        var appointmentDate: String? = null

        @SerializedName("job_image")
        @Expose
        var jobImage: List<JobImage>? = null

        @SerializedName("priceCategory")
        @Expose
        var priceCategory: Any? = null

        @SerializedName("minPrice")
        @Expose
        var minPrice: Any? = null

        @SerializedName("maxPrice")
        @Expose
        var maxPrice: Any? = null

        @SerializedName("cat")
        @Expose
        var cat: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("user_image")
        @Expose
        var userImage: UserImage? = null

        @SerializedName("already_applied")
        @Expose
        var already_applied = 0

        @SerializedName("location_type")
        @Expose
        var location_type = ""

        @SerializedName("complete_address")
        @Expose
        var complete_address = "Remote"

    }

    class JobImage {
        @SerializedName("name")
        @Expose
        var name: String? = null
    }

    class UserImage {
        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null
    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }

    class User {

        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("display_name")
        @Expose
        var displayName: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null
    }

}