package com.ablueclive.jobSection.newJobFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.new_job_detail.*

class NewJobDetailFragment : Fragment() {

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""

    companion object {
        val apiParam = HashMap<String, String>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.new_job_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        requestJobDetail()
    }

    private fun requestJobDetail() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        apiParam["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getJobDetail(apiParam).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<NewJobDetailResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: NewJobDetailResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            response.responseData?.let { loadData(it) }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }


    private fun updateJobStatus(status: String, serviceId: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val apiParam = HashMap<String, String>()

        apiParam["session_token"] = sessionToken
        apiParam["status"] = status
        apiParam["_id"] = serviceId

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.updateJobStatus(apiParam).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        CommonMethod.showAlert(requireActivity(), response.responseData?.updateJobStatus)
                        ProgressD.hide()
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }

    @SuppressLint("SetTextI18n")
    private fun loadData(responseData: NewJobDetailResponse.ResponseData) {

        val response = responseData.getjobDetails
        if (response != null) {
            tvDuration.text = CommonMethod.getTimesAgo(response.createdAt)
            tvTitle.text = response.jobTitle
            // tvPrice.text = CURRENCY + response.minPrice + " - " + CURRENCY + response.maxPrice
            tvDescription.text = response.jobDesc
            tvJobAddress.text = response.complete_address

            if (!response.jobImage.isNullOrEmpty()) {
                CommonMethod.loadImageResize(Constants.IMAGE_STORE_URL + response.jobImage?.get(0)?.name, ivImage)
            }


            switchStatus.isChecked = response.status == 1
            switchStatus.setOnCheckedChangeListener { buttonView, isChecked ->
                val status = if (isChecked) 1 else 5
                updateJobStatus(status.toString(), response.id.toString())
            }

            JobContainerActivity.imgEdit.visibility = View.VISIBLE
            JobContainerActivity.imgEdit.setOnClickListener {
                Constants.jobResponse = response
                CommonMethod.callJobContainer(requireActivity(), getString(R.string.edit_job_details))
            }

            btnCompleteJob.setOnClickListener {
                completeJob(response.id.toString())
            }


            val appliedUserList = responseData.users
            if (!appliedUserList.isNullOrEmpty()) {
                rvAppliedUsers.visibility = View.VISIBLE
                tvJobAppliedLabel.visibility = View.VISIBLE
                tvJobAppliedLabel.text = appliedUserList.size.toString() + " " + getString(R.string.applicants)
                rvAppliedUsers.apply {
                    layoutManager = LinearLayoutManager(requireActivity())
                    adapter = AppliedUsersAdapter(appliedUserList, response.id.toString())
                }
            }
        }
    }

    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }

    override fun onDestroyView() {
        super.onDestroyView()
        JobContainerActivity.imgEdit.visibility = View.INVISIBLE
    }


    private fun completeJob(jobId: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }
        ProgressD.show(requireActivity(), "")
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = jobId

        apiInterface.completeRequestApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showToastShort(response.responseData?.completeRequestApi, requireActivity())
                            requireActivity().finish()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }
}