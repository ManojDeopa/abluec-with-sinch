package com.ablueclive.jobSection.newJobFragment

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.newJobFragment.NewJobDetailFragment.Companion.apiParam
import com.ablueclive.jobSection.searchJob.JobRequestResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.MenuItemClickListener
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.new_jobs_item.view.*


class NewPostedJobsAdapter(private var list: List<JobRequestResponse.SeekerJobDatum>, var itemClickListener: RecyclerViewItemClick) : RecyclerView.Adapter<NewPostedJobsAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.ivDots.setOnClickListener {
                val itemList = arrayOf(context.getString(R.string.delete))
                CommonMethod.popupMenuTextList(context, itemView.ivDots, itemList, object : MenuItemClickListener {
                    override fun onMenuItemClick(text: String, position: Int) {
                        itemClickListener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString(), absoluteAdapterPosition)
                    }
                })
            }

            itemView.setOnClickListener {
                apiParam["service_id"] = list[absoluteAdapterPosition].jobId.toString()
                apiParam["status"] = list[absoluteAdapterPosition].jobStatus.toString()
                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.new_job_detail))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.new_jobs_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTitle.text = list[position].jobTitle

        var imageUrl = Constants.IMAGE_STORE_URL + "no_image.jpg"
        if (!list[position].jobImage.isNullOrEmpty()) {
            imageUrl = Constants.IMAGE_STORE_URL + (list[position].jobImage?.get(0) ?: "")
        }
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)
        holder.itemView.tvStatus.text = if (list[position].jobStatus == 1) "Status: Active" else "Status: Inactive"
        holder.itemView.tvDuration.text = CommonMethod.getTimesAgo(list[position].updatedAt.toString())
        val applyCount = list[position].applied_job_users_count
        if (applyCount != null && applyCount > 0) {
            holder.itemView.tvApplyCount.visibility = View.VISIBLE
            holder.itemView.tvApplyCount.text = "$applyCount " + context.getString(R.string.applicants)
        }
    }
}
