package com.ablueclive.jobSection.newJobFragment

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.newJobFragment.NewJobDetailFragment.Companion.apiParam
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.new_jobs_item.view.*


class NewJobsAdapter(private var list: List<JobListingResponse.GetjoblistingList>, var itemClickListener: RecyclerViewItemClick) : RecyclerView.Adapter<NewJobsAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.ivDots.setOnClickListener {
                val popup = PopupMenu(context, itemView.ivDots)
                popup.inflate(R.menu.delete_option_menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menuDelete -> {
                            itemClickListener.onRecyclerItemClick(list[absoluteAdapterPosition].id.toString(), absoluteAdapterPosition)
                        }
                    }
                    false
                }
                popup.show()
            }

            itemView.setOnClickListener {
                apiParam["service_id"] = list[absoluteAdapterPosition].id.toString()
                apiParam["status"] = list[absoluteAdapterPosition].status.toString()
                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.new_job_detail))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.new_jobs_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTitle.text = list[position].jobTitle

        var imageUrl = Constants.IMAGE_STORE_URL + "no_image.jpg"
        if (!list[position].jobImage.isNullOrEmpty()) {
            imageUrl = Constants.IMAGE_STORE_URL + (list[position].jobImage?.get(0) ?: "")
        }
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)
        holder.itemView.tvStatus.text = if (list[position].status == 1) "Status: Active" else "Status: Inactive"

        holder.itemView.tvDuration.text = CommonMethod.getTimesAgo(list[position].createdAt)

    }


}
