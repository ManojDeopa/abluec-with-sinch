package com.ablueclive.jobSection.newJobFragment

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.applied_users_item.view.*


class AppliedUsersAdapter(private var list: List<NewJobDetailResponse.User>, var jobId: String) : RecyclerView.Adapter<AppliedUsersAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.applied_users_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvName.text = list[position].name
        holder.itemView.tvEmail.text = list[position].email
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].profileImage, holder.itemView.ivImage)

        val status = list[position].status
        if (status == 1) {
            holder.itemView.tvStatus.visibility = View.VISIBLE
            holder.itemView.tvStatus.text = "Accepted"
        }

        holder.itemView.setOnClickListener {
            FriendProfileActivity.isFromJobPage = true
            FriendProfileActivity.jobId = jobId
            FriendProfileActivity.jobAcceptedStatus = list[position].status!!.toInt()
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].id.toString())
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }
    }


}
