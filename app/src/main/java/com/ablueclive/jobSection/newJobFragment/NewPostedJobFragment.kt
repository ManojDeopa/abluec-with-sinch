package com.ablueclive.jobSection.newJobFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.searchJob.JobRequestParentAdapter
import com.ablueclive.jobSection.searchJob.JobRequestResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.job_request_list.*
import kotlinx.android.synthetic.main.recycler_view_layout.*

class NewPostedJobFragment : Fragment(), RecyclerViewItemClick {

    lateinit var api: Observable<CommonResponse>
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_request_list, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        requestJobList()
    }

    private fun requestJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["status"] = "1"

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getNewPostedJobListing(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<JobRequestResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: JobRequestResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {

                            val list = response.responseData?.getjoblistingList
                            if (list.isNullOrEmpty()) {
                                CommonMethod.isEmptyView(true, requireContext(), getString(R.string.no_job_posted))
                            } else {
                                CommonMethod.isEmptyView(false, requireContext(), getString(R.string.no_job_posted))
                                recyclerView.apply {
                                    layoutManager = LinearLayoutManager(requireActivity())
                                    adapter = JobRequestParentAdapter(this@NewPostedJobFragment, Constants.NEW_POSTED_JOB, list)
                                }

                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
        tvJobText.visibility = View.GONE
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        deleteJob(text)
    }


    private fun deleteJob(id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = separated[0]

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteJob(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showToastShort(response.responseData?.deletejobUser, requireActivity())
                            requestJobList()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}