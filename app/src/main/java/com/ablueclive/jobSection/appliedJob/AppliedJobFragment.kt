package com.ablueclive.jobSection.appliedJob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.SimpleResponse
import com.ablueclive.global.fragment.JobListAdapter

import com.ablueclive.global.response.JobsListResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class AppliedJobFragment() : Fragment(), RecyclerViewItemClick {

    lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var jobListAdapter: JobListAdapter
    private var searchJobsList = ArrayList<JobsListResponse.GetjobUserList>()

    private var itemPos = 0
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""


    var isLoading = false
    var searchText = ""
    var pageCount = 1
    var rowCount = 30

    companion object {
        var shoulRefresh = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        if (shoulRefresh) {
            shoulRefresh = false
            apiCallOnTextChange("")
        }
    }


    private fun init() {

        shoulRefresh = false

        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector.isConnectingToInternet

        jobListAdapter = JobListAdapter(this@AppliedJobFragment, searchJobsList, true)
        linearLayoutManager = LinearLayoutManager(requireActivity())

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = jobListAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    getJobsList()
                }
            }
        })

        apiCallOnTextChange("")
    }


    private fun apiCallOnTextChange(text: String) {
        searchJobsList.clear()
        jobListAdapter.notifyDataSetChanged()
        tvHeaderText.visibility = View.GONE
        // if (text.isEmpty()) return
        pageCount = 1
        searchText = text
        getJobsList()
    }


    private fun getJobsList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getAppliedjobList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<JobsListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: JobsListResponse) {

                        ProgressD.hide()

                        try {
                            if (activity != null) {
                                if (response.responseStatus == 1) {

                                    val list = response.responseData?.getAppliedjobList
                                    searchJobsList.clear()
                                    if (!list.isNullOrEmpty()) {
                                        // if (pageCount == 1) searchJobsList.clear()
                                        searchJobsList.addAll(list)
                                        // unComment below lines when pagination will implement from backend.
                                        // isLoading = false
                                        // pageCount++
                                    }

                                    val count = searchJobsList.size
                                    jobListAdapter.notifyDataSetChanged()
                                    emptyParentLayout.visibility = if (count == 0) View.VISIBLE else View.GONE

                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }


                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    override fun onRecyclerItemClick(text: String, position: Int) {
        itemPos = position
        if (text == "delete") {
            removeJob()
        }

    }


    private fun removeJob() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = java.util.HashMap<String, String>()
        param["session_token"] = session_token
        param["job_id"] = searchJobsList[itemPos].id.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getcancelAppliedjobList(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SimpleResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SimpleResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                searchJobsList.removeAt(itemPos)
                                jobListAdapter.notifyItemRemoved(itemPos)
                                CommonMethod.showToastShort("Applied job deleted", requireContext())
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}