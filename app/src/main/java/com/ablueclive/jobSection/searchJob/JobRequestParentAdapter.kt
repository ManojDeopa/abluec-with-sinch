package com.ablueclive.jobSection.searchJob

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.newJobFragment.NewPostedJobsAdapter
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.parent_recycler_item.view.*

class JobRequestParentAdapter(var listener: RecyclerViewItemClick, var type: String, var list: List<JobRequestResponse.SeekerJobRequest>) : RecyclerView.Adapter<JobRequestParentAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.parent_recycler_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var title = getTitle(list[position].id)
        var childList = list[position].seekerJobData

        if (type == Constants.NEW_POSTED_JOB) {
            title = getNewJobTitle(list[position].id)
            childList = list[position].data

            if (!childList.isNullOrEmpty()) {
                holder.itemView.rv_child.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = NewPostedJobsAdapter(childList as MutableList, listener)
                }
            }

        } else {
            if (!childList.isNullOrEmpty()) {
                holder.itemView.rv_child.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = JobRequestAdapter(childList as MutableList, listener)
                }
            }
        }
        holder.itemView.textView.text = title
    }

    private fun getTitle(id: Int): String {
        var title = "NearBy"
        if (id == 0) {
            title = "Available work based on your skills"
        } else if (id == 1) {
            title = "Work posted directly to you"
        }
        return title
    }


    private fun getNewJobTitle(id: Int): String {
        var title = "Sent to selected service providers"
        if (id == 0) {
            title = "Pinned to a Job Board"
        } else if (id == 2) {
            title = "Sent to nearest service providers"
        }
        return title
    }

}
