package com.ablueclive.jobSection.searchJob

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.rateReview.RateReviewFragment
import com.ablueclive.jobSection.runningJob.PJobDetailResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.job_detail_fragment_s.*


class SJobDetailFragment : Fragment() {


    private lateinit var gpsTracker: GPSTracker
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""
    lateinit var api: Observable<CommonResponse>


    companion object {
        val apiParam = HashMap<String, String>()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_detail_fragment_s, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getJobDetail()
    }

    private fun getJobDetail() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        apiParam["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getJobUserDetails(apiParam).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<PJobDetailResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: PJobDetailResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            response.responseData?.getjobDetails?.let { loadData(it) }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }

    private fun loadData(it: PJobDetailResponse.GetjobDetails) {

        tvDuration.text = CommonMethod.getTimesAgo(it.updatedAt)
        tvName.text = it.userName
        tvService.text = it.cat

        tvJobTitle.text = it.jobTitle
        tvJobDescription.text = it.jobDesc

        tvJobAddress.text = it.complete_address

        CommonMethod.loadImageResize(Constants.PROFILE_IMAGE_URL + PJobDetailResponse.userImage, ivImage)

        if (JobContainerActivity.headerText == getString(R.string.requested_job_detail)) {
            tvReqJobId.visibility = View.GONE
            if (it.status == 6) {
                acceptRejectBtnLayout.visibility = View.VISIBLE
            } else {
                btnApply.visibility = View.VISIBLE
            }
        } else if (JobContainerActivity.headerText == getString(R.string.running_job_detail)) {
            gpsTracker = GPSTracker(requireContext())
            btnRateOrTrack.visibility = View.VISIBLE
            btnRateOrTrack.text = getString(R.string.track)
        } else if (JobContainerActivity.headerText == getString(R.string.complete_job_detail)) {
            btnRateOrTrack.visibility = View.VISIBLE
            btnRateOrTrack.text = getString(R.string.rate_the_customer)
            ivCall.visibility = View.GONE
            tvStatus.visibility = View.VISIBLE
        }

        ivCall.setOnClickListener { v ->
            val mobileNo = it.userMobile
            if (mobileNo.isNullOrEmpty()) {
                CommonMethod.showToastlong("Mobile No. Not Found", requireContext())
                return@setOnClickListener
            }
            CommonMethod.callIntent(mobileNo, requireContext())
        }


        btnApply.setOnClickListener { v ->

            val title = getString(R.string.apply)
            val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
            alert.setTitle(title)
            alert.setMessage(getString(R.string.before_apply_text))
            alert.setCancelable(false)
            alert.setPositiveButton(getString(R.string.agree)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
                requestData(0, it.id + ":" + it.providerId)
            }

            alert.setNegativeButton(getString(R.string.disagree)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
            }
            alert.show()

        }

        btnAccept.setOnClickListener { v ->

            val title = getString(R.string.accept_candidate)
            val alert = AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
            alert.setTitle(title)
            alert.setMessage(getString(R.string.before_apply_text))
            alert.setCancelable(false)
            alert.setPositiveButton(getString(R.string.agree)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
                requestData(1, it.id + ":" + it.providerId)
            }
            alert.setNegativeButton(getString(R.string.disagree)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
            }
            alert.show()
        }

        btnReject.setOnClickListener { v ->
            requestData(2, it.id + ":" + it.providerId)
        }


        btnRateOrTrack.setOnClickListener { v ->


            if (btnRateOrTrack.text == getString(R.string.track)) {
                try {
                    val lng = it._long
                    val lat = it.lat
                    val currentLat = gpsTracker.latitude
                    val currentLng = gpsTracker.longitude
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=$currentLat,$currentLng&daddr=$lat,$lng"))
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return@setOnClickListener
            }

            RateReviewFragment.apiParams["session_token"] = sessionToken
            RateReviewFragment.apiParams["user_id"] = it.providerId.toString()
            RateReviewFragment.apiParams["job_id"] = it.id.toString()

            RateReviewFragment.type = Constants.TYPE_PROVIDER
            RateReviewFragment.image = Constants.PROFILE_IMAGE_URL + PJobDetailResponse.userImage

            CommonMethod.callJobContainer(requireActivity(), getString(R.string.rate_review))

        }
    }

    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


    private fun requestData(position: Int, id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = separated[0]


        if (position == 0) {
            param["job_provider_id"] = separated[1]
            api = apiInterface.jobRequestApi(param)
        }

        if (position == 1) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.acceptJobRequestApi(param)
        }

        if (position == 2) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.rejectJobRequestApi(param)
        }


        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {

                        ProgressD.hide()

                        try {
                            if (position == 0) {
                                showAlert(requireActivity(), response.responseData?.jobRequestApi)
                            }

                            if (position == 1) {
                                showAlert(requireActivity(), response.responseData?.acceptJobRequestApi)
                            }

                            if (position == 2) {
                                showAlert(requireActivity(), response.responseData?.rejectJobRequestApi)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(context: Context, text: String?) {
        val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            activity?.finish()
        }
        alert.show()
    }

}