package com.ablueclive.jobSection.searchJob

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.job_request_list.*
import kotlinx.android.synthetic.main.recycler_view_layout.*

class JobRequestListFragment : Fragment(), RecyclerViewItemClick {

    lateinit var api: Observable<CommonResponse>
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""

    companion object {
        lateinit var tvJobs: TextView
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_request_list, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        requestJobList()
    }

    private fun requestJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getJobRequestApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<JobRequestResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: JobRequestResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            var count = 0
                            val list = response.responseData?.seekerJobRequest
                            if (list.isNullOrEmpty()) {
                                CommonMethod.isEmptyView(true, requireContext(), getString(R.string.no_requested_job))
                            } else {
                                CommonMethod.isEmptyView(false, requireContext(), getString(R.string.no_requested_job))
                                recyclerView.apply {
                                    layoutManager = LinearLayoutManager(requireActivity())
                                    adapter = JobRequestParentAdapter(this@JobRequestListFragment, Constants.SEARCH_ACCEPTED_JOB, list)
                                }
                                list.forEach {
                                    val innerList = it.seekerJobData
                                    if (innerList != null) {
                                        for (i in innerList.indices) {
                                            count += 1
                                        }
                                    }
                                }
                            }
                            tvJobText.text = count.toString() + " " + getString(R.string.jobs_based_on_your_profile)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
        tvJobs = tvJobText

    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        requestData(position, text)
    }


    private fun requestData(position: Int, id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = separated[0]


        if (position == 0) {
            param["job_provider_id"] = separated[1]
            api = apiInterface.jobRequestApi(param)
        }

        if (position == 1) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.acceptJobRequestApi(param)
        }

        if (position == 2) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.rejectJobRequestApi(param)
        }

        if (position == 3) {
            api = apiInterface.saveIgnoredJob(param)
        }


        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (position == 0) {
                                CommonMethod.showToastShort(response.responseData?.jobRequestApi, requireActivity())
                            }
                            if (position == 1) {
                                CommonMethod.showToastShort(response.responseData?.acceptJobRequestApi, requireActivity())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }


                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


}