package com.ablueclive.jobSection.searchJob

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.runningJob.PJobDetailResponse
import com.ablueclive.jobSection.searchJob.JobRequestListFragment.Companion.tvJobs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.job_request_item.view.*

class JobRequestAdapter(var list: MutableList<JobRequestResponse.SeekerJobDatum>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<JobRequestAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {

                PJobDetailResponse.userImage = list[absoluteAdapterPosition].userData?.profileImage
                PJobDetailResponse.minPrice = list[absoluteAdapterPosition].minPrice
                PJobDetailResponse.maxPrice = list[absoluteAdapterPosition].maxPrice
                PJobDetailResponse.reqJobId = list[absoluteAdapterPosition].jobReqId

                SJobDetailFragment.apiParam["service_id"] = list[absoluteAdapterPosition].serviceId.toString()
                SJobDetailFragment.apiParam["job_id"] = list[absoluteAdapterPosition].jobId.toString()
                SJobDetailFragment.apiParam["provider_id"] = list[absoluteAdapterPosition].customerId.toString()
                SJobDetailFragment.apiParam["status"] = list[absoluteAdapterPosition].jobStatus.toString()

                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.requested_job_detail))

            }

            itemView.btnAccept.setOnClickListener {

                val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
                alert.setTitle(itemView.btnAccept.text.toString())
                alert.setMessage(context.getString(R.string.before_apply_text))
                alert.setCancelable(false)
                alert.setPositiveButton(context.getString(R.string.agree)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                    if (itemView.btnAccept.text == context.getString(R.string.apply)) {
                        listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].customerId.toString(), 0)
                        list.removeAt(absoluteAdapterPosition)
                        notifyItemRemoved(absoluteAdapterPosition)
                        tvJobs.text = list.size.toString() + " " + context.getString(R.string.jobs_based_on_your_profile)
                    } else {
                        listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 1)
                        list.removeAt(absoluteAdapterPosition)
                        notifyItemRemoved(absoluteAdapterPosition)
                        tvJobs.text = list.size.toString() + " " + context.getString(R.string.jobs_based_on_your_profile)
                    }
                }
                alert.setNegativeButton(context.getString(R.string.disagree)) { dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                }
                alert.show()
            }

            itemView.ivCross.setOnClickListener {
                if (itemView.btnAccept.text == context.getString(R.string.accept_candidate)) {
                    listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 2)
                } else {
                    listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 3)
                }

                list.removeAt(absoluteAdapterPosition)
                notifyItemRemoved(absoluteAdapterPosition)
                tvJobs.text = list.size.toString() + " " + context.getString(R.string.jobs_based_on_your_profile)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.job_request_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var imageUrl = Constants.IMAGE_STORE_URL
        if (!list[position].jobImage.isNullOrEmpty()) {
            imageUrl += (list[position].jobImage?.get(0) ?: "")
        }
        CommonMethod.loadImageCircle(imageUrl, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].jobTitle
        holder.itemView.tvName.text = list[position].userData?.name

        if (list[position].jobStatus == 6) {
            holder.itemView.btnAccept.text = context.getString(R.string.accept_candidate)
        } else {
            holder.itemView.btnAccept.text = context.getString(R.string.apply)
        }
        // holder.itemView.tvAmount.text = CURRENCY + list[position].minPrice + "-$CURRENCY" + list[position].maxPrice
    }
}
