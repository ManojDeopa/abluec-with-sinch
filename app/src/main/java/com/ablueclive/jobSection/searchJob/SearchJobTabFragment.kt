package com.ablueclive.jobSection.searchJob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.ablueclive.jobSection.appliedJob.AppliedJobFragment
import com.ablueclive.jobSection.completeJob.SeekerCompleteJobFragment
import com.ablueclive.jobSection.runningJob.SRunningJobFragment
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import kotlinx.android.synthetic.main.search_job_tab_fragment.*

/**
 * A simple [Fragment] subclass.
 */
class SearchJobTabFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_job_tab_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTabViewPager()
    }


    private fun addTabViewPager() {
        val list = listOf(getString(R.string.posted), getString(R.string.applied), getString(R.string.running_jobs), getString(R.string.completed_jobs))
        val adapter = ViewPagerAdapter(requireActivity())
        viewpager.adapter = adapter
        TabLayoutMediator(tabs, viewpager,
                TabConfigurationStrategy { tab, position ->
                    tab.text = list[position]
                }).attach()
    }


    internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {
        @NonNull
        override fun createFragment(position: Int): Fragment {
            val fragment: Fragment? = when (position) {

                0 -> {
                    JobRequestListFragment()
                }
                1 -> {
                    AppliedJobFragment()
                }
                2 -> {
                    SRunningJobFragment()
                }
                else -> {
                    SeekerCompleteJobFragment()
                }
            }

            return fragment!!
        }

        override fun getItemCount(): Int {
            return 4
        }
    }
}