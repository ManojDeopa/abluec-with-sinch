package com.ablueclive.jobSection.rateReview

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ProgressD
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.rate_review_layout.*


class RateReviewFragment : Fragment() {


    companion object {

        var type = ""
        var image = ""
        var apiParams = HashMap<String, String>()
    }

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var timeToContact = "5 min"

    lateinit var api: Observable<CommonResponse>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.rate_review_layout, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        CommonMethod.loadImageCircle(image, ivImage)

        if (isUser()) {
            radioLayout.visibility = View.VISIBLE
            topLayout.text = getString(R.string.rate_the_service_provider)
            tvHowWas.text = getString(R.string.how_was_service)

            radioGroup.setOnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = group.findViewById(checkedId)
                timeToContact = radio.text.toString()
            }

        } else {
            topLayout.text = getString(R.string.rate_the_customer)
            tvHowWas.text = getString(R.string.how_was_the_customer)
        }


        btnSubmit.setOnClickListener { v ->

            if (!isInternetPresent) {
                CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
                return@setOnClickListener
            }

            ProgressD.show(requireActivity(), "")
            val retrofit = RetrofitClient.getRetrofitClient()
            val apiInterface = retrofit.create(ApiInterface::class.java)

            apiParams["rating"] = ratingBar.rating.toString()
            apiParams["review"] = etText.text.toString()

            if (isUser()) {
                apiParams["time_to_complete"] = timeToContact
                api = apiInterface.rateServiceProvider(apiParams)
            } else {
                api = apiInterface.rateJobProvider(apiParams)
            }

            api.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<CommonResponse?> {

                        override fun onSubscribe(d: Disposable) {}

                        override fun onComplete() {}

                        override fun onNext(response: CommonResponse) {
                            ProgressD.hide()
                            if (isUser()) {
                                showAlert(requireActivity(), response.responseData?.rateServiceProvider)
                            } else {
                                showAlert(requireActivity(), "Customer rating added successfully!")
                            }
                        }

                        override fun onError(e: Throwable) {
                            ProgressD.hide()
                            println(e.message)
                        }
                    })
        }
    }

    private fun isUser(): Boolean {
        return type == Constants.TYPE_USER
    }


    fun showAlert(context: Context, text: String?) {
        val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            activity?.finish()
        }
        alert.show()
    }

    private fun init() {
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


}