package com.ablueclive.jobSection.subCategory

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.common.AppGlobalTask
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.common.JobContainerActivity.Companion.isFromProfile
import com.ablueclive.common.JobContainerActivity.Companion.jobType
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.sub_category_fragment.*
import java.util.*

class SubCategoryFragment : Fragment(), SubCategoryContract.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sub_category_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnNext.setOnClickListener { v: View? ->

            if (JobContainerActivity.selectedSubCategoryId.isEmpty()) {
                CommonMethod.showToastlong("Please Select Sub Category", requireActivity())
                return@setOnClickListener
            }

            if (isFromProfile) {
                addSkill()
            } else {
                val title = if (jobType == "1") getString(R.string.select_service_provider) else getString(R.string.add_job_detail)
                CommonMethod.callJobContainer(requireActivity(), title)
            }


        }

        requestSubCategoryList()
    }

    private fun requestSubCategoryList() {
        val params = HashMap<String, String>()
        params["category_id"] = JobContainerActivity.selectedCategoryId
        SubCategoryPresenter(this).requestData(params)
    }

    override fun setDataToViews(response: SubCategoryResponse) {
        try {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireActivity())
                adapter = response.responseData?.subCategories?.let { SubCategoryAdapter(it) }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResponseFailure(throwable: Throwable?) {
        println("onResponseFailure -> " + throwable?.message)
    }

    override fun hideProgress() {
        ProgressD.hide()
    }

    override fun showProgress() {
        ProgressD.show(requireActivity(), "")
    }


    private fun addSkill() {

        if (!ConnectionDetector(requireContext()).isConnectingToInternet) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireContext(), "")

        val subCatList = arrayListOf(JobContainerActivity.selectedSubCategoryId)
        val subCatData: String = Gson().toJson(subCatList)
        val jsonParser = JsonParser()
        val jsonArray = jsonParser.parse(subCatData).asJsonArray


        val req = JsonObject()
        req.addProperty("session_token", BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN))
        req.addProperty("experience", "")
        req.addProperty("categoryId", JobContainerActivity.selectedCategoryId)
        req.add("subCategoryId", jsonArray)


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.addSkills(req).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()

                        try {
                            if (response.responseData?.addSkills == "Category Already Exists!") {
                                CommonMethod.showToastlong(response.responseData?.addSkills, requireContext())
                            } else {
                                AppGlobalTask({ finish() }, requireContext()).showAlert(response.responseData?.addSkills)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    fun finish() {
        requireActivity().finishAffinity()
        CommonMethod.callActivity(requireActivity(), Intent(requireActivity(), MainActivity::class.java))

    }
}