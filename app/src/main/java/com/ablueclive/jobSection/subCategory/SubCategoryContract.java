package com.ablueclive.jobSection.subCategory;

import java.util.HashMap;

public interface SubCategoryContract {

    interface Model {

        interface OnFinishedListener {

            void onFinished(SubCategoryResponse response);

            void onFailure(Throwable t);
        }

        void getData(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(SubCategoryResponse response);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {

        void onDestroy();

        void requestData(HashMap<String, String> param);
    }
}
