package com.ablueclive.jobSection.subCategory

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity.Companion.selectedSubCategoryId
import kotlinx.android.synthetic.main.sub_category_item.view.*

class SubCategoryAdapter(private var list: List<SubCategoryResponse.SubCategory>) : RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {

                list.forEach {
                    it.isChecked = false
                }
                list[absoluteAdapterPosition].isChecked = true
                selectedSubCategoryId = list[absoluteAdapterPosition].id.toString()
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.sub_category_item, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.cbTitle.text = list[position].name
        holder.itemView.cbTitle.isChecked = list[position].isChecked

        if (list[position].isChecked) {
            holder.itemView.cbTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.itemLayout.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        } else {
            holder.itemView.cbTitle.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.itemView.itemLayout.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
        }
    }

}
