package com.ablueclive.jobSection.subCategory;


import java.util.HashMap;

public class SubCategoryPresenter implements SubCategoryContract.Presenter, SubCategoryContract.Model.OnFinishedListener {

    private SubCategoryContract.View view;
    private SubCategoryContract.Model model;

    public SubCategoryPresenter(SubCategoryContract.View view) {
        this.view = view;
        this.model = new SubCategoryModel();
    }

    @Override
    public void onFinished(SubCategoryResponse response) {
        if (view != null) {
            view.hideProgress();
            view.setDataToViews(response);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (view != null) {
            view.hideProgress();
            view.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void requestData(HashMap<String, String> param) {
        if (view != null) {
            view.showProgress();
            model.getData(this, param);
        }

    }
}
