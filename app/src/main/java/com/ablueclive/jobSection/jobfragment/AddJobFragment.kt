package com.ablueclive.jobSection.jobfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity.Companion.jobType
import com.ablueclive.utils.CommonMethod
import kotlinx.android.synthetic.main.add_job.*

class AddJobFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_job, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        tv_job_dashbord.setOnClickListener { v: View? ->
            jobType="0"
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.select_category))
        }

        tv_nearset_job_seeker.setOnClickListener { v: View? ->
            jobType="2"
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.select_category))
        }

        tv_selected_job_seeker.setOnClickListener { v: View? ->
            jobType="1"
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.select_category))
        }

    }
}