package com.ablueclive.jobSection.jobfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.ablueclive.jobSection.completeJob.CompleteJobFragment
import com.ablueclive.jobSection.newJobFragment.NewPostedJobFragment
import com.ablueclive.jobSection.runningJob.PRunningJobFragment
import com.ablueclive.utils.CommonMethod
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import kotlinx.android.synthetic.main.job_post_tab_fragment.*


/**
 * A simple [Fragment] subclass.
 */
class JobPostTabFragment : Fragment() {

    companion object {
        lateinit var btnAddJob: FloatingActionButton
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_post_tab_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTabViewPager()
    }


    private fun addTabViewPager() {

        btnAddJob = img_add_job
        btnAddJob.setOnClickListener {
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.add_job))
        }

        val list = listOf(getString(R.string.new_jobs), getString(R.string.applicant_running), getString(R.string.completed_jobs))
        val adapter = ViewPagerAdapter(requireActivity())
        viewpager.adapter = adapter
        TabLayoutMediator(tabs, viewpager,
                TabConfigurationStrategy { tab, position ->
                    tab.text = list[position]
                }).attach()

    }


    internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {
        @NonNull
        override fun createFragment(position: Int): Fragment {
            val fragment: Fragment? = when (position) {
                0 -> {
                    NewPostedJobFragment/*NewJobFragment*/()
                }
                1 -> {
                    PRunningJobFragment()
                }
                else -> {
                    CompleteJobFragment()
                }
            }

            return fragment!!
        }

        override fun getItemCount(): Int {
            return 3
        }
    }

}