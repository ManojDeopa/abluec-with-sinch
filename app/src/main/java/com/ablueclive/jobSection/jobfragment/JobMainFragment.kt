package com.ablueclive.jobSection.jobfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity.Companion.typeSection
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.job_main_fragment.*

class JobMainFragment : Fragment() {


    companion object {

        fun newInstance(bundle: Bundle): JobMainFragment {
            val fragment = JobMainFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_main_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        jobPostLayout.setOnClickListener { v: View? ->
            typeSection = getString(R.string.job_post)
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.job_post))
        }

        jobSearchLayout.setOnClickListener { v: View? ->
            Constants.SEARCH_TYPE = getString(R.string.jobs)
            CommonMethod.callCommonContainer(requireActivity(), getString(R.string.search_all))
        }

        jobActivityLayout.setOnClickListener {
            typeSection = getString(R.string.search_job)
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.search_job))
        }

    }
}