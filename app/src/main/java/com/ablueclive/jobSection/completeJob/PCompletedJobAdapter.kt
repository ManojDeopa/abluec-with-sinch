package com.ablueclive.jobSection.completeJob

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.runningJob.PJobDetailFragment
import com.ablueclive.jobSection.runningJob.PJobDetailResponse
import com.ablueclive.jobSection.runningJob.RunningJobListResponse
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.completed_job_item.view.*


class PCompletedJobAdapter(private var list: List<RunningJobListResponse.Datum>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<PCompletedJobAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {

                PJobDetailResponse.userImage = list[adapterPosition].providerData?.profileImage
                PJobDetailResponse.minPrice = list[adapterPosition].minPrice
                PJobDetailResponse.maxPrice = list[adapterPosition].maxPrice
                PJobDetailResponse.reqJobId = list[adapterPosition].jobReqId

                PJobDetailFragment.apiParam["service_id"] = list[adapterPosition].serviceId.toString()
                PJobDetailFragment.apiParam["job_id"] = list[adapterPosition].jobId.toString()
                PJobDetailFragment.apiParam["provider_id"] = list[adapterPosition].providerData?.id.toString()
                PJobDetailFragment.apiParam["status"] = list[adapterPosition].jobStatus.toString()
                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.complete_job_detail))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.completed_job_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].providerData?.profileImage, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].jobTitle
        holder.itemView.tvName.text = list[position].providerData?.name
        holder.itemView.tvDuration.text = CommonMethod.getTimesAgo(list[position].updatedAt)
    }
}
