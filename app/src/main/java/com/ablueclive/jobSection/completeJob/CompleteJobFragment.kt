package com.ablueclive.jobSection.completeJob

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.runningJob.ParentRecyclerAdapter
import com.ablueclive.jobSection.runningJob.RunningJobListResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.date_pick_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class CompleteJobFragment : Fragment(), RecyclerViewItemClick {

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var session_token: String? = null

    var fromDate = ""/*getCurrentDate()*/
    var toDate = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.complete_job_layout, container, false)
    }


    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        completedJobList()
    }

    private fun init() {
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet

        fromLayout.setOnClickListener {
            showDatePicker(tvFrom)
        }

        toLayout.setOnClickListener {

            if (tvFrom.text.toString().isEmpty()) {
                CommonMethod.showToastlong("Please Select From Date", requireActivity())
                return@setOnClickListener
            }

            showDatePicker(tvTo)
        }

        btnArrow.setOnClickListener {

            if (tvFrom.text.toString().isEmpty() || tvTo.text.toString().isEmpty()) {
                CommonMethod.showToastlong("Please Select Date", requireActivity())
                return@setOnClickListener
            }
            fromDate = tvFrom.text.toString()
            toDate = tvTo.text.toString()

            completedJobList()

        }

    }


    private fun getCurrentDate(): String {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return df.format(c)
    }

    @SuppressLint("SetTextI18n")
    fun showDatePicker(textView: TextView) {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.DatePickerDialogThemes,
                { view, year, monthOfYear, dayOfMonth ->

                    var month = "" + (monthOfYear + 1)
                    if ((monthOfYear + 1) < 10) {
                        month = "0" + (monthOfYear + 1)
                    }

                    var day = "" + dayOfMonth
                    if (dayOfMonth < 10) {
                        day = "0$dayOfMonth"
                    }

                    textView.text = "$year-$month-$day"


                    try {

                        if (tvFrom.text.toString().isEmpty() || tvTo.text.toString().isEmpty()) {
                            return@DatePickerDialog
                        }

                        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        val dateFrom = sdf.parse(tvFrom.text.toString())
                        val dateTo = sdf.parse(tvTo.text.toString())

                        if (dateTo!!.before(dateFrom)) {
                            Log.e("showDatePicker", "dateTo is before dateFrom")
                            tvTo.text = null
                            tvTo.hint = getString(R.string.to)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
    }


    private fun completedJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["from_date"] = fromDate
        param["to_date"] = toDate
        param["status"] = "4"

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getRunningJobListing(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<RunningJobListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: RunningJobListResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            val list = response.responseData?.getjoblisting
                            if (list.isNullOrEmpty()) {
                                CommonMethod.isEmptyView(true, requireContext(), getString(R.string.no_completed_job))
                            } else {
                                CommonMethod.isEmptyView(false, requireContext(), getString(R.string.no_completed_job))
                                recyclerView.apply {
                                    layoutManager = LinearLayoutManager(requireActivity())
                                    adapter = ParentRecyclerAdapter(this@CompleteJobFragment, list, Constants.POST_COMPLETED_JOB)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {

    }
}