package com.ablueclive.jobSection.completeJob

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.runningJob.PJobDetailResponse
import com.ablueclive.jobSection.searchJob.SJobDetailFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.completed_job_item.view.*


class SCompletedJobAdapter(private var list: List<SCompleteJobResponse.SeekerCompleteJob>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<SCompletedJobAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {

                PJobDetailResponse.userImage = list[adapterPosition].seekerCompletedData?.userData?.profileImage
                PJobDetailResponse.minPrice = list[adapterPosition].seekerCompletedData?.minPrice
                PJobDetailResponse.maxPrice = list[adapterPosition].seekerCompletedData?.maxPrice
                PJobDetailResponse.reqJobId = list[adapterPosition].seekerCompletedData?.jobReqId

                SJobDetailFragment.apiParam["service_id"] = list[adapterPosition].seekerCompletedData?.serviceId.toString()
                SJobDetailFragment.apiParam["job_id"] = list[adapterPosition].seekerCompletedData?.jobId.toString()
                SJobDetailFragment.apiParam["provider_id"] = list[adapterPosition].seekerCompletedData?.userData?.id.toString()
                SJobDetailFragment.apiParam["status"] = list[adapterPosition].seekerCompletedData?.jobStatus.toString()
                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.complete_job_detail))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.completed_job_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].seekerCompletedData?.userData?.profileImage, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].seekerCompletedData?.jobTitle
        holder.itemView.tvName.text = list[position].seekerCompletedData?.userData?.name
        holder.itemView.tvDuration.text = CommonMethod.getTimesAgo(list[position].seekerCompletedData?.updatedAt.toString())
    }
}
