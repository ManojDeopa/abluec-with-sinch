package com.ablueclive.jobSection.runningJob

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PJobDetailResponse {


    companion object {

        var userImage: String? = null

        var reqJobId: String? = null

        var minPrice: String? = null

        var maxPrice: String? = null
    }


    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("getjobDetails")
        @Expose
        var getjobDetails: GetjobDetails? = null
    }

    public class GetjobDetails {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("job_title")
        @Expose
        var jobTitle: String? = null

        @SerializedName("job_desc")
        @Expose
        var jobDesc: String? = null

        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null

        @SerializedName("jobType")
        @Expose
        var jobType: Int? = null

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String = ""

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("job_image")
        @Expose
        var jobImage: List<JobImage>? = null

        @SerializedName("cat")
        @Expose
        var cat: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null

        @SerializedName("user_name")
        @Expose
        var userName: String? = null

        @SerializedName("user_mobile")
        @Expose
        var userMobile: String? = null

        @SerializedName("provider_id")
        @Expose
        var providerId: String? = null

        @SerializedName("lat")
        @Expose
        var lat: String? = null

        @SerializedName("long")
        @Expose
        var _long: String? = null

        @SerializedName("location_type")
        @Expose
        var location_type = ""

        @SerializedName("complete_address")
        @Expose
        var complete_address = "Remote"



    }

    class JobImage {
        @SerializedName("name")
        @Expose
        var name: String? = null
    }
}