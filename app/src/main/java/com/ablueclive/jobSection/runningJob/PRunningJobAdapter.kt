package com.ablueclive.jobSection.runningJob

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.runningJob.PJobDetailFragment.Companion.apiParam
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.running_job_item.view.*


class PRunningJobAdapter(private var list: List<RunningJobListResponse.Datum>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<PRunningJobAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {

                PJobDetailResponse.userImage = list[absoluteAdapterPosition].providerData?.profileImage
                PJobDetailResponse.minPrice = list[absoluteAdapterPosition].minPrice
                PJobDetailResponse.maxPrice = list[absoluteAdapterPosition].maxPrice
                PJobDetailResponse.reqJobId = list[absoluteAdapterPosition].jobReqId

                apiParam["service_id"] = list[absoluteAdapterPosition].serviceId.toString()
                apiParam["job_id"] = list[absoluteAdapterPosition].jobId.toString()
                apiParam["provider_id"] = list[absoluteAdapterPosition].providerData?.id.toString()
                apiParam["status"] = list[absoluteAdapterPosition].jobStatus.toString()

                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.applicants_job_detail))
            }

            itemView.tvComplete.setOnClickListener {
                listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 0)
            }

            itemView.btnAccept.setOnClickListener {
                listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 1)
            }

            itemView.ivCross.setOnClickListener {
                listener.onRecyclerItemClick(list[absoluteAdapterPosition].jobId.toString() + ":" + list[absoluteAdapterPosition].providerData?.id, 2)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.running_job_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].providerData?.profileImage, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].jobTitle
        holder.itemView.tvName.text = list[position].providerData?.name

        when (list[position].jobStatus) {
            2 -> {
                holder.itemView.acceptRejectLayout.visibility = View.VISIBLE
                holder.itemView.tvComplete.visibility = View.GONE
            }
            3 -> {
                holder.itemView.acceptRejectLayout.visibility = View.GONE
                holder.itemView.tvComplete.visibility = View.VISIBLE
            }
            else -> {
                holder.itemView.acceptRejectLayout.visibility = View.GONE
                holder.itemView.tvComplete.visibility = View.GONE
            }
        }
    }
}
