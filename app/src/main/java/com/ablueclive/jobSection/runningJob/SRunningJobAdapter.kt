package com.ablueclive.jobSection.runningJob

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.searchJob.SJobDetailFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.running_job_item.view.*


class SRunningJobAdapter(var list: List<SRunningJobResponse.SeekerRunningJob>, var listener: RecyclerViewItemClick) : RecyclerView.Adapter<SRunningJobAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {


                PJobDetailResponse.userImage = list[adapterPosition].seekerRunningData?.userData?.profileImage
                PJobDetailResponse.minPrice = list[adapterPosition].seekerRunningData?.minPrice
                PJobDetailResponse.maxPrice = list[adapterPosition].seekerRunningData?.maxPrice
                PJobDetailResponse.reqJobId = list[adapterPosition].seekerRunningData?.jobReqId

                SJobDetailFragment.apiParam["service_id"] = list[adapterPosition].seekerRunningData?.serviceId.toString()
                SJobDetailFragment.apiParam["job_id"] = list[adapterPosition].seekerRunningData?.jobId.toString()
                SJobDetailFragment.apiParam["provider_id"] = list[adapterPosition].seekerRunningData?.userData?.id.toString()
                SJobDetailFragment.apiParam["status"] = list[adapterPosition].seekerRunningData?.jobStatus.toString()

                CommonMethod.callJobContainer(context as Activity, context.getString(R.string.running_job_detail))

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.running_job_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].seekerRunningData?.userData?.profileImage, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].seekerRunningData?.jobTitle
        holder.itemView.tvName.text = list[position].seekerRunningData?.userData?.name
    }
}
