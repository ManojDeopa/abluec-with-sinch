package com.ablueclive.jobSection.runningJob

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.rateReview.RateReviewFragment
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.job_detail_fragment_p.*


class PJobDetailFragment : Fragment() {

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    var sessionToken = ""
    lateinit var api: Observable<CommonResponse>


    companion object {
        val apiParam = HashMap<String, String>()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_detail_fragment_p, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        requestJobList()
    }

    private fun requestJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        apiParam["session_token"] = sessionToken

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getJobUserDetails(apiParam).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<PJobDetailResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: PJobDetailResponse) {
                        println(response.responseMsg)
                        ProgressD.hide()
                        try {
                            response.responseData?.getjobDetails?.let { loadData(it) }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }

    private fun loadData(it: PJobDetailResponse.GetjobDetails) {


        tvDuration.text = CommonMethod.getTimesAgo(it.updatedAt)
        tvName.text = it.userName
        tvService.text = it.cat
        tvPhoneNo.text = it.userMobile

        tvJobTitle.text = it.jobTitle
        tvJobDescription.text = it.jobDesc

        tvJobAddress.text = it.complete_address

        CommonMethod.loadImageResize(Constants.PROFILE_IMAGE_URL + PJobDetailResponse.userImage, ivImage)

        if (JobContainerActivity.headerText == getString(R.string.complete_job_detail)) {
            btnApplicantDetail.visibility = View.GONE
            btnRateServiceProvider.visibility = View.VISIBLE
            tvStatus.visibility = View.VISIBLE

        } else if (JobContainerActivity.headerText == getString(R.string.applicants_job_detail)) {

            if (it.status == 2) {
                // tvAdvDueAmount.text = "Advance Payment due $ " + PJobDetailResponse.minPrice
                //tvEstimatePrice.text = "Estimated Price $ " + PJobDetailResponse.minPrice
                acceptRejectBtnLayout.visibility = View.VISIBLE
                tvPhoneNo.visibility = View.VISIBLE
            }

            if (it.status == 3) {
                btnComplete.visibility = View.VISIBLE
                tvPhoneNo.visibility = View.VISIBLE
            }
        }

        tvPhoneNo.setOnClickListener { v ->

            val mobileNo = it.userMobile
            if (mobileNo.isNullOrEmpty()) {
                CommonMethod.showToastlong("Mobile No. Not Found", requireContext())
                return@setOnClickListener
            }
            CommonMethod.callIntent(mobileNo, requireContext())
        }

        btnComplete.setOnClickListener { v ->
            requestData(0, it.id + ":" + it.providerId)
        }

        btnAccept.setOnClickListener { v ->
            requestData(1, it.id + ":" + it.providerId)
        }

        btnReject.setOnClickListener { v ->
            requestData(2, it.id + ":" + it.providerId)
        }

        btnRateServiceProvider.setOnClickListener { v ->

            RateReviewFragment.apiParams.clear()

            RateReviewFragment.apiParams["session_token"] = sessionToken
            RateReviewFragment.apiParams["service_provider_id"] = it.providerId.toString()
            RateReviewFragment.apiParams["job_id"] = it.id.toString()

            RateReviewFragment.type = Constants.TYPE_USER
            RateReviewFragment.image = Constants.PROFILE_IMAGE_URL + PJobDetailResponse.userImage

            CommonMethod.callJobContainer(requireActivity(), getString(R.string.rate_review))
        }

        btnApplicantDetail.setOnClickListener { v ->
            BMSPrefs.putString(requireContext(), Constants.FRIEND_ID, it.providerId)
            CommonMethod.callActivity(requireContext(), Intent(requireContext(), FriendProfileActivity::class.java))
        }
    }

    private fun init() {
        sessionToken = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


    private fun requestData(position: Int, id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = sessionToken
        param["job_id"] = separated[0]


        if (position == 0) {
            api = apiInterface.completeRequestApi(param)
        }

        if (position == 1) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.acceptJobRequestApi(param)
        }

        if (position == 2) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.rejectJobRequestApi(param)
        }


        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            if (response.responseStatus == 1) {
                                if (position == 0) {
                                    CommonMethod.showToastShort(response.responseData?.completeRequestApi, requireActivity())
                                }
                                if (position == 1) {
                                    CommonMethod.showToastShort(response.responseData?.acceptJobRequestApi, requireActivity())
                                }
                                if (position == 2) {
                                    CommonMethod.showToastShort(response.responseData?.rejectJobRequestApi, requireActivity())
                                }
                                requireActivity().finish()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(context: Context, text: String?) {
        val alert = AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            activity?.finish()
        }
        alert.show()
    }

}