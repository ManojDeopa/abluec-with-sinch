package com.ablueclive.jobSection.runningJob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*

class SRunningJobFragment : Fragment(), RecyclerViewItemClick {

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var session_token: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_view_layout, container, false)
    }



    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            init()
        }
    }

    override fun onResume() {
        super.onResume()
        runningJobList()
    }

    private fun init() {
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


    private fun runningJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireContext(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getSeekerRunningJob(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SRunningJobResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: SRunningJobResponse) {
                        ProgressD.hide()
                        println(response.responseMsg)

                        val list = response.responseData?.seekerRunningJob
                        if (list.isNullOrEmpty()) {
                            CommonMethod.isEmptyView(true, requireContext(), getString(R.string.no_running_job))
                        } else {
                            CommonMethod.isEmptyView(false, requireContext(), getString(R.string.no_running_job))
                            recyclerView.apply {
                                layoutManager = LinearLayoutManager(requireActivity())
                                adapter = SRunningJobAdapter(list, this@SRunningJobFragment)
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {

    }

}