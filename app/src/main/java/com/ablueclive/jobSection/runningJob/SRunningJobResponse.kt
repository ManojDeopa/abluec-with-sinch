package com.ablueclive.jobSection.runningJob

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SRunningJobResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("seekerRunningJob")
        @Expose
        var seekerRunningJob: List<SeekerRunningJob>? = null
    }

    class SeekerRunningJob {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("seekerRunningData")
        @Expose
        var seekerRunningData: SeekerRunningData? = null
    }


    class SeekerRunningData {
        @SerializedName("job_title")
        @Expose
        var jobTitle: String? = null

        @SerializedName("job_id")
        @Expose
        var jobId: String? = null

        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("service_id")
        @Expose
        var serviceId: String? = null

        @SerializedName("job_status")
        @Expose
        var jobStatus: Int? = null

        @SerializedName("minPrice")
        @Expose
        var minPrice: String? = null

        @SerializedName("maxPrice")
        @Expose
        var maxPrice: String? = null

        @SerializedName("category_data")
        @Expose
        var categoryData: CategoryData? = null

        @SerializedName("user_data")
        @Expose
        var userData: UserData? = null

        @SerializedName("provider_data")
        @Expose
        var providerData: ProviderData? = null

        @SerializedName("job_req_id")
        @Expose
        var jobReqId: String? = null

        @SerializedName("job_image")
        @Expose
        var jobImage: List<String>? = null
    }

    class UserData {
        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("_id")
        @Expose
        var id: String? = null
    }

    class ProviderData {
        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("_id")
        @Expose
        var id: String? = null
    }

    class CategoryData {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null

        @SerializedName("updatedon")
        @Expose
        var updatedon: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null
    }


}