package com.ablueclive.jobSection.runningJob

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.jobSection.completeJob.PCompletedJobAdapter
import com.ablueclive.utils.Constants
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.parent_recycler_item.view.*

class ParentRecyclerAdapter(var listener: RecyclerViewItemClick, private var list: List<RunningJobListResponse.Getjoblisting>, var type: String) : RecyclerView.Adapter<ParentRecyclerAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.parent_recycler_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.textView.text = list[position].name
        holder.itemView.rv_child.apply {
            layoutManager = LinearLayoutManager(context)
            if (type == Constants.POST_RUNNING_JOB) {
                adapter = list[position].data?.let { PRunningJobAdapter(it, listener) }
            } else if (type == Constants.POST_COMPLETED_JOB) {
                adapter = list[position].data?.let { PCompletedJobAdapter(it, listener) }
            }
        }
    }

}
