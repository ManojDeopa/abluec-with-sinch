package com.ablueclive.jobSection.categoryFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.networkClass.HeaderInterceptor
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.ProgressD
import kotlinx.android.synthetic.main.select_category_fragment.*
import java.util.*

class SelectCategoryFragment : Fragment(), CategoryListContract.View {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.select_category_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnNext.setOnClickListener { v: View? ->

            if (JobContainerActivity.selectedCategoryId.isEmpty()) {
                CommonMethod.showToastlong("Please Select Category", requireActivity())
                return@setOnClickListener
            }

            CommonMethod.callJobContainer(requireActivity(), getString(R.string.select_sub_category))
        }

        requestCategoryList()
    }

    private fun requestCategoryList() {
        val params = HashMap<String, String>()
        params["token"] = HeaderInterceptor.X_TOKEN
        CategoryListPresenter(this).requestCategoryList(params)
    }

    override fun setDataToViews(response: CategoryListResponse) {
        Log.e("response_msg->", "->${response.responseMsg}")

        val layoutManager = GridLayoutManager(requireContext(), 3)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = response.responseData?.let { CategoryListAdapter(it) }

    }

    override fun onResponseFailure(throwable: Throwable?) {
        println("onResponseFailure -> " + throwable?.message)
    }

    override fun hideProgress() {
        ProgressD.hide()
    }

    override fun showProgress() {
        ProgressD.show(requireActivity(), "")
    }
}