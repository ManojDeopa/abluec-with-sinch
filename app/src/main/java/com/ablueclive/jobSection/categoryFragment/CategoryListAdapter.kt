package com.ablueclive.jobSection.categoryFragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity.Companion.selectedCategoryId
import com.ablueclive.common.JobContainerActivity.Companion.selectedCategoryName
import com.ablueclive.utils.Constants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.category_item_layout.view.*

class CategoryListAdapter(private var list: List<CategoryListResponse.ResponseDatum>) : RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                list.forEach {
                    it.isChecked = false
                }
                list[absoluteAdapterPosition].isChecked = true
                selectedCategoryId = list[absoluteAdapterPosition].id.toString()
                selectedCategoryName = list[absoluteAdapterPosition].name.toString()
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.category_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // holder.itemView.tvTitle.text = list[position].name
        Picasso.get().load(Constants.IMAGE_CATEGORY_URL + list[position].catImage).error(R.drawable.placeholder).into(holder.itemView.ivImage)
        if (list[position].isChecked) {
            holder.itemView.ivAlphaView.visibility = View.VISIBLE
            holder.itemView.ivCheck.visibility = View.VISIBLE
        } else {
            holder.itemView.ivAlphaView.visibility = View.GONE
            holder.itemView.ivCheck.visibility = View.INVISIBLE
        }
    }
}
