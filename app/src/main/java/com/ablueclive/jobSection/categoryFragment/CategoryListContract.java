package com.ablueclive.jobSection.categoryFragment;

import java.util.HashMap;

public interface CategoryListContract {

    interface Model {
        interface OnFinishedListener {
            void onFinished(CategoryListResponse response);

            void onFailure(Throwable t);
        }

        void getCategoryList(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(CategoryListResponse response);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestCategoryList(HashMap<String, String> param);
    }
}
