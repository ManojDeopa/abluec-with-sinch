package com.ablueclive.jobSection.categoryFragment;


import java.util.HashMap;

public class CategoryListPresenter implements CategoryListContract.Presenter, CategoryListContract.Model.OnFinishedListener {
    private CategoryListContract.View categoryListView;
    private CategoryListContract.Model categoryListModel;

    public CategoryListPresenter(CategoryListContract.View resetPwdView) {
        this.categoryListView = resetPwdView;
        this.categoryListModel = new CategoryListModel();
    }

    @Override
    public void onFinished(CategoryListResponse response) {
        if (categoryListView != null) {
            categoryListView.hideProgress();
            categoryListView.setDataToViews(response);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (categoryListView != null) {
            categoryListView.hideProgress();
            categoryListView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        categoryListView = null;
    }

    @Override
    public void requestCategoryList(HashMap<String, String> param) {
        if (categoryListView != null) {
            categoryListView.showProgress();
        }
        categoryListModel.getCategoryList(this, param);
    }
}
