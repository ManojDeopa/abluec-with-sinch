package com.ablueclive.jobSection.categoryFragment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CategoryListResponse {
    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: List<ResponseDatum>? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    inner class ResponseDatum {

        var isChecked=false

        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null

        @SerializedName("updatedon")
        @Expose
        var updatedon: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null
    }
}