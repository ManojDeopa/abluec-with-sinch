package com.ablueclive.jobSection.serviceProviders

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.common.JobContainerActivity.Companion.selectedServiceProviderId
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.service_providers_item.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class ServiceProvidersAdapter(private var list: List<ServiceProvidersResponse.GetServiceProvider>) : RecyclerView.Adapter<ServiceProvidersAdapter.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.chkBox.setOnClickListener {
                list.forEach {
                    it.isChecked = false
                }
                list[absoluteAdapterPosition].isChecked = true
                selectedServiceProviderId = list[absoluteAdapterPosition].id.toString()
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.service_providers_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.chkBox.isChecked = list[position].isChecked
        holder.itemView.tvName.text = list[position].name
        // holder.itemView.tv2.text = list[position].email
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + list[position].profileImage, holder.itemView.ivImage)

        try {
            var distance = "0"
            val loc = list[position].loc
            if (loc != null && loc.toString() != "0.0" && !loc.toString().contains("-")) {
                distance = roundOffDecimal(loc.toString().toDouble()).toString()
            }
            holder.itemView.tvDistance.text = "$distance Miles"
        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder.itemView.tvName.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].id.toString())
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }

        holder.itemView.ivImage.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list[position].id.toString())
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }
    }


    private fun roundOffDecimal(number: Double): Double {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.FLOOR
        return df.format(number).toDouble()
    }

}
