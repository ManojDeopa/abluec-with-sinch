package com.ablueclive.jobSection.serviceProviders

import AppUtils
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*
import kotlinx.android.synthetic.main.search_tab_fragment.*
import kotlinx.android.synthetic.main.service_providers_fragment.*
import kotlinx.android.synthetic.main.service_providers_fragment.ivSearchTop
import kotlinx.android.synthetic.main.service_providers_fragment.ivCross
import kotlinx.android.synthetic.main.service_providers_fragment.progressBar
import java.io.IOException
import java.util.*

class ServiceProvidersFragment : Fragment() {


    private val AUTOCOMPLETE_REQUEST_CODE = 10
    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var serviceProviderAdapter: ServiceProvidersAdapter
    private var serviceProvidersList = ArrayList<ServiceProvidersResponse.GetServiceProvider>()

    private var lat: Double = 0.0
    private var lng: Double = 0.0

    var pageCount = 1
    var rowCount = 20
    var searchText = ""
    val noData = ""

    private var isLoading = false
    lateinit var linearLayoutManager: LinearLayoutManager


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_providers_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        compositeDisposable = CompositeDisposable()
        val gpsTracker = GPSTracker(requireActivity())
        lat = gpsTracker.latitude
        lng = gpsTracker.longitude

        var selectedLoc = getAddress(LatLng(lat, lng))
        txtChangeAddress.text = selectedLoc

        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }

        rootLayout.requestFocus()

        txtChangeAddress.visibility = View.VISIBLE
        txtChangeAddress.setOnClickListener {
            onSearchAddress()
        }

        btnNext.setOnClickListener { v: View? ->

            if (JobContainerActivity.selectedServiceProviderId.isEmpty()) {
                CommonMethod.showToastlong(getString(R.string.select_service_provider_text), requireActivity())
                return@setOnClickListener
            }
            CommonMethod.callJobContainer(requireActivity(), getString(R.string.add_job_detail))
        }

        serviceProviderAdapter = ServiceProvidersAdapter(serviceProvidersList)
        linearLayoutManager = LinearLayoutManager(requireActivity())
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = serviceProviderAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()
                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true
                    requestServiceProvidersList()
                }
            }
        })

        ivSearchTop.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchText = s.toString()
                if (searchText.length < 3) {
                    searchText = noData
                }
                ivCross.visibility = if (searchText.isEmpty()) View.GONE else View.VISIBLE
                apiCallOnTextChange()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })


        ivCross.setOnClickListener {
            ivSearchTop.text = null
            searchText = noData
            apiCallOnTextChange()
        }

        requestServiceProvidersList()
    }


    private fun apiCallOnTextChange() {
        compositeDisposable.clear()
        serviceProvidersList.clear()
        serviceProviderAdapter.notifyDataSetChanged()
        // if (searchText.isEmpty()) return
        pageCount = 1
        requestServiceProvidersList()
    }


    private fun requestServiceProvidersList() {

        if (!AppUtils.isOnline(requireContext())) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        progressBar.visibility = View.VISIBLE

        val params = HashMap<String, String>()
        params["session_token"] = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        params["categoryId"] = JobContainerActivity.selectedCategoryId
        params["subCategoryId"] = JobContainerActivity.selectedSubCategoryId

        params["search_txt"] = searchText
        params["lat"] = lat.toString()
        params["long"] = lng.toString()
        params["page"] = pageCount.toString()
        params["row_count"] = rowCount.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        val disposable = apiInterface.serviceProviderList(params).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribeWith(
                object : DisposableObserver<ServiceProvidersResponse?>() {
                    override fun onStart() {
                        super.onStart()
                    }

                    override fun onNext(response: ServiceProvidersResponse) {

                        try {
                            progressBar.visibility = View.GONE
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.getServiceProvider
                                if (!list.isNullOrEmpty()) {
                                    serviceProvidersList.clear()
                                    serviceProvidersList.addAll(list)
                                    isLoading = false
                                    pageCount++
                                }
                                val counts = serviceProvidersList.size
                                serviceProviderAdapter.notifyDataSetChanged()
                                emptyParentLayout.visibility = if (counts == 0) View.VISIBLE else View.GONE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        progressBar.visibility = View.GONE
                        println(e.message)
                    }

                    override fun onComplete() {}
                })

        compositeDisposable.add(disposable)

    }

    private fun getAddress(latLng: LatLng?): String {
        var addressText = ""
        try {
            val addresses: List<Address>
            val geoCoder = Geocoder(requireContext(), Locale.getDefault())
            addresses = geoCoder.getFromLocation(latLng?.latitude!!, latLng.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                addressText = (address)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return addressText
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val address = place.address
                    txtChangeAddress.text = address
                    lat = place.latLng?.latitude!!
                    lng = place.latLng?.longitude!!
                    apiCallOnTextChange()
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage!! + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    //CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }
    }
}