package com.ablueclive.jobSection.addJobDetail

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.text.style.CharacterStyle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.common.SelectMediaAdapter
import com.ablueclive.listener.onMenuItemListener
import com.ablueclive.modelClass.Files
import com.ablueclive.utils.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.add_job_detail.*
import kotlinx.android.synthetic.main.recycler_view_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


class AddJobDetailFragment() : Fragment(), onMenuItemListener, AddJobDetailContract.View {

    /*private lateinit var uploadPhotoAdapter: UploadPhotoAdapter*/
    /*var mediaArrayList = arrayListOf<Files>()*/


    private lateinit var uploadPhotoAdapter: SelectMediaAdapter
    var mediaArrayList = arrayListOf<Files>()

    var mResults = arrayListOf<String>()

    var REQUEST_CODE = 732
    var AUTOCOMPLETE_REQUEST_CODE = 121


    var locationType = ""
    var priceCategory = ""
    var lat = ""
    var lng = ""
    var jobId = ""

    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12
    private var file: Uri? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_job_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        doOnClicks()
    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }

    private fun doOnClicks() {
        btnAddJobNow.setOnClickListener { v: View? ->

            when {

                etJobTitle.text.isNullOrEmpty() -> {
                    CommonMethod.showToastlong("Please Enter Job Title", requireContext())
                }

                etPriceRange.text.isNullOrEmpty() -> {
                    CommonMethod.showToastlong("Please Select Price Range", requireContext())
                }

                etLocationType.text.isNullOrEmpty() -> {
                    CommonMethod.showToastlong("Please Select Remote or OnSite", requireContext())
                }

                locationType == getString(R.string.onSite) -> {

                    when {
                        etSelectAddress.text.isNullOrEmpty() -> {
                            CommonMethod.showToastlong("Please Select Address", requireContext())
                        }
                        etJobDescription.text.isNullOrEmpty() -> {
                            CommonMethod.showToastlong("Please Enter Job Description", requireContext())
                        }
                        else -> {
                            requestJobDetail()
                        }
                    }
                }

                /*etMinPrice.text.isNullOrEmpty() -> {
                    Toast.makeText(requireContext(), "Please Enter Min Price", Toast.LENGTH_LONG).show()
                }

                etMaxPrice.text.isNullOrEmpty() -> {
                    Toast.makeText(requireContext(), "Please Enter Max Price", Toast.LENGTH_LONG).show()
                }

                etMinPrice.text.toString().toFloat() > etMaxPrice.text.toString().toFloat() -> {
                    Toast.makeText(requireContext(), "Max price can't be less than Min price", Toast.LENGTH_LONG).show()
                }*/

                /* etSelectAddress.text.isNullOrEmpty() -> {
                     Toast.makeText(requireContext(), "Please Select Address", Toast.LENGTH_LONG).show()
                 }*/

                etJobDescription.text.isNullOrEmpty() -> {
                    CommonMethod.showToastlong("Please Enter Job Description", requireContext())
                }
                else -> {
                    requestJobDetail()
                }
            }
        }

        ivAdd.visibility = View.GONE
        ivAdd.setOnClickListener {
            resetFocus()
            selectImage()
        }

        etPriceRange.setOnClickListener {
            resetFocus()
            showPriceRangeList()
        }

        etLocationType.setOnClickListener {
            resetFocus()
            selectLocationType()
        }

        etSelectAddress.setOnClickListener {
            resetFocus()
            onSearchAddress()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init() {

        val apiKey = getString(R.string.api_key)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), apiKey)
        }

        if (JobContainerActivity.isEdit) {

            val data = Constants.jobResponse
            jobId = data.id.toString()
            etJobTitle.setText(data.jobTitle)
            priceCategory = data.priceCategory.toString()
            etPriceRange.setText(data.priceCategory.toString())
            etMinPrice.setText(data.minPrice.toString())
            etMaxPrice.setText(data.maxPrice.toString())

            locationType = getString(R.string.onSite)
            if (data.complete_address == "Remote") {
                locationType = getString(R.string.remote)
            }
            etLocationType.setText(locationType)

            if (!data.lat.isNullOrEmpty()) {
                lat = data.lat.toString()
                lng = data._long.toString()
                etSelectAddress.setText(data.complete_address)
            }

            if (!data.jobDesc.isNullOrEmpty()) {
                etJobDescription.setText(data.jobDesc)
            }

            JobContainerActivity.jobType = data.jobType.toString()
            JobContainerActivity.selectedCategoryId = data.cat.toString()
            JobContainerActivity.selectedSubCategoryId = data.subCategoryId.toString()
            JobContainerActivity.selectedServiceProviderId = data.serviceProviderIds.toString()


            labelDescription.text = "Job Description"
            btnAddJobNow.text = getString(R.string.update_job_now)

            val imageList = data.jobImage
            imageList?.forEach {
                val imgName = it.name.toString()
                if (imgName.trim().isNotEmpty()) {
                    val file = Files()
                    file.media_path = Constants.IMAGE_STORE_URL + imgName.trim()
                    file.type = "1"
                    file.source = "internal"
                    file.thumbpath = ""
                    mediaArrayList.add(file)
                }
            }

            if (locationType == getString(R.string.onSite)) {
                tvWorkAddressLabel.visibility = View.VISIBLE
                etSelectAddress.visibility = View.VISIBLE
            }
        }

        resetFocus()
        setPhotoAdapter()

        etJobDescription.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        etJobDescription.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(string: Editable?) {

                if (!string.isNullOrEmpty()) {
                    val toBeRemovedSpans: Array<CharacterStyle> = string.getSpans(0, string.length, CharacterStyle::class.java)
                    toBeRemovedSpans.forEach {
                        string.removeSpan(it)
                    }
                }


            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }


    private fun onSearchAddress() {
        val fields: List<Place.Field> = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)/*.setCountry(Constants.CURRENT_COUNTRY_CODE)*/.build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }


    private fun showPriceRangeList() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        val list = arrayOf(getString(R.string.full_time), getString(R.string.part_time), getString(R.string.hourly_project))
        val checkedItem = list.indexOf(priceCategory)
        builder.setSingleChoiceItems(list, checkedItem) { dialog, which ->
            priceCategory = list[which]
        }
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            etPriceRange.setText(priceCategory)
            dialog.dismiss()
        }
        builder.setNegativeButton(getString(R.string.cancel), null);
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun selectLocationType() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        val list = arrayOf(getString(R.string.remote), getString(R.string.onSite))
        val checkedItem = list.indexOf(locationType)
        builder.setSingleChoiceItems(list, checkedItem) { dialog, which ->
            locationType = list[which]
        }
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            etLocationType.setText(locationType)
            dialog.dismiss()

            if (locationType == getString(R.string.onSite)) {
                tvWorkAddressLabel.visibility = View.VISIBLE
                etSelectAddress.visibility = View.VISIBLE
            } else {
                tvWorkAddressLabel.visibility = View.GONE
                etSelectAddress.visibility = View.GONE
            }
        }
        builder.setNegativeButton(getString(R.string.cancel), null);
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


    private fun selectImage() {
        val alertDialog = android.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.type = "image/*"
            startActivityForResult(intent, REQUEST_CODE_GALLERY)
        }
    }

    private fun setPhotoAdapter() {
        uploadPhotoAdapter = SelectMediaAdapter(requireActivity(), mediaArrayList, this)
        //val layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);
        //val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        val layoutManager = GridLayoutManager(requireContext(), 2)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = uploadPhotoAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    mResults = getSelectedVideos(requestCode, data) as ArrayList<String>

                    for (i in mResults.indices) {
                        val file = Files()
                        file.media_path = mResults[i]
                        file.type = "1"
                        file.source = "internal"
                        file.thumbpath = ""
                        mediaArrayList.add(file)
                    }

                    /* Check element Duplication if than remove*/
                    for (i in mediaArrayList.indices) {
                        var j = i + 1
                        while (j < mediaArrayList.size) {
                            if (mediaArrayList[i].media_path.equals(mediaArrayList[j].media_path, ignoreCase = true)) {
                                mediaArrayList.removeAt(j)
                                j--
                            }
                            j++
                        }
                    }

                    recyclerView.visibility = View.VISIBLE
                    uploadPhotoAdapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                val mFile = Files()
                mFile.media_path = CommonMethods.getRealPathFromURI(requireActivity(), file)
                mFile.type = "1"
                mFile.source = "internal"
                mFile.thumbpath = ""
                mediaArrayList.add(mFile)
                recyclerView.visibility = View.VISIBLE
                uploadPhotoAdapter.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Log.i("AddJobDetail-", "Place: " + place.name + ", " + place.id + ", " + place.address)
                    val address = place.address
                    etSelectAddress.setText(address)

                    lat = place.latLng?.latitude.toString()
                    lng = place.latLng?.longitude.toString()

                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    CommonMethod.showToastlong(status.statusMessage!! + "", requireContext())
                }
                Activity.RESULT_CANCELED -> {
                    // CommonMethod.showToastlong("Search Cancelled", requireContext())
                }
            }
        }
    }

    private fun getSelectedVideos(requestCode: Int, data: Intent?): MutableList<String> {
        val result: MutableList<String> = ArrayList()
        var clipData: ClipData? = null
        clipData = data!!.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                val videoItem = clipData.getItemAt(i)
                val videoURI = videoItem.uri
                val filePath = Utility.getPath(requireContext(), videoURI)
                result.add(filePath)
            }
        } else {
            val videoURI = data.data
            val filePath = Utility.getPath(requireContext(), videoURI)
            result.add(filePath)
        }
        return result
    }

    override fun onMenuItemClick(position: Int) {

        if (position == 2) {
            resetFocus()
            selectImage()
        } else {
            if (mediaArrayList.size > 0) {
                mediaArrayList.removeAt(position)
                uploadPhotoAdapter.notifyDataSetChanged()
            }
        }

    }


    private fun requestJobDetail() {

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN))
        builder.addFormDataPart("service_id", JobContainerActivity.selectedCategoryId)
        builder.addFormDataPart("sub_category", JobContainerActivity.selectedSubCategoryId)
        builder.addFormDataPart("serviceProviderIds", JobContainerActivity.selectedServiceProviderId)

        builder.addFormDataPart("job_title", etJobTitle.text.toString().trim())
        builder.addFormDataPart("est_price", etMinPrice.text.toString().trim())
        builder.addFormDataPart("job_desc", etJobDescription.text.toString().trim())


        builder.addFormDataPart("jobType", JobContainerActivity.jobType)
        builder.addFormDataPart("priceCategory", etPriceRange.text.toString())
        builder.addFormDataPart("minPrice", etMinPrice.text.toString().trim())
        builder.addFormDataPart("maxPrice", etMaxPrice.text.toString().trim())

        builder.addFormDataPart("location_type", etLocationType.text.toString())

        if (locationType == getString(R.string.remote)) {
            builder.addFormDataPart("complete_address", "Remote")
            builder.addFormDataPart("lat", "0.0")
            builder.addFormDataPart("long", "0.0")
        } else {
            builder.addFormDataPart("complete_address", etSelectAddress.text.toString())
            builder.addFormDataPart("lat", lat)
            builder.addFormDataPart("long", lng)
        }

        val imgUrlList = arrayListOf<String>()
        mediaArrayList.forEach {
            if (uploadPhotoAdapter.isValidUrl(it.media_path)) {
                imgUrlList.add(it.media_path.replace(Constants.IMAGE_STORE_URL, ""))
            } else {
                val imgFile = File(it.media_path)
                val file = Compressor.getDefault(context).compressToFile(imgFile)
                builder.addFormDataPart("job_image", file.name, file.asRequestBody("image/jpg".toMediaTypeOrNull()))
            }
        }
        if (JobContainerActivity.isEdit) {
            builder.addFormDataPart("_id", jobId)
            builder.addFormDataPart("previous_images_name", imgUrlList.toString().trim().replace("[", "").replace("]", ""))
        }
        val requestBody = builder.build()
        AddJobDetailPresenter(this).requestData(requestBody)

    }

    override fun setDataToViews(response: CommonResponse?) {
        println(response?.responseMsg)
        if (response?.responseStatus == 1) {

            var message = "Job Posted Successfully."
            if (JobContainerActivity.isEdit) {
                message = "Job Updated Successfully."
            }
            val alert = AlertDialog.Builder(requireContext(), R.style.alertDialogTheme)
            alert.setTitle(getString(R.string.app_name))
            alert.setMessage(message)
            alert.setCancelable(false)
            alert.setPositiveButton(context!!.getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
                JobContainerActivity.isJobAdded = true
                dialog.dismiss()
                CommonMethod.callJobContainer(requireActivity(), getString(R.string.job_post))
            }
            alert.show()
        }
    }

    override fun onResponseFailure(throwable: Throwable?) {
        println(throwable?.message)
    }

    override fun hideProgress() {
        ProgressD.hide()
    }

    override fun showProgress() {
        ProgressD.show(requireActivity(), "")
    }
}