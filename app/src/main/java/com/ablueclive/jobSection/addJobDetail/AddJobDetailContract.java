package com.ablueclive.jobSection.addJobDetail;

import com.ablueclive.common.CommonResponse;

import okhttp3.RequestBody;


public interface AddJobDetailContract {

    interface Model {

        interface OnFinishedListener {

            void onFinished(CommonResponse response);

            void onFailure(Throwable t);
        }

        void getData(OnFinishedListener onFinishedListener, RequestBody file);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(CommonResponse response);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {

        void onDestroy();

        void requestData(RequestBody file);
    }
}
