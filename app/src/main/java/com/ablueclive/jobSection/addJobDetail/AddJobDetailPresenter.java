package com.ablueclive.jobSection.addJobDetail;


import com.ablueclive.common.CommonResponse;

import okhttp3.RequestBody;

public class AddJobDetailPresenter implements AddJobDetailContract.Presenter, AddJobDetailContract.Model.OnFinishedListener {

    private AddJobDetailContract.View view;
    private AddJobDetailContract.Model model;

    public AddJobDetailPresenter(AddJobDetailContract.View view) {
        this.view = view;
        this.model = new AddJobDetailModel();
    }

    @Override
    public void onFinished(CommonResponse response) {
        if (view != null) {
            view.hideProgress();
            view.setDataToViews(response);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (view != null) {
            view.hideProgress();
            view.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void requestData(RequestBody file) {
        if (view != null) {
            view.showProgress();
            model.getData(this, file);
        }

    }
}
