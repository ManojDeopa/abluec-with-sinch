package com.ablueclive.jobSection.addJobDetail;

import android.util.Log;

import com.ablueclive.common.CommonResponse;
import com.ablueclive.common.JobContainerActivity;
import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.networkClass.RetrofitClient;

import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class AddJobDetailModel implements AddJobDetailContract.Model {

    private static final String TAG = "AddJobDetailModel";
    private boolean isEdit = JobContainerActivity.Companion.isEdit();

    @Override
    public void getData(OnFinishedListener onFinishedListener, RequestBody file) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Observable<CommonResponse> apiCall = apiInterface.sendJobRequest(file);
        if (isEdit) {
            apiCall = apiInterface.editJobRequest(file);
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CommonResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CommonResponse response) {
                        onFinishedListener.onFinished(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, Objects.requireNonNull(e.getMessage()));
                        onFinishedListener.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
