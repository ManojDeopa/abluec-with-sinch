package com.ablueclive.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.listener.onDeleteSubCmntListener;
import com.ablueclive.modelClass.ReplyData;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ReplySubCmntAdapter extends RecyclerView.Adapter<ReplySubCmntAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<ReplyData> replyDataArrayList;
    private final onDeleteSubCmntListener onDeleteSubCmntListener;

    public ReplySubCmntAdapter(Context context, ArrayList<ReplyData> replyDataArrayList, onDeleteSubCmntListener onDeleteSubCmntListener) {
        this.context = context;
        this.replyDataArrayList = replyDataArrayList;
        this.onDeleteSubCmntListener = onDeleteSubCmntListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subcomment_adapter, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.text_report.setVisibility(View.GONE);
        viewHolder.text_cmnt_username.setText(replyDataArrayList.get(i).getUser_name());
        viewHolder.text_comment.setText(replyDataArrayList.get(i).getReply_user_comment());
        Glide.with(context)
                .load(Constants.PROFILE_IMAGE_URL + replyDataArrayList.get(i).getProfile_image())
                .skipMemoryCache(true)
                .error(R.drawable.user_profile)
                .into(viewHolder.img_comment_profile);

        viewHolder.text_deleteCmnt.setOnClickListener(v -> {
            onDeleteSubCmntListener.onDelSubCmntClick(replyDataArrayList.get(i).getReply_id());
            replyDataArrayList.remove(i);
            notifyDataSetChanged();
        });


        viewHolder.img_comment_profile.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, replyDataArrayList.get(i).getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });


        viewHolder.text_cmnt_username.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, replyDataArrayList.get(i).getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });


    }

    @Override
    public int getItemCount() {
        return replyDataArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text_cmnt_username;
        private final TextView text_comment;
        private final TextView text_report;
        private final TextView text_deleteCmnt;
        private final CircleImageView img_comment_profile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_cmnt_username = itemView.findViewById(R.id.text_cmnt_username);
            text_comment = itemView.findViewById(R.id.text_comment);
            text_report = itemView.findViewById(R.id.text_report);
            img_comment_profile = itemView.findViewById(R.id.img_comment_profile);
            text_deleteCmnt = itemView.findViewById(R.id.text_deleteCmnt);
        }
    }
}
