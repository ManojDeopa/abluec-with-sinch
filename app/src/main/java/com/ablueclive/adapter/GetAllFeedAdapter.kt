package com.ablueclive.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.frndProfile.FriendProfileActivity.Companion.display_name
import com.ablueclive.activity.mainActivity.MainActivity.Companion.getAdRequest
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.common.CommonContainerActivity.Companion.feedDetailTitle
import com.ablueclive.fragment.feedFragment.ClickPosition
import com.ablueclive.listener.*
import com.ablueclive.modelClass.Like_users
import com.ablueclive.modelClass.PostData
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.callCommonContainer
import com.ablueclive.utils.CommonMethod.extractUrl
import com.ablueclive.utils.CommonMethod.isValidUrl
import com.ablueclive.utils.CommonMethod.loadImageCircle
import com.ablueclive.utils.CommonMethod.makeTextViewResizable
import com.ablueclive.utils.CommonMethod.popupMenuTextList
import com.ablueclive.utils.Constants
import com.ablueclive.utils.MenuItemClickListener
import com.ablueclive.utils.OnSeeMoreClickListener
import com.google.android.gms.ads.AdView
import io.github.ponnamkarthik.richlinkpreview.ViewListener
import kotlinx.android.synthetic.main.user_feed_adapter.view.*
import java.util.*
import java.util.regex.Pattern

class GetAllFeedAdapter(private val context: Context,
                        var postDataArrayList: ArrayList<PostData>,
                        private val onReportPostListener: onReportPostListener,
                        private val user_id: String,
                        private val onDeletePostListener: onDeletePostListener,
                        private val onLikePostListener: onLikePostListener,
                        private val onCommentPostListener: onCommentPostListener,
                        private val onSharePostListener: onSharePostListener,
                        private val onEditPostListener: onEditPostListener,
                        private val onSeeMoreClickListener: OnSeeMoreClickListener) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>(), onFriendProfileListener, ClickPosition {


    private lateinit var selectedViewHolder: View
    var TAG = GetAllFeedAdapter::class.java.simpleName

    companion object {
        private const val LAYOUT_ADS = 0
        private const val LAYOUT_FEEDS = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        val viewHolder: RecyclerView.ViewHolder
        if (viewType == LAYOUT_ADS) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.native_add, parent, false)
            viewHolder = AdViewHolder(view)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.user_feed_adapter, parent, false)
            viewHolder = ViewHolder(view)
        }
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, i: Int) {
        try {
            System.gc()
            if (holder.itemViewType == LAYOUT_ADS) {
                val adHolder = holder as AdViewHolder
                adHolder.mAdView.removeAllViews()
                adHolder.mAdView.loadAd(getAdRequest())
            } else {
                val postHolder = holder as ViewHolder
                val viewHolder = postHolder.itemView
                val postData = postDataArrayList[i]
                val postUserData = postData.rePostUser
                if (postUserData != null && postUserData.isNotEmpty()) {
                    viewHolder.postContentItemLayout.setBackgroundResource(R.drawable.back_rectangular)
                    viewHolder.tvRePostContent.visibility = View.VISIBLE
                    viewHolder.tvRePostContent.text = postData.repost_content
                    viewHolder.postUserInfoLayout.visibility = View.VISIBLE
                    val ivPostUserImage: AppCompatImageView = viewHolder.postUserInfoLayout.findViewById(R.id.ivPostUserImage)
                    val tvPostUserName = viewHolder.postUserInfoLayout.findViewById<TextView>(R.id.tvPostUserName)
                    val data = postUserData[0]
                    tvPostUserName.text = data.name
                    loadImageCircle(Constants.PROFILE_IMAGE_URL + data.profile_image, ivPostUserImage)
                    ivPostUserImage.setOnClickListener { v: View? ->
                        BMSPrefs.putString(context, Constants.FRIEND_ID, data.id)
                        callActivity(context, Intent(context, FriendProfileActivity::class.java))
                    }
                    tvPostUserName.setOnClickListener { v: View? ->
                        BMSPrefs.putString(context, Constants.FRIEND_ID, data.id)
                        callActivity(context, Intent(context, FriendProfileActivity::class.java))
                    }
                } else {
                    viewHolder.tvRePostContent.visibility = View.GONE
                    viewHolder.postUserInfoLayout.visibility = View.GONE
                    viewHolder.postContentItemLayout.setBackgroundResource(0)
                }
                var waveCount = ""
                val shareUserList = postData.shareUserPost
                if (shareUserList != null && shareUserList.size > 0) {
                    waveCount = "" + shareUserList.size
                }
                viewHolder.tvTotalWaveCount.text = waveCount
                if (postData.isRetweeted) {
                    viewHolder.ivWave.setImageResource(R.drawable.wave_blue)
                } else {
                    viewHolder.ivWave.setImageResource(R.drawable.wave_black)
                }
                loadImageCircle(Constants.PROFILE_IMAGE_URL + postData.post_user_profile_image, viewHolder.img_user_profile)
                var displayName = ""
                if (!postData.post_user_display_name.isEmpty()) {
                    displayName = " @" + postData.post_user_display_name.replace("@", "")
                }
                displayName = "<font color='#1DA1F2'>$displayName</font>"
                viewHolder.text_user_name.text = HtmlCompat.fromHtml(postData.post_user_name + displayName, HtmlCompat.FROM_HTML_MODE_LEGACY)
                val timeAgo = postData.post_time_status
                viewHolder.text_time.text = timeAgo
                holder.itemView.setOnClickListener { v: View? ->
                    feedDetailTitle = postData.post_user_name
                    BMSPrefs.putString(context, Constants.FEED_POST_ID, postData._id)
                    callCommonContainer((context as Activity), context.getString(R.string.feed_details))
                }
                viewHolder.text_user_name.setOnClickListener { v: View? ->
                    BMSPrefs.putString(context, Constants.FRIEND_ID, postData.user_id)
                    callActivity(context, Intent(context, FriendProfileActivity::class.java))
                }
                viewHolder.img_user_profile.setOnClickListener { v: View? ->
                    BMSPrefs.putString(context, Constants.FRIEND_ID, postData.user_id)
                    callActivity(context, Intent(context, FriendProfileActivity::class.java))
                }
                if (postData.totalComments.equals("0", ignoreCase = true) || postData.totalComments == null || postData.totalComments.isEmpty()) {
                    viewHolder.tvTotalCommentCount.visibility = View.INVISIBLE
                } else {
                    viewHolder.tvTotalCommentCount.visibility = View.VISIBLE
                    viewHolder.tvTotalCommentCount.text = postData.totalComments + ""
                }
                var postContent = postData.post_content
                if (!TextUtils.isEmpty(postContent)) {
                    postContent = postContent.trim { it <= ' ' }
                    var url = extractUrl(postContent
                            .replace("[\\t\\n\\r]+".toRegex(), " ")
                            .replace("'".toRegex(), ""))
                    if (!TextUtils.isEmpty(url)) {
                        if (!isValidUrl(url)) {
                            url = "http://$url"
                        }
                        viewHolder.richLinkView.visibility = View.VISIBLE
                        viewHolder.richLinkView.setLink(url, object : ViewListener {
                            override fun onSuccess(status: Boolean) {}
                            override fun onError(e: Exception) {}
                        })
                        val text = postContent.replace(url, "")
                        if (!TextUtils.isEmpty(text)) {
                            viewHolder.text_feed.visibility = View.VISIBLE
                            viewHolder.text_feed.text = text.trim { it <= ' ' }
                        }
                    } else {
                        viewHolder.text_feed.visibility = View.VISIBLE
                        viewHolder.richLinkView.visibility = View.GONE
                        var areTaggedUser = false
                        val spannable = SpannableString(postContent)
                        val matcher = Pattern.compile("@\\s*(\\w+)").matcher(postContent)
                        while (matcher.find()) {
                            val displayText = matcher.group(1)!!
                            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                                override fun onClick(textView: View) {
                                    BMSPrefs.putString(context, Constants.FRIEND_ID,  /*postData.getUser_id()*/"123")
                                    display_name = displayText
                                    callActivity(context, Intent(context, FriendProfileActivity::class.java))
                                }

                                override fun updateDrawState(ds: TextPaint) {
                                    super.updateDrawState(ds)
                                    ds.isUnderlineText = false
                                    ds.color = Color.parseColor("#1DA1F2")
                                }
                            }
                            val cityIndex = postContent.indexOf(displayText) - 1
                            spannable.setSpan(clickableSpan, cityIndex, cityIndex + displayText.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                            areTaggedUser = true
                        }
                        if (areTaggedUser) {
                            viewHolder.text_feed.text = spannable
                            viewHolder.text_feed.movementMethod = LinkMovementMethod.getInstance()
                        } else {
                            viewHolder.text_feed.text = postContent
                            val finalPostContent = postContent
                            viewHolder.text_feed.post {
                                val lineCount = viewHolder.text_feed.lineCount
                                if (lineCount > 6) {
                                    makeTextViewResizable(viewHolder.text_feed, 6, " view more", true, onSeeMoreClickListener, i)
                                } else {
                                    viewHolder.text_feed.text = HtmlCompat.fromHtml(finalPostContent, HtmlCompat.FROM_HTML_MODE_LEGACY)
                                }
                            }
                        }
                    }
                }
                val imagesArrayList = postData.images
                if (imagesArrayList.size > 0) {
                    viewHolder.rvMedia.visibility = View.VISIBLE
                    var layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 2)
                    if (imagesArrayList.size == 1) {
                        layoutManager = LinearLayoutManager(context)
                    }
                    if (imagesArrayList.size == 3) {
                        layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
                    }
                    viewHolder.rvMedia.layoutManager = layoutManager
                    viewHolder.rvMedia.adapter = FeedMediaAdapter(imagesArrayList)
                } else {
                    viewHolder.rvMedia.visibility = View.GONE
                }
                viewHolder.img_option.setOnClickListener { v: View? ->
                    if (postData.user_id == user_id) {
                        val itemList = arrayOf(context.getString(R.string.edit), context.getString(R.string.del_post))
                        popupMenuTextList(context, viewHolder.img_option, itemList, object : MenuItemClickListener {
                            override fun onMenuItemClick(text: String, position: Int) {
                                if (position == 0) {
                                    onEditPostListener.onEditPostClick(postData._id, i)
                                }
                                if (position == 1) {
                                    onDeletePostListener.onDelPostClick(postData._id, i)
                                    postDataArrayList.removeAt(i)
                                    notifyItemRemoved(i)
                                }
                            }
                        })
                    } else {
                        val itemList = arrayOf(context.getString(R.string.report_post))
                        popupMenuTextList(context, viewHolder.img_option, itemList, object : MenuItemClickListener {
                            override fun onMenuItemClick(text: String, position: Int) {
                                onReportPostListener.onReportPostClick(postData._id, i)
                            }
                        })
                    }
                }
                selectedViewHolder = viewHolder
                setLikeCount(postData.like_users, postData.post_user_like_status, postData.totalCountLike)
                viewHolder.img_cheers.setOnClickListener { v: View? ->
                    selectedViewHolder = viewHolder
                    Constants.showLoader = false
                    onLikePostListener.onLikePostClick(postData._id, i)
                    if (selectedViewHolder.img_cheers.tag == "not_like") {
                        selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                        selectedViewHolder.img_cheers.tag = "like"
                    } else {
                        selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                        selectedViewHolder.img_cheers.tag = "not_like"
                    }
                }
                viewHolder.shareLayout.setOnClickListener { v: View? ->
                    try {
                        val sendIntent = Intent()
                        sendIntent.action = Intent.ACTION_SEND
                        sendIntent.putExtra(Intent.EXTRA_TEXT, postData.shareUrl)
                        sendIntent.type = "text/plain"
                        context.startActivity(Intent.createChooser(sendIntent, "Share Via : "))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                viewHolder.ivWave.setOnClickListener { v: View? -> showWaveDialog(postData, i) }
                viewHolder.ivComment.setOnClickListener { v: View? -> onCommentPostListener.onCmntPostClick(postData._id, i) }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun replaceLastChar(s: String): String {
        val length = s.length
        return if (length < 8) "Error: The provided string is not greater than four characters long." else s.substring(0, length - 8) + " See More"
    }

    private fun showWaveDialog(postData: PostData, i: Int) {
        val act = context as Activity
        val viewGroup = act.findViewById<ViewGroup>(android.R.id.content)
        val dialogView = LayoutInflater.from(act).inflate(R.layout.wave_dialog_layout, viewGroup, false)
        val builder = AlertDialog.Builder(act)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.show()
        val tvWave = dialogView.findViewById<TextView>(R.id.tvTotalWaveCount)
        val tvQuoteWave = dialogView.findViewById<TextView>(R.id.tvQuoteWave)
        val tvCancel = dialogView.findViewById<TextView>(R.id.tvCancel)
        if (postData.isRetweeted) {
            tvWave.text = context.getText(R.string.un_do_wave)
        }
        tvWave.setOnClickListener { v: View? ->
            alertDialog.dismiss()
            onSharePostListener.onSharePostClick(postData._id, i, tvWave.text.toString())
        }
        tvQuoteWave.setOnClickListener { v: View? ->
            alertDialog.dismiss()
            feedDetailTitle = postData.post_user_name
            BMSPrefs.putString(context, Constants.FEED_POST_ID, postData._id)
            callCommonContainer(context, context.getString(R.string.quote_wave))
        }
        tvCancel.setOnClickListener { v: View? -> alertDialog.dismiss() }
    }

    fun setLikeCount(like_usersArrayList: ArrayList<Like_users>?, post_user_like_status: String?, totalCountLike: String?) {
        val myUserId = BMSPrefs.getString(context, Constants._ID)
        var isLiked = false
        if (like_usersArrayList != null && like_usersArrayList.size > 0) {
            for (i in like_usersArrayList.indices) {
                if (myUserId == like_usersArrayList[i].user_id) {
                    isLiked = true
                }
            }
            selectedViewHolder.tvTotalLikeCount.text = totalCountLike
            if (isLiked) {
                selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                selectedViewHolder.img_cheers.tag = "like"
                selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            } else {
                selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                selectedViewHolder.img_cheers.tag = "not_like"
                selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.dot_dark_screen1))
            }
        } else {
            if (post_user_like_status != null) {
                if (post_user_like_status.equals("Like", ignoreCase = true)) {
                    selectedViewHolder.tvTotalLikeCount.text = totalCountLike
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                    selectedViewHolder.img_cheers.tag = "like"
                    selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                } else {
                    selectedViewHolder.tvTotalLikeCount.text = totalCountLike
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                    selectedViewHolder.img_cheers.tag = "not_like"
                    selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.dot_dark_screen1))
                }
            }
        }
    }

    override fun onFrndProfileClick(friend_id: String, position: Int) {
        BMSPrefs.putString(context, Constants.FRIEND_ID, friend_id)
        callActivity(context, Intent(context, FriendProfileActivity::class.java))
    }

    override fun onAddFriendClick(friend_id: String, position: Int) {}

    override fun getPosition(position: Int) {}

    override fun getItemViewType(position: Int): Int {
        return if (position != 0 && position % 75 == 0) {
            LAYOUT_ADS
        } else if (position == 9) {
            LAYOUT_ADS
        } else if (position == 27) {
            LAYOUT_ADS
        } else {
            LAYOUT_FEEDS
        }
    }

    override fun getItemCount(): Int {
        return postDataArrayList.size
    }

    class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mAdView: AdView = itemView.findViewById(R.id.adView)
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            var gravity = Gravity.START
            if (context.javaClass.simpleName == ProfileActivity::class.java.simpleName || context.javaClass.simpleName == FriendProfileActivity::class.java.simpleName) {
                gravity = Gravity.END or Gravity.CENTER
            }
            itemView.tvTotalCommentCount.gravity = gravity
        }
    }


}