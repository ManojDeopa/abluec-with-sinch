package com.ablueclive.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.playVideo.VideoPlayActivity
import com.ablueclive.modelClass.Images
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ShowFullImage
import kotlinx.android.synthetic.main.feed_media_adpter.view.*


class FeedMediaAdapterVertical(private var imagesArrayList: ArrayList<Images>) : RecyclerView.Adapter<FeedMediaAdapterVertical.ViewHolder>() {

    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setMargins(itemView.cardContainer, 5, 5, 5, 5)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.feed_media_adpter, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return imagesArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val type: String = imagesArrayList[position].imageType
        var image = imagesArrayList[position].imageName
        if (!CommonMethod.isValidUrl(image)) {
            image = Constants.POST_IMAGE_URL + imagesArrayList[position].imageName
        }

        if (type.contains("video/mp4")) {
            holder.itemView.img_play.visibility = View.VISIBLE
            image = imagesArrayList[position].thumbImage
            if (!CommonMethod.isValidUrl(image)) {
                image = Constants.POST_IMAGE_URL + imagesArrayList[position].thumbImage
            }
        } else {
            holder.itemView.img_play.visibility = View.GONE
        }

        CommonMethod.loadImageGlide(image, holder.itemView.img_post, R.drawable.placeholder_fitxy)

        holder.itemView.img_post.setOnClickListener { view1: View? ->
            val i = Intent(context, ShowFullImage::class.java)
            i.putExtra("ImageUrl", image)
            context.startActivity(i)
        }

        holder.itemView.img_play.setOnClickListener { v: View? ->
            callActivity(context, Intent(context, VideoPlayActivity::class.java))
            BMSPrefs.putString(context, "video_path", imagesArrayList.get(position).imageName)
        }

    }


    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is MarginLayoutParams) {
            val p = view.layoutParams as MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

}
