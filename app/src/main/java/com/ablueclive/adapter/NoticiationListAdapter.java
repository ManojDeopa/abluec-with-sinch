package com.ablueclive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.listener.onNotificationItemListener;
import com.ablueclive.modelClass.NotificationData;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;

import java.util.ArrayList;

public class NoticiationListAdapter extends RecyclerView.Adapter<NoticiationListAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<NotificationData> notificationDataArrayList;
    private final onNotificationItemListener onNotificationItemListener;

    public NoticiationListAdapter(Context context, ArrayList<NotificationData> notificationDataArrayList, onNotificationItemListener onNotificationItemListener) {
        this.context = context;
        this.notificationDataArrayList = notificationDataArrayList;
        this.onNotificationItemListener = onNotificationItemListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_list_adapter, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.text_name.setText(notificationDataArrayList.get(i).getName());
        viewHolder.text_notification.setText(notificationDataArrayList.get(i).getPost_comment());

        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + notificationDataArrayList.get(i).getProfile_image(), viewHolder.img_profile);


        viewHolder.linear_chat.setOnClickListener(v ->
                onNotificationItemListener.onNotificationClick(notificationDataArrayList.get(i).getNType(), notificationDataArrayList.get(i).getPost_id(),
                notificationDataArrayList.get(i).getMobile(), notificationDataArrayList.get(i).getName(),
                notificationDataArrayList.get(i).getTo_user_id(), notificationDataArrayList.get(i).getFrom_user_id(),notificationDataArrayList.get(i).getProfile_image()));
    }

    @Override
    public int getItemCount() {
        return notificationDataArrayList.size();
    }

    public void addAll(ArrayList<NotificationData> getAllCheersLists) {
        for (NotificationData getAllFriendsLists1 : getAllCheersLists) {
            add(getAllFriendsLists1);
        }
    }

    public void add(NotificationData user) {
        notificationDataArrayList.add(user);
        notifyItemInserted(notificationDataArrayList.size() - 1);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text_name;
        private final TextView text_notification;
        private final AppCompatImageView img_profile;
        private final LinearLayout linear_chat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_name = itemView.findViewById(R.id.text_name);
            img_profile = itemView.findViewById(R.id.img_profile);
            text_notification = itemView.findViewById(R.id.text_notification);
            linear_chat = itemView.findViewById(R.id.linear_chat);
        }
    }
}
