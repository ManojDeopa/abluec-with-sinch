package com.ablueclive.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.listener.onDelSubCmntListener;
import com.ablueclive.listener.onDeleteCmntListener;
import com.ablueclive.listener.onReplyCmntListener;
import com.ablueclive.listener.onReportPostListener;
import com.ablueclive.listener.onReportSubCmntListener;
import com.ablueclive.modelClass.AllComments;
import com.ablueclive.modelClass.Reply_comments;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<AllComments> allCommentsArrayList;
    private final onDeleteCmntListener onDeleteCmntListener;
    private final onReplyCmntListener onReplyCmntListener;
    private final String user_id;
    private final onReportPostListener onReportPostListener;
    private final onDelSubCmntListener onDeleteSubCmntListener;
    private final onReportSubCmntListener onReportSubCmntListener;

    public CommentAdapter(Context context, ArrayList<AllComments> allCommentsArrayList, onDeleteCmntListener onDeleteCmntListener,
                          String user_id, onReplyCmntListener onReplyCmntListener, onReportPostListener onReportPostListener,
                          onDelSubCmntListener onDeleteSubCmntListener, onReportSubCmntListener onReportSubCmntListener) {
        this.context = context;
        this.allCommentsArrayList = allCommentsArrayList;
        this.onDeleteCmntListener = onDeleteCmntListener;
        this.user_id = user_id;
        this.onReplyCmntListener = onReplyCmntListener;
        this.onReportPostListener = onReportPostListener;
        this.onDeleteSubCmntListener = onDeleteSubCmntListener;
        this.onReportSubCmntListener = onReportSubCmntListener;
    }

    @NonNull

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        AllComments allComments = allCommentsArrayList.get(pos);
        viewHolder.text_cmnt_username.setText(allComments.getUser_name());
        viewHolder.text_comment.setText(allComments.getUser_comment());
        Glide.with(context)
                .load(Constants.PROFILE_IMAGE_URL + allComments.getProfile_image())
                .skipMemoryCache(true)
                .error(R.drawable.user_profile)
                .into(viewHolder.img_comment_profile);

        if (allComments.getUser_id().equalsIgnoreCase(user_id) && allComments.getPost_user_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_reply.setVisibility(View.VISIBLE);
            viewHolder.text_report.setVisibility(View.GONE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        } else if (!allComments.getUser_id().equalsIgnoreCase(user_id) && allComments.getPost_user_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_reply.setVisibility(View.VISIBLE);
            viewHolder.text_report.setVisibility(View.VISIBLE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        } else if (allComments.getUser_id().equalsIgnoreCase(user_id)) {
            //  viewHolder.text_reply.setVisibility(View.GONE);
            viewHolder.text_report.setVisibility(View.GONE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        } else {
            viewHolder.text_reply.setVisibility(View.VISIBLE);
            viewHolder.text_report.setVisibility(View.VISIBLE);
            viewHolder.text_deleteCmnt.setVisibility(View.GONE);
        }


        viewHolder.text_cmnt_username.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, allComments.getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });

        viewHolder.img_comment_profile.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, allComments.getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });

        viewHolder.text_deleteCmnt.setOnClickListener(v -> {
            onDeleteCmntListener.onDelCmntClick(allComments.getPostId(), allComments.getComment_id());
            allCommentsArrayList.remove(pos);
            notifyDataSetChanged();
        });

        viewHolder.text_reply.setOnClickListener(v -> onReplyCmntListener.onReplyCmntClick(allComments.getUser_comment(), allComments.getUser_name(), allComments.getProfile_image(),
                allComments.getComment_id(),allComments.getUser_id()));


        viewHolder.text_report.setOnClickListener(v -> onReportPostListener.onReportPostClick(allComments.getComment_id(), pos));

        ArrayList<Reply_comments> reply_commentsArrayList = allCommentsArrayList.get(pos).getReply_comments();
        SubCommentAdapter subCommentAdapter = new SubCommentAdapter(context, reply_commentsArrayList, onDeleteSubCmntListener, onReportSubCmntListener, user_id);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        viewHolder.recycler_sub_comment.setLayoutManager(linearLayoutManager);
        viewHolder.recycler_sub_comment.setAdapter(subCommentAdapter);


    }

    public void addAll(ArrayList<AllComments> allComments) {
        for (AllComments allComments1 : allComments) {
            add(allComments1);
        }
    }

    public void add(AllComments allComments) {
        allCommentsArrayList.add(allComments);
        notifyItemInserted(allCommentsArrayList.size() - 1);
    }

    @Override
    public int getItemCount() {
        return allCommentsArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final RecyclerView recycler_sub_comment;
        private final TextView text_cmnt_username;
        private final TextView text_comment;
        private final TextView text_deleteCmnt;
        private final TextView text_report;
        private final TextView text_reply;
        private final CircleImageView img_comment_profile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recycler_sub_comment = itemView.findViewById(R.id.recycler_sub_comment);
            text_cmnt_username = itemView.findViewById(R.id.text_cmnt_username);
            text_comment = itemView.findViewById(R.id.text_comment);
            img_comment_profile = itemView.findViewById(R.id.img_comment_profile);
            text_deleteCmnt = itemView.findViewById(R.id.text_deleteCmnt);
            text_report = itemView.findViewById(R.id.text_report);
            text_reply = itemView.findViewById(R.id.text_reply);
        }
    }
}
