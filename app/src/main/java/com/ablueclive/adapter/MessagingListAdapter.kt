package com.ablueclive.adapter

import android.content.Context
import android.content.Intent
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.modelClass.GetAllFriendsList
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.getTimesAgoFromTimeStamp
import com.ablueclive.utils.CommonMethod.loadImageCircle
import com.ablueclive.utils.Constants
import java.util.*

class MessagingListAdapter(private var context: Context, private var getFriendDataArrayList: ArrayList<GetAllFriendsList>) : RecyclerView.Adapter<MessagingListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.messaging_list_adapter, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val getFriendData = getFriendDataArrayList[i]
        viewHolder.text_name.text = getFriendData.name
        if (getFriendData.last_message != null) {
            viewHolder.text_mutual_frnd.text = getFriendData.last_message
        } else {
            viewHolder.text_mutual_frnd.visibility = View.GONE
        }
        val timeStamp = getFriendDataArrayList[i].timestamp
        if (!timeStamp.isEmpty()) {
            viewHolder.text_time.text = getTimesAgoFromTimeStamp(timeStamp)
        }
        loadImageCircle(Constants.PROFILE_IMAGE_URL + getFriendData.profile_image, viewHolder.img_profile)


        //read
        if (getFriendData.unread_msg_count == 0) {
            viewHolder.img_read.visibility = View.GONE
        } else {
            viewHolder.img_read.visibility = View.VISIBLE
            viewHolder.img_read.text = getFriendData.unread_msg_count.toString() + ""
        }
        viewHolder.text_name.setOnClickListener { v: View? ->
            BMSPrefs.putString(context, Constants.FRIEND_ID, getFriendData._id)
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }


        viewHolder.linear_chat.setOnClickListener {
            BMSPrefs.putString(context, Constants.TO_ID, getFriendData._id)
            BMSPrefs.putString(context, Constants.FRIEND_MOBILE, getFriendData.mobile)
            BMSPrefs.putString(context, Constants.CHAT_USER_NAME, getFriendData.name)
            BMSPrefs.putString(context, Constants.CHAT_USER_IMAGE, getFriendData.profile_image)
            callActivity(context, Intent(context, StartChatActivity::class.java))
        }


    }

    private fun getDate(time: Long): String {
        val cal = Calendar.getInstance(Locale.getDefault())
        cal.timeInMillis = time * 1000
        return DateFormat.format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", cal).toString()
    }

    fun addAll(getAllFriendsLists: ArrayList<GetAllFriendsList>) {
        for (getAllFriendsLists1 in getAllFriendsLists) {
            add(getAllFriendsLists1)
        }
    }

    fun clearAll() {
        getFriendDataArrayList.clear()
        notifyDataSetChanged()
    }

    fun add(user: GetAllFriendsList) {
        getFriendDataArrayList.add(user)
        notifyItemInserted(getFriendDataArrayList.size - 1)
    }

    override fun getItemCount(): Int {
        return getFriendDataArrayList.size
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text_name: TextView
        val text_mutual_frnd: TextView
        val text_time: TextView
        val img_profile: AppCompatImageView
        val linear_chat: LinearLayout
        val img_read: TextView

        init {
            text_name = itemView.findViewById(R.id.text_name)
            img_profile = itemView.findViewById(R.id.img_profile)
            text_mutual_frnd = itemView.findViewById(R.id.text_mutual_frnd)
            text_time = itemView.findViewById(R.id.text_time)
            linear_chat = itemView.findViewById(R.id.linear_chat)
            img_read = itemView.findViewById(R.id.img_read)
        }
    }
}