package com.ablueclive.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.modelClass.CheerList;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;

import java.util.ArrayList;

public class CheersListAdapter extends RecyclerView.Adapter<CheersListAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<CheerList> cheerListArrayList;

    public CheersListAdapter(Context context, ArrayList<CheerList> cheerListArrayList) {
        this.context = context;
        this.cheerListArrayList = cheerListArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cheers_list_adapter, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.text_name.setText(cheerListArrayList.get(i).getName());
        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + cheerListArrayList.get(i).getProfile_image(), viewHolder.img_profile);
        viewHolder.itemView.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, cheerListArrayList.get(i).get_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });

    }

    @Override
    public int getItemCount() {
        return cheerListArrayList.size();
    }

    public void addAll(ArrayList<CheerList> getAllCheersLists) {
        for (CheerList getAllFriendsLists1 : getAllCheersLists) {
            add(getAllFriendsLists1);
        }
    }

    public void add(CheerList user) {
        cheerListArrayList.add(user);
        notifyItemInserted(cheerListArrayList.size() - 1);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text_name;
        private final CircleImageView img_profile;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_name = itemView.findViewById(R.id.text_name);
            img_profile = itemView.findViewById(R.id.img_profile);
        }
    }
}
