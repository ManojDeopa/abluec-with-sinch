package com.ablueclive.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.modelClass.ShareUsersPost;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SharedUserAdapter extends RecyclerView.Adapter<SharedUserAdapter.ViewHolder> {

    private Context context;
    private final ArrayList<ShareUsersPost> list;

    public SharedUserAdapter(ArrayList<ShareUsersPost> shareUserList) {
        list = shareUserList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.shared_user_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {

        viewHolder.tvName.setText(list.get(pos).name);
        viewHolder.tvPostedDate.setText(list.get(pos).posted_date);

        String postContent = list.get(pos).post_content;
        if (!TextUtils.isEmpty(postContent)) {
            viewHolder.tvPostContent.setVisibility(View.VISIBLE);
            viewHolder.tvPostContent.setText(postContent);
        } else {
            viewHolder.tvPostContent.setVisibility(View.GONE);
        }

        String image = list.get(pos).profile_image;
        if (image.isEmpty()) {
            viewHolder.ivImage.setImageResource(R.drawable.user_profile);
        } else {
            Glide.with(context)
                    .load(Constants.PROFILE_IMAGE_URL + image)
                    .skipMemoryCache(true)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.ivImage);
        }

        viewHolder.tvName.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list.get(pos).user_id);
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });

        viewHolder.ivImage.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, list.get(pos).user_id);
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName;
        private final TextView tvPostedDate;
        private final TextView tvPostContent;
        private final CircleImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvPostedDate = itemView.findViewById(R.id.tvPostedDate);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvPostContent = itemView.findViewById(R.id.tvPostContent);
        }
    }
}
