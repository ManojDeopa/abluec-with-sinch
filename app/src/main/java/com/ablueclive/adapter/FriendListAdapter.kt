package com.ablueclive.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.modelClass.GetFriendListData
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.loadImageCircle
import com.ablueclive.utils.Constants
import com.ablueclive.utils.CustomRecyclerViewItemClick
import java.util.*

class FriendListAdapter(private var context: Context, private var getFriendListDataArrayList: ArrayList<GetFriendListData>, private var listener: CustomRecyclerViewItemClick, private var isFriends: Boolean, var tvHeaderText: AppCompatTextView) : RecyclerView.Adapter<FriendListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.friendlist_adapter, viewGroup, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val getFriendListData = getFriendListDataArrayList[position]
        // if (isFriends) viewHolder.text_confirm.text = context.resources.getString(R.string.connect) else viewHolder.text_confirm.text = context.resources.getString(R.string.follow)
        viewHolder.text_name.text = getFriendListData.name
        loadImageCircle(Constants.PROFILE_IMAGE_URL + getFriendListData.profile_image, viewHolder.img_friend)
        viewHolder.text_confirm.setOnClickListener { v: View? ->
            listener.onRecyclerItemClick(isFriends, "confirm:" + getFriendListData._id, position)
            getFriendListDataArrayList.removeAt(position)
            notifyItemRemoved(position)
            if (getFriendListDataArrayList.isEmpty()) {
                tvHeaderText.visibility = View.GONE
            } else {
                tvHeaderText.text = getFriendListDataArrayList.size.toString() + getHeaderText()
            }
        }

        viewHolder.text_delete.setOnClickListener { v: View? ->
            listener.onRecyclerItemClick(isFriends, "delete:" + getFriendListData._id, position)
            getFriendListDataArrayList.removeAt(position)
            if (getFriendListDataArrayList.isEmpty()) {
                tvHeaderText.visibility = View.GONE
            } else {
                tvHeaderText.text = getFriendListDataArrayList.size.toString() + getHeaderText()
            }
            notifyItemRemoved(position)
        }
        viewHolder.itemView.setOnClickListener { v: View? ->
            BMSPrefs.putString(context, Constants.FRIEND_ID, getFriendListData._id)
            callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }
    }

    private fun getHeaderText(): String {
        var text = " " + context.getString(R.string.followed_you_to_connect)
        if (tvHeaderText.id == R.id.tvInvitedText) {
            text = " " + context.getString(R.string.invited_you_to_connect)
        }
        return text
    }

    fun addAll(getFriendListData: ArrayList<GetFriendListData>) {
        for (getFriendListData1 in getFriendListData) {
            add(getFriendListData1)
        }
    }

    fun add(getFriendListData: GetFriendListData) {
        getFriendListDataArrayList.add(getFriendListData)
        notifyItemInserted(getFriendListDataArrayList.size - 1)
    }

    override fun getItemCount(): Int {
        return getFriendListDataArrayList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text_name: TextView = itemView.findViewById(R.id.text_name)
        val text_confirm: TextView = itemView.findViewById(R.id.text_confirm)
        val img_friend: AppCompatImageView = itemView.findViewById(R.id.img_friend)
        var text_delete: ImageView = itemView.findViewById(R.id.text_delete)

    }
}