package com.ablueclive.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.frndProfile.FriendProfileActivity.Companion.display_name
import com.ablueclive.activity.mainActivity.MainActivity.Companion.getAdRequest
import com.ablueclive.common.CommonContainerActivity.Companion.feedDetailTitle
import com.ablueclive.fragment.feedFragment.ClickPosition
import com.ablueclive.fragment.feedFragment.TotalWaveFragment
import com.ablueclive.listener.*
import com.ablueclive.modelClass.HashTag
import com.ablueclive.modelClass.Like_users
import com.ablueclive.modelClass.PostData
import com.ablueclive.modelClass.ShareUsersPost
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.callCommonContainer
import com.ablueclive.utils.CommonMethod.extractUrl
import com.ablueclive.utils.CommonMethod.isValidUrl
import com.ablueclive.utils.CommonMethod.loadImageCircle
import com.ablueclive.utils.CommonMethod.popupMenuTextList
import com.ablueclive.utils.CommonMethod.showToastlong
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdView
import io.github.ponnamkarthik.richlinkpreview.ViewListener
import kotlinx.android.synthetic.main.user_feed_adapter.view.*
import java.util.*
import java.util.regex.Pattern

class FeedDetailAdapter(private val context: Context, var postDataArrayList: ArrayList<PostData>, private val onReportPostListener: onReportPostListener,
                        private val user_id: String, private val onDeletePostListener: onDeletePostListener, private val onLikePostListener: onLikePostListener,
                        private val onCommentPostListener: onCommentPostListener,
                        private val onMenuItemListener: onProductItemListener,
                        private val onSharePostListener: onSharePostListener,
                        private val onEditPostListener: onEditPostListener,
                        private val shouldHide: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), onFriendProfileListener, ClickPosition {


    private lateinit var selectedViewHolder: View

    companion object {
        private const val LAYOUT_ADS = 0
        private const val LAYOUT_FEEDS = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        val viewHolder: RecyclerView.ViewHolder
        if (viewType == LAYOUT_ADS) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.native_add, parent, false)
            viewHolder = AdViewHolder(view)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.user_feed_adapter, parent, false)
            viewHolder = ViewHolder(view)
        }
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, i: Int) {
        if (holder.itemViewType == LAYOUT_ADS) {
            val adHolder = holder as AdViewHolder
            adHolder.mAdView.loadAd(getAdRequest())
        } else {
            val postHolder = holder as ViewHolder
            val viewHolder = postHolder.itemView
            val postData = postDataArrayList[i]
            val postUserData = postData.rePostUser
            if (postUserData != null && postUserData.isNotEmpty()) {
                viewHolder.postContentItemLayout.setBackgroundResource(R.drawable.back_rectangular)
                viewHolder.tvRePostContent.visibility = View.VISIBLE
                viewHolder.tvRePostContent.text = postData.repost_content
                viewHolder.postUserInfoLayout.visibility = View.VISIBLE
                val ivPostUserImage: AppCompatImageView = viewHolder.postUserInfoLayout.findViewById(R.id.ivPostUserImage)
                val tvPostUserName = viewHolder.postUserInfoLayout.findViewById<TextView>(R.id.tvPostUserName)
                val data = postUserData[0]
                tvPostUserName.text = data.name
                loadImageCircle(Constants.PROFILE_IMAGE_URL + data.profile_image, ivPostUserImage)
            }
            val waveQuoteList = ArrayList<ShareUsersPost>()
            val waveList = ArrayList<ShareUsersPost>()
            val shareUserList = postData.shareUserPost
            if (shareUserList != null && shareUserList.size > 0) {
                val nameList = ArrayList<String>()
                for (pos in shareUserList.indices) {
                    nameList.add(shareUserList[pos].name)
                    if (TextUtils.isEmpty(shareUserList[pos].post_content)) {
                        waveList.add(shareUserList[pos])
                    } else {
                        waveQuoteList.add(shareUserList[pos])
                    }
                }
                val set: Set<String> = LinkedHashSet(nameList)
                nameList.clear()
                nameList.addAll(set)
                val shareUserNames = StringBuilder(nameList[0])
                for (pos in 1 until nameList.size) {
                    if (nameList.size - 1 == pos) {
                        shareUserNames.append(" and ").append(nameList[pos])
                    } else {
                        shareUserNames.append(",").append(nameList[pos])
                    }
                }
                if (shouldHide) {
                    if (feedDetailTitle.isEmpty()) {
                        viewHolder.postShareTitle.visibility = View.VISIBLE
                        viewHolder.postShareTitle.text = "$shareUserNames waved a post."
                        viewHolder.rvSharedUser.visibility = View.VISIBLE
                        viewHolder.rvSharedUser.layoutManager = LinearLayoutManager(context)
                        viewHolder.rvSharedUser.adapter = SharedUserAdapter(waveQuoteList)
                    }
                }
            } else {
                viewHolder.postShareTitle.visibility = View.GONE
                viewHolder.rvSharedUser.visibility = View.GONE
            }
            if (postData.isRetweeted) {
                viewHolder.ivWave.setImageResource(R.drawable.wave_blue)
            } else {
                viewHolder.ivWave.setImageResource(R.drawable.wave_black)
            }
            var waveTxt = "<font color='#787575'>" + " Waves" + "</font>"
            if (waveList.size == 1) {
                waveTxt = "<font color='#787575'>" + " Wave" + "</font>"
            }
            viewHolder.tvWaveCount.text = HtmlCompat.fromHtml(waveList.size.toString() + waveTxt, HtmlCompat.FROM_HTML_MODE_LEGACY)
            var quoteWaveTxt = "<font color='#787575'>" + " Quote Waves" + "</font>"
            if (waveQuoteList.size == 1) {
                quoteWaveTxt = "<font color='#787575'>" + " Quote Wave" + "</font>"
            }
            viewHolder.tvQuoteWaveCount.text = HtmlCompat.fromHtml(waveQuoteList.size.toString() + quoteWaveTxt, HtmlCompat.FROM_HTML_MODE_LEGACY)
            viewHolder.tvQuoteWaveCount.setOnClickListener { v: View? ->
                if (waveQuoteList.isEmpty()) {
                    showToastlong("No Quote Waves", context)
                    return@setOnClickListener
                }
                feedDetailTitle = ""
                BMSPrefs.putString(context, Constants.FEED_POST_ID, postData._id)
                callCommonContainer((context as Activity), context.getString(R.string.quote_wave))
            }
            viewHolder.tvWaveCount.setOnClickListener { v: View? ->
                if (waveList.isEmpty()) {
                    showToastlong("No Waves", context)
                    return@setOnClickListener
                }
                TotalWaveFragment.waveList = waveList
                callCommonContainer((context as Activity), context.getString(R.string.wave))
            }
            var commentTxt = "<font color='#787575'>" + " Comments" + "</font>"
            if (postData.totalComments == "1") {
                commentTxt = "<font color='#787575'>" + " Comment" + "</font>"
            }
            viewHolder.tvCommentCount.text = HtmlCompat.fromHtml(postData.totalComments + commentTxt, HtmlCompat.FROM_HTML_MODE_LEGACY)
            viewHolder.ivComment.setOnClickListener { v: View? -> onCommentPostListener.onCmntPostClick(postData._id, i) }
            viewHolder.tvCommentCount.setOnClickListener { v: View? -> onCommentPostListener.onCmntPostClick(postData._id, i) }
            loadImageCircle(Constants.PROFILE_IMAGE_URL + postData.post_user_profile_image, viewHolder.img_user_profile)
            var displayName = ""
            if (postData.post_user_display_name.isNotEmpty()) {
                displayName = " @" + postData.post_user_display_name.replace("@", "")
            }
            displayName = "<font color='#1DA1F2'>$displayName</font>"
            viewHolder.text_user_name.text = HtmlCompat.fromHtml(postData.post_user_name + displayName, HtmlCompat.FROM_HTML_MODE_LEGACY)
            val timeAgo = postData.post_time_status
            viewHolder.text_time.text = timeAgo
            if (postDataArrayList[i].hasTagNameUnsedForNow != null) {
                var hashTagsArrayList = ArrayList<HashTag>()
                hashTagsArrayList.clear()
                hashTagsArrayList = postDataArrayList[i].hasTagNameUnsedForNow
                if (hashTagsArrayList.isNotEmpty()) {
                    if (hashTagsArrayList.size == 1) {
                        viewHolder.text_user_name.text = viewHolder.text_user_name.text.toString() + " is with " + hashTagsArrayList[0].name
                    } else {
                        val count = (hashTagsArrayList.size - 1).toString()
                        viewHolder.text_user_name.text = viewHolder.text_user_name.text.toString() + " is with " + hashTagsArrayList[0].name + " and " + count + " Others"
                    }
                }
            }
            viewHolder.text_user_name.setOnClickListener { v: View? ->
                BMSPrefs.putString(context, Constants.FRIEND_ID, postData.user_id)
                callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
            viewHolder.img_user_profile.setOnClickListener { v: View? ->
                BMSPrefs.putString(context, Constants.FRIEND_ID, postData.user_id)
                callActivity(context, Intent(context, FriendProfileActivity::class.java))
            }
            var postContent = postData.post_content
            if (!TextUtils.isEmpty(postContent)) {
                postContent = postContent.trim { it <= ' ' }
                var url = extractUrl(postContent.replace("[\\t\\n\\r]+".toRegex(), " "))
                if (!TextUtils.isEmpty(url)) {
                    if (!isValidUrl(url)) {
                        url = "http://$url"
                    }
                    viewHolder.richLinkView.visibility = View.VISIBLE
                    viewHolder.richLinkView.setLink(url, object : ViewListener {
                        override fun onSuccess(status: Boolean) {}
                        override fun onError(e: Exception) {}
                    })
                    val text = postContent.replace(url, "")
                    if (!TextUtils.isEmpty(text)) {
                        viewHolder.text_feed.visibility = View.VISIBLE
                        viewHolder.text_feed.text = text.trim { it <= ' ' }
                    }
                } else {
                    viewHolder.text_feed.visibility = View.VISIBLE
                    viewHolder.richLinkView.visibility = View.GONE
                    var areTaggedUser = false
                    val spannable = SpannableString(postContent)
                    val matcher = Pattern.compile("@\\s*(\\w+)").matcher(postContent)
                    while (matcher.find()) {
                        val displayText = matcher.group(1)!!
                        val clickableSpan: ClickableSpan = object : ClickableSpan() {
                            override fun onClick(textView: View) {
                                BMSPrefs.putString(context, Constants.FRIEND_ID, "123")
                                display_name = displayText
                                callActivity(context, Intent(context, FriendProfileActivity::class.java))
                            }

                            override fun updateDrawState(ds: TextPaint) {
                                super.updateDrawState(ds)
                                ds.isUnderlineText = false
                                ds.color = Color.parseColor("#1DA1F2")
                            }
                        }
                        val cityIndex = postContent.indexOf(displayText) - 1
                        spannable.setSpan(clickableSpan, cityIndex, cityIndex + displayText.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                        areTaggedUser = true
                    }
                    if (areTaggedUser) {
                        viewHolder.text_feed.text = spannable
                        viewHolder.text_feed.movementMethod = LinkMovementMethod.getInstance()
                    } else {
                        viewHolder.text_feed.text = HtmlCompat.fromHtml(postContent, HtmlCompat.FROM_HTML_MODE_LEGACY)
                    }
                }
            }
            if (!shouldHide) {
                val commentsArrayList = postData.comments
                if (commentsArrayList.size > 0) {
                    viewHolder.linear_show_comment.visibility = View.VISIBLE
                    val usercomment = commentsArrayList[0].user_comment
                    Log.d("TAG", "onBindViewHolder: $usercomment")
                    viewHolder.text_comment.text = usercomment
                    viewHolder.text_comment_username.text = commentsArrayList[0].user_name
                    Glide.with(context)
                            .load(Constants.PROFILE_IMAGE_URL + commentsArrayList[0].profile_image)
                            .skipMemoryCache(true)
                            .error(R.drawable.user_profile)
                            .into(viewHolder.img_comment_profile)

                    /*  viewHolder.text_comment_username.setOnClickListener(v -> {
                    BMSPrefs.putString(context, Constants.FRIEND_ID, commentsArrayList.get(0).getUser_id());
                    CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
                });

                viewHolder.img_comment_profile.setOnClickListener(v -> {
                    BMSPrefs.putString(context, Constants.FRIEND_ID, commentsArrayList.get(0).getUser_id());
                    CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
                });*/viewHolder.linear_show_comment.setOnClickListener { v: View? -> onCommentPostListener.onCmntPostClick(postData._id, i) }
                } else {
                    viewHolder.linear_show_comment.visibility = View.GONE
                }
            }
            val imagesArrayList = postData.images
            if (imagesArrayList.size > 0) {
                viewHolder.rvMedia.visibility = View.VISIBLE
                var layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 2)
                if (imagesArrayList.size == 1) {
                    layoutManager = LinearLayoutManager(context)
                }
                if (imagesArrayList.size == 3) {
                    layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
                }
                viewHolder.rvMedia.layoutManager = layoutManager
                viewHolder.rvMedia.adapter = FeedMediaAdapter(imagesArrayList)
            } else {
                viewHolder.rvMedia.visibility = View.GONE
            }
            viewHolder.img_option.setOnClickListener { v: View? ->
                if (postData.user_id.equals(user_id, ignoreCase = true)) {
                    val itemList = arrayOf(context.getString(R.string.edit), context.getString(R.string.del_post))
                    popupMenuTextList(context, viewHolder.img_option, itemList, object : MenuItemClickListener {
                        override fun onMenuItemClick(text: String, position: Int) {
                            if (position == 0) {
                                onEditPostListener.onEditPostClick(postData._id, i)
                            }
                            if (position == 1) {
                                onDeletePostListener.onDelPostClick(postData._id, i)
                                postDataArrayList.removeAt(i)
                                notifyItemRemoved(i)
                            }
                        }
                    })
                } else {
                    val itemList = arrayOf(context.getString(R.string.report_post))
                    popupMenuTextList(context, viewHolder.img_option, itemList, object : MenuItemClickListener {
                        override fun onMenuItemClick(text: String, position: Int) {
                            onReportPostListener.onReportPostClick(postData._id, i)
                        }
                    })
                }
            }
            selectedViewHolder = viewHolder
            setLikeCount(postData.like_users, postData.post_user_like_status, postData.totalCountLike)
            viewHolder.img_cheers.setOnClickListener { v: View? ->
                selectedViewHolder = viewHolder
                Constants.showLoader = false
                onLikePostListener.onLikePostClick(postData._id, i)
                if (selectedViewHolder.img_cheers.tag == "not_like") {
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                    selectedViewHolder.img_cheers.tag = "like"
                } else {
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                    selectedViewHolder.img_cheers.tag = "not_like"
                }
            }
            viewHolder.tvLikeCount.setOnClickListener { v: View? ->
                if (postData.totalCountLike == null || postData.totalCountLike == "0") {
                    showToastlong("No Likes", context)
                    return@setOnClickListener
                }
                onMenuItemListener.onProductItemClick(postData._id)
            }
            viewHolder.shareLayout.setOnClickListener { v: View? ->
                // onSharePostListener.onSharePostClick(postData.get_id(), i, postData.getShareUrl());
                try {
                    val sendIntent = Intent()
                    sendIntent.action = Intent.ACTION_SEND
                    sendIntent.putExtra(Intent.EXTRA_TEXT, postData.shareUrl)
                    sendIntent.type = "text/plain"
                    context.startActivity(Intent.createChooser(sendIntent, "Share Via : "))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            viewHolder.ivWave.setOnClickListener { v: View? -> showWaveDialog(postData, i) }
        }
    }

    private fun showWaveDialog(postData: PostData, i: Int) {
        val act = context as Activity
        val viewGroup = act.findViewById<ViewGroup>(android.R.id.content)
        val dialogView = LayoutInflater.from(act).inflate(R.layout.wave_dialog_layout, viewGroup, false)
        val builder = AlertDialog.Builder(act)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.show()
        val tvWave = dialogView.findViewById<TextView>(R.id.tvTotalWaveCount)
        val tvQuoteWave = dialogView.findViewById<TextView>(R.id.tvQuoteWave)
        val tvCancel = dialogView.findViewById<TextView>(R.id.tvCancel)
        if (postData.isRetweeted) {
            tvWave.text = context.getText(R.string.un_do_wave)
        }
        tvWave.setOnClickListener { v: View? ->
            alertDialog.dismiss()
            onSharePostListener.onSharePostClick(postData._id, i, tvWave.text.toString())
        }
        tvQuoteWave.setOnClickListener { v: View? ->
            alertDialog.dismiss()
            feedDetailTitle = postData.post_user_name
            BMSPrefs.putString(context, Constants.FEED_POST_ID, postData._id)
            callCommonContainer(context, context.getString(R.string.quote_wave))
        }
        tvCancel.setOnClickListener { v: View? -> alertDialog.dismiss() }
    }

    @SuppressLint("SetTextI18n")
    fun setLikeCount(like_usersArrayList: ArrayList<Like_users>?, post_user_like_status: String?, totalCountLike: String) {
        val myUserId = BMSPrefs.getString(context, Constants._ID)
        var isLiked = false
        if (like_usersArrayList != null && like_usersArrayList.size > 0) {
            for (i in like_usersArrayList.indices) {
                if (myUserId == like_usersArrayList[i].user_id) {
                    isLiked = true
                }
            }
            if (isLiked) {
                selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                selectedViewHolder.img_cheers.tag = "like"
                selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            } else {
                selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                selectedViewHolder.img_cheers.tag = "not_like"
                selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.dot_dark_screen1))
            }
        } else {
            if (post_user_like_status != null) {
                if (post_user_like_status.equals("Like", ignoreCase = true)) {
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_blue)
                    selectedViewHolder.img_cheers.tag = "like"
                    selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                } else {
                    selectedViewHolder.img_cheers.setImageResource(R.drawable.like_gray)
                    selectedViewHolder.img_cheers.tag = "not_like"
                    selectedViewHolder.tv_like.setTextColor(ContextCompat.getColor(context, R.color.dot_dark_screen1))
                }
            }
        }
        var likeTxt = "<font color='#787575'>" + " Likes" + "</font>"
        if (totalCountLike == "1") {
            likeTxt = "<font color='#787575'>" + " Like" + "</font>"
        }
        selectedViewHolder.tvLikeCount.text = HtmlCompat.fromHtml(totalCountLike + likeTxt, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }

    override fun onFrndProfileClick(friend_id: String, position: Int) {
        BMSPrefs.putString(context, Constants.FRIEND_ID, friend_id)
        callActivity(context, Intent(context, FriendProfileActivity::class.java))
    }

    override fun onAddFriendClick(friend_id: String, position: Int) {}
    override fun getPosition(position: Int) {}

    override fun getItemViewType(position: Int): Int {
        return if (position != 0 && position % 4 == 0) {
            LAYOUT_ADS
        } else {
            LAYOUT_FEEDS
        }
    }

    override fun getItemCount(): Int {
        return postDataArrayList.size
    }

    class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mAdView: AdView = itemView.findViewById(R.id.adView)

    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            if (shouldHide) {
                itemView.img_option.visibility = View.GONE
                itemView.bottomActionLayout.visibility = View.GONE
                itemView.countDetailLayout.visibility = View.GONE
            } else {
                itemView.img_option.visibility = View.VISIBLE
                itemView.bottomActionLayout.visibility = View.VISIBLE
                itemView.countDetailLayout.visibility = View.VISIBLE
            }

        }
    }

}