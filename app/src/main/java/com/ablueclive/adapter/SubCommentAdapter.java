package com.ablueclive.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.listener.onDelSubCmntListener;
import com.ablueclive.listener.onReportSubCmntListener;
import com.ablueclive.modelClass.Reply_comments;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SubCommentAdapter extends RecyclerView.Adapter<SubCommentAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<Reply_comments> reply_commentsArrayList;
    private final onDelSubCmntListener onDeleteSubCmntListener;
    private final onReportSubCmntListener onReportSubCmntListener;
    private final String user_id;


    public SubCommentAdapter(Context context, ArrayList<Reply_comments> reply_commentsArrayList, onDelSubCmntListener onDeleteSubCmntListener,
                             onReportSubCmntListener onReportSubCmntListener, String user_id) {
        this.context = context;
        this.reply_commentsArrayList = reply_commentsArrayList;
        this.onDeleteSubCmntListener = onDeleteSubCmntListener;
        this.onReportSubCmntListener = onReportSubCmntListener;
        this.user_id = user_id;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subcomment_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        viewHolder.text_cmnt_username.setText(reply_commentsArrayList.get(pos).getUser_name());
        viewHolder.text_comment.setText(reply_commentsArrayList.get(pos).getReply_user_comment());
        Glide.with(context)
                .load(Constants.PROFILE_IMAGE_URL + reply_commentsArrayList.get(pos).getProfile_image())
                .skipMemoryCache(true)
                .error(R.drawable.user_profile)
                .into(viewHolder.img_comment_profile);


        viewHolder.img_comment_profile.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, reply_commentsArrayList.get(pos).getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });


        viewHolder.text_cmnt_username.setOnClickListener(v -> {
            BMSPrefs.putString(context, Constants.FRIEND_ID, reply_commentsArrayList.get(pos).getUser_id());
            CommonMethod.callActivity(context, new Intent(context, FriendProfileActivity.class));
        });

        if (!reply_commentsArrayList.get(pos).getPost_user_id().equalsIgnoreCase(user_id) && reply_commentsArrayList.get(pos).getUser_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_report.setVisibility(View.GONE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        } else if (!reply_commentsArrayList.get(pos).getPost_user_id().equalsIgnoreCase(user_id) && reply_commentsArrayList.get(pos).getPost_user_id().equalsIgnoreCase(user_id) && !reply_commentsArrayList.get(pos).getUser_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_report.setVisibility(View.VISIBLE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        } else if (!reply_commentsArrayList.get(pos).getPost_user_id().equalsIgnoreCase(user_id) && !reply_commentsArrayList.get(pos).getUser_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_report.setVisibility(View.VISIBLE);
            viewHolder.text_deleteCmnt.setVisibility(View.GONE);
        } else if (reply_commentsArrayList.get(pos).getPost_user_id().equalsIgnoreCase(user_id) && reply_commentsArrayList.get(pos).getUser_id().equalsIgnoreCase(user_id)) {
            viewHolder.text_report.setVisibility(View.GONE);
            viewHolder.text_deleteCmnt.setVisibility(View.VISIBLE);
        }


        viewHolder.text_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReportSubCmntListener.onReportSubCmntClick(reply_commentsArrayList.get(pos).getComment_id(), pos, reply_commentsArrayList, reply_commentsArrayList.get(pos).getReply_id());
            }
        });

        viewHolder.text_deleteCmnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteSubCmntListener.onDelSubCmntClick(reply_commentsArrayList.get(pos).getReply_id(), reply_commentsArrayList.get(pos).getComment_id());
                reply_commentsArrayList.remove(pos);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return reply_commentsArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView text_cmnt_username;
        private final TextView text_comment;
        private final TextView text_report;
        private final TextView text_deleteCmnt;
        private final CircleImageView img_comment_profile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_cmnt_username = itemView.findViewById(R.id.text_cmnt_username);
            text_comment = itemView.findViewById(R.id.text_comment);
            img_comment_profile = itemView.findViewById(R.id.img_comment_profile);
            text_report = itemView.findViewById(R.id.text_report);
            text_deleteCmnt = itemView.findViewById(R.id.text_deleteCmnt);
        }
    }
}
