package com.ablueclive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.listener.onMenuItemListener;
import com.ablueclive.modelClass.Files;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

public class UploadPhotoAdapter extends RecyclerView.Adapter<UploadPhotoAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<Files> mResults;
    private final onMenuItemListener onMenuItemListener;

    public UploadPhotoAdapter(Context context, ArrayList<Files> mResults, onMenuItemListener onMenuItemListener) {
        this.context = context;
        this.mResults = mResults;
        this.onMenuItemListener = onMenuItemListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.upload_photo_video_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        try {
            if (mResults.get(position).getType().equalsIgnoreCase("1")) {
                viewHolder.img_play.setVisibility(View.GONE);
                Glide.with(context)
                        .load(new File(mResults.get(position).getMedia_path()))
                        .skipMemoryCache(true)
                        .error(R.drawable.user_profile)
                        .into(viewHolder.img_photo);
            } else if (mResults.get(position).getType().equalsIgnoreCase("2")) {
                viewHolder.img_play.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(new File(mResults.get(position).getThumbpath()))
                        .skipMemoryCache(true)
                        .error(R.drawable.user_profile)
                        .into(viewHolder.img_photo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.img_close.setOnClickListener(v -> onMenuItemListener.onMenuItemClick(position));
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img_photo;
        private final ImageView img_close;
        private final ImageView img_play;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_photo = itemView.findViewById(R.id.img_photo);
            img_close = itemView.findViewById(R.id.img_close);
            img_play = itemView.findViewById(R.id.img_play);
        }
    }
}
