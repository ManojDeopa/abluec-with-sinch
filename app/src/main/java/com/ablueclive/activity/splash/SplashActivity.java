package com.ablueclive.activity.splash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.activity.mainActivity.MainActivity;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.GPSTracker;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    private String _id;
    final static int REQUEST_LOCATION = 199;
    String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String device_id = getDeviceId();
        BMSPrefs.putString(getApplicationContext(), Constants.DEVICE_ID, device_id);
        _id = BMSPrefs.getString(getApplicationContext(), Constants._ID);
        checkGPS();
    }


    private void getStarted() {
        // getCurrentLatLong();
        int SPLASH_TIME_OUT = 3000;
        new Handler(Looper.myLooper()).postDelayed(this::checkSession, SPLASH_TIME_OUT);
    }

    private void checkSession() {

        if (!TextUtils.isEmpty(_id)) {
            CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), MainActivity.class));
        } else {
            CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        }
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        finish();
    }


    private void getCurrentLatLong() {
        try {
            GPSTracker gpsTracker = new GPSTracker(this);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                Constants.CURRENT_COUNTRY_CODE = addresses.get(0).getCountryCode();
                Constants.CURRENT_LOCATION_ADDRESS = addresses.get(0).getAddressLine(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("HardwareIds")
    private String getDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public void checkGPS() {
        try {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                enableLocation();
            } else {
                getStarted();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void enableLocation() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, locationSettingsResponse -> {
            Log.e("enableLocation--", "addOnSuccessListener");
        });

        task.addOnFailureListener(this, e -> {
            if (e instanceof ResolvableApiException) {
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(SplashActivity.this, REQUEST_LOCATION);
                } catch (IntentSender.SendIntentException sendEx) {
                    sendEx.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == Activity.RESULT_CANCELED) {
                checkGPS();
            } else {
                getStarted();
            }
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void showUseLicenceDialog() {

        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.user_licence_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        dialogView.findViewById(R.id.btnAgree).setOnClickListener(v -> {
            BMSPrefs.putString(SplashActivity.this, Constants.EULA, "1");
            alertDialog.dismiss();
            checkSession();
        });

        dialogView.findViewById(R.id.btnDisAgree).setOnClickListener(v -> {
            BMSPrefs.putString(SplashActivity.this, Constants.EULA, "");
            alertDialog.dismiss();
            finishAffinity();
        });

        ProgressBar progress_bar = dialogView.findViewById(R.id.progress_bar);
        WebView webView = dialogView.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.loadUrl(Constants.USER_LICENCE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progress_bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progress_bar.setVisibility(View.GONE);
            }
        });

        alertDialog.show();
    }

}
