package com.ablueclive.activity.mainActivity

import AppUtils
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.ablueclive.R
import com.ablueclive.activity.adddPost.AddPostFragment
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.common.JobContainerActivity.Companion.isFromProfile
import com.ablueclive.common.SearchDisplayResponse
import com.ablueclive.fragment.feedFragment.FeedFragment
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.jobfragment.JobMainFragment
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.network.Network
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.sinch.BaseActivity
import com.ablueclive.sinch.OnCallClickListener
import com.ablueclive.sinch.SinchService
import com.ablueclive.storeSection.fragments.StoreMainFragment
import com.ablueclive.utils.*
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.sinch.android.rtc.PushTokenRegistrationCallback
import com.sinch.android.rtc.SinchError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_header.*
import java.util.*


class MainActivity : BaseActivity(),
        View.OnClickListener,
        SinchService.StartFailedListener,
        PushTokenRegistrationCallback,
        OnCallClickListener {

    private var profile_image: String = ""
    private var session_token: String = ""
    private var status: String = ""
    private var post_id: String = ""
    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    lateinit var ivUserProfile: AppCompatImageView
    lateinit var bottomLayout: BottomNavigationView
    var REQUEST_OVERLAY_PERMISSION = 1001

    val TAG = MainActivity::class.simpleName
    var searchDisplayNameList = arrayListOf<SearchDisplayResponse.SearchUserlist>()


    var unread_msg_count = 0
    private var mPushTokenIsRegistered = false

    var fcmListener: FCMEventListener? = null

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var mainActivity: MainActivity

        @SuppressLint("StaticFieldLeak")
        lateinit var postSubmit: TextView


        fun getAdRequest(): AdRequest {
            return AdRequest.Builder().build()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findView()

        signInAnonymously()
    }


    private fun requestCallOverLayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                val builder = AlertDialog.Builder(this, R.style.alertDialogTheme)
                builder.setTitle(getString(R.string.app_name))
                builder.setCancelable(false)
                builder.setMessage(getString(R.string.layout_over_lay_message))
                builder.setPositiveButton(getString(R.string.allow)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                    val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
                    startActivityForResult(intent, REQUEST_OVERLAY_PERMISSION)
                }
                builder.setNegativeButton(getString(R.string.no)) { dialogInterface, i -> dialogInterface.dismiss() }
                builder.create().show()
            }
        }
    }


    private fun signInAnonymously() {
        val auth = Firebase.auth
        auth.signInAnonymously()
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.e(TAG, "signInAnonymously:success")
                        val user = auth.currentUser
                        if (user != null) {
                            Log.e(TAG, "Auth UID :- " + user.uid)
                            val myRef = FirebaseDatabaseRef.getInstant().firebaseRefForUid
                            myRef.child(Constants.USER_DATABASE_NAME).child(user.uid).setValue(true)
                        }
                    } else {
                        Log.e(TAG, "signInAnonymously:failure", task.exception)
                    }
                }
    }


    private fun findView() {
        mainActivity = this
        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector.isConnectingToInternet
        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        profile_image = BMSPrefs.getString(applicationContext, Constants.PROFILE_IMAGE)
        status = BMSPrefs.getString(applicationContext, "status")
        post_id = BMSPrefs.getString(applicationContext, "to_id")
        ivUserProfile = findViewById(R.id.img_user_profile)
        postSubmit = tvPostSubmit

        bottomLayout = bottomNavigationView
        img_user_profile.setOnClickListener(this)
        ivSearchTop.setOnClickListener(this)
        img_chat.setOnClickListener(this)

        if (isFromProfile) {
            isFromProfile = false
            img_user_profile.callOnClick()
        }

        MobileAds.initialize(this) { initializationStatus: InitializationStatus? -> }

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigationView.selectedItemId = R.id.nav_home

    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {

            R.id.nav_home -> {
                loadFeedFragment()
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_friends -> {
                loadFriendsFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_add_post -> {
                loadPostFeedFragment()
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_store -> {
                loadStoreFragment()
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_job -> {
                loadJobFragment()
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    private fun loadJobFragment() {
        Constants.SEARCH_TYPE = getString(R.string.jobs)
        updateHeader(getString(R.string.jobs))
        updateChatCount()
        callFragment(JobMainFragment.newInstance(Bundle()))
    }

    private fun updateHeader(text: String) {

        if (text == getString(R.string.post)) {
            tvPostSubmit.visibility = View.VISIBLE
            img_user_profile.visibility = View.INVISIBLE
            img_chat.visibility = View.GONE
            ivSearchTop.visibility = View.GONE
            tvBadge.visibility = View.GONE
            switchAvailableLayout.visibility = View.GONE
            text_header.visibility = View.VISIBLE
            text_header.text = text

        } else {
            img_user_profile.visibility = View.VISIBLE
            switchAvailableLayout.visibility = View.VISIBLE
            ivSearchTop.visibility = View.VISIBLE
            img_chat.visibility = View.VISIBLE
            tvPostSubmit.visibility = View.GONE
            text_header.visibility = View.INVISIBLE
        }
    }

    private fun loadStoreFragment() {
        Constants.SEARCH_TYPE = getString(R.string.store)
        updateHeader(getString(R.string.store))
        updateChatCount()
        callFragment(StoreMainFragment.newInstance(Bundle()))
    }

    private fun loadFriendsFragment() {
        Constants.SEARCH_TYPE = ""
        updateHeader(getString(R.string.network))
        updateChatCount()
        callFragment(Network.newInstance(Bundle(), this))
    }

    private fun callFragment(fragment: Fragment) {
        AppUtils.addFragment(this, fragment, R.id.frame)
    }

    override fun onBackPressed() {
        BMSPrefs.putString(applicationContext, "status", "")
        AlertDialog.Builder(this, R.style.alertDialogTheme)
                .setTitle(getString(R.string.sure_to_exit))
                .setPositiveButton(getString(R.string.yes)) { dialog, id -> finish() }
                .setNegativeButton(getString(R.string.no)) { dialog, which -> dialog.dismiss() }
                .show()
    }

    override fun onClick(v: View) {

        when (v.id) {

            R.id.img_user_profile -> {
                CommonMethod.callActivity(applicationContext, Intent(applicationContext, ProfileActivity::class.java))
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
            }


            R.id.ivSearchTop -> {
                CommonMethod.callCommonContainer(this, getString(R.string.search_all))
            }


            R.id.img_chat -> {
                CommonMethod.callCommonContainer(this, getString(R.string.messaging))
            }
        }
    }

    private fun loadPostFeedFragment() {
        updateHeader(getString(R.string.post))
        callFragment(AddPostFragment.newInstance(Bundle()))
    }

    fun loadFeedFragment() {
        Constants.SEARCH_TYPE = ""
        updateHeader(getString(R.string.home))
        callFragment(FeedFragment.newInstance(Bundle()))
    }


    private val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action != null && action == Constants.NOTIFICATION_ACTION_FILTER) {
                val type = intent.getStringExtra("type")
                type?.let { fcmListener?.onDataReceived(it, "", 0) }

            }
        }
    }

    override fun onStart() {
        super.onStart()
        requestCallOverLayPermission()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction(Constants.NOTIFICATION_ACTION_FILTER)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter)
        getAllDisplayNames()
    }


    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }


    private fun getAllDisplayNames() {
        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["display_name"] = ""
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.findByDisplayName(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SearchDisplayResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: SearchDisplayResponse) {
                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.searchUserlist
                                if (!list.isNullOrEmpty()) {
                                    searchDisplayNameList.clear()
                                    searchDisplayNameList.addAll(list)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        println(e.message)
                    }
                })
    }

    private fun startSinchForceFully() {
        try {
            val userName = BMSPrefs.getString(this, Constants._ID)
            if (userName != sinchServiceInterface.userName) {
                sinchServiceInterface.stopClient()
            }
            if (!sinchServiceInterface.isStarted) {
                sinchServiceInterface.startClient(userName, BMSPrefs.getString(this, Constants.USER_NAME))
                Log.d("USERNAME", userName)
                if (!mPushTokenIsRegistered) {
                    sinchServiceInterface.getManagedPush(userName).registerPushToken(this)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onStartFailed(error: SinchError?) {
        Log.e(TAG, "onStartFailed : " + error?.message)
    }

    override fun onStarted() {
        startSinchForceFully()
    }

    override fun tokenRegistered() {
        mPushTokenIsRegistered = true
    }

    override fun tokenRegistrationFailed(p0: SinchError?) {
        mPushTokenIsRegistered = true
        Log.e(TAG, "tokenRegistrationFailed : " + p0?.message)
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        try {
            sinchServiceInterface.setStartListener(this)
            startSinchForceFully()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    fun updateChatCount() {
        tvBadge.visibility = if (unread_msg_count == 0) View.GONE else View.VISIBLE
        tvBadge.text = unread_msg_count.toString()
    }

    fun updateToggle() {
        switchAvailable.isChecked = ProfileActivity.isAvailable
        tvAvailable.visibility = if (switchAvailable.isChecked) View.VISIBLE else View.GONE
        switchAvailable.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!buttonView.isPressed) {
                return@setOnCheckedChangeListener
            }
            ProfileActivity.isAvailable = isChecked
            tvAvailable.visibility = if (isChecked) View.VISIBLE else View.GONE
            changeAvailabilityStatus()
        }
    }

    override fun onActionVideoCall(userId: String, toUserName: String, toUserImage: String) {
        actionVideoCall(userId, toUserName, toUserImage)
    }

    override fun onActionVoiceCall(userId: String, toUserName: String, toUserImage: String) {
        actionVoiceCall(userId, toUserName, toUserImage)
    }


    private fun changeAvailabilityStatus() {
        ProgressD.show(this, "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeAvailablityStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        CommonMethod.showToastlong(response.response_msg, applicationContext)
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}