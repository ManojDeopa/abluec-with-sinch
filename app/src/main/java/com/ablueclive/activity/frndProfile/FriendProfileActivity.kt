package com.ablueclive.activity.frndProfile

import AppUtils
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ProgressBar
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.ablueclive.activity.adddPost.AddPostActvity
import com.ablueclive.activity.cheersList.CheersListActivity
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.dialog.ReportPostDialog
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.profile.*
import com.ablueclive.activity.search_friend.AddFriendContract
import com.ablueclive.activity.search_friend.AddFriendPresenter
import com.ablueclive.activity.search_friend.UnFriendContract
import com.ablueclive.activity.search_friend.UnFriendPresenter
import com.ablueclive.activity.setting.SettingActivity
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.activity.userProfile.DeletePostContract
import com.ablueclive.activity.userProfile.DeletePostPresenter
import com.ablueclive.activity.userProfile.PostLikeContract
import com.ablueclive.activity.userProfile.PostLikePresenter
import com.ablueclive.adapter.GetAllFeedAdapter
import com.ablueclive.common.CommonResponse
import com.ablueclive.fragment.feedFragment.AllFeedContract
import com.ablueclive.fragment.feedFragment.FeedFragment
import com.ablueclive.fragment.friendList.ConfirmFriendContract
import com.ablueclive.fragment.friendList.DeleteFrndContract
import com.ablueclive.fragment.menu_fragment.LogoutContract
import com.ablueclive.global.fragment.BlankFragment
import com.ablueclive.global.fragment.FollowListFragment
import com.ablueclive.global.fragment.PostDataFragment
import com.ablueclive.global.fragment.SearchPeopleFragment
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.listener.*
import com.ablueclive.modelClass.*
import com.ablueclive.network.Network
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.actvity_friend_profile.*
import kotlinx.android.synthetic.main.contact_layout.*
import java.text.SimpleDateFormat
import java.util.*

class FriendProfileActivity :
        AppCompatActivity(),
        View.OnClickListener,
        FriendProfileContract.View,
        LogoutContract.View,
        AddFriendContract.View,
        UnFriendContract.View,
        onReportPostListener,
        onDeletePostListener,
        DeletePostContract.View,
        onLikePostListener,
        PostLikeContract.View,
        onCommentPostListener,
        onProductItemListener,
        onSharePostListener,
        onFriendProfileListener,
        onEditPostListener,
        onClickForDetailsListener,
        ProfiledataContract.View,
        OnSeeMoreClickListener,
        AllFeedContract.View,
        CustomRecyclerViewItemClick,
        ConfirmFriendContract.View,
        DeleteFrndContract.View {


    private var postData: List<MyProfileResponse.PostDatum>? = null
    private var profileData: MyProfileResponse.GetprofileInfo? = null
    private lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token: String? = null
    private var friendID: String? = null
    private var profile_image: String? = null
    private var user_name: String? = null
    private var item_position = 0

    lateinit var getAllFeedAdapter: GetAllFeedAdapter
    lateinit var postDataArrayList: ArrayList<PostData>

    lateinit var myExpertiseAdapter: MyExpertiseAdapter
    private val removePdfTopIcon = "javascript:(function() {" + "document.querySelector('[role=\"toolbar\"]').remove();})()"

    companion object {
        private const val TAG = "ProfileActivity"
        var display_name = ""
        var isFromJobPage = false
        var jobId = ""
        var jobAcceptedStatus = 0
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_friend_profile)
        findView()
        getProfileData()
    }


    private fun getProfileData() {
        if (isInternetPresent) {
            requestProfileParam()
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
        }
    }

    private fun requestProfileParam() {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["post_time_zone"] = TimeZone.getDefault().id
        params["page"] = "1"
        params["user_profile_id"] = friendID
        params["display_name"] = display_name
        Log.d("ProfileParam", params.toString())
        val profileDataPresenter = FriendProfilePresenter(this)
        profileDataPresenter.requestProfileData(params)
    }

    private fun findView() {
        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        friendID = BMSPrefs.getString(applicationContext, Constants.FRIEND_ID)
        user_name = BMSPrefs.getString(applicationContext, Constants.USER_NAME)
        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector.isConnectingToInternet
        profile_image = BMSPrefs.getString(applicationContext, Constants.PROFILE_IMAGE)

        if (friendID == BMSPrefs.getString(this, Constants._ID)) {
            finish()
            CommonMethod.callActivity(applicationContext, Intent(applicationContext, ProfileActivity::class.java))
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        }

        rootLayout.requestFocus()

        img_back.setOnClickListener(this)
        ivFavourite.setOnClickListener(this)
        img_setting.setOnClickListener(this)
        ProfileDataModel.TYPE = Constants.TYPE_F
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setDataToViews(responseModel: MyProfileResponse) {


        if (responseModel.response_status == 0) {
            finish()
            CommonMethod.showToastShort(responseModel.response_msg, this)
            return
        }

        profileData = responseModel.response_data?.getprofileInfofromuserid
        postData = responseModel.response_data?.postData

        var displayName = ""
        if (!profileData?.display_name.isNullOrEmpty()) {
            displayName = " @" + profileData?.display_name?.replace("@", "")
        }
        displayName = "<font color='#1DA1F2'>$displayName</font>"

        val name = profileData?.name + displayName
        val email = profileData?.email
        val profileImage = profileData?.profileImage
        val mobileNo = profileData?.mobile
        val countryCode = profileData?.countryCode
        val about = profileData?.about

        setData(name, email, profileImage, mobileNo, countryCode, about)


        if (postData.isNullOrEmpty()) {
            user_postLayout.visibility = View.GONE
        } else {
            Constants.showLoader = false
            user_postLayout.visibility = View.VISIBLE
            PostDataFragment.scrollView = scrollView
            AppUtils.addFragment(this, PostDataFragment.newInstance(Bundle(), Constants.TYPE_F), R.id.postContainerF)
        }


        val expList = profileData?.addSkills
        if (!expList.isNullOrEmpty()) {
            expertiseLayout.visibility = View.VISIBLE
            myExpertiseAdapter = MyExpertiseAdapter(expList)
            rvExpertise.apply {
                layoutManager = LinearLayoutManager(this@FriendProfileActivity)
                adapter = myExpertiseAdapter
            }
        } else {
            expertiseLayout.visibility = View.GONE
        }

        val workExpList = profileData?.addExperiences
        if (!workExpList.isNullOrEmpty()) {
            experienceLayout.visibility = View.VISIBLE
            val workExpAdapter = WorkExperienceAdapter(workExpList)
            rvExperience.apply {
                layoutManager = LinearLayoutManager(this@FriendProfileActivity)
                adapter = workExpAdapter
            }
        } else {
            experienceLayout.visibility = View.GONE
        }

        val academicList = profileData?.addAcademic
        if (!academicList.isNullOrEmpty()) {
            academicLayout.visibility = View.VISIBLE
            val academicAdapter = AcademicAdapter(academicList)
            rcAcademic.apply {
                layoutManager = LinearLayoutManager(this@FriendProfileActivity)
                adapter = academicAdapter
            }
        } else {
            academicLayout.visibility = View.GONE
        }


        val address = profileData?.address
        if (address != null) {
            addressLayout.visibility = View.VISIBLE
            etFlatNo.setText(address.flatno)
            etStreet.setText(address.street)
            etCountry.setText(address.country)
            etState.setText(address.state)
            etCity.setText(address.city)
            etZipCode.setText(address.zip)
        } else {
            addressLayout.visibility = View.GONE
        }


        if (isFavourite(profileData?.favStatusData)) {
            ivFavourite.setImageResource(R.drawable.ic_favorite_check)
        } else {
            ivFavourite.setImageResource(R.drawable.ic_favorite_uncheck)
        }


        if (profileData?.is_friend == 1) {
            btnAddFriend.text = getString(R.string.UNFRIEND)
            if (friendID == Constants.ABLUEC_USER_ID) {
                btnAddFriend.isEnabled = false
                btnAddFriend.setBackgroundResource(R.drawable.gray_non_active_border)
            }

        } else {
            if (profileData?.need_to_respond == 1) {
                btnAddFriend.text = getString(R.string.ADDFRIEND)
            } else {
                if (profileData?.already_send_request == 1) {
                    btnAddFriend.text = getString(R.string.REQUESTED)
                    btnAddFriend.setBackgroundResource(R.drawable.red_button_border)
                } else {
                    btnAddFriend.text = getString(R.string.ADDFRIEND)
                }
            }
        }

        if (btnAddFriend.text.toString() == getString(R.string.UNFRIEND)) {
            userContactLayout.visibility = View.VISIBLE
            btnMessaging.visibility = View.VISIBLE
            if (!profileData?.resume.isNullOrEmpty()) {
                resumeLayout.visibility = View.VISIBLE
                loadWebView(ivResume, Constants.POST_IMAGE_URL + profileData?.resume, pbResume, btnViewResume)
            }

            if (!profileData?.coverLetter.isNullOrEmpty()) {
                coverLetterLayout.visibility = View.VISIBLE
                loadWebView(ivCoverLetter, Constants.POST_IMAGE_URL + profileData?.coverLetter, pbCoverLetter, btnViewCoverLetter)
            }
        } else {
            hideInfoLayout()
        }

        btnAddFriend.setOnClickListener {
            if (btnAddFriend.text.toString() == getString(R.string.ADDFRIEND)) {
                sendFriendRequestParam()
            } else {
                unFriendRequestParam()
            }

            if (SearchPeopleFragment.shouldRefresh == 1) SearchPeopleFragment.shouldRefresh = 2
            Network.shouldRefresh = true
            FeedFragment.shouldDataRefresh = true

        }

        btnMessaging.setOnClickListener {
            onMessagingClick(friendID, name, mobileNo)
        }


        tvFollowers.text = profileData?.follow.toString() + "\n" + getString(R.string.followers)
        tvFollowing.text = profileData?.following.toString() + "\n" + getString(R.string.followings)

        tvFriends.text = profileData?.friends_count.toString() + "\n" + getString(R.string.friends)
        tvPost.text = profileData?.post_count.toString() + "\n" + getString(R.string.post)


        when (profileData?.is_followed) {
            0 -> {
                btnFollow.text = getString(R.string.FOLLOW)
            }
            2 -> {
                btnFollow.text = getString(R.string.follow_requested)
            }
            else -> {
                btnFollow.text = getString(R.string.UNFOLLOW)
                if (friendID == Constants.ABLUEC_USER_ID) {
                    btnFollow.isEnabled = false
                    btnFollow.setBackgroundResource(R.drawable.gray_non_active_border)
                }
            }
        }

        btnFollow.setOnClickListener {

            if (btnFollow.text.toString() == getString(R.string.follow_requested)) {
                return@setOnClickListener
            }
            if (btnFollow.text.toString() == getString(R.string.UNFOLLOW)) {
                getString(R.string.sure_to_un_follow).showUnFollowAlert(this)
            } else {
                followUnFollowApi()
            }

        }

        tvFollowers.setOnClickListener {

            val count: String = tvFollowers.text.toString().replace(getString(R.string.followers), "").trim()
            FollowListFragment.totalCount = count

            FollowListFragment.user_profileId = profileData?.id.toString()
            CommonMethod.callCommonContainer(this, getString(R.string.followers))
        }

        tvFollowing.setOnClickListener {

            val count: String = tvFollowing.text.toString().replace(getString(R.string.followings), "").trim()
            FollowListFragment.totalCount = count

            FollowListFragment.user_profileId = profileData?.id.toString()
            CommonMethod.callCommonContainer(this, getString(R.string.followings))

        }

        if (profileData?.is_private == 1) {
            tvFollowers.isClickable = false
            tvFollowing.isClickable = false
            hideInfoLayout()
        }

        if (isFromJobPage) {

            if (jobAcceptedStatus == 0) {

                userContactLayout.visibility = View.VISIBLE
                socialButtonLayout.visibility = View.GONE
                acceptRejectLayout.visibility = View.VISIBLE
                btnAccept.setOnClickListener {
                    acceptRejectJob(1)
                }
                btnReject.setOnClickListener {
                    acceptRejectJob(2)
                }
            }
        }

        if (friendID == Constants.ABLUEC_USER_ID) {
            socialCountLayout.visibility = View.GONE
        }


    }

    private fun String?.showUnFollowAlert(context: Context) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(context, R.style.alertDialogTheme)
        alert.setTitle(context.getString(R.string.app_name))
        alert.setMessage(this)
        alert.setCancelable(false)
        alert.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            followUnFollowApi()
        }

        alert.setNegativeButton(context.getString(R.string.no)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        alert.show()
    }

    private fun hideInfoLayout() {
        if (!isFromJobPage) {
            userContactLayout.visibility = View.GONE
            btnMessaging.visibility = View.GONE
            resumeLayout.visibility = View.GONE
            coverLetterLayout.visibility = View.GONE
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(webV: WebView, url: String, progress: ProgressBar, ivRedirect: Button) {
        ivRedirect.visibility = View.VISIBLE
        progress.visibility = View.VISIBLE
        webV.invalidate()
        webV.settings.javaScriptEnabled = true
        webV.settings.builtInZoomControls = false

        webV.loadUrl("http://docs.google.com/gview?embedded=true&url=$url")
        webV.webViewClient = object : WebViewClient() {
            var checkOnPageStartedCalled = false
            override fun onPageStarted(view: WebView?, urls: String?, favicon: Bitmap?) {
                checkOnPageStartedCalled = true
                Log.e("onPageStarted", "--$urls")
            }

            override fun onPageFinished(view: WebView?, urls: String?) {
                if (checkOnPageStartedCalled) {
                    progress.visibility = View.GONE
                    webV.loadUrl(removePdfTopIcon)
                    webV.setBackgroundColor(Color.TRANSPARENT);
                    webV.zoomBy(1.5f)
                    webV.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
                } else {
                    loadWebView(webV, url, progress, ivRedirect)
                }
            }
        }

        ivRedirect.setOnClickListener {
            AppUtils.callBrowserIntent(this, url)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData(name: String?, email: String?, profile_image: String?, mobile: String?, countryCode: String?, about: String?) {

        var nameAll = name

        if (profileData?.is_private == 1) {
            nameAll = name + " " + getString(R.string.this_account_private)
        }

        text_user_name.text = HtmlCompat.fromHtml(nameAll.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
        tv_email.text = email

        if (mobile.isNullOrEmpty()) {
            tv_mobile_no.visibility = View.GONE
        } else {
            tv_mobile_no.text = countryCode + mobile
        }

        if (!about.isNullOrEmpty()) {
            tv_about.visibility = View.VISIBLE
            tv_about.text = about
        }

        if (!profileData?.createdAt.isNullOrEmpty()) {
            tvJoinedOn.visibility = View.VISIBLE
            tvJoinedOn.text = "Joined On " + joinedOn(profileData?.createdAt)
        }

        val coverImage = profileData?.cover_photo

        Glide.with(applicationContext)
                .load(Constants.COVER_IMAGE_URL + coverImage)
                .skipMemoryCache(true)
                .error(R.drawable.blue_image)
                .into(ivCoverImage)

        ivCoverImage.setOnClickListener { v: View? ->
            val i = Intent(this@FriendProfileActivity, ShowFullImage::class.java)
            i.putExtra("ImageUrl", Constants.COVER_IMAGE_URL + coverImage)
            startActivity(i)

        }

        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + profile_image, img_user_profile)

        img_user_profile.setOnClickListener { v: View? ->
            val i = Intent(this@FriendProfileActivity, ShowFullImage::class.java)
            i.putExtra("ImageUrl", Constants.PROFILE_IMAGE_URL + profile_image)
            startActivity(i)
        }

    }

    override fun onClick(v: View) {
        when (v.id) {

            R.id.ivFavourite -> {
                favouriteProvider()
            }

            R.id.img_back -> onBackPressed()
            R.id.img_setting -> {
                CommonMethod.callActivity(this, Intent(this, SettingActivity::class.java))
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
            }
        }
    }

    override fun showProgress() {
        if (Constants.showLoader) {
            show(this, "")
        }
        Constants.showLoader = true
    }

    override fun hideProgress() {
        hide()
    }

    override fun setDataDeleteFriend(response_status: String?, response_msg: String?, response_invalid: String?) {
    }

    override fun unFriendResponse(response_status: String?, response_msg: String?, response_invalid: String?) {
        if (response_status == "1") {
            btnAddFriend.text = getString(R.string.ADDFRIEND)
            btnAddFriend.setBackgroundResource(R.drawable.blue_button_border)
            //CommonMethod.showToastlong(response_msg, this)
            Network.shouldRefresh = true
        } else {
            CommonMethod.showToastShort(response_msg, this)
        }
    }


    override fun setDataToViews(response_status: String, response_msg: String) {
        if (response_status == "1") {
            logout(response_msg)
        } else if (response_msg == "Your session has been expired. Please login again.") {
            logout(response_msg)
        } else {
            CommonMethod.showToastShort(response_msg, this)
        }
    }

    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, this)
        BMSPrefs.putString(this, Constants._ID, "")
        BMSPrefs.putString(this, Constants.isStoreCreated, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE, "")
        // BMSPrefs.putString(this,Constants.PROFILE_IMAGE_URL,"");
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE_BACK, "")
        BMSPrefs.putString(this, Constants.USER_SKYPE_ID, "")
        BMSPrefs.putString(this, Constants.USER_ABOUT, "")
        BMSPrefs.putString(this, Constants.USER_NAME, "")
        CommonMethod.callActivityFinish(this, Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }


    @SuppressLint("SetTextI18n")
    private fun unUsedForNow(responseModel: MyProfileResponse) {

        tvCompletedCount.text = responseModel.response_data?.completedCount.toString() + "\n" + "Service Completed"
        tvRejectedCount.text = responseModel.response_data?.rejcetedCount.toString() + "\n" + "Service Rejected"

        addTabViewPager()

        val ratingList = responseModel.response_data?.serviceProviderRatings
        if (ratingList.isNullOrEmpty()) {
            ratingLayout.visibility = View.GONE
            tvNoReviewRating.visibility = View.VISIBLE
            rvReviewRating.visibility = View.GONE
        } else {
            tvNoReviewRating.visibility = View.GONE
            rvReviewRating.visibility = View.VISIBLE
            rvReviewRating.apply {
                layoutManager = LinearLayoutManager(this@FriendProfileActivity, LinearLayoutManager.HORIZONTAL, false)
                adapter = ReviewRatingsAdapter(ratingList)
            }
        }


        val mediaList = responseModel.response_data?.dataAllImages
        if (mediaList.isNullOrEmpty()) {
            photoVideoLayout.visibility = View.GONE
            tvNoPhotoVideo.visibility = View.VISIBLE
            rvPhotosVideos.visibility = View.GONE
        } else {
            tvNoPhotoVideo.visibility = View.GONE
            rvPhotosVideos.visibility = View.VISIBLE

            rvPhotosVideos.apply {
                layoutManager = LinearLayoutManager(this@FriendProfileActivity, LinearLayoutManager.HORIZONTAL, false)
                adapter = ImageVideosAdapter(mediaList)
            }
        }
    }

    private fun isFriend(friendStatusData: Int): Boolean {
        return friendStatusData == 2 || friendStatusData == 3
    }

    private fun isFavourite(favStatusData: Int?): Boolean {
        return favStatusData == 1
    }


    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }


    private fun addTabViewPager() {
        val list = listOf("Posts", "Experience & Skill")
        val adapter = ViewPagerAdapter(this)
        viewpager.adapter = adapter
        // viewpager.isUserInputEnabled = false
        TabLayoutMediator(tabs, viewpager,
                TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                    tab.text = list[position]
                }).attach()

    }


    internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {

        val previousJobs = profileData?.previousJobs

        @NonNull
        override fun createFragment(position: Int): Fragment {
            val fragment: Fragment? = when (position) {
                0 -> {
                    /*PostDataFragment.newInstance(Bundle())*/BlankFragment()
                }
                else -> {
                    ExpSkillFragment(previousJobs)
                }
            }

            return fragment!!
        }

        override fun getItemCount(): Int {
            return 2
        }
    }


    private fun favouriteProvider() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["fav_provider_id"] = friendID.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.addFavServiceProvider(param)
        if (isFavourite(profileData?.favStatusData)) {
            apiCall = apiInterface.removeFavServiceProvider(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                if (isFavourite(profileData?.favStatusData)) {
                                    profileData?.favStatusData = 0
                                    ivFavourite.setImageResource(R.drawable.ic_favorite_uncheck)
                                } else {
                                    profileData?.favStatusData = 1
                                    ivFavourite.setImageResource(R.drawable.ic_favorite_check)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    override fun addFreindResponse(response_status: String?, response_msg: String?, response_invalid: String?) {
        if (response_status == "1") {
            btnAddFriend.text = getString(R.string.REQUESTED)
            btnAddFriend.setBackgroundResource(R.drawable.red_button_border)
            //CommonMethod.showToastlong(response_msg, this)
        }
    }


    private fun sendFriendRequestParam() {
        val params = HashMap<String, String>()
        params["add_friend_id"] = friendID.toString()
        params["session_token"] = session_token.toString()
        Log.d("sendFriendRequestParam", params.toString())
        val addFriendPresenter = AddFriendPresenter(this)
        addFriendPresenter.requestAddFriend(params)
    }

    private fun unFriendRequestParam() {
        val params = HashMap<String, String>()
        params["un_friend_id"] = friendID.toString()
        params["session_token"] = session_token.toString()
        Log.d("unFriendRequestParam", params.toString())
        val unFriendPresenter = UnFriendPresenter(this)
        unFriendPresenter.requestUnFriend(params)
    }

    private fun onMessagingClick(friend_id: String?, user_name: String?, mobile: String?) {
        BMSPrefs.putString(this, Constants.TO_ID, friend_id)
        BMSPrefs.putString(this, Constants.FRIEND_MOBILE, mobile)
        BMSPrefs.putString(this, Constants.CHAT_USER_NAME, profileData?.name)
        BMSPrefs.putString(this, Constants.CHAT_USER_IMAGE, profileData?.profileImage)
        CommonMethod.callActivity(this, Intent(this, StartChatActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }


    private fun followUnFollowApi() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["user_id"] = BMSPrefs.getString(this, Constants._ID)
        param["session_token"] = session_token.toString()
        param["to_user_id"] = friendID.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.follow(param)
        if (btnFollow.text.toString() == getString(R.string.UNFOLLOW)) {
            apiCall = apiInterface.unFollow(param)
        } else if (btnFollow.text.toString() == getString(R.string.follow_requested)) {
            apiCall = apiInterface.unFollow(param)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {

                                //CommonMethod.showToastlong(response.responseMsg, this@FriendProfileActivity)
                                var intFollow: Int = tvFollowers.text.toString().replace(getString(R.string.followers), "").trim().toInt()
                                var stringFollow = getString(R.string.FOLLOW)
                                if (btnFollow.text.toString() == getString(R.string.FOLLOW)) {
                                    stringFollow = getString(R.string.UNFOLLOW)
                                    if (profileData?.is_private == 1) {
                                        stringFollow = getString(R.string.follow_requested)
                                    }
                                    intFollow += 1
                                } else {
                                    intFollow = if (intFollow < 0) 0 else intFollow - 1
                                }

                                btnFollow.text = stringFollow
                                tvFollowers.text = intFollow.toString() + "\n" + getString(R.string.followers)

                                SearchPeopleFragment.shouldRefresh = 2
                                Network.shouldRefresh = true
                                FeedFragment.shouldDataRefresh = true
                            } else {
                                CommonMethod.showToastlong(response.responseMsg, this@FriendProfileActivity)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    override fun onReportPostClick(post_id: String?, position: Int) {
        val reportPostDialog = ReportPostDialog(this, post_id, postDataArrayList, position, getAllFeedAdapter)
        reportPostDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    override fun onDelPostClick(post_id: String?, position: Int) {
        if (isInternetPresent) {
            deletePostRequest(post_id!!)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), this@FriendProfileActivity)
        }
    }


    private fun deletePostRequest(post_id: String) {
        val params = HashMap<String, String>()
        params["session_token"] = session_token!!
        params["post_id"] = post_id
        Log.d("deletePostRequest", params.toString())
        val deletePostPresenter = DeletePostPresenter(this)
        deletePostPresenter.requestDeletePost(params)
    }

    override fun onLikePostClick(post_id: String?, position: Int) {
        if (isInternetPresent) {
            item_position = position
            likePostRequest(post_id!!, item_position)
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), this@FriendProfileActivity)
        }
    }


    private fun likePostRequest(postId: String, item_position: Int) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["postId"] = postId
        params["user_name"] = user_name
        Log.d("likePostRequest", params.toString())
        val postLikePresenter = PostLikePresenter(this)
        postLikePresenter.requestPostLike(params)
    }


    override fun onCmntPostClick(post_id: String?, pos: Int) {
        FeedFragment.shouldDataRefresh = true
        BMSPrefs.putString(this, Constants.FEED_POST_ID, post_id)
        CommonMethod.callActivity(this, Intent(this, CommentActivity::class.java))
    }

    override fun onProductItemClick(product_id: String?) {
        BMSPrefs.putString(this, Constants.FEED_POST_ID, product_id)
        CommonMethod.callActivity(this, Intent(this, CheersListActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onSharePostClick(post_id: String, position: Int, type: String) {
        item_position = position
        sharePost(post_id, type)

    }

    override fun onAddFriendClick(friend_id: String?, position: Int) {
        if (position == 0) {
            cancelRequest(friend_id)
        }

        if (position == 1) {
            sendFriendRequestParam()
        }
    }

    override fun onFrndProfileClick(friend_id: String?, position: Int) {
        BMSPrefs.putString(this, Constants.FRIEND_ID, friend_id)
        CommonMethod.callActivity(this, Intent(this@FriendProfileActivity, FriendProfileActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    override fun onEditPostClick(post_id: String?, position: Int) {
        AddPostActvity.data = postDataArrayList[position]
        val intent = Intent(this, AddPostActvity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }


    override fun onClickForDetails(postData: PostData?) {

    }

    override fun onSeeMoreClick(text: String, position: Int) {
//        if (text == "collapse") {
//            val y = recycler_all_feed.getChildAt(position).y.toInt()
//            scrollView.post {
//                scrollView.fling(0)
//                scrollView.smoothScrollTo(0, y)
//            }
//        }

    }

    override fun setDeletePost(response_status: String?, response_msg: String?, response_invalid: String?) {
        when {
            response_status == "1" -> {
                CommonMethod.showToastShort(response_msg, this@FriendProfileActivity)

            }
            response_invalid == "1" -> {
                logout(response_msg!!)
            }
            else -> {
                CommonMethod.showToastShort(response_msg, this@FriendProfileActivity)
            }
        }
    }


    override fun setPostLike(response_status: String?, response_msg: String?, response_invalid: String?, response_data: Response_data?) {
        when {
            response_status == "1" -> {
                getAllFeedAdapter.setLikeCount(null, response_data?.status, response_data?.totalCountLike)
            }
            response_invalid == "1" -> {
                logout(response_msg!!)
            }
            else -> {
                CommonMethod.showToastShort(response_msg, this@FriendProfileActivity)
            }
        }
    }

    override fun setDataToRecyclerView(response_status: String?, response_msg: String?, response_invalid: String?, response_data: Response_data?) {

    }

    override fun setDataConfirmFriend(response_status: String?, response_msg: String?, response_invalid: String?) {
        CommonMethod.showAlert(this, response_msg)
    }


    override fun onRecyclerItemClick(isTrue: Boolean, text: String, position: Int) {

    }


    private fun sharePost(post_id: String, type: String) {
        show(this, "")
        val hashMap = HashMap<String, String>()
        hashMap["post_id"] = post_id
        hashMap[" session_token"] = session_token.toString()
        hashMap["post_time_zone"] = TimeZone.getDefault().id
        Log.d("sharePost", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.reTweetPost(hashMap)
        if (type == getString(R.string.un_do_wave)) {
            apiCall = apiInterface.deleteReTweetPost(hashMap)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<SharePostResponse?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(response: SharePostResponse) {
                        hide()

                        try {
                            if (response.responseStatus == 1) {
                                val list = response.responseData?.data
                                if (!list.isNullOrEmpty()) {
                                    val shareUserList = list[0].shareUserPost
                                    if (!shareUserList.isNullOrEmpty()) {
                                        val newList = arrayListOf<ShareUsersPost>()
                                        shareUserList.forEach {
                                            newList.add(it)
                                        }
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = newList
                                    } else {
                                        getAllFeedAdapter.postDataArrayList[item_position].shareUserPost = null
                                    }
                                }
                                getAllFeedAdapter.postDataArrayList[item_position].isRetweeted = type != getString(R.string.un_do_wave)
                                getAllFeedAdapter.notifyItemChanged(item_position)
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, this@FriendProfileActivity)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        Log.d(TAG, e.message!!)
                    }

                    override fun onComplete() {

                    }

                })
    }


    private fun cancelRequest(id: Any?) {
        show(this, "")
        val param = HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        param["un_friend_id"] = id.toString()
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.UnFriendRequest(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    @SuppressLint("SetTextI18n")
                    override fun onNext(response: ResponseModel) {
                        hide()
                        try {
                            if (response.response_status == "1") {
                                CommonMethod.showAlert(this@FriendProfileActivity, "Request Cancelled Successfully!")
                            } else {
                                CommonMethod.showAlert(this@FriendProfileActivity, response.response_msg)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun joinedOn(strDate: String?): String? {
        if (strDate == null || strDate.trim { it <= ' ' }.isEmpty()) {
            return ""
        }
        try {
            /* var spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)*/
            var spf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            val newDate = spf.parse(strDate)
            spf = SimpleDateFormat("MMM yyyy", Locale.getDefault())
            return spf.format(newDate!!)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return strDate
    }

    override fun onDestroy() {
        super.onDestroy()
        display_name = ""
        isFromJobPage = false
    }


    private fun acceptRejectJob(type: Int) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["job_id"] = jobId
        param["service_provider_id"] = friendID.toString()

        var api = apiInterface.acceptJobRequestApi(param)
        if (type == 2) {
            api = apiInterface.rejectJobRequestApi(param)
        }


        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (type == 2) {
                                CommonMethod.showToastShort(response.responseData?.rejectJobRequestApi, this@FriendProfileActivity)
                            } else {
                                CommonMethod.showToastShort(response.responseData?.acceptJobRequestApi, this@FriendProfileActivity)
                            }
                            finish()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

}