package com.ablueclive.activity.frndProfile;

import android.util.Log;

import com.ablueclive.activity.profile.MyProfileResponse;
import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.networkClass.RetrofitClient;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class FriendProfileModel implements FriendProfileContract.Model {
    private static final String TAG = "FriendProfileModel";

    @Override
    public void ProfileDetail(OnFinishedListener onFinishedListener, HashMap<String, String> param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.getFriendProfileData(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyProfileResponse>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull MyProfileResponse responseModel) {
                        if (responseModel != null) {
                            onFinishedListener.onFinished(responseModel);
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        try {
                            Log.d(TAG, e.getMessage()+"");
                            onFinishedListener.onFailure(e);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


}

