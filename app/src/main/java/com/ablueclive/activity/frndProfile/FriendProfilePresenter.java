package com.ablueclive.activity.frndProfile;


import com.ablueclive.activity.profile.MyProfileResponse;

import java.util.HashMap;

public class FriendProfilePresenter implements FriendProfileContract.Presenter, FriendProfileContract.Model.OnFinishedListener {
    private FriendProfileContract.View profileDataView;
    private FriendProfileContract.Model profileDataModel;

    public FriendProfilePresenter(FriendProfileContract.View profileDataView) {
        this.profileDataView = profileDataView;
        this.profileDataModel = new FriendProfileModel();
    }


    @Override
    public void onFinished(MyProfileResponse responseModel) {
        if (profileDataView != null) {
            profileDataView.hideProgress();
            profileDataView.setDataToViews(responseModel);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (profileDataView != null) {
            profileDataView.hideProgress();
            profileDataView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        profileDataView = null;
    }

    @Override
    public void requestProfileData(HashMap<String, String> param) {
        if (profileDataView != null) {
            profileDataView.showProgress();
        }
        profileDataModel.ProfileDetail(this, param);
    }
}
