package com.ablueclive.activity.frndProfile;

import com.ablueclive.activity.profile.MyProfileResponse;

import java.util.HashMap;

public interface FriendProfileContract {


    interface Model {
        interface OnFinishedListener {
            void onFinished(MyProfileResponse responseModel);

            void onFailure(Throwable t);
        }

        void ProfileDetail(FriendProfileContract.Model.OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(MyProfileResponse responseModel);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestProfileData(HashMap<String, String> param);
    }
}

