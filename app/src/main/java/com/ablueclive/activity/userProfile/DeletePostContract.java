package com.ablueclive.activity.userProfile;

import java.util.HashMap;

public interface DeletePostContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void deletePost(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {
        void showProgress();

        void hideProgress();

        void setDeletePost(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestDeletePost(HashMap<String, String> param);

    }
}
