package com.ablueclive.activity.userProfile;

import com.ablueclive.modelClass.Response_data;

import okhttp3.RequestBody;

public class ProfileImagePresenter implements ProfileImageContract.Presenter, ProfileImageContract.Model.OnFinishedListener {
    private ProfileImageContract.View profileImageView;
    private ProfileImageContract.Model profileImageModel;

    public ProfileImagePresenter(ProfileImageContract.View profileImageView) {
        this.profileImageView = profileImageView;
        this.profileImageModel = new ProfileImageModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (profileImageView != null) {
            profileImageView.hideProgress();
            profileImageView.setData(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (profileImageView != null) {
            profileImageView.hideProgress();
            profileImageView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        profileImageView = null;
    }

    @Override
    public void requestProfileImage(RequestBody param) {
        if (profileImageView != null) {
            profileImageView.showProgress();
            profileImageModel.profileImage(this, param);
        }

    }
}
