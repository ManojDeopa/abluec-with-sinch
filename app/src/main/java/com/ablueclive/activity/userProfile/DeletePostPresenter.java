package com.ablueclive.activity.userProfile;

import java.util.HashMap;

public class DeletePostPresenter implements DeletePostContract.Presenter, DeletePostContract.Model.OnFinishedListener {
    private DeletePostContract.View delPostView;
    private DeletePostContract.Model delPostModel;

    public DeletePostPresenter(DeletePostContract.View delPostView) {
        this.delPostView = delPostView;
        this.delPostModel = new DeletePostModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (delPostView != null) {
            delPostView.hideProgress();
            delPostView.setDeletePost(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (delPostView != null) {
            delPostView.hideProgress();
            delPostView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        delPostView = null;
    }

    @Override
    public void requestDeletePost(HashMap<String, String> param) {
        if (delPostView != null) {
            delPostView.showProgress();
            delPostModel.deletePost(this, param);
        }

    }
}
