package com.ablueclive.activity.userProfile;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class PostLikePresenter implements PostLikeContract.Presenter, PostLikeContract.Model.OnFinishedListener {
    private PostLikeContract.View postLikeView;
    private PostLikeContract.Model postLikeModel;

    public PostLikePresenter(PostLikeContract.View postLikeView) {
        this.postLikeView = postLikeView;
        this.postLikeModel = new PostLikeModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (postLikeView != null) {
            postLikeView.hideProgress();
            postLikeView.setPostLike(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (postLikeView != null) {
            postLikeView.hideProgress();
            postLikeView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        postLikeView = null;
    }

    @Override
    public void requestPostLike(HashMap<String, String> param) {
        if (postLikeView != null) {
            postLikeView.showProgress();
            postLikeModel.postLike(this, param);
        }

    }
}
