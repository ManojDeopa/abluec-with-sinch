package com.ablueclive.activity.userProfile;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.networkClass.RetrofitClient;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class ProfileImageModel implements ProfileImageContract.Model {
    private static final String TAG = "ProfileImageModel";

    @Override
    public void profileImage(OnFinishedListener onFinishedListener, RequestBody param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.profileImage(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel != null) {
                            String response_status = responseModel.getResponse_status();
                            String response_msg = responseModel.getResponse_msg();
                            String response_invalid = responseModel.getResponse_invalid();
                            Response_data response_data = null;
                            if (response_status.equalsIgnoreCase("1")) {
                                response_data = responseModel.getResponse_data();
                                onFinishedListener.onFinished(response_status, response_msg, response_invalid, response_data);
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        onFinishedListener.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
