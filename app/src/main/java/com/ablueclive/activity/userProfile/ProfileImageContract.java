package com.ablueclive.activity.userProfile;
import com.ablueclive.modelClass.Response_data;
import java.util.HashMap;
import okhttp3.RequestBody;

public interface ProfileImageContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void profileImage(OnFinishedListener onFinishedListener, RequestBody param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setData(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {
        void onDestroy();

        void requestProfileImage(RequestBody param);

    }
}
