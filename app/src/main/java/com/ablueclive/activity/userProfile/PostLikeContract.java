package com.ablueclive.activity.userProfile;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface PostLikeContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void postLike(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setPostLike(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {
        void onDestroy();

        void requestPostLike(HashMap<String, String> param);

    }
}
