package com.ablueclive.activity.adddPost

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.ImageView
import android.widget.ListPopupWindow
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.activity.mainActivity.MainActivity.Companion.mainActivity
import com.ablueclive.adapter.UploadPhotoAdapter
import com.ablueclive.common.SearchDisplayResponse
import com.ablueclive.listener.onMenuItemListener
import com.ablueclive.modelClass.Files
import com.ablueclive.modelClass.PostData
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.callActivityFinish
import com.ablueclive.utils.CommonMethod.showToastShort
import com.ablueclive.utils.CommonMethod.showToastlong
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.actvity_post_type.*
import kotlinx.android.synthetic.main.add_post_frgment.*
import kotlinx.android.synthetic.main.new_app_header.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.*
import java.net.URL
import java.util.*
import java.util.regex.Pattern


class AddPostActvity : AppCompatActivity(), View.OnClickListener, onMenuItemListener, AddPostContract.View {


    private val REQUEST_CODE_TAKE_PICTURE = 11
    private val REQUEST_CODE_GALLERY = 12

    var taggedIdList = arrayListOf<String>()
    private var output = ""
    private lateinit var displayListPopupWindow: ListPopupWindow
    private lateinit var searchDisplayNameadapter: SearchDisplayNameAdapter

    private var profile_image: String? = null
    private var session_token: String? = null
    private var post_content = ""

    private var uploadPhotoAdapter: UploadPhotoAdapter? = null
    private val mediaArrayList = ArrayList<Files>()
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var size: Long = 0

    private var userName: String? = null


    var LAUNCH_SECOND_ACTIVITY = 1
    private val REQUEST_CODE_CAPTURE_VIDEO = 111
    private val REQUEST_CODE_SELECT_VIDEO = 100
    private var file: Uri? = null
    private var post_id = ""

    lateinit var imgBack: ImageView
    lateinit var imgUserProfile: CircleImageView

    lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_post_frgment)
        findView()
    }

    // initialize object
    @SuppressLint("ClickableViewAccessibility")
    private fun findView() {
        context = this

        profile_image = BMSPrefs.getString(this, Constants.PROFILE_IMAGE)
        userName = BMSPrefs.getString(applicationContext, Constants.USER_NAME)
        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector!!.isConnectingToInternet


        imgBack = img_back
        imgUserProfile = img_user_profile



        radio_public.isChecked = true
        actvity_post_type.visibility = View.GONE
        upload_photo.setOnClickListener(this)
        edit_post_type.setOnClickListener(this)
        radio_public.setOnClickListener(View.OnClickListener { view: View -> onClick(view) })
        radio_private.setOnClickListener(this)
        tv_add_hastag.setOnClickListener(this)
        upload_video.setOnClickListener(this)
        submit_post.visibility = View.VISIBLE
        submit_post.setOnClickListener(this)


        user_name.text = userName
        setPhotoAdapter()


        if (data != null) {

            addPostHeader.visibility = View.GONE
            editPostHeader.visibility = View.VISIBLE
            imgBack = img_back_edit
            imgUserProfile = img_user_profile_edit
            post_id = data!!._id
            edt_post.setText(data!!.post_content)


            val postContent = data?.post_content
            if (!postContent.isNullOrEmpty()) {
                makeSpannable(postContent)
            }

            /*val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)*/
            val hashList = data!!.hasTagNameUnsedForNow
            if (hashList != null && hashList.size > 0) {
                hashList.forEach {
                    taggedIdList.add(it._id.toString())
                }
            }

            convertImages()

        } else {
            addPostHeader.visibility = View.VISIBLE
            editPostHeader.visibility = View.GONE
            view_close.setOnTouchListener(OnTouchListener { v: View?, e: MotionEvent ->
                if (e.actionMasked == MotionEvent.ACTION_DOWN) {
                    actvity_post_type.visibility = View.GONE
                    r2.visibility = View.VISIBLE
                    layout_image_upload.visibility = View.VISIBLE
                    tv_add_hastag.visibility = View.VISIBLE
                    // perceive a touch action.
                } else {
                    if (e.actionMasked != MotionEvent.ACTION_UP) {
                        e.actionMasked
                    }
                }
                false
            })
        }

        Glide.with(applicationContext)
                .load(Constants.PROFILE_IMAGE_URL + profile_image)
                .skipMemoryCache(true)
                .error(R.drawable.user_profile)
                .into(imgUserProfile)


        imgBack.setOnClickListener(View.OnClickListener { v: View? -> onBackPressed() })

        edt_post.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }


        findByDisplayName()

        edt_post.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(str: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {
                val s = str.toString()
                if (s.isNotEmpty()) {
                    if (s.length >= 2) {
                        if (s.contains("@")) {
                            val text = s
                            val startChar = "@"
                            val endChar = " "
                            output = getStringBetweenTwoChars(text, startChar, endChar)
                            Log.e("output", "--$output")

                            if (output.isNotEmpty()) {
                                updateAdapter()
                            }
                        }
                    } else {
                        clearView()
                    }
                } else {
                    clearView()
                }

            }
        })

    }


    private val isValidated: Boolean
        get() = if (mediaArrayList.size > 0) {
            true
        } else !post_content.trim { it <= ' ' }.isEmpty()

    //set image adapter
    private fun setPhotoAdapter() {
        uploadPhotoAdapter = UploadPhotoAdapter(this, mediaArrayList, this)
        /* staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);
        recycler_image.setLayoutManager(staggeredGridLayoutManager);*/
        val horizontalLayoutManagaer: LinearLayoutManager = GridLayoutManager(this, 2)
        recycler_image!!.layoutManager = horizontalLayoutManagaer
        recycler_image!!.adapter = uploadPhotoAdapter
    }

    // button listener
    override fun onClick(view: View) {
        when (view.id) {
            R.id.upload_photo -> {
                onSelectPhoto()
            }
            R.id.upload_video -> {
                onSelectVideo()
            }
            R.id.submit_post -> {
                post_content = edt_post!!.text.toString().trim { it <= ' ' }
                if (isInternetPresent) {
                    requestAddPost()
                } else {
                    showToastShort(getString(R.string.internet_toast), applicationContext)
                }
            }
            R.id.edit_post_type -> {
                r2.visibility = View.GONE
                actvity_post_type.visibility = View.VISIBLE
                layout_image_upload!!.visibility = View.GONE
                tv_add_hastag!!.visibility = View.GONE
            }
            R.id.radio_public -> {
                radio_public!!.isChecked = true
                radio_private!!.isChecked = false
                edit_post_type!!.text = "Public"
            }
            R.id.radio_private -> {
                radio_private!!.isChecked = true
                radio_public!!.isChecked = false
                edit_post_type!!.text = "Private"
            }

        }
    }

    private fun onSelectPhoto() {
        val alertDialog = android.app.AlertDialog.Builder(this, R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.type = "image/*"
            startActivityForResult(intent, REQUEST_CODE_GALLERY)
        }
    }

    private fun onSelectVideo() {
        val alertDialog = AlertDialog.Builder(this, R.style.alertDialogTheme)
        alertDialog.setMessage(getString(R.string.select_video_from))
        alertDialog.setPositiveButton(getString(R.string.gallery)) { dialog, which ->
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.type = "video/mp4"
            startActivityForResult(intent, REQUEST_CODE_SELECT_VIDEO)
        }
        alertDialog.setNegativeButton(getString(R.string.camera)) { dialog, which -> captureVideo() }
        alertDialog.show()
    }

    private fun captureVideo() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "video/mp4")
        intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        file = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_VIDEO)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == REQUEST_CODE_GALLERY) {
            if (resultCode == RESULT_OK) {
                val mResults = getSelectedVideos(requestCode, data)
                for (i in mResults.indices) {
                    val file = Files()
                    file.media_path = mResults[i]
                    file.type = "1"
                    file.source = "internal"
                    file.thumbpath = ""
                    mediaArrayList.add(file)
                }
                for (i in mediaArrayList.indices) {
                    var j = i + 1
                    while (j < mediaArrayList.size) {
                        if (mediaArrayList[i].media_path.equals(mediaArrayList[j].media_path, ignoreCase = true)) {
                            mediaArrayList.removeAt(j)
                            j--
                        }
                        j++
                    }
                }
                text1?.visibility = View.VISIBLE
                uploadPhotoAdapter?.notifyDataSetChanged()
            }
        }


        if (requestCode == REQUEST_CODE_TAKE_PICTURE) {

            try {
                val mFile = Files()
                mFile.media_path = CommonMethods.getRealPathFromURI(this, file)
                mFile.type = "1"
                mFile.source = "internal"
                mFile.thumbpath = ""
                mediaArrayList.add(mFile)

                text1?.visibility = View.VISIBLE
                uploadPhotoAdapter?.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        if (requestCode == REQUEST_CODE_SELECT_VIDEO) {
            if (resultCode == RESULT_OK) {
                try {
                    val selectedVideos = getSelectedVideos(requestCode, data)
                    Log.d("size", "onActivityResult: size" + selectedVideos.size + "")
                    for (i in selectedVideos.indices) {
                        val file = File(selectedVideos[i])
                        size += file.length()
                        val thumb = ThumbnailUtils.createVideoThumbnail(selectedVideos[i], MediaStore.Images.Thumbnails.MINI_KIND)
                        val s = File(selectedVideos[i]).name
                        val ss = s.substring(0, s.lastIndexOf("."))
                        val path = thumb?.let { storeImage(it, ss) }
                        val filem = Files()
                        filem.media_path = selectedVideos[i]
                        filem.type = "2"
                        filem.source = "internal"
                        filem.thumbpath = path
                        mediaArrayList.add(filem)
                    }
                    text1!!.visibility = View.VISIBLE
                    uploadPhotoAdapter!!.notifyDataSetChanged()
                    size /= (1024 * 1024)
                    Log.d(TAG, "onActivityResult: $size")
                    if (size > 80) {
                        selectedVideos.clear()
                        showToastShort("Please Upload Below Size of 80 MB", this)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
        if (requestCode == REQUEST_CODE_CAPTURE_VIDEO) {
            try {
                var videoPath = CommonMethods.getRealPathFromURI(this, file)
                var thumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND)


                if (thumb == null) {
                    videoPath = CommonMethods.getRealPathFromURI(this, data?.data)
                    thumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND)
                }

                val s = File(videoPath).name
                val ss = s.substring(0, s.lastIndexOf("."))
                val path = thumb?.let { storeImage(it, ss) }
                val filem = Files()
                filem.media_path = videoPath
                filem.type = "2"
                filem.source = "internal"
                filem.thumbpath = path
                mediaArrayList.add(filem)
                text1!!.visibility = View.VISIBLE
                uploadPhotoAdapter!!.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun requestAddPost() {
        if (!isValidated) {
            showToastlong("The Post Can't Be Empty", this)
            return
        }
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("post_title", "")
        builder.addFormDataPart("post_content", post_content)
        builder.addFormDataPart("session_token", session_token!!)

        if (edit_post_type.text.toString() == "Private") {
            builder.addFormDataPart("is_private", "1")
        } else {
            builder.addFormDataPart("is_private", "0")
        }

        if (data != null) {
            builder.addFormDataPart("post_id", post_id)
        } else {
            if (edit_post_type.text.toString() == "Private") {
                builder.addFormDataPart("hashStatus", "2")
            }
        }
        if (taggedIdList.size > 0) {
            val hashTags = taggedIdList.toString().replace("[", "").replace("]", "")
            builder.addFormDataPart("hasTag", hashTags.replace(" ", "").trim())
            Log.e("hashTags", "--$hashTags")
        }


        for (i in mediaArrayList.indices) {
            if (mediaArrayList[i].type == "2") {
                val imgFile = File(mediaArrayList[i].media_path)
                val file = Compressor.getDefault(context).compressToFile(imgFile)
                builder.addFormDataPart("post_files", file.name, file.asRequestBody("video/mp4".toMediaTypeOrNull()))
                val thumbs = File(mediaArrayList[i].thumbpath)
                builder.addFormDataPart("post_files", thumbs.name, thumbs.asRequestBody("image/jpg".toMediaTypeOrNull()))
            }
            if (mediaArrayList[i].type == "1") {
                val imgFile = File(mediaArrayList[i].media_path)
                val file = Compressor.getDefault(context).compressToFile(imgFile)
                builder.addFormDataPart("post_images", file.name, file.asRequestBody("image/jpg".toMediaTypeOrNull()))
            }
        }


        val requestBody = builder.build()
        val addPostPresenter = AddPostPresenter(this)
        addPostPresenter.requestAddPost(requestBody)
        Log.d("sharePost", requestBody.toString())

    }

    override fun onMenuItemClick(position: Int) {
        if (mediaArrayList.size > 0) {
            mediaArrayList.removeAt(position)
            uploadPhotoAdapter!!.notifyDataSetChanged()
        }
    }

    override fun showProgress() {
        show(this, "")
    }

    override fun hideProgress() {
        hide()
    }

    override fun setProductToViews(response_status: String, response_msg: String, response_invalid: String) {
        when {
            response_status == "1" -> {
                showToastShort(response_msg, applicationContext)
                onBackPressed()
            }
            response_invalid.equals("1", ignoreCase = true) -> {
                logout(response_msg)
            }
            else -> {
                showToastShort(response_msg, applicationContext)
            }
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, "" + throwable.message)
    }

    private fun logout(response_msg: String) {
        showToastShort(response_msg, applicationContext)
        BMSPrefs.putString(applicationContext, Constants._ID, "")
        BMSPrefs.putString(applicationContext, Constants.isStoreCreated, "")
        BMSPrefs.putString(applicationContext, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(applicationContext, Constants.PROFILE_IMAGE_BACK, "")
        callActivityFinish(applicationContext, Intent(applicationContext, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }

    private fun storeImage(thumb: Bitmap, name: String): String {
        val pictureFile = getOutputMediaFile(name, thumb)
        val videoThumbPath = pictureFile!!.path
        try {
            val fos = FileOutputStream(pictureFile)
            thumb.compress(Bitmap.CompressFormat.PNG, 90, fos)
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.d("error_saving", "File not found:" + e.message)
        } catch (e: IOException) {
            Log.d("error_saving", "Error accessing file: " + e.message)
        }
        return pictureFile.absolutePath
    }

    private fun getSelectedVideos(requestCode: Int, data: Intent?): MutableList<String> {
        val result: MutableList<String> = ArrayList()
        var clipData: ClipData? = null
        clipData = data!!.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                val videoItem = clipData.getItemAt(i)
                val videoURI = videoItem.uri
                val filePath = Utility.getPath(this, videoURI)
                result.add(filePath)
            }
        } else {
            val videoURI = data.data
            val filePath = Utility.getPath(this, videoURI)
            result.add(filePath)
        }
        return result
    }

    fun getOutputMediaFile(name: String, thumb: Bitmap?): File? {
        val mediaStorageDir = File("$filesDir/Android/data/$packageName/Files")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }
        val mediaFile: File
        val mImageName = "$name.jpg"
        mediaFile = File(mediaStorageDir.path + File.separator + mImageName)
        return mediaFile
    }

    override fun onBackPressed() {
        super.onBackPressed()
        data = null
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }


    override fun onDestroy() {
        super.onDestroy()
        data = null
    }


    private fun convertImages() {

        Thread {

            try {
                val imageArrayListConvert = data?.images

                if (imageArrayListConvert.isNullOrEmpty()) return@Thread

                runOnUiThread {
                    show(context, "")
                }

                imageArrayListConvert.forEach {

                    if (it.imageName.trim().isNotEmpty()) {
                        val ext = ".jpg"
                        var imgUrl = Constants.POST_IMAGE_URL + it.imageName
                        if (it.imageType.contains("video/mp4")) {
                            imgUrl = Constants.POST_IMAGE_URL + it.thumbImage
                            // imgUrl = Constants.POST_IMAGE_URL + it.media_path
                        }

                        val `in` = URL(imgUrl).openStream()
                        val bmp = BitmapFactory.decodeStream(`in`)
                        val bytes = ByteArrayOutputStream()
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, bytes)
                        // val file = File(Environment.getExternalStorageDirectory().toString() + File.separator + System.currentTimeMillis() + ext)
                        val file = File(getExternalFilesDir(null)?.absolutePath + File.separator + System.currentTimeMillis() + ext)

                        try {
                            val fo = FileOutputStream(file)
                            fo.write(bytes.toByteArray())
                            fo.flush()
                            fo.close()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                        val path = file.toString()

                        if (it.imageType.contains("video/mp4")) {
                            val file2 = Files()
                            file2.media_path = path
                            file2.type = "2"
                            file2.source = "internal"
                            file2.thumbpath = path
                            mediaArrayList.add(file2)

                        } else {
                            val file1 = Files()
                            file1.media_path = file.toString()
                            file1.type = "1"
                            file1.source = "internal"
                            file1.thumbpath = ""
                            mediaArrayList.add(file1)
                        }
                    }
                }

                runOnUiThread {
                    hide()
                    setPhotoAdapter()
                }

            } catch (e: java.lang.Exception) {
                Log.e("Error", "--" + e.message)
                e.printStackTrace()
            }
        }.start()

    }


    companion object {
        private const val REQUEST_CODE_IMAGE_SELECTOR = 732
        private const val TAG = "AddPostActvity"

        @JvmStatic
        var data: PostData? = null
    }


    private fun findByDisplayName() {


        displayListPopupWindow = ListPopupWindow(this)
        displayListPopupWindow.anchorView = viewPost
        displayListPopupWindow.setDropDownGravity(Gravity.CENTER)
        displayListPopupWindow.height = ListPopupWindow.WRAP_CONTENT
        val width: Int = resources.getDimensionPixelSize(R.dimen.display_popup_menu_width)
        displayListPopupWindow.width = width


        searchDisplayNameadapter = SearchDisplayNameAdapter(this, mainActivity.searchDisplayNameList)
        displayListPopupWindow.setAdapter(searchDisplayNameadapter)

    }


    private fun updateAdapter() {

        val filterList = mutableListOf<SearchDisplayResponse.SearchUserlist>()
        mainActivity.searchDisplayNameList.forEach {
            if (it.displayName.toString().toLowerCase(Locale.getDefault()).contains(output.toLowerCase(Locale.getDefault()))) {
                if (!filterList.contains(it)) {
                    filterList.add(it)
                }
            }
        }


        searchDisplayNameadapter.updateList(filterList)

        displayListPopupWindow.setOnItemClickListener { parent, view, position, id ->
            var strData = edt_post.text.toString()
            val displaySelected = "@" + searchDisplayNameadapter.searchDisplayNameList[position].displayName.toString().replace("@", "")
            val finalText = strData.replace("@" + output, displaySelected)


            val taggedId = searchDisplayNameadapter.searchDisplayNameList[position].id.toString()
            if (!taggedIdList.contains(taggedId)) {
                taggedIdList.add(taggedId)
            }

            makeSpannable(finalText)
            edt_post.setSelection(finalText.length)

            displayListPopupWindow.dismiss()
        }

        if (!displayListPopupWindow.isShowing) {
            displayListPopupWindow.show()
        }
    }

    private fun makeSpannable(test: String) {
        val spannable = SpannableString(test)
        val matcher = Pattern.compile("@\\s*(\\w+)").matcher(test)
        while (matcher.find()) {
            val displayText = matcher.group(1)
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(textView: View) {
                    println(displayText)
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.color = Color.parseColor("#1DA1F2")
                }
            }
            val cityIndex = test.indexOf(displayText!!) - 1
            spannable.setSpan(clickableSpan, cityIndex, cityIndex + displayText.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        edt_post.setText("")
        edt_post.setText(spannable)
        edt_post.movementMethod = LinkMovementMethod.getInstance()
    }

    fun getStringBetweenTwoChars(input: String, startChar: String, endChar: String): String {

        var outPut = input
        try {
            val start = outPut.indexOf(startChar)
            if (start != -1) {
                val end = outPut.indexOf(endChar, start + startChar.length)
                if (end != -1) {
                    var str = outPut.substring(end + startChar.length, start)
                    // return outPut.substring(start + startChar.length, end)
                    return str
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        if (edt_post.text.toString() == outPut) {
            outPut = outPut.substring(outPut.lastIndexOf("@") + 1)
        }

        return outPut
    }

    private fun clearView() {
        if (displayListPopupWindow.isShowing) {
            displayListPopupWindow.dismiss()
        }
    }

}