package com.ablueclive.activity.adddPost;

import okhttp3.RequestBody;

public class AddPostPresenter implements AddPostContract.Presenter, AddPostContract.Model.OnFinishedListener {
    private AddPostContract.View addPostView;
    private final AddPostContract.Model addPostModel;

    public AddPostPresenter(AddPostContract.View addPostView) {
        this.addPostView = addPostView;
        this.addPostModel = new AddPostModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (addPostView != null) {
            addPostView.hideProgress();
            addPostView.setProductToViews(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (addPostView != null) {
            addPostView.hideProgress();
            addPostView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        addPostView = null;
    }

    @Override
    public void requestAddPost(RequestBody file) {
        if (addPostView != null) {
            addPostView.showProgress();
            addPostModel.addPost(this, file);
        }

    }
}
