package com.ablueclive.activity.adddPost;

import android.util.Log;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.networkClass.RetrofitClient;

import org.jetbrains.annotations.NotNull;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class AddPostModel implements AddPostContract.Model {
    private static final String TAG = "AddPostModel";

    @Override
    public void addPost(OnFinishedListener onFinishedListener, RequestBody file) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Observable<ResponseModel> apiCall = apiInterface.addPost(file);
        if (AddPostActvity.getData() != null) {
            apiCall = apiInterface.editPost(file);
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull ResponseModel responseModel) {
                        if (responseModel != null) {
                            String response_status = responseModel.getResponse_status();
                            String response_msg = responseModel.getResponse_msg();
                            String response_invalid = responseModel.getResponse_invalid();
                            onFinishedListener.onFinished(response_status, response_msg, response_invalid);
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        try {
                            Log.d(TAG, "" + e.getMessage());
                            onFinishedListener.onFailure(e);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
