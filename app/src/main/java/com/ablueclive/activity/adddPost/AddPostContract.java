package com.ablueclive.activity.adddPost;

import okhttp3.RequestBody;

public interface AddPostContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void addPost(OnFinishedListener onFinishedListener, RequestBody file);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setProductToViews(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestAddPost(RequestBody file);
    }
}
