package com.ablueclive.activity.changePwd;

import java.util.HashMap;

public interface ChangePwdContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, int response_invalid);

            void onFailure(Throwable t);
        }

        void changePwd(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg, int response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestChangePwd(HashMap<String, String> param);
    }
}
