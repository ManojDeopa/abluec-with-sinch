package com.ablueclive.activity.changePwd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.activity.mainActivity.MainActivity;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.HashMap;

import static com.ablueclive.utils.CommonMethod.isValidPassword;

public class ChangePwdActivity extends AppCompatActivity implements View.OnClickListener, ChangePwdContract.View {
    private ImageView img_back;
    private EditText edt_old_pwd, edt_new_password, edt_confirm_password;
    private Button btn_submit;
    private String session_token, old_password, new_password, confirm_password, user_email;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private static final String TAG = "ChangePwdActivity";
    private ChangePwdPresenter changePwdPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        findView();
    }

    // initialize object
    private void findView() {
        session_token = BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN);
        user_email = BMSPrefs.getString(getApplicationContext(), Constants.USER_EMAIL);
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        img_back = findViewById(R.id.img_back);
        edt_old_pwd = findViewById(R.id.edt_old_pwd);
        edt_new_password = findViewById(R.id.edt_new_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);
        btn_submit = findViewById(R.id.btn_submit);
        img_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    // validate signup form
    private String validateSignupForm() {
        old_password = edt_old_pwd.getText().toString().trim();
        new_password = edt_new_password.getText().toString().trim();
        confirm_password = edt_confirm_password.getText().toString().trim();

        if (old_password.isEmpty()) {
            edt_old_pwd.setFocusable(true);
            edt_old_pwd.requestFocus();
            return getString(R.string.valid_old_password);
        }

        if (old_password.length() < 8) {
            edt_old_pwd.setFocusable(true);
            edt_old_pwd.requestFocus();
            return getString(R.string.valid_password_digit);
        }

        if (new_password.isEmpty()) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.valid_new_password);
        }

        if (!isValidPassword(new_password)) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.alpha_numeric_password_validation);
        }

        if (new_password.length() < 8) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.valid_password_digit);
        }

        if (confirm_password.isEmpty()) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.valid_password_blank);
        }

        if (!confirm_password.contentEquals(new_password)) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.same_password);
        }
        return "success";
    }

    // change pwd request param
    private void changePwdParam() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("old_pass", old_password);
        params.put("new_pass", new_password);
        params.put("session_token", session_token);
        params.put("email", user_email);
        Log.d("changePwdParam", params.toString());
        changePwdPresenter = new ChangePwdPresenter(this);
        changePwdPresenter.requestChangePwd(params);
    }


    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }

            case R.id.btn_submit: {
                CommonMethod.hideKeyBoard(ChangePwdActivity.this);
                String result = validateSignupForm();
                if (result.equalsIgnoreCase("success")) {
                    if (isInternetPresent) {
                        changePwdParam();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }
        }
    }

    // backpress method
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        finish();
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, int response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
            CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), MainActivity.class));
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
            finish();
        } else if (response_invalid == 1) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, "" + throwable.getMessage());
    }

    // logout method
    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getApplicationContext());
        BMSPrefs.putString(getApplicationContext(), Constants._ID, "");
        BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, "");
        CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        finish();
    }

}
