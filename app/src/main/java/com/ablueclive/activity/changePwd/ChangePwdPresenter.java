package com.ablueclive.activity.changePwd;
import java.util.HashMap;

public class ChangePwdPresenter implements ChangePwdContract.Presenter, ChangePwdContract.Model.OnFinishedListener {
    private ChangePwdContract.View changePwdView;
    private final ChangePwdContract.Model changePwdModel;

    public ChangePwdPresenter(ChangePwdContract.View changePwdView) {
        this.changePwdView = changePwdView;
        this.changePwdModel = new ChangePwdModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, int response_invalid) {
        if (changePwdView != null) {
            changePwdView.hideProgress();
            changePwdView.setDataToViews(response_status, response_msg, response_invalid);
        }


    }

    @Override
    public void onFailure(Throwable t) {
        if (changePwdView != null) {
            changePwdView.hideProgress();
            changePwdView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        changePwdView = null;
    }

    @Override
    public void requestChangePwd(HashMap<String, String> param) {
        if (changePwdView != null) {
            changePwdView.showProgress();
            changePwdModel.changePwd(this, param);
        }

    }
}
