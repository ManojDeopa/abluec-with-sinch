package com.ablueclive.activity.playVideo

import AppUtils
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.ablueclive.R
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.activity_video_play.*

class VideoPlayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_play)
        findView()
    }

    private fun findView() {

        var mUrl = BMSPrefs.getString(applicationContext, "video_path")
        if (!AppUtils.isValidUrl(mUrl)) {
            mUrl = Constants.POST_IMAGE_URL + BMSPrefs.getString(applicationContext, "video_path")
        }

        println("videoUrl: $mUrl")

        val video = Uri.parse(mUrl)
        //Setting MediaController and URI, then starting the videoView
        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoView1)
        videoView1.setMediaController(mediaController)
        videoView1.setVideoURI(video)
        videoView1.requestFocus()
        videoView1.start()

        // Close the progress bar and play the video
        videoView1.setOnPreparedListener { mp: MediaPlayer? -> progrss.visibility = View.GONE }
    }
}