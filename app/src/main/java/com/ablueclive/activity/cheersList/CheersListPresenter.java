package com.ablueclive.activity.cheersList;
import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class CheersListPresenter implements CheerListContract.Presenter, CheerListContract.Model.OnFinishedListener {
    private CheerListContract.View cheersListView;
    private final CheerListContract.Model cheersListModel;

    public CheersListPresenter(CheerListContract.View cheersListView) {
        this.cheersListView = cheersListView;
        this.cheersListModel = new CheersListModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (cheersListView != null) {
            cheersListView.hideProgress();
        }
        cheersListView.setAllCheersList(response_status, response_msg, response_invalid, response_data);
    }

    @Override
    public void onFailure(Throwable t) {
        if (cheersListView != null) {
            cheersListView.hideProgress();
            cheersListView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        cheersListView = null;
    }

    @Override
    public void requestCheersList(HashMap<String, String> param) {
        if (cheersListView != null) {
            cheersListView.showProgress();
            cheersListModel.getCheersList(this, param);
        }

    }
}
