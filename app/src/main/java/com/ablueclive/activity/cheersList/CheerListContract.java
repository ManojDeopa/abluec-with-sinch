package com.ablueclive.activity.cheersList;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface CheerListContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void getCheersList(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {
        void showProgress();

        void hideProgress();

        void setAllCheersList(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestCheersList(HashMap<String, String> param);

    }
}
