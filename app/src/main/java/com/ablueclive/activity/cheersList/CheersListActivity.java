package com.ablueclive.activity.cheersList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.adapter.CheersListAdapter;
import com.ablueclive.modelClass.CheerList;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.ArrayList;
import java.util.HashMap;

public class CheersListActivity extends AppCompatActivity implements View.OnClickListener, CheerListContract.View {
    private RecyclerView recycler_all_cheers;
    private LinearLayoutManager linearLayoutManager;
    private String session_token, post_id;
    private boolean isInternetPresent = false;
    private CheersListAdapter cheersListAdapter;
    private ArrayList<CheerList> cheerListArrayList = new ArrayList<>();
    private int pageNo = 1;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheers_list);
        findView();
        if (isInternetPresent) {
            requestCheersList(pageNo);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }

    }

    // initialize object
    private void findView() {
        recycler_all_cheers = findViewById(R.id.recycler_all_cheers);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        session_token = BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN);
        post_id = BMSPrefs.getString(getApplicationContext(), Constants.FEED_POST_ID);
        ImageView img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        setCheersAdapter();
        setListeners();
    }

    private void setListeners() {
        recycler_all_cheers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                    isLoading = true;
                    requestCheersList(pageNo);
                }
            }
        });
    }


    //set friend adapter
    private void setCheersAdapter() {
        cheersListAdapter = new CheersListAdapter(getApplicationContext(), cheerListArrayList);
        recycler_all_cheers.setLayoutManager(linearLayoutManager);
        recycler_all_cheers.setAdapter(cheersListAdapter);
    }

    //cheers list api param
    private void requestCheersList(int pageNo) {
        HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("post_id", post_id);
        params.put("page", "" + pageNo);
        Log.d("CheersListParams", params.toString());
        //   allProductPresenter.getMoreData(params);
        CheersListPresenter cheersListPresenter = new CheersListPresenter(this);
        cheersListPresenter.requestCheersList(params);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }
        }
    }


    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setAllCheersList(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            isLoading = false;
            cheerListArrayList = response_data.getCheerList();
            if (cheerListArrayList.size() > 0) {
                cheersListAdapter.addAll(cheerListArrayList);
                pageNo++;
            } else {
                CommonMethod.showToastShort(getString(R.string.no_data_found), getApplicationContext());
            }

        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    // logout method
    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getApplicationContext());
        BMSPrefs.putString(getApplicationContext(), Constants._ID, "");
        BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, "");
        CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        finish();
    }
}
