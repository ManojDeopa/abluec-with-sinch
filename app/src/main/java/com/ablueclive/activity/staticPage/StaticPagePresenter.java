package com.ablueclive.activity.staticPage;
import com.ablueclive.modelClass.Response_data;
import java.util.HashMap;

public class StaticPagePresenter implements StaticPageContract.Presenter, StaticPageContract.Model.OnFinishedListener {
    private StaticPageContract.View staticPageView;
    private StaticPageContract.Model staticPageModel;

    public StaticPagePresenter(StaticPageContract.View staticPageView) {
        this.staticPageView = staticPageView;
        this.staticPageModel = new StaticPageModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, Response_data response_data) {
        if (staticPageView != null) {
            staticPageView.hideProgress();
        }
        staticPageView.setDataToViews(response_status, response_msg, response_data);
    }

    @Override
    public void onFailure(Throwable t) {
        if (staticPageView != null) {
            staticPageView.hideProgress();
        }
        staticPageView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        staticPageView = null;
    }

    @Override
    public void requestStaticResponse(HashMap<String, String> param) {
        if (staticPageView != null) {
            staticPageView.showProgress();
        }
        staticPageModel.getStaticResponse(this, param);
    }
}

