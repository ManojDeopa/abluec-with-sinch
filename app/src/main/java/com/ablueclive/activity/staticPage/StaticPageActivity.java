package com.ablueclive.activity.staticPage;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.HashMap;

public class StaticPageActivity extends AppCompatActivity implements StaticPageContract.View, View.OnClickListener {
    private TextView text_title;
    private WebView web_view;
    private String page_name;
    private static final String TAG = "StaticPageActivity";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_page_api);
        findView();

        String url = getIntent().getStringExtra("url");
        String title = getIntent().getStringExtra("title");
        text_title.setText(title);

        /*  if (isInternetPresent) {
            requestStaticPageParam();
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }*/


        ProgressBar progress_bar = findViewById(R.id.progressBar);
        WebView webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.loadUrl(url);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progress_bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progress_bar.setVisibility(View.GONE);
            }
        });


    }

    // initialize object...
    private void findView() {
        page_name = BMSPrefs.getString(getApplicationContext(), Constants.STATIC_PAGE_NAME);
        ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
        boolean isInternetPresent = connectionDetector.isConnectingToInternet();
        text_title = findViewById(R.id.text_title);
        ImageView img_back = findViewById(R.id.img_back);
        web_view = findViewById(R.id.web_view);
        web_view.getSettings().setJavaScriptEnabled(true);
        img_back.setOnClickListener(this);
    }


    //api param
    private void requestStaticPageParam() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("page_name", page_name);
        Log.d("StaticPageParam", hashMap.toString());
        StaticPagePresenter staticPagePresenter = new StaticPagePresenter(this);
        staticPagePresenter.requestStaticResponse(hashMap);
    }


    // button listener
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }
        }
    }


    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }


    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            text_title.setText(response_data.getTitle());
            web_view.loadData(response_data.getDesc(), "text/html", "UTF-8");
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, "" + throwable.getMessage());
    }

}
