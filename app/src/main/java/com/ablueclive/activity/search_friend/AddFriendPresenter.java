package com.ablueclive.activity.search_friend;

import java.util.HashMap;

public class AddFriendPresenter implements AddFriendContract.Presenter, AddFriendContract.Model.OnFinishedListener {
    private AddFriendContract.View addFrndView;
    private AddFriendContract.Model addFrndModel;

    public AddFriendPresenter(AddFriendContract.View addFrndView) {
        this.addFrndView = addFrndView;
        this.addFrndModel = new AddFriendModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (addFrndView != null) {
            addFrndView.hideProgress();
            addFrndView.addFreindResponse(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (addFrndView != null) {
            addFrndView.hideProgress();
            addFrndView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        addFrndView = null;
    }

    @Override
    public void requestAddFriend(HashMap<String, String> param) {
        if (addFrndView != null) {
            addFrndView.showProgress();
            addFrndModel.addFriend(this, param);
        }

    }
}
