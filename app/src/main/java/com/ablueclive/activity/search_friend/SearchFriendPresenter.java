package com.ablueclive.activity.search_friend;
import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class SearchFriendPresenter implements SearchFriendContract.Presenter, SearchFriendContract.Model.OnFinishedListener {
    private SearchFriendContract.View searchFrndView;
    private SearchFriendContract.Model searchFrndModel;

    public SearchFriendPresenter(SearchFriendContract.View searchFrndView) {
        this.searchFrndView = searchFrndView;
        this.searchFrndModel = new SearchFriendModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (searchFrndView != null) {
         //   searchFrndView.hideProgress();
        }
        searchFrndView.setDataToViews(response_status, response_msg,response_invalid, response_data);
    }

    @Override
    public void onFailure(Throwable t) {
        if (searchFrndView != null) {
          //  searchFrndView.hideProgress();
        }
        searchFrndView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        searchFrndView = null;
    }

    @Override
    public void requestSearchFriend(HashMap<String, String> param) {
        if (searchFrndView != null) {
       //     searchFrndView.showProgress();
        }
        searchFrndModel.getFriendList(this, param);
    }
}
