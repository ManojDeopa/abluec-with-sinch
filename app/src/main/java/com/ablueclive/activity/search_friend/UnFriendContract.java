package com.ablueclive.activity.search_friend;

import java.util.HashMap;

public interface UnFriendContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void unFriend(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void unFriendResponse(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestUnFriend(HashMap<String, String> param);
    }
}
