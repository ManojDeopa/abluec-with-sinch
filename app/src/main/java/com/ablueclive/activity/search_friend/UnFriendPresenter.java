package com.ablueclive.activity.search_friend;
import java.util.HashMap;

public class UnFriendPresenter implements UnFriendContract.Presenter, UnFriendContract.Model.OnFinishedListener {
    private UnFriendContract.View unFrndView;
    private UnFriendContract.Model unFrndModel;

    public UnFriendPresenter(UnFriendContract.View unFrndView) {
        this.unFrndView = unFrndView;
        this.unFrndModel = new UnFriendModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (unFrndView != null) {
            unFrndView.hideProgress();
            unFrndView.unFriendResponse(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (unFrndView != null) {
            unFrndView.hideProgress();
            unFrndView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        unFrndView = null;
    }

    @Override
    public void requestUnFriend(HashMap<String, String> param) {
        if (unFrndView != null) {
            unFrndView.showProgress();
            unFrndModel.unFriend(this, param);
        }

    }
}
