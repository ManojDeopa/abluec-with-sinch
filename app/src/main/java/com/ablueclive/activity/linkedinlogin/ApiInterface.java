package com.ablueclive.activity.linkedinlogin;


import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by admin on 4/17/2017.
 */
public interface ApiInterface {

    @Headers("Content-Type: application/x-www-form-urlencoded")

    @POST("accessToken")
    @FormUrlEncoded
    Call<AccessTokenResponse> access_token (@FieldMap HashMap<String, String> map);

    @Headers("Content-Type: application/x-www-form-urlencoded")

    @GET("emailAddress")
    Call<JSONObject> getEmail (@Header("Authorization") String st, @QueryMap HashMap<String, String> map);


}
