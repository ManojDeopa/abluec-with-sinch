package com.ablueclive.activity.linkedinlogin;





import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 4/17/2017.
 */
public class ApiClient {


    public static final String BASE_URL = "https://www.linkedin.com/oauth/v2/";



    public static Retrofit getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


            return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.readTimeout(200, TimeUnit.SECONDS).writeTimeout(200, TimeUnit.SECONDS).build())
                    .build();
        }


        public static Retrofit getEmailClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


            return new Retrofit.Builder()
                    .baseUrl("https://api.linkedin.com/v2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.readTimeout(200, TimeUnit.SECONDS).writeTimeout(200, TimeUnit.SECONDS).build())
                    .build();
        }



 }





