package com.ablueclive.activity.linkedinlogin;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ablueclive.R;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class LinkedInLoginClass extends AppCompatActivity {

    private WebView mWebView = null;
    public static String emailId;
    public static String idLinkedin, firstName, lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linkedin_actvity);


        mWebView = (WebView) findViewById(R.id.main_activity_web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new LinkedInWebViewClient());

        mWebView.loadUrl("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86irj0djc7eoku&redirect_uri=https://www.appypie.com&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social");


    }

    private class LinkedInWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains("https://www.appypie.com")) {
                int position = url.indexOf("=");
                int lastPosition = url.indexOf("&state");

                String token = url.substring(position + 1, lastPosition);

                Log.d("access_token", token);


                call(token);

            }

            Log.d("url", url);
            view.loadUrl(url);
            return true;
        }
    }

    private void call(String token) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("grant_type", "authorization_code");
        map.put("redirect_uri", "https://www.appypie.com");
        map.put("client_id", "86irj0djc7eoku");
        map.put("client_secret", "UOfj96JTC79adH9k");
        map.put("code", token);

        Call<AccessTokenResponse> call = apiService.access_token(map);
        call.enqueue(new Callback<AccessTokenResponse>() {
            @Override
            public void onResponse(Call<AccessTokenResponse> call, retrofit2.Response<AccessTokenResponse> response) {
                Log.d("access_token", response.body().getAccess_token());
                getEmailId(response.body().getAccess_token());
            }

            @Override
            public void onFailure(Call<AccessTokenResponse> call, Throwable t) {
                Log.d("error", t.getMessage());


            }
        });


    }

    private void getEmailId(final String token) {

        /*ApiInterface apiService = ApiClient.getEmailClient().create(ApiInterface.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("q", "members");
        map.put("projection", "(elements*(handle~))");
        Call<JSONObject> call = apiService.getEmail("Bearer "+token,map);
        call.enqueue(new Callback<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<JSONObject> call, retrofit2.Response<JSONObject> response) {
                try {




                    String elements= response.body().getString("elements");
                    JsonObject jo=new JsonObject(elements);
                    JSONArray array=new JSONArray(jo);
                    String email=array.getJSONObject(0).getString("handle~`");
                    JSONObject jo1=new JSONObject(email);

                    Log.d("Final_email",jo1.getString("emailAddress"));



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.d("error", t.getMessage());


            }
        });*/


        RequestQueue volleyRequest = Volley.newRequestQueue(LinkedInLoginClass.this);
        StringRequest myReq = new StringRequest(Request.Method.GET, "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))",
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.d("success", response.toString());
                        try {

                            JSONObject JO = new JSONObject(response);
                            String elements = JO.optString("elements");
                            JSONArray array = new JSONArray(elements);
                            JSONObject jo = array.getJSONObject(0);
                            String handle = jo.optString("handle~");
                            JSONObject jo1 = new JSONObject(handle);

                            emailId = jo1.getString("emailAddress");




                            Log.d("Final_email", jo1.getString("emailAddress"));
                            getprofileData(token, emailId);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("error", error.getMessage());

            }
        }) {
            protected HashMap<String, String> getParams() throws com.android.volley.AuthFailureError {
                HashMap<String, String> kvPair = new HashMap<String, String>();
                // kvPair.put("cat_slug", "hellllllo");
                //System.out.println("register request" + kvPair);
                return kvPair;
            }

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        myReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyRequest.add(myReq);


    }

    private void getprofileData(String token, String emailId) {


        RequestQueue volleyRequest = Volley.newRequestQueue(LinkedInLoginClass.this);
        StringRequest myReq = new StringRequest(Request.Method.GET, "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))",
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.d("success", response.toString());
                        try {

                            JSONObject JO = new JSONObject(response);
                            String id = JO.getString("id");
                            String firstNameob = JO.getString("firstName");
                            JSONObject jo1 = new JSONObject(firstNameob);
                            String firstNameob2 = jo1.getString("localized");
                            JSONObject jo2 = new JSONObject(firstNameob2);
                            String finalFirstName = jo2.getString("en_US");

                            String lastNameob = JO.getString("lastName");
                            JSONObject lo1 = new JSONObject(lastNameob);
                            String lastNameob2 = jo1.getString("localized");
                            JSONObject lo2 = new JSONObject(lastNameob2);
                            String finallastName = jo2.getString("en_US");

                          /*  JSONArray jsonArray = JO.getJSONArray("identifiers");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = (JSONObject) jsonArray.get(i);
                                String imageUrl = object.optString("identifier");
                            }*/




                            idLinkedin = id;
                            firstName = finalFirstName;
                            lastName = finallastName;

                            Log.d("firstName", "onResponse: " + finalFirstName);

                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("error", error.getMessage());

            }
        }) {
            protected HashMap<String, String> getParams() throws com.android.volley.AuthFailureError {
                HashMap<String, String> kvPair = new HashMap<String, String>();
                // kvPair.put("cat_slug", "hellllllo");
                //System.out.println("register request" + kvPair);
                return kvPair;
            }

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        myReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyRequest.add(myReq);
    }
}
