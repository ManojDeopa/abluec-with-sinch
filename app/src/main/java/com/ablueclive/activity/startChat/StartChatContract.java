package com.ablueclive.activity.startChat;

import java.util.HashMap;

public interface StartChatContract {

    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void getLastMsg(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

//        void showProgress();
//
//        void hideProgress();

        void setDataToViews(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestLastMsg(HashMap<String, String> param);
    }
}
