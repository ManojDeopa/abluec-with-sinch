package com.ablueclive.activity.startChat;

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DownloadManager
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.location.Address
import android.location.Geocoder
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.frndProfile.FriendProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.fcmClass.MyFirebaseMessagingService
import com.ablueclive.fcmClass.MyFirebaseMessagingService.Companion.isOnChatScreen
import com.ablueclive.fragment.allFriendListChat.ReadUnreadContract
import com.ablueclive.fragment.allFriendListChat.ReadUnreadPresenter
import com.ablueclive.fragment.feedFragment.FeedFragment
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.ReportUserReasonData
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.sinch.BaseActivity
import com.ablueclive.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.theartofdev.edmodo.cropper.CropImage
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.report_dialog_for_chat.*
import java.io.*
import java.util.*
import kotlin.collections.ArrayList


class StartChatActivity : BaseActivity(), View.OnClickListener,
        StartChatContract.View,
        ReadUnreadContract.View,
        OnChatActions,
        ChatAdapter.ActionOnChatScreen {

    private lateinit var file: Uri
    private lateinit var img_back: ImageView

    private lateinit var imgAttachment: ImageView
    private lateinit var actionVideo: ImageView
    private lateinit var actionCallVoice: ImageView
    private lateinit var actionMenu: ImageView
    private lateinit var edt_chat: EditText
    private lateinit var text_title: TextView

    private var session_token = ""
    private var user_id = ""
    private var user_name = ""
    private var to_id = ""
    private var finalNode = ""
    private var chat_user_name = ""
    private lateinit var linear_submit: LinearLayout
    private lateinit var recycler_chat_list: RecyclerView
    private lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private lateinit var chatListAdapter: ChatAdapter
    private lateinit var myRef: DatabaseReference

    private var uploadUrl: Uri? = null
    private var audioUrl = ""
    private var UploadType = ""
    private lateinit var mStorage: StorageReference


    private var chatMap: Map<*, *>? = null
    private val RESULT_LOAD_IMAGE = 101
    private val RESULT_LOAD_FILE = 102
    private val REQUEST_CODE_TAKE_PICTURE = 111
    private val RESULT_AUDIO = 103
    private val REQUEST_CODE_VIDEO = 104
    private val REQUEST_CODE_LOCATION = 105
    private val REQUEST_CODE_SUSPISIOUS_LIST = 106
    private var lastMessageValue = ""
    private var mtype = ""
    private var messageType = ""
    private var assetImageUrl = ""
    private var videoUrl = ""
    private var videoThumb = ""
    private var currentLat = ""
    private var currentLng = ""
    private var chatUserImage = ""
    private var imagedata: ByteArray? = null
    private val messageList: ArrayList<Map<String, String>> = ArrayList()
    private var PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 120
    private var blockStatus = 0
    private var muteStatus = 0
    private var friend_mobile = ""
    private var locationPermissionGranted = false
    private lateinit var alertDialog: AlertDialog
    private lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_chat_new)
        findView()
        if (isInternetPresent) {
            readMessage()
            requestReadStatus("1")
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), applicationContext)
        }

    }


    private fun findView() {

        context = this
        ProgressD.show(this, "")

        to_id = BMSPrefs.getString(applicationContext, Constants.TO_ID)
        friend_mobile = BMSPrefs.getString(applicationContext, Constants.FRIEND_MOBILE)
        chat_user_name = BMSPrefs.getString(applicationContext, Constants.CHAT_USER_NAME)
        chatUserImage = BMSPrefs.getString(applicationContext, Constants.CHAT_USER_IMAGE)

        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        user_id = BMSPrefs.getString(this, Constants._ID)
        user_name = BMSPrefs.getString(applicationContext, Constants.USER_NAME)

        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector.isConnectingToInternet
        img_back = findViewById(R.id.img_back)
        edt_chat = findViewById(R.id.edt_chat)

        linear_submit = findViewById(R.id.linear_submit)
        recycler_chat_list = findViewById(R.id.recycler_chat_list)
        text_title = findViewById(R.id.action_title)

        imgAttachment = findViewById(R.id.imgAttachment)
        actionVideo = findViewById(R.id.actionVideo)
        actionCallVoice = findViewById(R.id.actionCallVoice)
        actionMenu = findViewById(R.id.actionMenu)

        img_back.setOnClickListener(this)
        linear_submit.setOnClickListener(this)

        imgAttachment.setOnClickListener(this)
        actionMenu.setOnClickListener(this)
        actionCallVoice.setOnClickListener(this)
        actionVideo.setOnClickListener(this)


        text_title.text = chat_user_name
        text_title.setOnClickListener {
            BMSPrefs.putString(context, Constants.FRIEND_ID, to_id)
            CommonMethod.callActivity(context, Intent(context, FriendProfileActivity::class.java))
        }

        setAdapter()
        getNode()
    }

    @SuppressLint("NonConstantResourceId")
    override fun onClick(v: View) {
        when (v.id) {

            R.id.img_back -> {
                onBackPressed()
            }

            R.id.imgAttachment -> {

                if (blockStatus == 0) {
                    checkOtherBlock {
                        openMediaDialog()
                    }
                } else {
                    unblockDialog()
                }
                // openMediaDialog()
            }
            R.id.actionMenu -> {

                var muteNotificationStr = getString(R.string.mute_notification)
                if (muteStatus == 1) {
                    muteNotificationStr = getString(R.string.un_mute_notification)
                }

                var blockStatusStr = getString(R.string.un_block)
                if (blockStatus == 0) {
                    blockStatusStr = getString(R.string.block)
                }

                val itemList = mutableListOf(
                        getString(R.string.delete_chat),
                        muteNotificationStr,
                        getString(R.string.report),
                        blockStatusStr)

                if (messageList.size == 0) {
                    itemList.remove(getString(R.string.delete_chat))
                }

                CommonMethod.popupMenuTextList(context, actionMenu, itemList.toTypedArray(), object : MenuItemClickListener {
                    override fun onMenuItemClick(text: String, position: Int) {

                        when (text) {
                            getString(R.string.delete_chat) -> {
                                val builder = AlertDialog.Builder(context, R.style.alertDialogTheme)
                                builder.setTitle(getString(R.string.delete_chat))
                                builder.setMessage(getString(R.string.delete_chat_msg))
                                builder.setPositiveButton(getString(R.string.yes)) { dialogInterface: DialogInterface, i: Int ->
                                    dialogInterface.dismiss()
                                    try {
                                        deleteChat()
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                                builder.setNegativeButton(getString(R.string.no)) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
                                builder.create().show()
                            }

                            getString(R.string.report) -> {
                                reportUserReason()
                            }

                            blockStatusStr -> {
                                blockUserUpdate(to_id, getBlockStatus(blockStatusStr))
                            }

                            muteNotificationStr -> {
                                updateMuteStatus(to_id, getMuteStatus(muteNotificationStr))
                            }
                        }
                    }

                })
            }
            R.id.actionCallVoice -> {
                actionVoiceCall(to_id, chat_user_name, chatUserImage)
            }
            R.id.actionVideo -> {
                actionVideoCall(to_id, chat_user_name, chatUserImage)
            }
            R.id.linear_submit -> {

                if (blockStatus == 0) {
                    checkOtherBlock { actionSendText() }
                } else {
                    unblockDialog()
                }

                // actionSendText()
            }
        }
    }

    private fun dataFromFirebase() {

        myRef = FirebaseDatabaseRef.getInstant().getFirebaseRef(finalNode)
        val childEventListener = object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                val map: MutableMap<String, String>? = dataSnapshot.value as MutableMap<String, String>?
                messageList.add(map!!)
                if (messageList.size > 0) {
                    notifyAdapter()
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                /*val map: MutableMap<String, String>? = p0.value as MutableMap<String, String>?
                messageList.remove(map!!)*/
                messageList.clear()
                notifyAdapter()
                deleteChat(to_id)
            }

        }
        myRef.addChildEventListener(childEventListener)
    }


    override fun onDestroy() {
        super.onDestroy()

        if (isInternetPresent) {
            BMSPrefs.putString(applicationContext, "status", "")
            requestReadStatus("0")
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), applicationContext)
        }
    }


    private fun deleteChat(to_user_id: String) {
        val hashMap = HashMap<String, String>()
        hashMap["to_user_id"] = to_user_id
        hashMap["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        Log.d("deleteChat : ", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteChat(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {}
                })
    }

    private fun blockUserUpdate(to_user_id: String, status: String) {
        val hashMap = HashMap<String, String>()
        hashMap["to_user_id"] = to_user_id
        hashMap["status"] = status
        hashMap["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        Log.d("blockUserUpdate : ", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.blockChat(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        CommonMethod.showToastlong(responseModel.response_msg, context)
                        if (responseModel.response_status == "1") {
                            blockStatus = if (status.equals("1", true)) {
                                1
                            } else {
                                0
                            }
                        }
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {}
                })
    }

    private fun reportUserReason() {
        ProgressD.show(context, "")
        val hashMap = HashMap<String, String>()
        hashMap["session_token"] = session_token
        Log.d("reportUserReason : ", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.reportUserReason(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        ProgressD.hide()
                        if (responseModel.response_status.equals("1") && !responseModel.response_data.reasons.isNullOrEmpty()) {
                            reportUser(responseModel.response_data.reasons)
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                    override fun onComplete() {}
                })
    }


    private fun reportUser(to_user_id: String, reasonId: String, reason_txt: String) {
        ProgressD.show(this, "")
        val hashMap = HashMap<String, String>()
        hashMap["to_user_id"] = to_user_id
        hashMap["reason_id"] = reasonId
        hashMap["reason_txt"] = reason_txt
        hashMap["session_token"] = session_token
        Log.d("reportUser : ", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.reportUser(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        ProgressD.hide()
                        CommonMethod.showToastlong(responseModel.response_msg, context)
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }

                    override fun onComplete() {}
                })
    }

    private fun updateMuteStatus(to_user_id: String, status: String) {
        val hashMap = HashMap<String, String>()
        hashMap["to_user_id"] = to_user_id
        hashMap["status"] = status
        hashMap["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        Log.d("updateMuteStatus : ", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.muteChatNotification(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        CommonMethod.showToastlong(responseModel.response_msg, context)
                        if (responseModel.response_status == "1") {
                            muteStatus = if (status.equals("0", true)) {
                                0
                            } else {
                                1
                            }
                        }
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {}
                })
    }


    //api param
    private fun requestLastMsgParam(msg: String?, messagevalue: String) {
        val hashMap = HashMap<String, String?>()
        hashMap["to_user_id"] = to_id
        hashMap["from_user_name"] = user_name
        hashMap["session_token"] = session_token
        hashMap["timestamp"] = "" + System.currentTimeMillis()
        hashMap["last_message"] = msg
        hashMap["chat_type"] = messagevalue
        hashMap["mobile"] = ""
        Log.d("LastMsgParam", hashMap.toString())
        val startChatPresenter = StartChatPresenter(this)
        startChatPresenter.requestLastMsg(hashMap)
    }

    // read unread api param
    private fun requestReadStatus(online_status: String) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["online_status"] = online_status
        params["to_user_id"] = to_id
        Log.d("ReadStatus", params.toString())
        val readUnreadPresenter = ReadUnreadPresenter(this)
        readUnreadPresenter.requestReadStatus(params)
    }

    private fun dialogForVideoOrPicture() {
        alertDialog = AlertDialog.Builder(this, R.style.alertDialogTheme)
                .setTitle(getString(R.string.choose_option))
                .setPositiveButton(getString(R.string.video)) { dialogInterface: DialogInterface?, i: Int ->
                    val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                    if (takeVideoIntent.resolveActivity(this.packageManager) != null) {
                        startActivityForResult(takeVideoIntent, REQUEST_CODE_VIDEO)
                    }
                    mtype = "3"
                    messageType = "Video"
                    alertDialog.dismiss()

                }
                .setNegativeButton(getString(R.string.photo)) { dialogInterface: DialogInterface?, i: Int ->
                    takePicture()
                    alertDialog.dismiss()
                }
                .setOnCancelListener { dialogInterface: DialogInterface? -> alertDialog.dismiss() }
                .create()
        alertDialog.show()
    }


    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    private fun dialogForPhotoOrVideo() {
        alertDialog = AlertDialog.Builder(this, R.style.alertDialogTheme)
                .setTitle(getString(R.string.choose_option))
                .setPositiveButton(getString(R.string.video)) { dialogInterface: DialogInterface?, i: Int ->
                    openGalleryForVideo()
                    mtype = "3"
                    messageType = "Video"
                    alertDialog.dismiss()

                }
                .setNegativeButton(getString(R.string.photo)) { dialogInterface: DialogInterface?, i: Int ->
                    val intent = Intent()
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_GET_CONTENT
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE)
                }
                .setOnCancelListener { dialogInterface: DialogInterface? -> alertDialog.dismiss() }
                .create()

        alertDialog.show()
    }


    private fun actionSendText() {
        if (edt_chat.text.toString().trim() != "") {
            mtype = "1"
            messageType = edt_chat.text.toString().trim()
            sendMEssage(null, "0")
        }
    }

    private fun checkOtherBlock(bar: () -> Unit) {
        val hashMap = HashMap<String, String>()
        hashMap["session_token"] = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        hashMap["to_user_id"] = to_id
        Log.d("DeleteChat", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.checkOtherBlockYou(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        try {
                            if (responseModel.response_status.equals("1")) {
                                bar()
                            } else {
                                val builder = AlertDialog.Builder(context, R.style.alertDialogTheme)
                                builder.setMessage(responseModel.response_msg)
                                builder.setPositiveButton(getString(R.string.ok)) { dialogInterface: DialogInterface, i: Int ->
                                    dialogInterface.dismiss()
                                    try {
                                        finish()
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                                builder.create().show()
                            }
                        } catch (e: Exception) {
                        }
                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {}
                })
    }


    private fun unblockDialog() {
        val builder = AlertDialog.Builder(context, R.style.alertDialogTheme)
        builder.setTitle(getString(R.string.block))
        builder.setMessage(getString(R.string.block_user_text))
        builder.setPositiveButton(getString(R.string.un_block)) { dialogInterface: DialogInterface, i: Int ->
            dialogInterface.dismiss()
            try {
                blockUserUpdate(to_id, "0")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        builder.setNegativeButton(getString(R.string.ok)) { dialogInterface: DialogInterface, i: Int -> dialogInterface.dismiss() }
        builder.create().show()
    }

    private fun getBlockStatus(stringVal: String): String {
        return if (!stringVal.equals(getString(R.string.block), true)) {
            "0"
        } else {
            "1"
        }

    }

    private fun getMuteStatus(stringVal: String): String {
        return if (!stringVal.equals(getString(R.string.mute_notification), true)) {
            "0"
        } else {
            "1"
        }

    }

    private fun reportUser(reasons: ArrayList<ReportUserReasonData>) {
        val dialog = Dialog(context, R.style.alertDialogTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.report_dialog_for_chat)
        val window = dialog.window
        window!!.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
//        val string = SpannableString("Other (Free text up to 10 words)")
//        val string = SpannableString("  Other (Free text up to 10 words)")
//        string.setSpan(RelativeSizeSpan(0.7f), 7, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//        string.setSpan(ForegroundColorSpan(Color.BLACK), 7, 34, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
//        dialog.radio_6.text = string
//        dialog.radio_6.setOnCheckedChangeListener { p0, p1 ->
//            if (p1) {
//                dialog.relativeEditBox.visibility = View.VISIBLE
//            } else {
//                dialog.relativeEditBox.visibility = View.GONE
//            }
//        }

        for (data in reasons) {
            dialog.mainLinear.addView(addSingleReasonRow(data, dialog.mainLinear))
        }

        dialog.btn_submit.setOnClickListener {

            for (i in 0 until dialog.mainLinear.childCount) {
                val relMain = dialog.mainLinear.getChildAt(i) as RelativeLayout
                val button = relMain.getChildAt(0) as AppCompatRadioButton
                val relativeOther = relMain.getChildAt(1) as RelativeLayout
                val editText = relativeOther.getChildAt(0) as EditText
                val singleData = button.tag as ReportUserReasonData
                if (button.isChecked) {
                    if (relativeOther.visibility == View.VISIBLE && editText.text.toString().trim().isEmpty()) {
                        CommonMethod.showToastShort(getString(R.string.please_enter_reason), this)
                    } else {
                        reportUser(to_id, reasonId = singleData._id!!, editText.text.toString().trim())
                        dialog.cancel()
                    }
                }
            }

        }
        dialog.crossImage.setOnClickListener { dialog.cancel() }
        dialog.show()
    }

    private fun addSingleReasonRow(data: ReportUserReasonData, linearLayout: LinearLayout): View {
        val childView = layoutInflater.inflate(R.layout.layout_single_reason, linearLayout, false) as RelativeLayout
        val radioSingle = childView.findViewById<AppCompatRadioButton>(R.id.radioSingle)
        val editTextRelative = childView.findViewById<RelativeLayout>(R.id.relativeEditBox)
        val editText = childView.findViewById<EditText>(R.id.editOther)
        radioSingle.tag = data
        radioSingle.text = data.name
        radioSingle.setOnClickListener {
            for (i in 0 until linearLayout.childCount) {
                val relMain = linearLayout.getChildAt(i) as RelativeLayout
                val button = relMain.getChildAt(0) as AppCompatRadioButton
                button.isChecked = false
            }
            radioSingle.isChecked = true
            if (radioSingle.text.toString().trim().contains("other")) {
                editTextRelative.visibility = View.VISIBLE
            } else {
                editTextRelative.visibility = View.GONE
            }
        }
        return childView
    }

    override fun onLocation() {
        getLocationPermission()
    }

    override fun onGallery() {
        dialogForPhotoOrVideo()
    }

    override fun onCamera() {
        dialogForVideoOrPicture()
    }

    override fun onContact() {

    }

    override fun onVideo() {
        openGalleryForVideo()
        mtype = "3"
        messageType = "Video"
    }

    private fun openMediaDialog() {
        val sharePostDialog = OptionMediaDialog(this)
        sharePostDialog.show(supportFragmentManager, "f")

    }

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */

        val result = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED) {
            updateLocation()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    private fun updateLocation() {
        val gpsTracker = GPSTracker(context)
        if (gpsTracker.canGetLocation()) {
            currentLat = gpsTracker.latitude.toString()
            currentLng = gpsTracker.longitude.toString()
            mtype = "4"
            messageType = "Location"
            sendMEssage(null, "0")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                    updateLocation()
                }
            }
        }
    }

    private fun openGalleryForVideo() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_PICK
        startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_CODE_VIDEO)
    }

    //api response
    override fun setDataToViews(response_status: String, response_msg: String, response_invalid: String) {
        when {
            response_status.equals("1", ignoreCase = true) -> {
                //    CommonMethod.showToastShort(response_msg, getApplicationContext());
            }
            response_invalid.equals("1", ignoreCase = true) -> {
                logout(response_msg)
            }
            else -> {
                CommonMethod.showToastShort(response_msg, applicationContext)
            }
        }
    }

    override fun setReadStatus(response_status: String, response_msg: String, response_invalid: String) {
        when {
            response_status.equals("1", ignoreCase = true) -> {
            }
            response_invalid.equals("1", ignoreCase = true) -> {
                logout(response_msg)
            }
            else -> {
                CommonMethod.showToastShort(response_msg, applicationContext)
            }
        }
    }

    private fun compressImageFile(fileUri: Uri?) {
        val bmp = MediaStore.Images.Media.getBitmap(contentResolver, fileUri)
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos)
        imagedata = baos.toByteArray()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (REQUEST_CODE_TAKE_PICTURE == requestCode) {
                try {
                    CropImage.activity(file).start(this)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (data == null) return

            when (requestCode) {
                CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                    val result = CropImage.getActivityResult(data)
                    if (resultCode == Activity.RESULT_OK) {
                        val resultUri = result.uri
                        val fileUri = resultUri
                        val path = resultUri.path
                        compressImageFile(fileUri)
                        UploadType = "1"
                        mtype = "2"
                        messageType = "Image"
                        lastMessageValue = "2"
                        sendMEssage(fileUri, "1")
//                        showProgressDialog(false)
                    }
                }

                RESULT_LOAD_IMAGE -> {
                    val selectedImage = data.data
                    CropImage.activity(selectedImage).start(this)
                }


                RESULT_LOAD_FILE -> {
                    val fileUri = data.data
                    if (fileUri != null) {
                        val fileExtension = getFileExtension(fileUri)
                        UploadType = "5"
                        mtype = "5"
                        messageType = "Document"
                        lastMessageValue = "5"
                        sendMEssage(fileUri, "1", fileUri.path!!, fileExtension)
                    }

                }

                RESULT_AUDIO -> {
//                    val uri = data!!.data
//                    fileUri = uri
//                    UplaodType = "2"
//                    upload()
                }
                REQUEST_CODE_VIDEO -> {
                    val uri = data.data
//                    showProgressDialog(false)
                    if (uri != null) {
                        try {
                            val thumbBitmap = ThumbnailUtils.createVideoThumbnail(getPath(uri), MediaStore.Images.Thumbnails.MINI_KIND)
                            val s: String = File(getSelectedVideos(data)).name
                            val ss = s.substring(0, s.lastIndexOf("."))
                            videoThumb = thumbBitmap?.let { storeImage(it, ss).toString() }.toString()
                            UploadType = "3"
                            lastMessageValue = "3"
                            sendMEssage(uri, "1")
                        } catch (e: Exception) {
                        }
                    }

                }

                REQUEST_CODE_LOCATION -> {
                    currentLat = data.getStringExtra("lat")!!
                    currentLng = data.getStringExtra("lng")!!
                    sendMEssage(null, "0")
                }

                REQUEST_CODE_SUSPISIOUS_LIST -> {
//                    messageType = data!!.getStringExtra("suspisous_name")
//                    assetImageUrl = data!!.getStringExtra("suspisous_image")
//                    currentLat = Preferences.getData(PrefKeys.CURRENT_LAT, "")
//                    currntLng = Preferences.getData(PrefKeys.CURRENT_LONG, "")
//                    sendMEssage()
                }

            }
        }
    }

    private fun getFileExtension(uri: Uri?): String {
        val extension: String
        val contentResolver = contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        extension = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri!!))!!
        return extension
    }

    fun getOutputMediaFile(name: String, thumb: Bitmap?): File? {
        // To be safe, you should check that the SDCard is mountedo
        // using Environment.getExternalStorageState() before doing getActivity().
        val mediaStorageDir: File = File(this.filesDir.toString() + "/Android/data/" + this.packageName + "/Files")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        //String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        val mediaFile: File
        val mImageName = "$name.jpg"
        mediaFile = File(mediaStorageDir.path + File.separator + mImageName)
        return mediaFile
    }

    private fun storeImage(thumb: Bitmap, name: String): String? {
        val pictureFile: File = getOutputMediaFile(name, thumb)!!
        val videoThumbPath = pictureFile.path
        try {
            val fos = FileOutputStream(pictureFile)
            thumb.compress(Bitmap.CompressFormat.PNG, 90, fos)
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.d("error_saving", "File not found:" + e.message)
        } catch (e: IOException) {
            Log.d("error_saving", "Error accessing file: " + e.message)
        }
        return pictureFile.absolutePath
    }

    // logout method
    private fun getSelectedVideos(data: Intent): String {
        val result: MutableList<String> = ArrayList()
        var clipData: ClipData? = null
        clipData = data.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                val videoItem = clipData.getItemAt(i)
                val videoURI = videoItem.uri
                val filePath = Utility.getPath(this, videoURI)
                result.add(filePath)
            }
        } else {
            val videoURI = data.data
            val filePath = Utility.getPath(this, videoURI)
            result.add(filePath)
        }
        return result.toString()
    }


    private fun upload(uri: Uri?, size: Int, fileName: String = "") {
        when {
            UploadType == "1" -> {
                mStorage = FirebaseStorage.getInstance().getReference("Images")
            }
            UploadType == "2" -> {
                mStorage = FirebaseStorage.getInstance().getReference("Audios")
            }
            UploadType == "3" -> {
                mStorage = FirebaseStorage.getInstance().getReference("Videos")
            }
            UploadType == "5" -> {
                mStorage = FirebaseStorage.getInstance().getReference("Document")
            }
        }
        val mReference = mStorage.child(uri!!.lastPathSegment!!)
        try {
            mReference.putFile(uri).addOnSuccessListener {
                mReference.downloadUrl
                        .addOnSuccessListener { url ->
                            when {
                                UploadType == "1" -> {
                                    assetImageUrl = url.toString()
                                    uploadUrl = null
                                    val data = messageList[size - 1] as MutableMap<String, String>
                                    data["image"] = assetImageUrl
                                    data["LocalPath"] = ""
                                    data["isUploadLocal"] = "0"
                                    videoUrl = ""
                                    audioUrl = ""
                                    messageList.removeAt(size - 1)
                                    notifyAdapter()
                                    myRef.push().setValue(data)
                                    requestLastMsgParam("Photo", "2")
                                }
                                UploadType.equals("5") -> {
                                    val data = messageList[size - 1] as MutableMap<String, String>
                                    data["Document"] = url.toString()
                                    data["LocalPath"] = ""
                                    data["isUploadLocal"] = "0"
                                    videoUrl = ""
                                    audioUrl = ""
                                    messageList.removeAt(size - 1)
                                    notifyAdapter()
                                    myRef.push().setValue(data)
                                    requestLastMsgParam(fileName, "5")
                                }
                                UploadType == "3" -> {
                                    uploadThumbImage(FirebaseStorage.getInstance().getReference("Images"), size, url.toString())
                                }
                            }

                            edt_chat.setText("")
                            audioUrl = ""
                            videoUrl = ""
                            assetImageUrl = ""
                            currentLng = ""
                            currentLat = ""
                        }
                mReference.downloadUrl.addOnFailureListener {
//                    Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show()
                }
                /*  val dwnTxt = findViewById<View>(R.id.dwnTxt) as TextView
                dwnTxt.text = url.toString()*/
                //  Toast.makeText(this, "Successfully Uploaded :)", Toast.LENGTH_LONG).show()
            }
            mReference.putFile(uri).addOnFailureListener {
//                Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            ProgressD.hide()
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }

    }

    private fun uploadThumbImage(reference: StorageReference, size: Int, toString: String) {
        val uri = Uri.fromFile(File(videoThumb.toString()))
        reference.putFile(uri).addOnSuccessListener {
            reference.downloadUrl.addOnSuccessListener { url ->
                val data = messageList[size - 1] as MutableMap<String, String>
                data["video"] = toString
                data["thumb_video"] = url.toString()
                data["isUploadLocal"] = "0"
                messageList.removeAt(size - 1)
                notifyAdapter()
                myRef.push().setValue(data)
                requestLastMsgParam("Video", "3")
            }
        }
    }

    private fun sendMEssage(uri: Uri?, isUploadLocal: String, fileName: String = "", fileExtensionName: String = "") {
        putValueInFirebase(uri, myRef, isUploadLocal, fileName, fileExtensionName)
    }

    private fun putValueInFirebase(uri: Uri?, refrence: DatabaseReference?, isUploadLocal: String, fileName: String = "", fileExtensionName: String = "") {
        val map1: MutableMap<String, String> = mutableMapOf()
        map1["from_id"] = "" + user_id
        map1["from_name"] = "" + user_name
        map1["message"] = messageType.toString()
        if (edt_chat.text.toString().trim() != "") {
            map1["msg"] = edt_chat.text.toString().trim { it <= ' ' }
            lastMessageValue = "1"
        } else {
            map1["msg"] = ""
        }
        map1["status"] = "66"
        map1["timestamp"] = System.currentTimeMillis().toString()
        map1["to_id"] = "" + to_id
        map1["type"] = mtype
        map1["user_id"] = "" + "" + user_id

        if (!currentLat.equals("")) {
            lastMessageValue = "4"
            map1["location"] = "$currentLat,$currentLng"
        } else {
            currentLng = ""
            currentLat = ""
            map1["location"] = ""
        }

        if (!videoUrl.equals("")) {
            lastMessageValue = "3"
            map1["video"] = videoUrl.toString()

        } else {
            videoUrl = ""
            map1["video"] = ""
        }
//        if (!audioUrl.equals("")) {
//            lastMessagevalue = "Audio"
//            map1["audio"] = audioUrl.toString()
//        } else {
//            audioUrl = ""
//            map1["audio"] = ""
//        }
        if (!assetImageUrl.equals("")) {

            map1["image"] = assetImageUrl
        } else {
            assetImageUrl = ""
            map1["image"] = ""
        }
        if (edt_chat.text.toString().trim() != "") {
            lastMessageValue = "1"
        }
        messageType = ""
        map1["isUploadLocal"] = isUploadLocal
        if (isUploadLocal == "1") {
            try {
                val filePath = Utility.getPath(this, uri)
                map1["LocalPath"] = filePath
            } catch (e: Exception) {
            }
            map1["thumb_video"] = videoThumb
        }

        when (lastMessageValue) {
            "1" -> {
                myRef.push().setValue(map1)
                requestLastMsgParam(edt_chat.text.toString().trim(), lastMessageValue)
                edt_chat.setText("")
                currentLng = ""
                currentLat = ""
                CommonMethod.hideKeyBoard(context)
            }
            "4" -> {
                myRef.push().setValue(map1)
                requestLastMsgParam(getAddressFromLatLng(LatLng(currentLat.toDouble(), currentLng.toDouble())), lastMessageValue)
                edt_chat.setText("")
                currentLng = ""
                currentLat = ""
                CommonMethod.hideKeyBoard(context)
            }
            "5" -> {
                val name: String = uri?.path!!.substring(uri.path!!.lastIndexOf("/") + 1)
                val fileUri = File(uri.path.toString())
                fileUri.absoluteFile.name
                map1["fileName"] = getFileNameNew(uri)
                map1["fileExtension"] = fileExtensionName
                messageList.add(map1)
                notifyAdapter()
                upload(uri, messageList.size, getFileNameNew(uri))
            }
            else -> {
                messageList.add(map1)
                notifyAdapter()
                upload(uri, messageList.size)
            }
        }

        ProgressD.hide()


    }

    fun getFileNameNew(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    private fun getAddressFromLatLng(latLng: LatLng): String? {
        val addresses: List<Address>
        val geocoder = Geocoder(context, Locale.getDefault())
        return try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            addresses[0].locality
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ""
        }
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context)
        chatListAdapter = ChatAdapter(context, messageList, this)
        recycler_chat_list.layoutManager = layoutManager
        recycler_chat_list.adapter = chatListAdapter

    }


    private fun notifyAdapter() {
        try {
            recycler_chat_list.layoutManager?.scrollToPosition(chatListAdapter.itemCount - 1)
            chatListAdapter.notifyDataSetChanged()
        } catch (e: Exception) {
        }
    }


    private fun getPath(uri: Uri?): String {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = this.managedQuery(uri, projection, null, null, null)
        val column_index: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }


    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, "" + throwable.message)
    }

    private fun getNode() {
        try {
            myRef = FirebaseDatabaseRef.getInstant().firebaseRef
            myRef.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.e(TAG, "childCount--" + snapshot.childrenCount)
                    val fromId = BMSPrefs.getString(context, Constants._ID)
                    val friend_id = BMSPrefs.getString(context, Constants.TO_ID)
                    val node1 = friend_id + "_" + fromId
                    val node2 = fromId + "_" + friend_id
                    finalNode = node1
                    val snapshotChildren = snapshot.children
                    for (user in snapshotChildren) {
                        if (Objects.requireNonNull(user.key) == node1) {
                            finalNode = node1
                        } else if (user.key == node2) {
                            finalNode = node2
                        }
                    }
                    Log.e(TAG, "finalNode---$finalNode")
                    dataFromFirebase()
                    ProgressD.hide()
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    ProgressD.hide()
                    Log.e(TAG, "DatabaseError--$databaseError")
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun deleteChat() {
        try {
            FirebaseDatabaseRef.getInstant().getFirebaseRef(finalNode).removeValue()
            deleteChat(to_id)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    companion object {
        private val TAG = StartChatActivity::class.simpleName
    }

    override fun onDocument() {
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.type = "*/*"
        startActivityForResult(Intent.createChooser(i, "select file"), RESULT_LOAD_FILE)
    }

    private var downLoadId: Long = 0
    override fun downLoadFile(url: String?, fileName: String?, fileExtensionName: String?) {
        registerReceiver(onDownloadComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        downLoadId = checkFileExist(url, fileName.toString(), fileExtensionName.toString())
    }


    private val onDownloadComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            //Fetching the download id received with the broadcast
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (downLoadId == id) {
                notifyAdapter()
            }
        }
    }

    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, this)
        BMSPrefs.putString(this, Constants._ID, "")
        BMSPrefs.putString(this, Constants.isStoreCreated, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE_BACK, "")
        CommonMethod.callActivityFinish(this, Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }


    private fun readMessage() {
        val hashMap = HashMap<String, String>()
        hashMap["to_user_id"] = to_id
        hashMap["user_id"] = user_id
        hashMap["session_token"] = session_token
        Log.d("readMessage", hashMap.toString())
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.readMessage(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(responseModel: ResponseModel) {
                        FeedFragment.shouldDataRefresh = true
                        if (responseModel.response_status == "1") {
                            if (responseModel.response_data != null) {
                                muteStatus = responseModel.response_data.is_chat_notification_mute
                                blockStatus = responseModel.response_data.is_user_chat_block_by_you
                            }
                        }

                    }

                    override fun onError(e: Throwable) {
                    }

                    override fun onComplete() {}
                })
    }

    override fun onResume() {
        super.onResume()
        isOnChatScreen = true
        val filter = IntentFilter()
        filter.addAction(Constants.NOTIFICATION_ACTION_FILTER)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter)
    }


    override fun onStop() {
        super.onStop()
        isOnChatScreen = false
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }

    private val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action != null && action == Constants.NOTIFICATION_ACTION_FILTER) {
                val type = intent.getStringExtra("type")
                if (type == MyFirebaseMessagingService.STATUS_CHAT_MESSAGE) {
                    readMessage()
                }
            }
        }
    }

}

interface OnChatActions {
    fun onLocation()
    fun onGallery()
    fun onCamera()
    fun onContact()
    fun onVideo()
    fun onDocument()
}