package com.ablueclive.activity.startChat

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.playVideo.VideoPlayActivity
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ShowFullImage
import com.squareup.picasso.Picasso
import java.io.File
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


class ChatAdapter(
        var context: Context,
        var dataList: ArrayList<Map<String, String>>,
        var action: ActionOnChatScreen
) : RecyclerView.Adapter<ChatAdapter.ViewHolder?>() {


    private val MSG_UPDATE_SEEK_BAR = 1845
    private val REQUEST_CODE_LOCATION = 106
    private val uiUpdateHandler: Handler = Handler(Looper.myLooper()!!)
    private var playingPosition = -1
    private var isVisibleToday = false
    private var dateNext: String? = ""
    private var playingHolder: ViewHolder? = null

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): ViewHolder {
        val view: View =
                LayoutInflater.from(parent.context).inflate(R.layout.chat_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
            holder: ViewHolder,
            position: Int) {


        if (position == playingPosition) {
            playingHolder = holder
            // this view holder corresponds to the currently playing audio cell
            // update its view to show playing progress
            updatePlayingView()
        } else {
            // and this one corresponds to non playing
            updateNonPlayingView(holder)
        }


        val u_id: String? = BMSPrefs.getString(context, Constants._ID)
        if (!dataList[position]["timestamp"].isNullOrBlank()) {

            val date: String = getDate(java.lang.Long.valueOf(dataList[position]["timestamp"]!!.toLong())).toString()
            dateNext = if (position == 0) {
                null
            } else {
                getDate(java.lang.Long.valueOf(dataList[position - 1]["timestamp"]!!.toLong()))
            }
            if (dateNext != null && date.equals(dateNext, ignoreCase = true)) {
                holder.txt_date_msg.visibility = View.GONE
            } else {
                if (currentDate().equals(getDate(dataList[position]["timestamp"]?.toLong()), true)) {
                    holder.txt_date_msg.visibility = View.VISIBLE
                    holder.txt_date_msg.text = "Today"
                } else {
                    holder.txt_date_msg.visibility = View.VISIBLE
                    holder.txt_date_msg.text = getDate(dataList[position]["timestamp"]!!.toLong())
                }
            }


        }
        if (u_id != null)
            if (dataList[position]["from_id"] != null)
                if (u_id == dataList[position]["from_id"]) {
                    holder.leftlayout.visibility = View.GONE

                    if (dataList[position]["isUploadLocal"].equals("1")) {
                        holder.txtDate.text = context.getString(R.string.sending)
                    } else {
                        holder.txtDate.text = getTimeFromTimestamp(dataList[position]["timestamp"]!!.toLong(), "hh:mm a")
                    }
                    if (dataList[position]["type"].equals("1")) {

                        holder.rightlayout.visibility = View.VISIBLE
                        holder.txt_layout_right.visibility = View.VISIBLE
                        holder.documentRight.visibility = View.GONE
//                        holder!!.right_audio.visibility = View.GONE
                        holder.video_frame_right.visibility = View.GONE
                        holder.video_chat_right.visibility = View.GONE
                        holder.webview_right.visibility = View.GONE
                        holder.txtMessage.text = "" + dataList[position]["message"]
                        lastMeassge = "" + dataList[position]["message"]
                    }

                    if (dataList[position]["type"].equals("2")) {
                        holder.img_chat_right.visibility = View.VISIBLE
                        holder.txt_layout_right.visibility = View.GONE
                        holder.webview_right.visibility = View.GONE
                        holder.documentRight.visibility = View.GONE
//                        holder!!.right_audio.visibility = View.GONE
                        lastMeassge = "Image"
                        if (dataList[position]["LocalPath"].isNullOrEmpty()) {
                            loadImageResize(dataList[position]["image"], holder.img_chat_right)
                        } else {
                            try {
                                val myBitmap = BitmapFactory.decodeFile(dataList[position]["LocalPath"])
                                holder.img_chat_right.setImageBitmap(myBitmap)
                                holder.txtDate.text = context.getString(R.string.sending)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                    }

                    if (dataList[position]["type"].equals("3")) {
//                        holder.right_audio.visibility = View.GONE
                        holder.leftlayout.visibility = View.GONE
                        holder.rightlayout.visibility = View.VISIBLE
                        holder.webview_right.visibility = View.GONE
                        holder.video_frame_right.visibility = View.VISIBLE
                        holder.video_chat_right.visibility = View.VISIBLE
                        holder.txt_layout_right.visibility = View.GONE
                        holder.documentRight.visibility = View.GONE
                        lastMeassge = "Video"
                        val thumb_video = dataList[position]["thumb_video"]
                        if (dataList[position]["isUploadLocal"].equals("1")) {
                            holder.txtDate.text = context.getString(R.string.sending)
                            try {
                                val myBitmap = BitmapFactory.decodeFile(dataList[position]["thumb_video"])
                                holder.video_chat_right.setImageBitmap(myBitmap)

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        } else {
                            loadImageResize(dataList[position]["thumb_video"], holder.video_chat_right)

                        }
                    }

                    if (dataList[position]["type"].equals("4")) {
                        holder.leftlayout.visibility = View.GONE
                        holder.txt_layout_right.visibility = View.GONE
                        holder.webview_right.visibility = View.VISIBLE
                        holder.documentRight.visibility = View.GONE
//                        holder!!.right_audio.visibility = View.GONE
                        lastMeassge = "Location"
                        val mapImageUrl = "https://image.maps.api.here.com/mia/1.6/mapview?c=" + dataList[position]["location"] + "&z=14&app_id=MhZrUGW4VPfit6VvWc1V&app_code=spqYmKR3IyGbXNgmuIzLyg"
                        loadImageResize(mapImageUrl, holder.webview_right)
                    }
                    if (dataList[position]["type"].equals("5")) {
                        holder.leftlayout.visibility = View.GONE
                        holder.txt_layout_right.visibility = View.GONE
                        holder.documentRight.visibility = View.VISIBLE
//                        holder.rightFileName.visibility = View.VISIBLE
                        holder.webview_right.visibility = View.GONE
//                        holder!!.right_audio.visibility = View.GONE
                        holder.fileNameRight.text = dataList[position]["fileName"]
                        lastMeassge = "Document"
                        var mapImageUrl = dataList[position]["Document"]

                        if (isFileExist(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])) {
                            holder.imageDownloadFileRight.background = context.resources.getDrawable(R.drawable.ic_action_complete)
                            holder.imageDownloadFileRight.isClickable = false
                            holder.documentRight.setOnClickListener {
                                action.downLoadFile(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])
                            }
                        } else {
                            holder.imageDownloadFileRight.background = context.resources.getDrawable(R.drawable.ic_action_download)
                            holder.imageDownloadFileRight.setOnClickListener {
                                action.downLoadFile(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])
                            }
                        }
                    }
                } else {

                    holder.txtDateleft.text = getTimeFromTimestamp(dataList[position]["timestamp"]!!.toLong(), "hh:mm a")
                    if (dataList[position]["type"].equals("1")) {

                        holder.leftlayout.visibility = View.VISIBLE
                        holder.rightlayout.visibility = View.GONE
                        holder.left_audio.visibility = View.GONE
                        holder.video_frame_left.visibility = View.GONE
                        holder.webview_left.visibility = View.GONE
                        holder.documentLeft.visibility = View.GONE
                        holder.txt_layout_left.visibility = View.VISIBLE

                        holder.txtMessageleft.text = "" + dataList[position]["message"]
                        lastMeassge = "" + dataList[position]["message"]
                    }
                    if (dataList[position]["type"].equals("2")) {
                        holder.video_frame_left.visibility = View.GONE
                        holder.video_chat_left.visibility = View.GONE
                        holder.rightlayout.visibility = View.GONE
                        holder.img_chat_left.visibility = View.VISIBLE
                        holder.txt_layout_left.visibility = View.GONE
                        holder.documentLeft.visibility = View.GONE
                        holder.webview_left.visibility = View.GONE
                        lastMeassge = "Image"

                        loadImageResize(dataList[position]["image"], holder.img_chat_left)

                        /*     val imageBitmap = BitmapFactory.decodeFile(dataList[position]["Image"])
                        holder.img_chat_left.setImageBitmap(imageBitmap)*/
                        holder.left_audio.visibility = View.GONE
                    }

                    if (dataList[position]["type"].equals("3")) {
                        holder.txt_layout_left.visibility = View.GONE
                        holder.left_audio.visibility = View.GONE
                        holder.video_chat_left.visibility = View.VISIBLE
                        holder.rightlayout.visibility = View.GONE
                        holder.documentLeft.visibility = View.GONE
                        holder.video_frame_left.visibility = View.VISIBLE
                        holder.webview_left.visibility = View.GONE
                        lastMeassge = "Video"

                        if (dataList[position]["isUploadLocal"].equals("1")) {
                            holder.txtDate.text = context.getString(R.string.sending)
                            try {
                                val myBitmap = BitmapFactory.decodeFile(dataList[position]["thumb_video"])
                                holder.video_chat_left.setImageBitmap(myBitmap)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        } else {
                            loadImageResize(dataList[position]["thumb_video"], holder.video_chat_left)
                        }

                    }
                    if (dataList[position]["type"].equals("5")) {
                        holder.left_audio.visibility = View.GONE
                        holder.video_frame_left.visibility = View.GONE
                        holder.txt_layout_left.visibility = View.GONE
                        holder.rightlayout.visibility = View.GONE
                        holder.webview_left.visibility = View.GONE
                        holder.documentLeft.visibility = View.VISIBLE
                        lastMeassge = "Document"
                        holder.fileNameLeft.text = dataList[position]["fileName"]
                        lastMeassge = "Document"
                        var mapImageUrl = dataList[position]["Document"]

                        if (isFileExist(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])) {
                            holder.imageDownloadFileLeft.background = context.resources.getDrawable(R.drawable.ic_action_complete)
                            holder.imageDownloadFileLeft.isClickable = false
                            holder.documentLeft.setOnClickListener {
                                action.downLoadFile(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])
                            }
                        } else {
                            holder.imageDownloadFileLeft.background = context.resources.getDrawable(R.drawable.ic_action_download)
                            holder.imageDownloadFileLeft.setOnClickListener {
                                action.downLoadFile(dataList[position]["Document"], dataList[position]["fileName"], dataList[position]["fileExtension"])
                            }
                        }
                    }

                    if (dataList[position]["type"].equals("4")) {
                        holder.rightlayout.visibility = View.GONE
                        holder.txt_layout_left.visibility = View.GONE
                        holder.webview_left.visibility = View.VISIBLE
                        holder.left_audio.visibility = View.GONE
                        lastMeassge = "Location"

                        val mapImageUrl = "https://image.maps.api.here.com/mia/1.6/mapview?c=" + dataList[position]["location"] + "&z=14&app_id=MhZrUGW4VPfit6VvWc1V&app_code=spqYmKR3IyGbXNgmuIzLyg"
                        loadImageResize(mapImageUrl, holder.webview_left)

                    }

                }


        holder.img_chat_left.setOnClickListener {

            if (holder.txtDate.text.toString() != context.getString(R.string.sending)) {
                val images = dataList[position]["image"]
                val i = Intent(context, ShowFullImage::class.java)
                i.putExtra("ImageUrl", images)
                context.startActivity(i)
            }
        }
        holder.img_chat_right.setOnClickListener {

            if (holder.txtDate.text.toString() != context.getString(R.string.sending)) {
                val images = dataList[position]["image"]
                val i = Intent(context, ShowFullImage::class.java)
                i.putExtra("ImageUrl", images)
                context.startActivity(i)
            }


        }
        holder.ll_suspicious_left.setOnClickListener {
            val currentLocationlink = Constants.MAPURL + dataList[position]["location"]
            GetCurrentLOcation(currentLocationlink)
        }


//        holder.layout_suspicious_right.setOnClickListener {
//            var currentLocationlink = Constants.MAPURL + dataList[position]["location"]
//            GetCurrentLOcation(currentLocationlink)
//        }

        holder.video_chat_right.setOnClickListener {
            //    videoDialog(dataList[position]["video"])
            // PlayVideoFile(dataList[position]["video"])

            if (holder.txtDate.text.toString() != context.getString(R.string.sending)) {
                callVideoViewClass(dataList[position]["video"])
            }
        }
        holder.video_chat_left.setOnClickListener {
            // videoDialog(dataList[position]["video"])
            if (holder.txtDate.text.toString() != context.getString(R.string.sending)) {
                callVideoViewClass(dataList[position]["video"])
            }
        }

        holder.webview_left.setOnClickListener {
            val currentLocationlink = Constants.MAPURL + dataList[position]["location"]
            GetCurrentLOcation(currentLocationlink)
        }

        holder.webview_right.setOnClickListener {
            val currentLocationlink = Constants.MAPURL + dataList[position]["location"]
            GetCurrentLOcation(currentLocationlink)
        }
    }

    private fun isFileExist(url: String?, fileName: String?, fileExtension: String?): Boolean {
        val badgeFile: String = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + fileName
        if (File(badgeFile).exists()) {
            return true
        }
        return false
    }

    private fun callVideoViewClass(videoUrl: String?) {
        BMSPrefs.putString(context, "video_path", videoUrl)
        CommonMethod.callActivity(context, Intent(context, VideoPlayActivity::class.java))
    }


    private fun GetCurrentLOcation(urlLink: String?) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlLink))
        intent.setClassName(
                "com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity")
        context.startActivity(intent)
    }


    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        if (playingPosition == holder.absoluteAdapterPosition) {
            // view holder displaying playing audio cell is being recycled
            // change its state to non-playing
            playingHolder?.let { updateNonPlayingView(it) }
            playingHolder = null
        }
    }

    private fun updateNonPlayingView(holder: ViewHolder) {
        if (holder === playingHolder) {
            uiUpdateHandler!!.removeMessages(MSG_UPDATE_SEEK_BAR)
        }
//        holder.sbProgress.setEnabled(false)
//        holder.sbProgress1.setEnabled(false)
//        holder.sbProgress.setProgress(0)
//        holder.sbProgress1.setProgress(0)
//        holder.ivPlayPause.setImageResource(android.R.drawable.ic_media_play)
//        holder.ivPlayPause1.setImageResource(android.R.drawable.ic_media_play)
    }


    private fun updatePlayingView() {
        playingHolder!!.sbProgress.max = mediaPlayer!!.duration
        playingHolder!!.sbProgress.progress = mediaPlayer!!.currentPosition
        playingHolder!!.sbProgress.isEnabled = true

//        playingHolder!!.sbProgress1.setMax(mediaPlayer!!.duration)
//        playingHolder!!.sbProgress1.setProgress(mediaPlayer!!.currentPosition)
//        playingHolder!!.sbProgress1.setEnabled(true)
        if (mediaPlayer!!.isPlaying) {
            uiUpdateHandler!!.sendEmptyMessageDelayed(
                    MSG_UPDATE_SEEK_BAR,
                    100
            )
            playingHolder!!.ivPlayPause.setImageResource(android.R.drawable.ic_media_pause)
//            playingHolder!!.ivPlayPause1.setImageResource(android.R.drawable.ic_media_pause)
        } else {
            uiUpdateHandler!!.removeMessages(MSG_UPDATE_SEEK_BAR)
            playingHolder!!.ivPlayPause.setImageResource(android.R.drawable.ic_media_play)
//            playingHolder!!.ivPlayPause1.setImageResource(android.R.drawable.ic_media_play)
        }
    }


    fun stopPlayer() {
        if (null != mediaPlayer) {
            releaseMediaPlayer()
        }
    }

    @Override
    fun handleMessage(msg: Message): Boolean {
        when (msg.what) {
            MSG_UPDATE_SEEK_BAR -> {
                playingHolder!!.sbProgress.progress = mediaPlayer!!.currentPosition
                uiUpdateHandler!!.sendEmptyMessageDelayed(
                        MSG_UPDATE_SEEK_BAR,
                        100
                )
                return true
            }
        }
        return false
    }

    inner class ViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView), View.OnClickListener,
            SeekBar.OnSeekBarChangeListener {
        var txtMessage: TextView
        var txtMessageleft: TextView
        var txt_date_msg: TextView
        var txtDate: TextView
        var txtDateleft: TextView
        var content: LinearLayout? = null
        var l_main: LinearLayout
        var leftlayout: LinearLayout
        var rightlayout: LinearLayout
        var img_chat_left: ImageView
        var img_chat_right: ImageView
        var txt_layout_left: LinearLayout
        var txt_layout_right: LinearLayout
        var left_audio: LinearLayout

        //        var right_audio: LinearLayout
//        var ivPlayPause1: ImageView
        var ivPlayPause: ImageView

        //        var sbProgress1: SeekBar
        var sbProgress: SeekBar
        var video_chat_right: ImageView
        var playVideoRight: ImageView
        var playVideoLeft: ImageView
        var video_chat_left: ImageView
        var video_frame_right: FrameLayout
        var video_frame_left: FrameLayout

        //  var video_play_button: ImageView
        // var video_play_button_left: ImageView
        var webview_right: ImageView

        var webview_left: ImageView
        var documentLeft: RelativeLayout
        var documentRight: RelativeLayout
        var fileNameRight: TextView
        var fileNameLeft: TextView
        var imageDownloadFileLeft: ImageView
        var imageDownloadFileRight: ImageView

        //        var layout_suspicious_right: LinearLayout
//        var img_suspicious_right: ImageView
//        var suspicious_Name_right: TextView
        var ll_suspicious_left: LinearLayout
        var img_suspicious_left: ImageView
        var txt_suspicious_left: TextView

//        var imageUserRight: CircleImageView

        init {
            txtMessage = itemView.findViewById<View>(R.id.txtMessage) as TextView
            documentRight = itemView.findViewById<View>(R.id.documentRight) as RelativeLayout
            imageDownloadFileLeft = itemView.findViewById<View>(R.id.imageDownloadFileLeft) as ImageView
            fileNameLeft = itemView.findViewById<View>(R.id.fileNameLeft) as TextView


            documentLeft = itemView.findViewById<View>(R.id.documentLeft) as RelativeLayout

            imageDownloadFileRight = itemView.findViewById<View>(R.id.imageDownloadFileRight) as ImageView
            fileNameRight = itemView.findViewById<View>(R.id.fileNameRight) as TextView


            txtMessageleft =
                    itemView.findViewById<View>(R.id.txtMessageleft) as TextView
            txtDate = itemView.findViewById<View>(R.id.txtDate) as TextView
            txtDateleft = itemView.findViewById<View>(R.id.txtDateleft) as TextView
            txt_date_msg = itemView.findViewById<View>(R.id.txt_date_msg) as TextView

            rightlayout = itemView.findViewById<View>(R.id.rightlayout) as LinearLayout
            leftlayout = itemView.findViewById<View>(R.id.leftlayout) as LinearLayout
            l_main = itemView.findViewById(R.id.l_main)
            img_chat_left = itemView.findViewById(R.id.img_chat_left)
            img_chat_right = itemView.findViewById(R.id.img_chat_right)
            txt_layout_right = itemView.findViewById(R.id.txt_layout_right)
            txt_layout_left = itemView.findViewById(R.id.txt_layout_left)

            left_audio = itemView.findViewById(R.id.left_audio)
//            right_audio = itemView.findViewById(R.id.right_audio)
//            ivPlayPause1 = itemView.findViewById(R.id.ivPlayPause1)
            ivPlayPause = itemView.findViewById(R.id.ivPlayPause)
//            sbProgress1 = itemView.findViewById(R.id.sbProgress1)
            sbProgress = itemView.findViewById(R.id.sbProgress)
            video_chat_left = itemView.findViewById(R.id.video_chat_left)
            video_chat_right = itemView.findViewById(R.id.video_chat_right)
            video_frame_right = itemView.findViewById(R.id.video_frame_right)
            video_frame_left = itemView.findViewById(R.id.video_frame_left)
            //    video_play_button_left = itemView.findViewById(R.id.video_play_button_left)
            //   video_play_button = itemView.findViewById(R.id.video_play_button)
            webview_left = itemView.findViewById(R.id.webview_left)
            webview_right = itemView.findViewById(R.id.webview_right)

//            layout_suspicious_right = itemView.findViewById(R.id.layout_suspicious_right)
//            img_suspicious_right = itemView.findViewById(R.id.img_suspicious_right)
//            suspicious_Name_right = itemView.findViewById(R.id.suspicious_Name_right)

            ll_suspicious_left = itemView.findViewById(R.id.ll_suspicious_left)
            img_suspicious_left = itemView.findViewById(R.id.img_suspicious_left)
            txt_suspicious_left = itemView.findViewById(R.id.txt_suspicious_left)
            playVideoRight = itemView.findViewById(R.id.playVideoRight)
            playVideoLeft = itemView.findViewById(R.id.playVideoLeft)

//            imageUserRight = itemView.findViewById(R.id.imageUserRight)

            sbProgress.setOnSeekBarChangeListener(this)
//            sbProgress1.setOnSeekBarChangeListener(this!!)
//            ivPlayPause1.setOnClickListener(this!!)
            ivPlayPause.setOnClickListener(this)
            //  video_play_button.setOnClickListener(this!!)


        }

        override fun onClick(v: View?) {
            if (absoluteAdapterPosition == playingPosition) {
                // toggle between play/pause of audio
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.pause()
                } else {
                    mediaPlayer!!.start()
                }
            } else {
                // start another audio playback
                playingPosition = absoluteAdapterPosition
                if (mediaPlayer != null) {
                    if (null != playingHolder) {
                        updateNonPlayingView(playingHolder!!)
                    }
                    mediaPlayer!!.release()
                }
                playingHolder = this
                startMediaPlayer(dataList[playingPosition]["audio"]!!.toString())


            }
            updatePlayingView()
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mediaPlayer!!.seekTo(progress)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    private fun releaseMediaPlayer() {
        if (null != playingHolder) {
            updateNonPlayingView(playingHolder!!)
        }
        mediaPlayer!!.release()
        mediaPlayer = null
        playingPosition = -1
    }


    private fun startMediaPlayer(audioResId: String) {
        val myUri: Uri = Uri.parse(audioResId)
        mediaPlayer = MediaPlayer.create(context, myUri)
        mediaPlayer!!.setOnCompletionListener(MediaPlayer.OnCompletionListener { releaseMediaPlayer() })
        mediaPlayer!!.start()
    }

    companion object {
        var mediaPlayer: MediaPlayer? = null
        var selectedList: ArrayList<String>? = null
        var lastMeassge: String? = ""
    }

    private fun checkDateForToday(toLong: Long): Boolean {
        val chatDate = Timestamp(toLong)
        val objChatDate = Date(chatDate.time)
        val todayDate = Timestamp(System.currentTimeMillis())
        val objDate = Date(todayDate.time)
        if (objChatDate.compareTo(objDate) == 0) {
            isVisibleToday = true
            return true
        }
        return false
    }

    interface ActionOnChatScreen {
        fun downLoadFile(url: String?, fileName: String?, fileExtensionName: String?)
    }


    private fun getTimeFromTimestamp(timestamp: Long, formate: String?): String? {
        val sdf = SimpleDateFormat(formate, Locale.US)
        /*sdf.setTimeZone(TimeZone.getTimeZone("GMT"));*/
        val date = Date(timestamp)
        return sdf.format(date)
    }

    private fun getDate(timeStamp: Long?): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = timeStamp!!
        return DateFormat.format("MMM dd, yyyy", cal).toString()
    }

    private fun currentDate(): String? {
        val c = Calendar.getInstance().time
        println("Current time => $c")
        val df = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        return df.format(c)
    }


    private fun loadImageResize(imgUrl: String?, ivImage: ImageView?) {
        try {
            Log.e("imgUrl -->", "" + imgUrl)
            val placeHolder = R.drawable.chat_place_holder
            Picasso.get().load(imgUrl).resize(500, 500).placeholder(placeHolder).into(ivImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}