package com.ablueclive.activity.startChat;

import java.util.HashMap;

public class StartChatPresenter implements StartChatContract.Presenter, StartChatContract.Model.OnFinishedListener {
    private StartChatContract.View startChatView;
    private StartChatContract.Model startChatModel;

    public StartChatPresenter(StartChatContract.View startChatView) {
        this.startChatView = startChatView;
        this.startChatModel = new StartChatModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (startChatView != null) {
            startChatView.setDataToViews(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        startChatView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        startChatView = null;
    }

    @Override
    public void requestLastMsg(HashMap<String, String> param) {
        startChatModel.getLastMsg(this, param);

    }
}
