package com.ablueclive.activity.startChat;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.ablueclive.R;



public class OptionMediaDialog extends DialogFragment implements View.OnClickListener {
    private View view;
    private final OnChatActions onChatActions;

    public OptionMediaDialog(OnChatActions onChatActions) {
//        super(context);
        this.onChatActions = onChatActions;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = getLayoutInflater().inflate(R.layout.option_media_dialog, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findView();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT)
            );
            dialog.setCancelable(true);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
        return dialog;
    }

    private void findView() {
        ImageView imageCamera = view.findViewById(R.id.imageCamera);
        ImageView imageGallery = view.findViewById(R.id.imageGallery);
        ImageView imageLocation = view.findViewById(R.id.imageLocation);
        ImageView imageDocument = view.findViewById(R.id.imageDocument);
        imageCamera.setOnClickListener(this);
        imageGallery.setOnClickListener(this);
        imageLocation.setOnClickListener(this);
        imageDocument.setOnClickListener(this);
        if (getDialog() != null) {
            getDialog().setCancelable(true);
        }
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageCamera:
                getDialog().cancel();
                onChatActions.onCamera();
                break;
            case R.id.imageGallery:
                getDialog().cancel();
                onChatActions.onGallery();
                break;
            case R.id.imageLocation:
                getDialog().cancel();
                onChatActions.onLocation();
                break;
            case R.id.imageDocument:
                getDialog().cancel();
                onChatActions.onDocument();
                break;
        }
    }
}