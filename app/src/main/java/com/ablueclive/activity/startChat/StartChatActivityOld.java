package com.ablueclive.activity.startChat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.fragment.allFriendListChat.ReadUnreadContract;
import com.ablueclive.fragment.allFriendListChat.ReadUnreadPresenter;
import com.ablueclive.modelClass.ChatModel;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.FirebaseDatabaseRef;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class StartChatActivityOld extends AppCompatActivity implements View.OnClickListener, StartChatContract.View, ReadUnreadContract.View {
    private EditText edt_chat;
    private String session_token;
    private String user_id;
    private String user_name;
    private String to_id;
    private String finalNode = "";
    private String friend_mobile;
    private RecyclerView recycler_chat_list;
    private boolean isInternetPresent = false;
    ChatAdapterOld chatAdapter;
    private DatabaseReference myRef;

    public static final String CHAT_DATABASE = "FriendChat";
    private static final String TAG = StartChatActivityOld.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_chat);
        findView();
        if (isInternetPresent) {
            requestReadStatus("1");
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }

    private void findView() {
        to_id = BMSPrefs.getString(getApplicationContext(), "to_id");
        friend_mobile = BMSPrefs.getString(getApplicationContext(), Constants.FRIEND_MOBILE);
        session_token = BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN);
        user_id = BMSPrefs.getString(getApplicationContext(), Constants._ID);
        String chat_user_name = BMSPrefs.getString(getApplicationContext(), "chat_user_name");
        user_name = BMSPrefs.getString(getApplicationContext(), Constants.USER_NAME);
        ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        ImageView img_back = findViewById(R.id.img_back);
        edt_chat = findViewById(R.id.edt_chat);
        LinearLayout linear_submit = findViewById(R.id.linear_submit);
        recycler_chat_list = findViewById(R.id.recycler_chat_list);
        TextView text_title = findViewById(R.id.text_title);
        ImageView img_call = findViewById(R.id.img_call);
        img_back.setOnClickListener(this);
        linear_submit.setOnClickListener(this);
        img_call.setOnClickListener(this);
        text_title.setText(chat_user_name);
        text_title.setOnClickListener(v -> {
            BMSPrefs.putString(StartChatActivityOld.this, Constants.FRIEND_ID, to_id);
            CommonMethod.callActivity(StartChatActivityOld.this, new Intent(StartChatActivityOld.this, FriendProfileActivity.class));
        });
        bindChatAdapter();

        if (!friend_mobile.trim().isEmpty()) {
            img_call.setVisibility(View.VISIBLE);
        }


        myRef = FirebaseDatabaseRef.getInstant().getFirebaseRef()/*.child(CHAT_DATABASE)*/;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.e(TAG, "childCount--" + snapshot.getChildrenCount());


                String fromId = BMSPrefs.getString(StartChatActivityOld.this, Constants._ID);
                String friend_id = BMSPrefs.getString(StartChatActivityOld.this, Constants.TO_ID);
                String node1 = friend_id + "_" + fromId;
                String node2 = fromId + "_" + friend_id;
                finalNode = node1;

                Iterable<DataSnapshot> snapshotChildren = snapshot.getChildren();
                for (DataSnapshot user : snapshotChildren) {

                    if (Objects.requireNonNull(user.getKey()).equals(node1)) {
                        finalNode = node1;
                    } else if (user.getKey().equals(node2)) {
                        finalNode = node2;
                    }
                }

                Log.e(TAG, "finalNode--" + "-" + finalNode);

                DataSnapshot dataSnapshot1 = snapshot.child(finalNode);
                Iterable<DataSnapshot> dataSnapshotIterable = dataSnapshot1.getChildren();

                ArrayList<ChatModel> chatModelArrayList = new ArrayList<>();
                for (DataSnapshot user : dataSnapshotIterable) {
                    ChatModel chatData = user.getValue(ChatModel.class);
                    chatModelArrayList.add(chatData);
                }

                Log.e(TAG, "nodeCount--" + chatModelArrayList.size());

                groupDataIntoHashMap(chatModelArrayList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "DatabaseError--" + databaseError.toString());
            }
        });


    }


    private void groupDataIntoHashMap(List<ChatModel> chatModelList) {
        LinkedHashMap<String, Set<ChatModel>> groupedHashMap = new LinkedHashMap<>();
        Set<ChatModel> list = null;
        for (ChatModel chatModel : chatModelList) {
            long timeStamp = 0;
            if (!chatModel.timestamp.isEmpty()) {
                timeStamp = Long.parseLong(chatModel.timestamp);
            }
            String hashMapKey = CommonMethod.getDateFromTimestamp(timeStamp, "yyyy-MM-dd");
            if (groupedHashMap.containsKey(hashMapKey)) {
                Objects.requireNonNull(groupedHashMap.get(hashMapKey)).add(chatModel);
            } else {
                list = new LinkedHashSet<>();
                list.add(chatModel);
                groupedHashMap.put(hashMapKey, list);
            }
        }
        generateListFromMap(groupedHashMap);
    }


    private void generateListFromMap(LinkedHashMap<String, Set<ChatModel>> groupedHashMap) {
        List<ListObject> consolidatedList = new ArrayList<>();
        for (String date : groupedHashMap.keySet()) {
            DateObject dateItem = new DateObject();
            dateItem.setDate(date);
            consolidatedList.add(dateItem);
            for (ChatModel chatModel : Objects.requireNonNull(groupedHashMap.get(date))) {
                ChatModelObject generalItem = new ChatModelObject();
                generalItem.setChatModel(chatModel);
                consolidatedList.add(generalItem);
            }
        }

        chatAdapter.setDataChange(consolidatedList);
        if (chatAdapter.getItemCount() != 0)
            recycler_chat_list.smoothScrollToPosition(chatAdapter.getItemCount() - 1);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isInternetPresent) {
            BMSPrefs.putString(getApplicationContext(), "status", "");
            requestReadStatus("0");
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }

    private void writeChatData(String msg, String user_id, String user_name, String to_id) {
        ChatModel chatData = new ChatModel();
        chatData.msg = msg;
        chatData.message = msg;
        chatData.status = Constants.CHAT_DATA;
        chatData.timestamp = "" + System.currentTimeMillis();
        chatData.user_id = user_id;
        chatData.from_id = user_id;
        chatData.to_id = to_id;
        chatData.from_name = user_name;
        myRef.child(finalNode).push().setValue(chatData);
        Log.e(TAG, "writeChatData" + System.currentTimeMillis());
    }


    private void bindChatAdapter() {
        chatAdapter = new ChatAdapterOld(null);
        chatAdapter.setUser(BMSPrefs.getString(this, Constants._ID));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(StartChatActivityOld.this);
        recycler_chat_list.setLayoutManager(linearLayoutManager);
        recycler_chat_list.setAdapter(chatAdapter);
    }


    //api param
    private void requestLastMsgParam(String msg) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("to_user_id", to_id);
        hashMap.put("from_user_name", user_name);
        hashMap.put("session_token", session_token);
        hashMap.put("timestamp", "" + System.currentTimeMillis());
        hashMap.put("last_message", msg);
        hashMap.put("mobile", friend_mobile);
        Log.d("requestLastMsgParam", hashMap.toString());
        StartChatPresenter startChatPresenter = new StartChatPresenter(this);
        startChatPresenter.requestLastMsg(hashMap);
    }


    // read unread api param
    private void requestReadStatus(String online_status) {
        HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("online_status", online_status);
        params.put("to_user_id", to_id);
        Log.d("requestReadStatus", params.toString());
        ReadUnreadPresenter readUnreadPresenter = new ReadUnreadPresenter(this);
        readUnreadPresenter.requestReadStatus(params);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }
            case R.id.linear_submit: {
                if (!edt_chat.getText().toString().trim().isEmpty()) {
                    String last_msg = edt_chat.getText().toString().trim();
                    CommonMethod.hideKeyBoard(StartChatActivityOld.this);
                    requestLastMsgParam(last_msg);
                    writeChatData(edt_chat.getText().toString().trim(), user_id, user_name, to_id);
                    edt_chat.setText("");
                }
                break;
            }

            case R.id.img_call: {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", friend_mobile, null));
                    startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    //api response
    @Override
    public void setDataToViews(String response_status, String response_msg, String response_invalid) {
        if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        }
    }

    @Override
    public void setReadStatus(String response_status, String response_msg, String response_invalid) {
        if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, "" + throwable.getMessage());
    }


    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getApplicationContext());
        BMSPrefs.putString(getApplicationContext(), Constants._ID, "");
        BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, "");
        CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        finish();
    }


}
