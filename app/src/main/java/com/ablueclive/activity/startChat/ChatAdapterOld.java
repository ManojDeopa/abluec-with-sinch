package com.ablueclive.activity.startChat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.modelClass.ChatModel;
import com.ablueclive.utils.CommonMethod;

import java.util.List;

public class ChatAdapterOld extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListObject> listObjects;
    private String loggedInUserId;

    public ChatAdapterOld(List<ListObject> listObjects) {
        this.listObjects = listObjects;
    }

    public void setUser(String userId) {
        this.loggedInUserId = userId;
    }

    public void setDataChange(List<ListObject> asList) {
        this.listObjects = asList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ListObject.TYPE_GENERAL_RIGHT:
                View currentUserView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_chat_list_row_right, parent, false);
                viewHolder = new ChatRightViewHolder(currentUserView);
                break;
            case ListObject.TYPE_GENERAL_LEFT:
                View otherUserView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_chat_list_row_left, parent, false);
                viewHolder = new ChatLeftViewHolder(otherUserView);
                break;
            case ListObject.TYPE_DATE:
                View v2 = inflater.inflate(R.layout.chat_date_layout, parent, false);
                viewHolder = new DateViewHolder(v2);
                break;
        }

        assert viewHolder != null;
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case ListObject.TYPE_GENERAL_RIGHT:
                ChatModelObject generalItem = (ChatModelObject) listObjects.get(position);
                ChatRightViewHolder chatViewHolder = (ChatRightViewHolder) viewHolder;
                chatViewHolder.bind(generalItem.getChatModel());
                break;
            case ListObject.TYPE_GENERAL_LEFT:
                ChatModelObject generalItemLeft = (ChatModelObject) listObjects.get(position);
                ChatLeftViewHolder chatLeftViewHolder = (ChatLeftViewHolder) viewHolder;
                chatLeftViewHolder.bind(generalItemLeft.getChatModel());
                break;
            case ListObject.TYPE_DATE:
                DateObject dateItem = (DateObject) listObjects.get(position);
                DateViewHolder dateViewHolder = (DateViewHolder) viewHolder;
                dateViewHolder.bind(dateItem.getDate());
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (listObjects != null) {
            return listObjects.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return listObjects.get(position).getType(loggedInUserId);
    }


    public static class ChatRightViewHolder extends RecyclerView.ViewHolder {
        private final String TAG = ChatRightViewHolder.class.getSimpleName();
        TextView tvMessage, tvTime;

        public ChatRightViewHolder(View itemView) {
            super(itemView);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTime = itemView.findViewById(R.id.tvTime);
        }

        public void bind(final ChatModel chatModel) {
            tvMessage.setText(chatModel.message);
            if (!chatModel.timestamp.trim().isEmpty()) {
                tvTime.setText(CommonMethod.getDateFromTimestamp(Long.parseLong(chatModel.timestamp), "hh:mm a"));
            }
        }
    }

    public static class ChatLeftViewHolder extends RecyclerView.ViewHolder {
        private final String TAG = ChatRightViewHolder.class.getSimpleName();
        TextView tvMessage, tvTime;

        public ChatLeftViewHolder(View itemView) {
            super(itemView);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTime = itemView.findViewById(R.id.tvTime);
        }

        public void bind(final ChatModel chatModel) {
            tvMessage.setText(chatModel.message);
            if (!chatModel.timestamp.trim().isEmpty()) {
                tvTime.setText(CommonMethod.getDateFromTimestamp(Long.parseLong(chatModel.timestamp), "hh:mm a"));
            }
        }
    }


    public static class DateViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate;

        public DateViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
        }

        public void bind(final String date) {
            String today = CommonMethod.getDateFromTimestamp(System.currentTimeMillis(), "yyyy-MM-dd");
            if (today.equals(date)) {
                tvDate.setText("Today");
            } else {
                tvDate.setText(date);
            }
        }
    }
}