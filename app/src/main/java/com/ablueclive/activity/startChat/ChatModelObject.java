package com.ablueclive.activity.startChat;

import com.ablueclive.modelClass.ChatModel;

public class ChatModelObject extends ListObject {

    private ChatModel chatModel;

    public ChatModel getChatModel() {
        return chatModel;
    }

    public void setChatModel(ChatModel chatModel) {
        this.chatModel = chatModel;
    }

    @Override
    public int getType(String userId) {
        if (this.chatModel.user_id.equals(userId)) {
            return TYPE_GENERAL_RIGHT;
        } else
            return TYPE_GENERAL_LEFT;
    }
}