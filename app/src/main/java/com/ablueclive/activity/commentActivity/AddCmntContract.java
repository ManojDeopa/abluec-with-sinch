package com.ablueclive.activity.commentActivity;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class AddCmntContract {
    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void addComment(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setAddCmnt(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void requestAddComment(HashMap<String, String> param);
    }

}
