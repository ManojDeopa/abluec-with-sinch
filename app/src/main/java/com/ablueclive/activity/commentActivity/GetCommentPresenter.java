package com.ablueclive.activity.commentActivity;
import com.ablueclive.modelClass.Response_data;
import java.util.HashMap;

public class GetCommentPresenter implements GetCommentContract.Presenter, GetCommentContract.Model.OnFinishedListener {
    private GetCommentContract.View getCommentView;
    private final GetCommentContract.Model getCommentModel;

    public GetCommentPresenter(GetCommentContract.View getCommentView) {
        this.getCommentView = getCommentView;
        this.getCommentModel = new GetCommentModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (getCommentView != null) {
            getCommentView.hideProgress();
            getCommentView.setDataToRecyclerView(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (getCommentView != null) {
            getCommentView.hideProgress();
            getCommentView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        getCommentView = null;
    }

    @Override
    public void requestgetComment(HashMap<String, String> param) {
        if (getCommentView != null) {
            getCommentView.showProgress();
            getCommentModel.getComment(this, param);
        }

    }
}
