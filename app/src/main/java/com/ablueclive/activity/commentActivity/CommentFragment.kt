package com.ablueclive.activity.commentActivity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.replyComment.DeleleCmntContract
import com.ablueclive.activity.replyComment.DeleteCmntPresenter
import com.ablueclive.activity.replyComment.ReplyCommentActivity
import com.ablueclive.activity.reportComment.ReportCommentDialog
import com.ablueclive.activity.viewAllComment.ViewAllCmntActivity
import com.ablueclive.adapter.CommentAdapter
import com.ablueclive.listener.*
import com.ablueclive.modelClass.AllComments
import com.ablueclive.modelClass.Reply_comments
import com.ablueclive.modelClass.Response_data
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod.callActivity
import com.ablueclive.utils.CommonMethod.callActivityFinish
import com.ablueclive.utils.CommonMethod.enableDisableViewGroup
import com.ablueclive.utils.CommonMethod.hideKeyBoard
import com.ablueclive.utils.CommonMethod.showToastShort
import com.ablueclive.utils.ConnectionDetector
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.comment_fragment.*
import java.util.*

class CommentFragment : Fragment(), View.OnClickListener, GetCommentContract.View, AddCmntContract.View, onDeleteCmntListener, DelCmntContract.View, onReplyCmntListener, onReportPostListener, onDelSubCmntListener, DeleleCmntContract.View, onReportSubCmntListener {

    private var session_token: String? = null
    private var comment: String? = null
    private var post_id: String? = null
    private var user_id: String? = null
    private var commentAdapter: CommentAdapter? = null
    private var isInternetPresent = false
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isLoading = false
    private val pageNo = 1
    private var total_count = 0
    private var allCommentsArrayList = ArrayList<AllComments>()
    private val reply_comments = ArrayList<Reply_comments>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.comment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView(view)
    }

    override fun onResume() {
        super.onResume()
        if (isInternetPresent) {
            requestGetComment(post_id, pageNo)
        } else {
            showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    private fun findView(view: View) {
        session_token = BMSPrefs.getString(requireContext(), Constants.SESSION_TOKEN)
        post_id = BMSPrefs.getString(requireContext(), Constants.FEED_POST_ID)
        user_id = BMSPrefs.getString(requireContext(), Constants._ID)
        val connectionDetector = ConnectionDetector(requireContext())
        isInternetPresent = connectionDetector.isConnectingToInternet

        linear_all_cmnts.setOnClickListener(this)

        img_submit_comment.setOnClickListener(this)
    }

    private fun setListeners() {
        recycler_comment.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (allCommentsArrayList.size >= 10) {
                    val visibleItemCount = linearLayoutManager!!.childCount
                    val totalItemCount = linearLayoutManager!!.itemCount
                    val pastVisibleItems = linearLayoutManager!!.findFirstVisibleItemPosition()
                    if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                        isLoading = true
                        total_count = totalItemCount
                        requestGetComment(post_id, pageNo)
                    }
                }
            }
        })
    }

    // get comment request param
    private fun requestGetComment(post_id: String?, pageNo: Int) {
        val params = HashMap<String, String?>()
        params["post_id"] = post_id
        params["page"] = "" + pageNo
        params["session_token"] = session_token
        Log.d("GetCommentParam", params.toString())
        val getCommentPresenter = GetCommentPresenter(this)
        getCommentPresenter.requestgetComment(params)
    }

    // add comment request param
    private fun requestAddComment(post_id: String?) {
        val params = HashMap<String, String?>()
        params["post_id"] = post_id
        params["comment_user_id"] = user_id
        params["post_comment"] = comment
        params["session_token"] = session_token
        Log.d("AddCommentParam", params.toString())
        val addCmntPresenter = AddCmntPresenter(this)
        addCmntPresenter.requestAddComment(params)
    }

    // delete comment request param
    private fun requestDeleteComment(post_id: String, comment_id: String) {
        val params = HashMap<String, String?>()
        params["post_id"] = post_id
        params["comment_id"] = comment_id
        params["session_token"] = session_token
        Log.d("delCommentParam", params.toString())
        // main comment
        val delCmntPresenter = DelCmntPresenter(this)
        delCmntPresenter.requestDelComment(params)
    }

    // delete sub  comment request param
    private fun requestDeleteSubComment(reply_id: String, comment_id: String) {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["post_id"] = post_id
        params["comment_id"] = comment_id
        params["reply_id"] = reply_id
        Log.d("DelSubCommentParam", params.toString())
        // sub cmnt
        val deleteCmntPresenter = DeleteCmntPresenter(this)
        deleteCmntPresenter.requestDelSubComment(params)
    }

    //set user feed adapter
    private fun setCommentAdapter() {
        commentAdapter = CommentAdapter(requireActivity(), allCommentsArrayList, this, user_id,
                this, this, this, this)
        linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recycler_comment.layoutManager = linearLayoutManager
        recycler_comment.adapter = commentAdapter
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_submit_comment -> {
                hideKeyBoard(requireContext())
                comment = edt_comment.text.toString().trim { it <= ' ' }
                if (!TextUtils.isEmpty(comment)) {
                    if (isInternetPresent) {
                        requestAddComment(post_id)
                    } else {
                        showToastShort(getString(R.string.internet_toast), requireContext())
                    }
                }
            }
            R.id.linear_all_cmnts -> {
                if (allCommentsArrayList.size > 0) {
                    callActivity(requireContext(), Intent(requireContext(), ViewAllCmntActivity::class.java))
                    requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
                }
            }
        }
    }

    override fun showProgress() {
        progressBar?.visibility = View.VISIBLE
        enableDisableViewGroup(rootLayout!!, false)
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        enableDisableViewGroup(rootLayout!!, true)
    }

    // delete sub comment
    override fun delSubComment(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status.equals("1", ignoreCase = true) -> {
                    //  CommonMethod.showToastShort(response_msg, requireContext());
                }
                response_invalid.equals("1", ignoreCase = true) -> {
                    logout(response_msg)
                }
                else -> {
                    showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // comment delete
    override fun setDelCmnt(response_status: String, response_msg: String, response_invalid: String) {
        try {
            when {
                response_status.equals("1", ignoreCase = true) -> {
                    //    CommonMethod.showToastShort(response_msg, requireContext());
                }
                response_invalid.equals("1", ignoreCase = true) -> {
                    logout(response_msg)
                }
                else -> {
                    showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // add cmnt
    override fun setAddCmnt(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            when {
                response_status.equals("1", ignoreCase = true) -> {
                    tvNoComments?.visibility = View.GONE
                    edt_comment?.setText("")
                    val allComments = AllComments()
                    allComments.comment_id = response_data.allComments[0].comment_id
                    allComments.user_id = response_data.allComments[0].user_id
                    allComments.user_name = response_data.allComments[0].user_name
                    allComments.profile_image = response_data.allComments[0].profile_image
                    allComments.user_comment = response_data.allComments[0].user_comment
                    allComments.comment_timestamp = response_data.allComments[0].comment_timestamp
                    allComments.setPostId(response_data.allComments[0].getPostId())
                    allComments.post_user_id = response_data.allComments[0].post_user_id
                    allComments.reply_comments = response_data.allComments[0].reply_comments
                    allCommentsArrayList.add(allComments)
                    // tempCommentsArrayList.add(allComments);
                    commentAdapter?.notifyDataSetChanged()
                    recycler_comment.layoutManager?.scrollToPosition(recycler_comment.adapter?.itemCount!! - 1)
                }
                response_invalid.equals("1", ignoreCase = true) -> {
                    logout(response_msg)
                }
                else -> {
                    showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //all cmnt
    override fun setDataToRecyclerView(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        try {
            when {
                response_status.equals("1", ignoreCase = true) -> {
                    allCommentsArrayList.clear()
                    allCommentsArrayList = response_data.allComments
                    if (allCommentsArrayList.size > 0) {
                        tvNoComments?.visibility = View.GONE
                        viewAllComments?.visibility = View.VISIBLE
                    } else {
                        tvNoComments?.visibility = View.VISIBLE
                        viewAllComments?.visibility = View.GONE
                    }
                    setCommentAdapter()
                }
                response_invalid.equals("1", ignoreCase = true) -> {
                    logout(response_msg)
                }
                else -> {
                    showToastShort(response_msg, requireContext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, "" + throwable.message)
    }

    // logout method
    private fun logout(response_msg: String) {
        showToastShort(response_msg, requireContext())
        BMSPrefs.putString(requireContext(), Constants._ID, "")
        BMSPrefs.putString(requireContext(), Constants.isStoreCreated, "")
        BMSPrefs.putString(requireContext(), Constants.PROFILE_IMAGE, "")
        callActivityFinish(requireContext(), Intent(requireContext(), LoginActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        requireActivity().finish()
    }

    // delete cmnt
    override fun onDelCmntClick(post_id: String, comment_id: String) {
        if (isInternetPresent) {
            requestDeleteComment(post_id, comment_id)
        } else {
            showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // reply comment
    override fun onReplyCmntClick(user_comment: String, user_name: String, user_image: String, comment_id: String, user_id: String) {
        BMSPrefs.putString(requireContext(), "user_comment", user_comment)
        BMSPrefs.putString(requireContext(), "user_name", user_name)
        BMSPrefs.putString(requireContext(), "user_image", user_image)
        BMSPrefs.putString(requireContext(), "comment_id", comment_id)
        BMSPrefs.putString(requireContext(), "user_comment_id", user_id)
        callActivity(requireContext(), Intent(requireContext(), ReplyCommentActivity::class.java))
        requireActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    // report comment
    override fun onReportPostClick(comment_id: String, position: Int) {
        BMSPrefs.putString(requireContext(), "comments_type", "1")
        val reportPostDialog = ReportCommentDialog(requireContext(), comment_id, post_id,
                allCommentsArrayList, position, commentAdapter, "")
        reportPostDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    // delete sub comment
    override fun onDelSubCmntClick(reply_id: String, comment_id: String) {
        if (isInternetPresent) {
            requestDeleteSubComment(reply_id, comment_id)
        } else {
            showToastShort(getString(R.string.internet_toast), requireContext())
        }
    }

    // report sub comment
    override fun onReportSubCmntClick(comment_id: String, position: Int, reply_commentsArrayList: ArrayList<Reply_comments>, reply_id: String) {
        val reply_comments1 = reply_commentsArrayList[position]
        //        reply_comments=allCommentsArrayList.get(position).getReply_comments();
        reply_comments.add(reply_comments1)
        BMSPrefs.putString(requireContext(), "comments_type", "2")
        val reportPostDialog = ReportCommentDialog(requireContext(), reply_id, post_id, reply_commentsArrayList, position, commentAdapter, reply_id)
        reportPostDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportPostDialog.show()
    }

    companion object {
        private const val TAG = "CommentFragment"
    }
}