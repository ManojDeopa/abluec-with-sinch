package com.ablueclive.activity.commentActivity;

import java.util.HashMap;

public class DelCmntPresenter implements DelCmntContract.Presenter, DelCmntContract.Model.OnFinishedListener {
    private DelCmntContract.View delCommentView;
    private DelCmntContract.Model delCommentModel;

    public DelCmntPresenter(DelCmntContract.View delCommentView) {
        this.delCommentView = delCommentView;
        this.delCommentModel = new DelCmntModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (delCommentView != null) {
            delCommentView.hideProgress();
            delCommentView.setDelCmnt(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (delCommentView != null) {
            delCommentView.hideProgress();
            delCommentView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        delCommentView = null;
    }

    @Override
    public void requestDelComment(HashMap<String, String> param) {
        if (delCommentView != null) {
            delCommentView.showProgress();
            delCommentModel.delComment(this, param);
        }

    }
}
