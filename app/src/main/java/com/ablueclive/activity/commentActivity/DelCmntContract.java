package com.ablueclive.activity.commentActivity;

import java.util.HashMap;

public interface DelCmntContract {
    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void delComment(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDelCmnt(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void requestDelComment(HashMap<String, String> param);
    }
}
