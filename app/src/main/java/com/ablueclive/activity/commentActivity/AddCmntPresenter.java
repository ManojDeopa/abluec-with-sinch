package com.ablueclive.activity.commentActivity;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class AddCmntPresenter implements AddCmntContract.Presenter, AddCmntContract.Model.OnFinishedListener {
    private AddCmntContract.View addCmntView;
    private final AddCmntContract.Model addCmntModel;

    public AddCmntPresenter(AddCmntContract.View addCmntView) {
        this.addCmntView = addCmntView;
        this.addCmntModel = new AddCmntModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (addCmntView != null) {
            addCmntView.hideProgress();
            addCmntView.setAddCmnt(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (addCmntView != null) {
            addCmntView.hideProgress();
            addCmntView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        addCmntView = null;
    }

    @Override
    public void requestAddComment(HashMap<String, String> param) {
        if (addCmntView != null) {
            addCmntView.showProgress();
            addCmntModel.addComment(this, param);
        }

    }
}
