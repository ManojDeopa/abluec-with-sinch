package com.ablueclive.activity.replyComment;

import java.util.HashMap;

public interface DeleleCmntContract
{
    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void delSubComment(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void delSubComment(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void requestDelSubComment(HashMap<String, String> param);
    }
}
