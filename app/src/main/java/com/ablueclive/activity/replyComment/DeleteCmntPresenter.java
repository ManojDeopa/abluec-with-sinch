package com.ablueclive.activity.replyComment;

import java.util.HashMap;

public class DeleteCmntPresenter implements DeleleCmntContract.Presenter, DeleleCmntContract.Model.OnFinishedListener {
    private DeleleCmntContract.View delSubCmntView;
    private DeleleCmntContract.Model delSubCmntModel;

    public DeleteCmntPresenter(DeleleCmntContract.View delSubCmntView) {
        this.delSubCmntView = delSubCmntView;
        this.delSubCmntModel = new DeleteCmntModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (delSubCmntView != null) {
            delSubCmntView.hideProgress();
            delSubCmntView.delSubComment(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (delSubCmntView != null) {
            delSubCmntView.hideProgress();
            delSubCmntView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        delSubCmntView=null;
    }

    @Override
    public void requestDelSubComment(HashMap<String, String> param) {
        if (delSubCmntView != null) {
            delSubCmntView.showProgress();
            delSubCmntModel.delSubComment(this, param);
        }

    }
}
