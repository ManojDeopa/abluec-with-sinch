package com.ablueclive.activity.replyComment;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class ReplyCmntPresenter implements ReplyCmntContract.Presenter, ReplyCmntContract.Model.OnFinishedListener {
    private ReplyCmntContract.View replyCmntView;
    private ReplyCmntContract.Model replyCmntModel;

    public ReplyCmntPresenter(ReplyCmntContract.View replyCmntView) {
        this.replyCmntView = replyCmntView;
        this.replyCmntModel = new ReplyCmntModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (replyCmntView != null) {
            replyCmntView.hideProgress();
            replyCmntView.setReplyCmnt(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (replyCmntView != null) {
            replyCmntView.hideProgress();
            replyCmntView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        replyCmntView = null;
    }

    @Override
    public void requestReplyComment(HashMap<String, String> param) {
        if (replyCmntView != null) {
            replyCmntView.showProgress();
            replyCmntModel.replyComment(this, param);
        }

    }
}
