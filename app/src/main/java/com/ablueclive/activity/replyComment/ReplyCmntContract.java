package com.ablueclive.activity.replyComment;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface ReplyCmntContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void replyComment(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setReplyCmnt(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void requestReplyComment(HashMap<String, String> param);
    }
}
