package com.ablueclive.activity.replyComment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.activity.frndProfile.FriendProfileActivity;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.adapter.ReplySubCmntAdapter;
import com.ablueclive.listener.onDeleteSubCmntListener;
import com.ablueclive.modelClass.ReplyData;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CircleImageView;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class ReplyCommentActivity extends AppCompatActivity implements View.OnClickListener,
        ReplyCmntContract.View, onDeleteSubCmntListener, DeleleCmntContract.View {
    private TextView text_comment, text_cmnt_username;
    private ImageView img_back, img_submit_comment;
    private CircleImageView img_comment_profile;
    private EditText edt_comment;
    private ReplySubCmntAdapter replySubCmntAdapter;
    private String user_comment, user_name, user_image = "",
            session_token, post_id, user_id, comment_id, post_comment, user_comment_id;
    private RecyclerView recycler_sub_comment;
    private ProgressDialog progressDialog;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private static final String TAG = "ReplyCommentActivity";
    private ReplyCmntPresenter replyCmntPresenter;
    private DeleteCmntPresenter deleteCmntPresenter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ReplyData> replyDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_comment);
        findView();
        setData();

    }

    // initialize object here...
    private void findView() {
        session_token = BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN);
        post_id = BMSPrefs.getString(getApplicationContext(), Constants.FEED_POST_ID);
        user_id = BMSPrefs.getString(getApplicationContext(), Constants._ID);
        user_comment = BMSPrefs.getString(getApplicationContext(), "user_comment");
        user_name = BMSPrefs.getString(getApplicationContext(), "user_name");
        user_image = BMSPrefs.getString(getApplicationContext(), "user_image");
        comment_id = BMSPrefs.getString(getApplicationContext(), "comment_id");
        user_comment_id = BMSPrefs.getString(getApplicationContext(), "user_comment_id");
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        text_comment = findViewById(R.id.text_comment);
        text_cmnt_username = findViewById(R.id.text_cmnt_username);
        img_comment_profile = findViewById(R.id.img_comment_profile);
        recycler_sub_comment = findViewById(R.id.recycler_sub_comment);
        img_submit_comment = findViewById(R.id.img_submit_comment);
        edt_comment = findViewById(R.id.edt_comment);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        img_submit_comment.setOnClickListener(this);
        setCommentAdapter();
    }

    // set data
    private void setData() {
        text_comment.setText(user_comment);
        text_cmnt_username.setText(user_name);
        Glide.with(getApplicationContext())
                .load(Constants.PROFILE_IMAGE_URL + user_image)
                .error(R.drawable.user_profile)
                .skipMemoryCache(true)
                .into(img_comment_profile);

        text_cmnt_username.setOnClickListener(v -> {
            BMSPrefs.putString(ReplyCommentActivity.this, Constants.FRIEND_ID, user_comment_id);
            CommonMethod.callActivity(ReplyCommentActivity.this, new Intent(ReplyCommentActivity.this, FriendProfileActivity.class));
        });

        img_comment_profile.setOnClickListener(v -> {
            BMSPrefs.putString(ReplyCommentActivity.this, Constants.FRIEND_ID, user_comment_id);
            CommonMethod.callActivity(ReplyCommentActivity.this, new Intent(ReplyCommentActivity.this, FriendProfileActivity.class));
        });
    }

    // reply sub  comment request param
    private void requestSubComment() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("post_id", post_id);
        params.put("comment_user_id", user_id);
        params.put("session_token", session_token);
        params.put("comment_id", comment_id);
        params.put("post_comment", post_comment);
        Log.d("SubCommentParam", params.toString());
        replyCmntPresenter = new ReplyCmntPresenter(this);
        replyCmntPresenter.requestReplyComment(params);
    }

    // delete sub  comment request param
    private void requestDeleteSubComment(String reply_id) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("post_id", post_id);
        params.put("comment_id", comment_id);
        params.put("reply_id", reply_id);
        Log.d("DelSubCommentParam", params.toString());
        deleteCmntPresenter = new DeleteCmntPresenter(this);
        deleteCmntPresenter.requestDelSubComment(params);
    }


    //set user feed adapter
    private void setCommentAdapter() {
        replySubCmntAdapter = new ReplySubCmntAdapter(ReplyCommentActivity.this, replyDataArrayList, this);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recycler_sub_comment.setLayoutManager(linearLayoutManager);
        recycler_sub_comment.setAdapter(replySubCmntAdapter);

    }

    // validate  form
    private String validateForm() {
        post_comment = edt_comment.getText().toString().trim();

        if (post_comment.isEmpty() || post_comment.equals("")) {
            edt_comment.setFocusable(true);
            edt_comment.requestFocus();
            return getString(R.string.valid_comment);
        }
        return "success";
    }


    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }
            case R.id.img_submit_comment: {
                CommonMethod.hideKeyBoard(ReplyCommentActivity.this);
                String result = validateForm();
                if (result.equalsIgnoreCase("success")) {
                    if (isInternetPresent) {
                        requestSubComment();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }
        }
    }

    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    // delete comment
    @Override
    public void delSubComment(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            //  CommonMethod.showToastShort(response_msg, getApplicationContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    // reply comment
    @Override
    public void setReplyCmnt(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            edt_comment.setText("");
            ReplyData replyData = new ReplyData();
            replyData.setReply_id(response_data.getReplyData().get(0).getReply_id());
            replyData.setUser_id(response_data.getReplyData().get(0).getUser_id());
            replyData.setUser_name(response_data.getReplyData().get(0).getUser_name());
            replyData.setProfile_image(response_data.getReplyData().get(0).getProfile_image());
            replyData.setReply_user_comment(response_data.getReplyData().get(0).getReply_user_comment());
            replyData.setComment_timestamp(response_data.getReplyData().get(0).getComment_timestamp());
            replyDataArrayList.add(replyData);
            // tempCommentsArrayList.add(allComments);
            replySubCmntAdapter.notifyDataSetChanged();
            recycler_sub_comment.getLayoutManager().scrollToPosition(recycler_sub_comment.getAdapter().getItemCount() - 1);


        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }

    // logout method
    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getApplicationContext());
        BMSPrefs.putString(getApplicationContext(), Constants._ID, "");
        BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, "");
        CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        finish();
    }

    //delete sub cmnt
    @Override
    public void onDelSubCmntClick(String reply_id) {
        if (isInternetPresent) {
            requestDeleteSubComment(reply_id);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }
}
