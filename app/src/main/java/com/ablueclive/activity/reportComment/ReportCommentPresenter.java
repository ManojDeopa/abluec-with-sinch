package com.ablueclive.activity.reportComment;

import java.util.HashMap;

public class ReportCommentPresenter implements ReportCommentContract.Presenter, ReportCommentContract.Model.OnFinishedListener {
    private ReportCommentContract.View reportCommentView;
    private ReportCommentContract.Model reportCommentModel;

    public ReportCommentPresenter(ReportCommentContract.View reportCommentView) {
        this.reportCommentView = reportCommentView;
        this.reportCommentModel = new ReportCommentModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (reportCommentView != null) {
            reportCommentView.hideProgress();
            reportCommentView.reportComment(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (reportCommentView != null) {
            reportCommentView.hideProgress();
            reportCommentView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        reportCommentView = null;
    }

    @Override
    public void requestReportComment(HashMap<String, String> param) {
        if (reportCommentView != null) {
            reportCommentView.showProgress();
            reportCommentModel.reportComment(this, param);
        }

    }
}
