package com.ablueclive.activity.reportComment;

import java.util.HashMap;

public interface ReportCommentContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void reportComment(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void reportComment(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {
        void onDestroy();

        void requestReportComment(HashMap<String, String> param);
    }
}
