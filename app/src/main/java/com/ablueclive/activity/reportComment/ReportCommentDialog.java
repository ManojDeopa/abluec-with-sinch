package com.ablueclive.activity.reportComment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ablueclive.R;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportCommentDialog extends Dialog implements View.OnClickListener, ReportCommentContract.View {
    private Button btn_submit;
    private CheckBox chk_one, chk_two, chk_three;
    private String comment_id, session_token, report_content, post_id, content, comments_type, reply_id = "";
    private ProgressDialog progressDialog;
    private static final String TAG = "ReportCommentDialog";
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ReportCommentPresenter reportCommentPresenter;
    private ArrayList allCommentsArrayList = new ArrayList<>();
    //    private ArrayList<Reply_comments> reply_commentsArrayList;
    int position;
    private RecyclerView.Adapter commentAdapter;

    public ReportCommentDialog(@NonNull Context context, String comment_id, String post_id, ArrayList allCommentsArrayList,
                               int position, RecyclerView.Adapter commentAdapter, String reply_id
//            , ArrayList<Reply_comments> reply_commentsArrayList
    ) {
        super(context);
        this.comment_id = comment_id;
        this.post_id = post_id;
        this.allCommentsArrayList = allCommentsArrayList;
        this.position = position;
        this.commentAdapter = commentAdapter;
        this.reply_id = reply_id;
//        this.reply_commentsArrayList = reply_commentsArrayList;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.report_dialog);
        findView();
    }

    private void findView() {
        session_token = BMSPrefs.getString(getContext(), Constants.SESSION_TOKEN);
        comments_type = BMSPrefs.getString(getContext(), "comments_type");
        connectionDetector = new ConnectionDetector(getContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        chk_one = findViewById(R.id.chk_one);
        chk_two = findViewById(R.id.chk_two);
        chk_three = findViewById(R.id.chk_three);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        chk_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    report_content = "1";
                    content = "it's annoying or not interesting";
                    chk_two.setChecked(false);
                    chk_three.setChecked(false);
                }
            }
        });

        chk_two.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    report_content = "2";
                    content = "I think it should'nt be on craftsmen's Lodge ";
                    chk_one.setChecked(false);
                    chk_three.setChecked(false);
                }
            }
        });

        chk_three.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    report_content = "3";
                    content = "It's Spam";
                    chk_one.setChecked(false);
                    chk_two.setChecked(false);
                }
            }
        });
    }

    // report post param
    private void reportPostParam() {
        HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("post_id", post_id);
        params.put("report_reason", report_content);
        params.put("report_content", content);
        params.put("comment_id", comment_id);
        params.put("comments_type", comments_type);
        params.put("reply_id", "");
        Log.d("reportCommentPAram", params.toString());
        //   allProductPresenter.getMoreData(params);
        reportCommentPresenter = new ReportCommentPresenter(this);
        reportCommentPresenter.requestReportComment(params);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit: {
                if (!TextUtils.isEmpty(report_content)) {
                    if (isInternetPresent) {
                        reportPostParam();
                        allCommentsArrayList.remove(position);
                        commentAdapter.notifyDataSetChanged();
                        dismiss();
                    } else {
                        CommonMethod.showToastShort(getContext().getString(R.string.internet_toast), getContext());
                    }
                } else {
                    CommonMethod.showToastShort("please choose above one option", getContext());
                }

                break;
            }
        }
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(getContext(), "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void reportComment(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
        } else {
            CommonMethod.showToastShort(response_msg, getContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }
}
