package com.ablueclive.activity.allFriendList;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface AllFrndContract {
    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data);

            void onFailure(Throwable t);
        }

        void getAllFrnd(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(String response_status, String response_msg, String response_invalid, Response_data response_data);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {

        void onDestroy();

        void requestAllFrnd(HashMap<String, String> param);
    }
}
