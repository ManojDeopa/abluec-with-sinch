package com.ablueclive.activity.allFriendList;

import android.util.Log;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.networkClass.RetrofitClient;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class AllFrndModel implements AllFrndContract.Model {
    private static final String TAG = "AllFrndModel";

    @Override
    public void getAllFrnd(OnFinishedListener onFinishedListener, HashMap<String, String> hashMap) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.getAllFriendsList(hashMap).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }
                    @Override
                    public void onNext(@NotNull ResponseModel responseModel) {
                        String response_status = responseModel.getResponse_status();
                        String response_msg = responseModel.getResponse_msg();
                        String response_invalid = responseModel.getResponse_invalid();
                        Response_data response_data = new Response_data();
                        if (response_status.equalsIgnoreCase("1")) {
                            response_data = responseModel.getResponse_data();
                            onFinishedListener.onFinished(response_status, response_msg, response_invalid, response_data);
                        }

                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.d(TAG, e.getMessage());
                        onFinishedListener.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
