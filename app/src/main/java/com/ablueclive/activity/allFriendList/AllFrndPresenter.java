package com.ablueclive.activity.allFriendList;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class AllFrndPresenter implements AllFrndContract.Presenter, AllFrndContract.Model.OnFinishedListener {
    private AllFrndContract.View allFrndView;
    private final AllFrndContract.Model allFrndModel;

    public AllFrndPresenter(AllFrndContract.View allFrndView) {
        this.allFrndView = allFrndView;
        this.allFrndModel = new AllFrndModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (allFrndView != null) {
            allFrndView.hideProgress();
            allFrndView.setDataToRecyclerView(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (allFrndView != null) {
            allFrndView.hideProgress();
            allFrndView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        allFrndView = null;
    }

    @Override
    public void requestAllFrnd(HashMap<String, String> param) {
        if (allFrndView != null) {
            allFrndView.showProgress();
            allFrndModel.getAllFrnd(this, param);
        }

    }
}
