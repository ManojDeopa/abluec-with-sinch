package com.ablueclive.activity.signup;

import android.util.Log;

import androidx.annotation.NonNull;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.networkClass.RetrofitClient;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SignUpOtpModel implements SignUpOtpContract.Model {
    private static final String TAG = "SignUpOtpModel";

    @Override
    public void getRegisterOtp(OnFinishedListener onFinishedListener, HashMap<String, String> param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.register(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull ResponseModel responseModel) {
                        String response_status = responseModel.getResponse_status();
                        String response_msg = responseModel.getResponse_msg();
                        Response_data response_data = null;
                        if (response_status.equals("1")) {
                            response_data = responseModel.getResponse_data();
                        }
                        onFinishedListener.onFinished(response_status, response_msg, response_data);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d(TAG, "" + e.getMessage());
                        onFinishedListener.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }
}
