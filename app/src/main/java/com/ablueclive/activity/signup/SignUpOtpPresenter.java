package com.ablueclive.activity.signup;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class SignUpOtpPresenter implements SignUpOtpContract.Presenter, SignUpOtpContract.Model.OnFinishedListener {
    private SignUpOtpContract.View signupOtpView;
    private SignUpOtpContract.Model signupOtpModel;

    public SignUpOtpPresenter(SignUpOtpContract.View signupOtpView) {
        this.signupOtpView = signupOtpView;
        this.signupOtpModel = new SignUpOtpModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, Response_data response_data) {
        if (signupOtpView != null) {
            signupOtpView.hideProgress();
            signupOtpView.setDataToViews(response_status, response_msg, response_data);
        }


    }

    @Override
    public void onFailure(Throwable t) {
        if (signupOtpView != null) {
            signupOtpView.hideProgress();
            signupOtpView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        // signupOtpView = null;
    }

    @Override
    public void requestRegisterOtp(HashMap<String, String> param) {
        if (signupOtpView != null) {
            signupOtpView.showProgress();
            signupOtpModel.getRegisterOtp(this, param);
        }

    }
}
