package com.ablueclive.activity.signup;
import com.ablueclive.modelClass.Response_data;
import java.util.HashMap;

public interface SignUpOtpContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, Response_data response_data);

            void onFailure(Throwable t);
        }

        void getRegisterOtp(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg, Response_data response_data);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestRegisterOtp(HashMap<String, String> param);
    }
}
