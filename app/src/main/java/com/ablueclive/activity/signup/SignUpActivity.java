package com.ablueclive.activity.signup;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.activity.staticPage.StaticPageActivity;
import com.ablueclive.global.activity.InviteContactsActivity;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.GPSTracker;
import com.ablueclive.utils.ProgressD;
import com.hbb20.CountryCodePicker;

import java.util.HashMap;

import static com.ablueclive.utils.CommonMethod.isValidPassword;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, SignUpOtpContract.View, CountryCodePicker.OnCountryChangeListener {
    private LinearLayout signup_view;
    private LinearLayout verification_view;
    private EditText edt_name, edt_displayName, edt_email, edt_phn_number, edt_password, edt_confirm_password, edt_code;
    private String name;
    private String displayName;
    private String email;
    private String phone_number;
    private String password;
    private String device_id;
    private String device_token;
    private String otp;
    private boolean isInternetPresent = false;
    private static final String TAG = "SignUpActivity";
    private boolean isCheckedd = false;
    private boolean isPrivacyChecked = false;
    private boolean isEulaChecked = false;
    private GPSTracker gpsTracker;
    private CountryCodePicker ccp;
    String countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        findView();
        getCurrentLatLong();
    }

    private void getCurrentLatLong() {
        gpsTracker = new GPSTracker(SignUpActivity.this);
    }

    // initialize object
    @SuppressLint("ClickableViewAccessibility")
    private void findView() {
        device_id = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_ID);
        device_token = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_TOKEN);

        ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        signup_view = findViewById(R.id.signup_view);
        verification_view = findViewById(R.id.verification_view);
        Button btn_submits = findViewById(R.id.btn_submits);
        TextView text_signin = findViewById(R.id.text_signin);
        edt_name = findViewById(R.id.edt_name);
        edt_displayName = findViewById(R.id.edt_displayName);
        edt_email = findViewById(R.id.edt_email);
        edt_phn_number = findViewById(R.id.edt_phn_number);
        edt_password = findViewById(R.id.edt_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);
        edt_code = findViewById(R.id.edt_code);
        Button btn_submit_otp = findViewById(R.id.btn_submit_otp);

        TextView text_terms = findViewById(R.id.text_terms);
        TextView tvPrivacyPolicy = findViewById(R.id.tvPrivacyPolicy);
        TextView tvEula = findViewById(R.id.tvEula);

        ccp = findViewById(R.id.ccp);
        ccp.setOnCountryChangeListener(this);
        countryCode = ccp.getDefaultCountryCodeWithPlus();


        ImageView img_back = findViewById(R.id.img_back);
        TextView title = findViewById(R.id.title);
        TextView txt_send_code = findViewById(R.id.txt_send_code);
        btn_submits.setOnClickListener(this);
        text_signin.setOnClickListener(this);
        btn_submit_otp.setOnClickListener(this);
        txt_send_code.setOnClickListener(this);
        img_back.setOnClickListener(this);

        text_terms.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
        tvEula.setOnClickListener(this);

        CheckBox chk_select = findViewById(R.id.chk_select);
        chk_select.setOnCheckedChangeListener((buttonView, isChecked) -> isCheckedd = isChecked);

        CheckBox cbPrivacyPolicy = findViewById(R.id.cbPrivacyPolicy);
        cbPrivacyPolicy.setOnCheckedChangeListener((buttonView, isChecked) -> isPrivacyChecked = isChecked);

        CheckBox cbEula = findViewById(R.id.cbEula);
        cbEula.setOnCheckedChangeListener((buttonView, isChecked) -> isEulaChecked = isChecked);

        chk_select.setOnCheckedChangeListener((buttonView, isChecked) -> isCheckedd = isChecked);

        edt_phn_number.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1 && s.toString().startsWith("0")) {
                    s.clear();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        EditText etAt = findViewById(R.id.etAt);
        edt_displayName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (etAt.getVisibility() == View.GONE) {
                    etAt.setVisibility(View.VISIBLE);
                }
            }
        });

        title.setText(getString(R.string.create_account));
    }


    private boolean keyboardShown(View rootView) {

        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }


    // otp request param
    private void signUpOtpRequest(String request_type) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("display_name", displayName);
        params.put("email", email);
        params.put("mobile", phone_number);
        params.put("password", password);
        params.put("device_id", device_id);
        params.put("device_token", device_token);
        params.put("device_type", "1");
        params.put("request_type", request_type);
        params.put("country_code", countryCode);
        params.put("lat", String.valueOf(gpsTracker.getLatitude()));
        params.put("long", String.valueOf(gpsTracker.getLongitude()));

        Log.d(TAG, "signUpOtpRequest: " + params);
        SignUpOtpPresenter signUpOtpPresenter = new SignUpOtpPresenter(this);
        signUpOtpPresenter.requestRegisterOtp(params);
    }


    // listener
    @SuppressLint({"ResourceAsColor", "NonConstantResourceId"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submits: {
                CommonMethod.hideKeyBoard(SignUpActivity.this);
                String result = validateSignupForm();
                if (result.equalsIgnoreCase("success")) {
                    signUpOtpRequest("1");
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }

            case R.id.text_signin: {
                CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                finish();
                break;
            }

            case R.id.btn_submit_otp: {
                CommonMethod.hideKeyBoard(SignUpActivity.this);
                String result = validateOtpForm();
                if (result.equalsIgnoreCase("success")) {
                    if (isInternetPresent) {
                        signUpOtpRequest("2");
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }
            case R.id.text_terms: {

                // BMSPrefs.putString(getApplicationContext(), Constants.STATIC_PAGE_NAME, "terms");
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), StaticPageActivity.class).
                        putExtra("url", Constants.TERMS).putExtra("title", getString(R.string.terms_amp_conditions)));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.tvPrivacyPolicy: {

                // BMSPrefs.putString(getApplicationContext(), Constants.STATIC_PAGE_NAME, "terms");
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), StaticPageActivity.class).
                        putExtra("url", Constants.PRIVACY).putExtra("title", getString(R.string.privacy_policy)));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.tvEula: {

                // BMSPrefs.putString(getApplicationContext(), Constants.STATIC_PAGE_NAME, "terms");
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), StaticPageActivity.class).
                        putExtra("url", Constants.USER_LICENCE).putExtra("title", getString(R.string.end_user_licence_agreement)));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }


            case R.id.img_back: {
                onBackPressed();
                break;
            }

            case R.id.txt_send_code: {
                if (isInternetPresent) {
                    signUpOtpRequest("1");
                } else {
                    CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                }
                break;
            }

        }
    }

    // validate signup form
    private String validateSignupForm() {

        name = edt_name.getText().toString().trim();
        displayName = edt_displayName.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        phone_number = edt_phn_number.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        String confirm_password = edt_confirm_password.getText().toString().trim();

        if (name.isEmpty()) {
            edt_name.setFocusable(true);
            edt_name.requestFocus();
            return getString(R.string.valid_name);
        }

        if (displayName.isEmpty()) {
            edt_displayName.setFocusable(true);
            edt_displayName.requestFocus();
            return getString(R.string.valid_display_name);
        }

        if (CommonMethod.isValidDisplayName(displayName)) {
            edt_displayName.setFocusable(true);
            edt_displayName.requestFocus();
            return getString(R.string.invalid_display_name);
        }

        if (email.isEmpty() || !CommonMethod.isEmailValid(email)) {
            edt_email.setFocusable(true);
            edt_email.requestFocus();
            return getString(R.string.valid_email);
        }

        if (TextUtils.isEmpty(countryCode)) {
            return getString(R.string.select_country_code);
        }

        if (phone_number.isEmpty() || phone_number.length() < 6) {
            edt_phn_number.setFocusable(true);
            edt_phn_number.requestFocus();
            return getString(R.string.valid_phone);
        }

        if (password.isEmpty()) {
            edt_password.setFocusable(true);
            edt_password.requestFocus();
            return getString(R.string.valid_password);
        }

        if (!isValidPassword(password)) {
            edt_password.setFocusable(true);
            edt_password.requestFocus();
            return getString(R.string.alpha_numeric_password_validation);
        }

        if (password.length() < 8) {
            edt_password.setFocusable(true);
            edt_password.requestFocus();
            return getString(R.string.valid_password_digit);
        }

        if (confirm_password.isEmpty()) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.valid_password_blank);
        }

        if (!confirm_password.contentEquals(password)) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.same_password);
        }

        if (!isCheckedd) {
            return getString(R.string.chk_terms_amp_conditions);
        }

        if (!isPrivacyChecked) {
            return getString(R.string.chk_privacy_policy);
        }

        if (!isEulaChecked) {
            return getString(R.string.chk_eula);
        }

        if (!isInternetPresent) {
            return getString(R.string.internet_toast);
        }

        return "success";
    }


    // validate otp form
    private String validateOtpForm() {
        String otp_code = edt_code.getText().toString().trim();
        if (otp_code.isEmpty()) {
            edt_code.setFocusable(true);
            edt_code.requestFocus();
            return getString(R.string.valid_code);
        }

        if (!otp_code.contentEquals(otp)) {
            edt_code.setFocusable(true);
            edt_code.requestFocus();
            return getString(R.string.valid_code);
        }

        return "success";
    }


    // backpress
    @Override
    public void onBackPressed() {
        if (signup_view.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        } else if (verification_view.getVisibility() == View.VISIBLE) {
            verification_view.setVisibility(View.GONE);
            verification_view.animate();
            signup_view.setVisibility(View.VISIBLE);
            edt_code.setText("");
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        }
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(SignUpActivity.this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    // signup otp
    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        ProgressD.Companion.hide();

        if (response_status.equalsIgnoreCase("1")) {
            if (response_data.getOTP() != null) {
                otp = response_data.getOTP();
                verification_view.setVisibility(View.VISIBLE);
                signup_view.setVisibility(View.GONE);
            } else {
                BMSPrefs.putString(this, "IS_SIGN_UP", "1");
                BMSPrefs.putString(getApplicationContext(), Constants.SESSION_TOKEN, response_data.getProfile().getSession_token());
                BMSPrefs.putString(getApplicationContext(), Constants._ID, response_data.getProfile().get_id());
                BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, response_data.getProfile().getIsStoreCreated());
                BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, response_data.getProfile().getProfile_image());
                BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, response_data.getProfile().getBackground_image());
                BMSPrefs.putString(getApplicationContext(), Constants.USER_NAME, response_data.getProfile().getName());
                BMSPrefs.putString(getApplicationContext(), Constants.DISPLAY_NAME, response_data.getProfile().getDisplay_name());
                BMSPrefs.putString(getApplicationContext(), Constants.USER_EMAIL, response_data.getProfile().getEmail());
                BMSPrefs.putString(getApplicationContext(), Constants.USER_PHONE, response_data.getProfile().getMobile());
                BMSPrefs.putBoolean(getApplicationContext(), Constants.ARE_INVITED, false);
                CommonMethod.callActivity(this, new Intent(this, InviteContactsActivity.class));
                finish();
            }

        } else {
            CommonMethod.showToastlong(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, "" + throwable.getMessage());
    }

    @Override
    public void onCountrySelected() {
        countryCode = ccp.getSelectedCountryCodeWithPlus();
    }
}
