package com.ablueclive.activity.viewAllComment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;

import com.ablueclive.R;
import com.ablueclive.activity.commentActivity.DelCmntContract;
import com.ablueclive.activity.commentActivity.DelCmntPresenter;
import com.ablueclive.activity.commentActivity.GetCommentContract;
import com.ablueclive.activity.commentActivity.GetCommentPresenter;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.activity.replyComment.DeleleCmntContract;
import com.ablueclive.activity.replyComment.DeleteCmntPresenter;
import com.ablueclive.activity.replyComment.ReplyCommentActivity;
import com.ablueclive.activity.reportComment.ReportCommentDialog;
import com.ablueclive.adapter.CommentAdapter;
import com.ablueclive.listener.onDelSubCmntListener;
import com.ablueclive.listener.onDeleteCmntListener;
import com.ablueclive.listener.onReplyCmntListener;
import com.ablueclive.listener.onReportPostListener;
import com.ablueclive.listener.onReportSubCmntListener;
import com.ablueclive.modelClass.AllComments;
import com.ablueclive.modelClass.Reply_comments;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewAllCmntActivity extends AppCompatActivity implements View.OnClickListener,
        GetCommentContract.View, onDeleteCmntListener, DelCmntContract.View, onReplyCmntListener, onReportPostListener,
        onDelSubCmntListener, onReportSubCmntListener, DeleleCmntContract.View {
    private ImageView img_back;
    private RecyclerView recycler_comment;
    private String session_token, post_id, user_id;
    private CommentAdapter commentAdapter;
    private ProgressDialog progressDialog;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private LinearLayoutManager linearLayoutManager;
    private static final String TAG = "CommentActivity";
    private GetCommentPresenter getCommentPresenter;
    private DelCmntPresenter delCmntPresenter;
    private DeleteCmntPresenter deleteCmntPresenter;
    private boolean isLoading = false;
    private int pageNo = 1, total_count;
    private ArrayList<AllComments> allCommentsArrayList = new ArrayList<>();
    private ArrayList<Reply_comments> reply_comments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_cmnt);
        findView();
        if (isInternetPresent) {
            requestGetComment(post_id, pageNo);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }


    // initialize object...
    private void findView() {
        session_token = BMSPrefs.getString(getApplicationContext(), Constants.SESSION_TOKEN);
        post_id = BMSPrefs.getString(getApplicationContext(), Constants.FEED_POST_ID);
        user_id = BMSPrefs.getString(getApplicationContext(), Constants._ID);
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        img_back = findViewById(R.id.img_back);
        recycler_comment = findViewById(R.id.recycler_comment);

        img_back.setOnClickListener(this);

        setCommentAdapter();
        setListeners();

    }


    private void setListeners() {
        recycler_comment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (allCommentsArrayList.size() >= 10) {
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (dy > 0 && visibleItemCount + pastVisibleItems >= totalItemCount && !isLoading) {
                        isLoading = true;
                        total_count = totalItemCount;
                        requestGetComment(post_id, pageNo);
                    }
                }
            }
        });
    }

    // get comment request param
    private void requestGetComment(String post_id, int pageNo) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("post_id", post_id);
        params.put("page", "" + pageNo);
        params.put("session_token", session_token);
        Log.d("GetCommentParam", params.toString());
        getCommentPresenter = new GetCommentPresenter(this);
        getCommentPresenter.requestgetComment(params);
    }

    // delete comment request param
    private void requestDeleteComment(String post_id, String comment_id) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("post_id", post_id);
        params.put("comment_id", comment_id);
        params.put("session_token", session_token);
        Log.d("delCommentParam", params.toString());
        delCmntPresenter = new DelCmntPresenter(this);
        delCmntPresenter.requestDelComment(params);
    }

    // delete sub  comment request param
    private void requestDeleteSubComment(String reply_id, String comment_id) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("post_id", post_id);
        params.put("comment_id", comment_id);
        params.put("reply_id", reply_id);
        Log.d("DelSubCommentParam", params.toString());
        deleteCmntPresenter = new DeleteCmntPresenter(this);
        deleteCmntPresenter.requestDelSubComment(params);
    }


    //set user feed adapter
    private void setCommentAdapter() {
        commentAdapter = new CommentAdapter(ViewAllCmntActivity.this, allCommentsArrayList, this, user_id,
                this, this, this, this);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recycler_comment.setLayoutManager(linearLayoutManager);
        recycler_comment.setAdapter(commentAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }

        }
    }

    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this,"");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    // del sub cmnt
    @Override
    public void delSubComment(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    // comment delete
    @Override
    public void setDelCmnt(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }


    //all cmnt
    @Override
    public void setDataToRecyclerView(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            isLoading = false;
            allCommentsArrayList = response_data.getAllComments();
            if (allCommentsArrayList.size() > 0) {
                commentAdapter.addAll(allCommentsArrayList);
                pageNo++;
            } else {
                if (pageNo == 1) {

                } else {
                    CommonMethod.showToastShort(getString(R.string.no_more_data), getApplicationContext());
                }
            }
        } else if (response_invalid.equalsIgnoreCase("1")) {
            logout(response_msg);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }

    // logout method
    private void logout(String response_msg) {
        CommonMethod.showToastShort(response_msg, getApplicationContext());
        BMSPrefs.putString(getApplicationContext(), Constants._ID, "");
        BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, "");
        BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, "");
        CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
        finish();
    }


    // delete cmnt
    @Override
    public void onDelCmntClick(String post_id, String comment_id) {
        if (isInternetPresent) {
            requestDeleteComment(post_id, comment_id);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }

    // reply comment
    @Override
    public void onReplyCmntClick(String user_comment, String user_name, String user_image, String comment_id, String user_id) {
        BMSPrefs.putString(getApplicationContext(), "user_comment", user_comment);
        BMSPrefs.putString(getApplicationContext(), "user_name", user_name);
        BMSPrefs.putString(getApplicationContext(), "user_image", user_image);
        BMSPrefs.putString(getApplicationContext(), "comment_id", comment_id);
        BMSPrefs.putString(getApplicationContext(), "user_comment_id", user_id);

        CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), ReplyCommentActivity.class));
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }


    @Override
    public void onReportPostClick(String comment_id, int position) {
        BMSPrefs.putString(getApplicationContext(), "comments_type", "1");
        ReportCommentDialog reportPostDialog = new ReportCommentDialog(ViewAllCmntActivity.this, comment_id, post_id,
                allCommentsArrayList, position, commentAdapter,"");
        reportPostDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        reportPostDialog.show();
    }


    @Override
    public void onDelSubCmntClick(String reply_id, String comment_id) {
        if (isInternetPresent) {
            requestDeleteSubComment(reply_id, comment_id);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }

    }

    // report sub comment
    @Override
    public void onReportSubCmntClick(String comment_id, int position,ArrayList<Reply_comments> reply_commentsArrayList,String reply_id) {
        Reply_comments reply_comments1=reply_commentsArrayList.get(position);
//        reply_comments=allCommentsArrayList.get(position).getReply_comments();
        reply_comments.add(reply_comments1);
        BMSPrefs.putString(getApplicationContext(), "comments_type", "2");
        ReportCommentDialog reportPostDialog = new ReportCommentDialog(ViewAllCmntActivity.this, reply_id, post_id,
                reply_commentsArrayList, position, commentAdapter,reply_id);
        reportPostDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        reportPostDialog.show();
    }
}
