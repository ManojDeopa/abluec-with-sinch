package com.ablueclive.activity.editProfile;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class EditProfilePresenter implements EditProfileContract.Presenter, EditProfileContract.Model.OnFinishedListener {
    private EditProfileContract.View editProfileView;
    private EditProfileContract.Model editProfileModel;

    public EditProfilePresenter(EditProfileContract.View editProfileView) {
        this.editProfileView = editProfileView;
        this.editProfileModel = new EditProfileModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid, Response_data response_data) {
        if (editProfileView != null) {
            editProfileView.hideProgress();
            editProfileView.setDataToBackground(response_status, response_msg, response_invalid, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (editProfileView != null) {
            editProfileView.hideProgress();
            editProfileView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        editProfileView = null;
    }

    @Override
    public void requestEditProfile(HashMap<String, String> param) {
        if (editProfileView != null) {
            editProfileView.showProgress();
            editProfileModel.editProfile(this, param);
        }


    }
}
