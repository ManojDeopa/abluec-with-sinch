package com.ablueclive.activity.editProfile

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.appcompat.app.AppCompatActivity
import com.ablueclive.R
import com.ablueclive.activity.changePwd.ChangePwdActivity
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.activity.userProfile.ProfileImageContract
import com.ablueclive.activity.userProfile.ProfileImagePresenter
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.Response_data
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.simplecropimage.CropImage
import com.ablueclive.utils.*
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import com.hbb20.CountryCodePicker
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.edit_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.util.*


class EditProfileActivity : AppCompatActivity(),
        View.OnClickListener, EditProfileContract.View,
        ProfileImageContract.View, CountryCodePicker.OnCountryChangeListener {

    private var name: String = ""
    private var displayName: String = ""
    private var email: String = ""
    private var phone_number: String = ""
    private var user_name: String = ""
    private var user_email: String = ""
    private var user_phone: String = ""
    private var session_token: String = ""
    private var skype: String = ""
    private var strAbout: String = ""
    private var path = ""
    private var profile_image: String = ""
    private var cover_image: String = ""

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var file: Uri? = null

    val TYPE_PROFILE = "TYPE_PROFILE"
    val TYPE_COVER = "TYPE_COVER"
    var IMAGE_TYPE = ""
    var countryCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile)
        findView()
        setData()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun findView() {
        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
        user_name = BMSPrefs.getString(applicationContext, Constants.USER_NAME)
        displayName = BMSPrefs.getString(applicationContext, Constants.DISPLAY_NAME)
        user_email = BMSPrefs.getString(applicationContext, Constants.USER_EMAIL)
        user_phone = BMSPrefs.getString(applicationContext, Constants.USER_PHONE)
        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        profile_image = BMSPrefs.getString(applicationContext, Constants.PROFILE_IMAGE)
        cover_image = BMSPrefs.getString(applicationContext, Constants.COVER_IMAGE)
        strAbout = BMSPrefs.getString(applicationContext, Constants.USER_ABOUT)
        skype = BMSPrefs.getString(applicationContext, Constants.USER_SKYPE_ID)
        countryCode = BMSPrefs.getString(applicationContext, Constants.USER_COUNTRY_CODE)
        rootLayout.requestFocus()

        img_back.setOnClickListener(this)
        btn_submits.setOnClickListener(this)

        ccp.setOnCountryChangeListener(this)
        if (countryCode.isEmpty()) {
            countryCode = ccp.defaultCountryCodeWithPlus
        }

        img_main_profile.setOnClickListener {
            IMAGE_TYPE = TYPE_PROFILE
            onSelectImage()
        }

        imgEdit.setOnClickListener {
            IMAGE_TYPE = TYPE_COVER
            onSelectImage()
        }
        tv_change_password.setOnClickListener { view: View -> onClick(view) }
        edt_phone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        edt_about.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        edt_displayName.onFocusChangeListener = OnFocusChangeListener { view, hasFocus ->
            if (etAt.visibility == View.GONE) {
                etAt.visibility = View.VISIBLE
            }
        }
    }

    private fun keyboardShown(rootView: View): Boolean {
        val softKeyboardHeight = 100
        val r = Rect()
        rootView.getWindowVisibleDisplayFrame(r)
        val dm = rootView.resources.displayMetrics
        val heightDiff = rootView.bottom - r.bottom
        return heightDiff > softKeyboardHeight * dm.density
    }

    private fun setData() {
        edt_name.setText(user_name)
        if (displayName.trim().isEmpty()) {
            etAt.visibility = View.GONE
        } else {
            etAt.visibility = View.VISIBLE
            edt_displayName.setText(displayName.replace("@", ""))
        }

        edt_email.setText(user_email)
        edt_phone.setText(user_phone)
        ccp.textView_selectedCountry.text = countryCode
        edt_about.setText(strAbout)


        if (profile_image.isNotEmpty()) {
            Picasso.get().load(Constants.PROFILE_IMAGE_URL + profile_image)
                    .placeholder(R.drawable.user_profile).error(R.drawable.user_profile)
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(img_user_profile)
        }

        if (cover_image.isNotEmpty()) {
            Picasso.get().load(Constants.COVER_IMAGE_URL + cover_image)
                    .placeholder(R.drawable.blue_image).error(R.drawable.blue_image)
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(ivCoverImage)
        }


    }


    private fun editProfileRequest() {
        val params = HashMap<String, String?>()
        params["name"] = name
        params["display_name"] = edt_displayName.text.toString()
        params["country_code"] = countryCode
        params["phone"] = phone_number
        params["session_token"] = session_token
        params["email"] = email
        params["skype"] = skype
        params["about"] = strAbout
        Log.d("editProfileRequest : ", params.toString())
        val editProfilePresenter = EditProfilePresenter(this)
        editProfilePresenter.requestEditProfile(params)
    }


    override fun onClick(view: View) {

        when (view.id) {
            R.id.img_back -> {
                onBackPressed()
            }
            R.id.btn_submits -> {
                CommonMethod.hideKeyBoard(this@EditProfileActivity)
                val result = validateForm()
                if (result.equals("success", ignoreCase = true)) {
                    if (isInternetPresent) {
                        editProfileRequest()
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), applicationContext)
                    }
                } else {
                    CommonMethod.showToastShort(result, applicationContext)
                }
            }

            R.id.tv_change_password -> {
                CommonMethod.callActivity(applicationContext, Intent(applicationContext, ChangePwdActivity::class.java))
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
            }
        }
    }

    private fun onSelectImage() {
        val alertDialog = AlertDialog.Builder(this@EditProfileActivity, R.style.alertDialogTheme)
        alertDialog.setMessage(R.string.select_photo_from)
        alertDialog.setPositiveButton(R.string.gallery) { dialog, which -> takePictureFromGallery() }
        alertDialog.setNegativeButton(R.string.camera) { dialog, which ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_CAMERA)
            } else {
                takePicture()
            }
        }
        alertDialog.show()
    }


    private fun validateForm(): String {
        name = edt_name.text.toString().trim()
        displayName = edt_displayName.text.toString().trim()
        email = edt_email.text.toString().trim()
        phone_number = edt_phone.text.toString().trim()
        strAbout = edt_about.text.toString().trim()

        if (name.isEmpty()) {
            edt_name.isFocusable = true
            edt_name.requestFocus()
            return getString(R.string.valid_name)
        }


        if (displayName.isEmpty()) {
            edt_displayName.isFocusable = true
            edt_displayName.requestFocus()
            return getString(R.string.valid_display_name)
        }

        if (CommonMethod.isValidDisplayName(displayName)) {
            edt_displayName.isFocusable = true
            edt_displayName.requestFocus()
            return getString(R.string.invalid_display_name)
        }


        if (email.isEmpty() || !CommonMethod.isEmailValid(email)) {
            edt_email.isFocusable = true
            edt_email.requestFocus()
            return getString(R.string.valid_email)
        }
        if (phone_number.isEmpty() || phone_number.length < 6) {
            edt_phone.isFocusable = true
            edt_phone.requestFocus()
            return getString(R.string.valid_phone)
        }

        if (strAbout.isEmpty()) {
            edt_about.isFocusable = true
            edt_about.requestFocus()
            return getString(R.string.enter_about)
        }
        return "success"
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }


    private fun profileImageRequest() {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", session_token)
        if (path.isNotEmpty()) {
            val file = File(path)
            // hashMap["profile_image\"; filename=\"pp.png\" "] = RequestBody.create(parse.parse("image/*"), file)
            builder.addFormDataPart("profile_image", file.name, file.asRequestBody("image/jpg".toMediaTypeOrNull()))
        }

        val requestBody = builder.build()
        val profileImagePresenter = ProfileImagePresenter(this)
        profileImagePresenter.requestProfileImage(requestBody)
    }


    private fun coverImageRequest() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", session_token)
        if (path.isNotEmpty()) {
            val file = File(path)
            builder.addFormDataPart("cover_photo", file.name, file.asRequestBody("image/jpg".toMediaTypeOrNull()))

        }

        val requestBody = builder.build()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        apiInterface.uploadCoverPhoto(requestBody).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                CommonMethod.showAlert(this@EditProfileActivity, response.responseMsg)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

    override fun showProgress() {
        show(this, "")
    }

    override fun hideProgress() {
        hide()
    }


    override fun setData(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        if (response_status.equals("1", ignoreCase = true)) {
            CommonMethod.showToastShort(response_msg, applicationContext)
            BMSPrefs.putString(applicationContext, Constants.PROFILE_IMAGE, response_data.profile_image)
        } else {
            CommonMethod.showToastShort(response_msg, applicationContext)
        }
    }

    override fun setDataToBackground(response_status: String, response_msg: String, response_invalid: String, response_data: Response_data) {
        if (response_status == "1") {
            CommonMethod.showToastShort(response_msg, applicationContext)
            BMSPrefs.putString(applicationContext, Constants.USER_NAME, response_data.name)
            BMSPrefs.putString(applicationContext, Constants.USER_EMAIL, response_data.email)
            BMSPrefs.putString(applicationContext, Constants.USER_PHONE, response_data.mobile)
            BMSPrefs.putString(applicationContext, Constants.USER_COUNTRY_CODE, response_data.country_code)
            BMSPrefs.putString(applicationContext, Constants.USER_SKYPE_ID, response_data.skype)
            BMSPrefs.putString(applicationContext, Constants.USER_ABOUT, response_data.about)
            ProfileActivity.shouldRefresh = true
            finish()
        } else {
            CommonMethod.showToastlong(response_msg, applicationContext)
        }
    }

    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }

    private fun takePictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {

            //val intent = Intent()
            //intent.type = "image/*"
            //intent.action = Intent.ACTION_GET_CONTENT
            //startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GALLERY)

            //val photoPickerIntent = Intent(Intent.ACTION_PICK)
            //photoPickerIntent.type = "image/*"
            //startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)

            //val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            //startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY)

            //  val getIntent = Intent(Intent.ACTION_GET_CONTENT)
            //   getIntent.type = "image/*"
            //  val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            //  pickIntent.type = "image/*"
            //   val chooserIntent = Intent.createChooser(getIntent, "Select Image")
            //   chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))
            //    startActivityForResult(chooserIntent, REQUEST_CODE_GALLERY)

            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                CommonMethod.showToastlong(getString(R.string.go_to_setting_to_enable_permission), this)
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePicture()
            } else {
                CommonMethod.showToastlong(getString(R.string.go_to_setting_to_enable_permission), this)
            }
        }
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureFromGallery()
            } else {
                CommonMethod.showToastlong(getString(R.string.go_to_setting_to_enable_permission), this)
            }
        }
    }

    private fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_CAMERA)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val values = ContentValues(1)
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
            file = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CODE_GALLERY) {

                if (data != null) {
                    // val file = File(CommonMethods.getRealPathFromURI(this@EditProfileActivity, data.data))
                    val selectedImage = data.data
                    com.theartofdev.edmodo.cropper.CropImage.activity(selectedImage).start(this)
                } else {
                    Log.d("TAG", "you don`t pic any image")
                }
            } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
                try {
                    startCropImage(File(CommonMethods.getRealPathFromURI(this@EditProfileActivity, file)))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
                val imagePath = data!!.getStringExtra(CropImage.IMAGE_PATH) ?: return
                path = imagePath
                val bitmap = BitmapFactory.decodeFile(imagePath)

                if (IMAGE_TYPE == TYPE_COVER) {
                    ivCoverImage.setImageBitmap(bitmap)
                    coverImageRequest()
                } else {
                    img_user_profile.setImageBitmap(bitmap)
                    if (isInternetPresent) {
                        profileImageRequest()
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), applicationContext)
                    }
                }
            } else if (requestCode == com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result = com.theartofdev.edmodo.cropper.CropImage.getActivityResult(data)
                val resultUri = result.uri
                val bmOptions = BitmapFactory.Options()
                val bitmap = BitmapFactory.decodeFile(resultUri.path, bmOptions)
                path = resultUri.path.toString()

                if (IMAGE_TYPE == TYPE_COVER) {
                    ivCoverImage.setImageBitmap(bitmap)
                    coverImageRequest()
                } else {
                    img_user_profile.setImageBitmap(bitmap)
                    if (isInternetPresent) {
                        profileImageRequest()
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), applicationContext)
                    }
                }
            }
        }
    }

    private fun startCropImage(mFileTemp: File) {
        val intent = Intent(this@EditProfileActivity, CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 4)
        intent.putExtra(CropImage.ASPECT_Y, 4)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }

    companion object {
        private const val TAG = "EditProfileActivity"
        private const val REQUEST_CODE_GALLERY = 12
        private const val REQUEST_CODE_CROP_IMAGE = 13
        private const val REQUEST_CODE_TAKE_PICTURE = 11
    }

    override fun onCountrySelected() {
        countryCode = ccp.selectedCountryCodeWithPlus
    }
}