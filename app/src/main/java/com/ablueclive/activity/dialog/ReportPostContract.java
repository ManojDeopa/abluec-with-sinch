package com.ablueclive.activity.dialog;

import java.util.HashMap;

public interface ReportPostContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, String response_invalid);

            void onFailure(Throwable t);
        }

        void reportPost(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToBackground(String response_status, String response_msg, String response_invalid);

        void onResponseFailure(Throwable throwable);

    }

    interface Presenter {
        void onDestroy();

        void requestReportPost(HashMap<String, String> param);
    }
}
