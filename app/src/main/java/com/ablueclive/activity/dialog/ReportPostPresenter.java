package com.ablueclive.activity.dialog;

import java.util.HashMap;

public class ReportPostPresenter implements ReportPostContract.Presenter, ReportPostContract.Model.OnFinishedListener {
    private ReportPostContract.View reportPostView;
    private ReportPostContract.Model reportPostModel;

    public ReportPostPresenter(ReportPostContract.View reportPostView) {
        this.reportPostView = reportPostView;
        this.reportPostModel = new ReportPostModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, String response_invalid) {
        if (reportPostView != null) {
            reportPostView.hideProgress();
            reportPostView.setDataToBackground(response_status, response_msg, response_invalid);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (reportPostView != null) {
            reportPostView.hideProgress();
            reportPostView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        reportPostView = null;
    }

    @Override
    public void requestReportPost(HashMap<String, String> param) {
        if (reportPostView != null) {
            reportPostView.showProgress();
            reportPostModel.reportPost(this, param);
        }


    }
}
