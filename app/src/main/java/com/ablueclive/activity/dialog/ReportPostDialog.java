package com.ablueclive.activity.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;

import com.ablueclive.R;
import com.ablueclive.adapter.GetAllFeedAdapter;
import com.ablueclive.fragment.feedFragment.FeedFragment;
import com.ablueclive.modelClass.PostData;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.ProgressD;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportPostDialog extends Dialog implements View.OnClickListener, ReportPostContract.View {
    private String postId, report_content = "", session_token;
    private CheckBox chk_one, chk_two, chk_three;
    private static final String TAG = "ReportPostDialog";
    private boolean isInternetPresent = false;
    private ArrayList<PostData> postDataArrayList;
    private int pos;
    private GetAllFeedAdapter getAllFeedAdapter = null;


    public ReportPostDialog(@NonNull Context context, String post_id, ArrayList<PostData> postDataArrayList, int pos, GetAllFeedAdapter getAllFeedAdapter) {
        super(context);
        postId = post_id;
        this.postDataArrayList = postDataArrayList;
        this.pos = pos;
        this.getAllFeedAdapter = getAllFeedAdapter;
    }

    public ReportPostDialog(@NonNull Context context, String post_id, ArrayList<PostData> postDataArrayList, int pos) {
        super(context);
        postId = post_id;
        this.postDataArrayList = postDataArrayList;
        this.pos = pos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.report_dialog);
        findView();
    }

    // initialize object
    private void findView() {
        session_token = BMSPrefs.getString(getContext(), Constants.SESSION_TOKEN);

        ConnectionDetector connectionDetector = new ConnectionDetector(getContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        Button btn_submit = findViewById(R.id.btn_submit);
        chk_one = findViewById(R.id.chk_one);
        chk_two = findViewById(R.id.chk_two);
        chk_three = findViewById(R.id.chk_three);
        btn_submit.setOnClickListener(this);

        chk_one.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                report_content = "1";
                chk_two.setChecked(false);
                chk_three.setChecked(false);
            }
        });

        chk_two.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                report_content = "2";
                chk_one.setChecked(false);
                chk_three.setChecked(false);
            }
        });

        chk_three.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                report_content = "3";
                chk_one.setChecked(false);
                chk_two.setChecked(false);
            }
        });
    }

    // report post param
    private void reportPostParam() {
        HashMap<String, String> params = new HashMap<>();
        params.put("session_token", session_token);
        params.put("post_id", postId);
        params.put("report_reason", report_content);
        params.put("report_content", report_content);
        Log.d("reportPostPAram", params.toString());
        //   allProductPresenter.getMoreData(params);
        ReportPostPresenter reportPostPresenter = new ReportPostPresenter(this);
        reportPostPresenter.requestReportPost(params);
    }

    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit: {
                if (!TextUtils.isEmpty(report_content)) {
                    if (isInternetPresent) {
                        reportPostParam();
                        dismiss();
                    } else {
                        CommonMethod.showToastShort(getContext().getString(R.string.internet_toast), getContext());
                    }

                } else {
                    CommonMethod.showToastShort("please choose above one option", getContext());
                }

                break;
            }
        }
    }


    @Override
    public void showProgress() {
        ProgressD.Companion.show(getContext(), "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToBackground(String response_status, String response_msg, String response_invalid) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
            if (getAllFeedAdapter != null) {
                postDataArrayList.remove(pos);
                getAllFeedAdapter.notifyDataSetChanged();
            } else {
                FeedFragment.Companion.setShouldDataRefresh(true);
            }
        } else if (response_invalid.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getContext());
        } else {
            CommonMethod.showToastShort(response_msg, getContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, "" + throwable.getMessage());
    }


}
