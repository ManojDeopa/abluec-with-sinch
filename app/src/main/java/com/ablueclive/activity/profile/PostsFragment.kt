package com.ablueclive.activity.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.common.CommonResponse
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.jobSection.runningJob.ParentRecyclerAdapter
import com.ablueclive.jobSection.runningJob.RunningJobListResponse
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_layout.*


class PostsFragment : Fragment(), RecyclerViewItemClick {

    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var session_token: String? = null

    lateinit var api: Observable<CommonResponse>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_view_layout, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        runningJobList()
    }

    private fun init() {
        session_token = BMSPrefs.getString(context, Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(context)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
    }


    private fun runningJobList() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["status"] = "2"

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getRunningJobListing(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<RunningJobListResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: RunningJobListResponse) {
                        ProgressD.hide()

                        try {
                            val list = response.responseData?.getjoblisting
                            if (list.isNullOrEmpty()) {
                                CommonMethod.isEmptyView(true, requireContext(),getString(R.string.no_job_found))
                            } else {
                                CommonMethod.isEmptyView(false, requireContext(), getString(R.string.no_job_found))
                                recyclerView.apply {
                                    layoutManager = LinearLayoutManager(requireContext())
                                    adapter = ParentRecyclerAdapter(this@PostsFragment, list, Constants.POST_RUNNING_JOB)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    override fun onRecyclerItemClick(text: String, position: Int) {
        requestData(position, text)
    }


    private fun requestData(position: Int, id: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireActivity())
            return
        }

        ProgressD.show(requireActivity(), "")

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        val separated = id.split(":".toRegex()).toTypedArray()

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["job_id"] = separated[0]


        if (position == 0) {
            api = apiInterface.completeRequestApi(param)
        }

        if (position == 1) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.acceptJobRequestApi(param)
        }

        if (position == 2) {
            param["service_provider_id"] = separated[1]
            api = apiInterface.rejectJobRequestApi(param)
        }


        api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {


                        try {
                            if (position == 0) {
                                CommonMethod.showAlert(requireActivity(), response.responseData?.completeRequestApi)
                            }

                            if (position == 1) {
                                CommonMethod.showAlert(requireActivity(), response.responseData?.acceptJobRequestApi)
                            }


                            if (position == 2) {
                                CommonMethod.showAlert(requireActivity(), response.responseData?.rejectJobRequestApi)
                            }


                            ProgressD.hide()
                            runningJobList()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}