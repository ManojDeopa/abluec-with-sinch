package com.ablueclive.activity.profile

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.activity.playVideo.VideoPlayActivity
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import com.ablueclive.utils.ShowFullImage
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.upload_photo_video_adapter.view.*

class ImageVideosAdapter(var list: List<GetImageVideosResponse.DataAllImage>) : RecyclerView.Adapter<ImageVideosAdapter.ViewHolder>() {


    lateinit var context: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        context = viewGroup.context
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.upload_photo_video_adapter, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val type = list[position].imageType
        var imageUrl = Constants.POST_IMAGE_URL + list[position].imageName

        if (isVideoFile(type)) {
            imageUrl = Constants.POST_IMAGE_URL + list[position].thumbImage
            viewHolder.itemView.img_play.visibility = View.VISIBLE
        } else {
            viewHolder.itemView.img_play.visibility = View.GONE
        }

        Glide.with(context)
                .load(imageUrl)
                .skipMemoryCache(true)
                .error(R.drawable.user_profile)
                .into(viewHolder.itemView.img_photo)

    }

    private fun isVideoFile(type: String?): Boolean {

        if (type.isNullOrEmpty()) {
            return false
        }
        if (type.contains("video/mp4")) {
            return true
        }
        return false
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.img_close.visibility = View.GONE
            itemView.setOnClickListener {

                val type = list[absoluteAdapterPosition].imageType
                var url = Constants.POST_IMAGE_URL + list[absoluteAdapterPosition].imageName

                if (isVideoFile(type)) {
                    url = list[absoluteAdapterPosition].imageName.toString()
                    CommonMethod.callActivity(context, Intent(context, VideoPlayActivity::class.java))
                    BMSPrefs.putString(context, "video_path", url)
                } else {
                    val i = Intent(context, ShowFullImage::class.java)
                    i.putExtra("ImageUrl", url)
                    context.startActivity(i)
                }
            }

            itemView.img_play.setOnClickListener {
                val url = list[absoluteAdapterPosition].imageName.toString()
                CommonMethod.callActivity(context, Intent(context, VideoPlayActivity::class.java))
                BMSPrefs.putString(context, "video_path", url)
            }
        }
    }

}