package com.ablueclive.activity.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import kotlinx.android.synthetic.main.recycler_view_layout.*
import kotlinx.android.synthetic.main.recycler_with_title_layout.*


class ExpSkillFragment(var previousJobs: List<MyProfileResponse.PreviousJob>?) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recycler_with_title_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = previousJobs
        if (!list.isNullOrEmpty()) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireActivity())
                adapter = ExpSkillsAdapter(list)
            }
        } else {
            tvHeaderText.visibility = View.VISIBLE
            tvHeaderText.text = "No Experience & Skills."
        }


    }

}