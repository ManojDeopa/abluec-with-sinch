package com.ablueclive.activity.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.expertise_adapter_item.view.*

class MyExpertiseAdapter(var list: List<MyProfileResponse.AddSkill>) : RecyclerView.Adapter<MyExpertiseAdapter.ViewHolder>() {


    lateinit var context: Context
    lateinit var listener: RecyclerViewItemClick

    constructor(list: List<MyProfileResponse.AddSkill>, listenerObj: RecyclerViewItemClick) : this(list) {
        listener = listenerObj
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            if (isFriendProfile()) {
                itemView.ivDelete.visibility = View.GONE
                itemView.ivEdit.visibility = View.GONE
            }

            itemView.ivDelete.setOnClickListener {
                listener.onRecyclerItemClick("deleteSkill", absoluteAdapterPosition)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.expertise_adapter_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvCatName.text = list[position].categoryName
        val subCat = list[position].subCat

        if (!subCat.isNullOrEmpty()) {
            holder.itemView.tvSubCatName.text = list[position].subCat?.get(0)?.name
            val exp = list[position].subCat?.get(0)?.experience
            if (!exp.isNullOrEmpty()) {
                holder.itemView.etExperience.setText(exp)
            }

            holder.itemView.ivEdit.setOnClickListener {

                if (holder.itemView.ivEdit.tag == "edit") {
                    holder.itemView.ivEdit.tag = "done"
                    holder.itemView.ivEdit.setImageResource(R.drawable.ic_done_green)
                    holder.itemView.etExperience.isEnabled = true
                    holder.itemView.etExperience.requestFocus()
                    holder.itemView.etExperience.setBackgroundResource(R.drawable.et_custom_underline)

                } else {

                    holder.itemView.ivEdit.tag = "edit"
                    holder.itemView.ivEdit.setImageResource(R.drawable.edit)
                    holder.itemView.etExperience.isEnabled = false
                    holder.itemView.etExperience.background = null

                    list[position].subCat?.get(0)?.experience = holder.itemView.etExperience.text.toString().trim()
                    listener.onRecyclerItemClick("editSkill", position)

                    holder.itemView.etExperience.clearFocus()
                }

            }
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }


    fun isFriendProfile(): Boolean {
        return !::listener.isInitialized
    }

}
