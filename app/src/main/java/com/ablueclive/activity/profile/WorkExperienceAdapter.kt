package com.ablueclive.activity.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.work_experience_item.view.*


class WorkExperienceAdapter(var list: List<MyProfileResponse.AddExperience>) : RecyclerView.Adapter<WorkExperienceAdapter.ViewHolder>() {


    lateinit var itemData: MyProfileResponse.AddExperience
    lateinit var context: Context
    lateinit var listener: RecyclerViewItemClick

    constructor(listObj: List<MyProfileResponse.AddExperience>, listenerObj: RecyclerViewItemClick) : this(listObj) {
        list = listObj
        listener = listenerObj
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            if (isFriendProfile()) {
                itemView.ivDelete.visibility = View.GONE
            }

            itemView.ivDelete.setOnClickListener {
                listener.onRecyclerItemClick("deleteExperience", absoluteAdapterPosition)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.work_experience_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvCompanyName.text = list[position].company
        holder.itemView.tvDescription.text = list[position].description
    }

    override fun getItemCount(): Int {
        return list.size
    }


    fun isFriendProfile(): Boolean {
        return !::listener.isInitialized
    }

}
