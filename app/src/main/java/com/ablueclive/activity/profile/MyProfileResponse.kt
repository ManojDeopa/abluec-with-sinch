package com.ablueclive.activity.profile

import com.ablueclive.activity.profile.GetImageVideosResponse.DataAllImage
import com.ablueclive.modelClass.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MyProfileResponse {


    var response_status: Int? = null
    var response_msg: String? = null
    var response_data: ResponseData? = null
    var response_invalid: Int? = null


    class ResponseData {
        @SerializedName("getFriendData")
        @Expose
        var getFriendData: List<GetFriendDatum>? = null

        @SerializedName("postData")
        @Expose
        var postData: List<PostDatum>? = null

        @SerializedName("getprofileInfo")
        @Expose
        var getprofileInfo: GetprofileInfo? = null

        @SerializedName("rejcetedCount")
        @Expose
        var rejcetedCount: Int? = null

        @SerializedName("completedCount")
        @Expose
        var completedCount: Int? = null

        @SerializedName("serviceProviderRatings")
        @Expose
        var serviceProviderRatings: List<ServiceProviderRating>? = null

        @SerializedName("dataAllImages")
        @Expose
        var dataAllImages: List<DataAllImage>? = null

        @SerializedName("getprofileInfofromuserid")
        @Expose
        var getprofileInfofromuserid: GetprofileInfo? = null

        @SerializedName("getUserInfo")
        @Expose
        var getUserInfo: GetprofileInfo? = null

    }

    class GetFriendDatum {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null
    }

    class PostDatum {

        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("post_title")
        @Expose
        var postTitle: String? = null

        @SerializedName("post_content")
        @Expose
        var postContent: String? = null

        @SerializedName("images")
        @Expose
        var images: ArrayList<Images>? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("post_UTC_Date")
        @Expose
        var postUTCDate: String? = null

        @SerializedName("store_status")
        @Expose
        var storeStatus: Int? = null

        @SerializedName("store_id")
        @Expose
        var storeId: String? = null

        @SerializedName("post_user_name")
        @Expose
        var postUserName: String? = null

        @SerializedName("post_user_profile_image")
        @Expose
        var postUserProfileImage: String? = null

        @SerializedName("totalCountLike")
        @Expose
        var totalCountLike: Int? = null

        @SerializedName("like_users")
        @Expose
        var likeUsers: ArrayList<Like_users>? = null

        @SerializedName("comments")
        @Expose
        var comments: ArrayList<Comments>? = null

        @SerializedName("post_user_like_status")
        @Expose
        var postUserLikeStatus: String? = null

        @SerializedName("post_reports_status")
        @Expose
        var postReportsStatus: Int? = null

        @SerializedName("post_time_status")
        @Expose
        var postTimeStatus: String? = null


        @SerializedName("totalComments")
        @Expose
        var totalComments: Int? = null

        @SerializedName("share_count")
        @Expose
        var shareCount: Int? = null

        @SerializedName("retweet_user_post")
        @Expose
        var retweet_user_post: ArrayList<ShareUsersPost>? = null

        @SerializedName("shareUrl")
        @Expose
        var shareUrl: String = ""

        @SerializedName("repostUser")
        @Expose
        var repostUser: ArrayList<RePostUser>? = null

        @SerializedName("repost_content")
        @Expose
        var repost_content: String = ""


    }


    class GetprofileInfo {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("device_id")
        @Expose
        var deviceId: String? = null

        @SerializedName("device_token")
        @Expose
        var deviceToken: String? = null

        @SerializedName("device_type")
        @Expose
        var deviceType: Int? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("display_name")
        @Expose
        var display_name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("language")
        @Expose
        var language: String? = null

        @SerializedName("verification_otp")
        @Expose
        var verificationOtp: String? = null

        @SerializedName("rating")
        @Expose
        var rating: Int? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("skype")
        @Expose
        var skype: Any? = null

        @SerializedName("about")
        @Expose
        var about: String? = null

        @SerializedName("session_token")
        @Expose
        var sessionToken: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("online_status")
        @Expose
        var onlineStatus: Int? = null

        @SerializedName("status")
        @Expose
        var status: Int? = null

        @SerializedName("debug_mode")
        @Expose
        var debugMode: Int? = null

        @SerializedName("isStoreCreated")
        @Expose
        var isStoreCreated: Int? = null

        @SerializedName("loc")
        @Expose
        var loc: Loc? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("cover_photo")
        @Expose
        var cover_photo: String? = null


        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null

        @SerializedName("address")
        @Expose
        var address: Address? = null

        @SerializedName("favStatusData")
        @Expose
        var favStatusData: Int = 0

        @SerializedName("friendStatusData")
        @Expose
        var friendStatusData: Int? = null

        @SerializedName("addSkills")
        @Expose
        var addSkills: List<AddSkill>? = null

        @SerializedName("previous_jobs")
        @Expose
        var previousJobs: List<PreviousJob>? = null

        @SerializedName("addExperiences")
        @Expose
        var addExperiences: List<AddExperience>? = null

        @SerializedName("addAcademic")
        @Expose
        var addAcademic: List<AddAcademic>? = null


        @SerializedName("resume")
        @Expose
        var resume: String? = null

        @SerializedName("cover_letter")
        @Expose
        var coverLetter: String? = null


        @SerializedName("follow")
        @Expose
        var follow: Int? = null

        @SerializedName("following")
        @Expose
        var following: Int? = null

        @SerializedName("is_followed")
        @Expose
        var is_followed: Int? = null

        @SerializedName("post_count")
        @Expose
        var post_count: Int? = null

        @SerializedName("friends_count")
        @Expose
        var friends_count: Int? = null

        @SerializedName("need_to_respond")
        @Expose
        var need_to_respond: Int? = null

        @SerializedName("is_friend")
        @Expose
        var is_friend: Int? = null

        @SerializedName("already_send_request")
        @Expose
        var already_send_request: Int? = null

        @SerializedName("is_private")
        @Expose
        var is_private: Int? = null

        @SerializedName("availability_status")
        @Expose
        var is_available: Int? = null


    }

    class Address {
        @SerializedName("flatno")
        @Expose
        var flatno: String? = null

        @SerializedName("street")
        @Expose
        var street: String? = null

        @SerializedName("city")
        @Expose
        var city: String? = null

        @SerializedName("state")
        @Expose
        var state: String? = null

        @SerializedName("zip")
        @Expose
        var zip: String? = null

        @SerializedName("country")
        @Expose
        var country: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Float? = null

        @SerializedName("long")
        @Expose
        var _long: Float? = null

        @SerializedName("complete_address")
        @Expose
        var completeAddress: String? = null
    }

    class Loc {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("coordinates")
        @Expose
        var coordinates: List<Float>? = null
    }

    class ServiceProviderRating {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("job_id")
        @Expose
        var jobId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("service_provider_id")
        @Expose
        var serviceProviderId: String? = null

        @SerializedName("rating")
        @Expose
        var rating: String? = null

        @SerializedName("time_to_complete")
        @Expose
        var timeToComplete: Any? = null

        @SerializedName("review")
        @Expose
        var review: String? = null

        @SerializedName("_created_at")
        @Expose
        var createdAt: String = ""

        @SerializedName("_updated_at")
        @Expose
        var updatedAt: String? = null
    }

    class AddSkill {

        @SerializedName("categoryId")
        @Expose
        var categoryId: String? = null

        @SerializedName("randNum")
        @Expose
        var randNum: Int? = null

        @SerializedName("categoryName")
        @Expose
        var categoryName: String? = null

        @SerializedName("subCat")
        @Expose
        var subCat: List<SubCat>? = null

        @SerializedName("_id")
        @Expose
        var id: String? = null
    }

    class PreviousJob {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("job_title")
        @Expose
        var jobTitle: String? = null

        @SerializedName("job_id")
        @Expose
        var jobId: String? = null

        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

        @SerializedName("service_id")
        @Expose
        var serviceId: String? = null

        @SerializedName("job_status")
        @Expose
        var jobStatus: Int? = null

        @SerializedName("category_data")
        @Expose
        var categoryData: CategoryData? = null

        @SerializedName("sub_category_data")
        @Expose
        var subCategoryData: SubCategoryData? = null
    }

    class SubCat {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("experience")
        @Expose
        var experience: String = ""
    }

    class SubCategoryData {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("category_id")
        @Expose
        var categoryId: String? = null

        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null

        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
    }

    class CategoryData {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("addedon")
        @Expose
        var addedon: String? = null

        @SerializedName("updatedon")
        @Expose
        var updatedon: String? = null

        @SerializedName("cat_image")
        @Expose
        var catImage: String? = null
    }

    class AddExperience {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("company")
        @Expose
        var company: String? = null

        @SerializedName("description")
        @Expose
        var description: String? = null

        @SerializedName("add_on")
        @Expose
        var addOn: String? = null
    }


    class AddAcademic {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("college_or_school_name")
        @Expose
        var college_or_school_name: String? = null

        @SerializedName("start_year")
        @Expose
        var start_year: Int? = null

        @SerializedName("end_year")
        @Expose
        var end_year: Int? = null

        @SerializedName("persuing")
        @Expose
        var persuing: Int? = null
    }

}