package com.ablueclive.activity.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.RecyclerViewItemClick
import kotlinx.android.synthetic.main.work_experience_item.view.*


class AcademicAdapter(var list: List<MyProfileResponse.AddAcademic>) : RecyclerView.Adapter<AcademicAdapter.ViewHolder>() {


    lateinit var context: Context
    lateinit var listener: RecyclerViewItemClick

    constructor(listObj: List<MyProfileResponse.AddAcademic>, listenerObj: RecyclerViewItemClick) : this(listObj) {
        list = listObj
        listener = listenerObj
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {

            itemView.tvDate.visibility = View.VISIBLE

            if (isFriendProfile()) {
                itemView.ivDelete.visibility = View.GONE
            }

            itemView.ivDelete.setOnClickListener {
                listener.onRecyclerItemClick("deleteAcademic", absoluteAdapterPosition)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.work_experience_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvCompanyName.text = list[position].title
        holder.itemView.tvDescription.text = list[position].college_or_school_name

        var date = list[position].start_year.toString() + " - " + list[position].end_year.toString()
        if (list[position].persuing != null && list[position].persuing == 1) {
            date = list[position].start_year.toString() + " - " + context.getString(R.string.pursuing)

        }
        holder.itemView.tvDate.text = date

    }

    override fun getItemCount(): Int {
        return list.size
    }


    fun isFriendProfile(): Boolean {
        return !::listener.isInitialized
    }

}
