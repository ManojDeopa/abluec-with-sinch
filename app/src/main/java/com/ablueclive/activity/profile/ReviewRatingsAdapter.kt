package com.ablueclive.activity.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.CommonMethod
import kotlinx.android.synthetic.main.review_rating_item.view.*

class ReviewRatingsAdapter(var list: List<MyProfileResponse.ServiceProviderRating>) : RecyclerView.Adapter<ReviewRatingsAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.review_rating_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rating = list[position].rating
        if (!rating.isNullOrEmpty()) {
            holder.itemView.myRatingBar.rating = rating.toFloat()
        }

        holder.itemView.tvTime.text = CommonMethod.getTimesAgo(list[position].createdAt)
        holder.itemView.tvText.text = list[position].review

    }

    override fun getItemCount(): Int {
        return list.size
    }

}
