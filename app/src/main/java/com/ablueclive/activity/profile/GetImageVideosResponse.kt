package com.ablueclive.activity.profile

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class GetImageVideosResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    class ResponseData {
        @SerializedName("dataAllImages")
        @Expose
        var dataAllImages: List<DataAllImage>? = null
    }

    class DataAllImage {
        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("post_id")
        @Expose
        var postId: String? = null

        @SerializedName("user_id")
        @Expose
        var userId: String? = null

        @SerializedName("image_name")
        @Expose
        var imageName: String? = null

        @SerializedName("Image_type")
        @Expose
        var imageType: String? = null

        @SerializedName("thumb_Image")
        @Expose
        var thumbImage: String? = null

        @SerializedName("created_date")
        @Expose
        var createdDate: String? = null
    }

}