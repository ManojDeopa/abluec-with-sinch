package com.ablueclive.activity.profile;


import java.util.HashMap;

public class ProfileDataPresenter implements ProfiledataContract.Presenter, ProfiledataContract.Model.OnFinishedListener {
    private ProfiledataContract.View profileDataView;
    private ProfiledataContract.Model profileDataModel;

    public ProfileDataPresenter(ProfiledataContract.View profileDataView) {
        this.profileDataView = profileDataView;
        this.profileDataModel = new ProfileDataModel();
    }


    @Override
    public void onFinished(MyProfileResponse responseModel) {
        if (profileDataView != null) {
            profileDataView.hideProgress();
            profileDataView.setDataToViews(responseModel);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (profileDataView != null) {
            profileDataView.hideProgress();
            profileDataView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        profileDataView = null;
    }

    @Override
    public void requestProfileData(HashMap<String, String> param) {
        if (profileDataView != null) {
            profileDataView.showProgress();
            profileDataModel.ProfileDetail(this, param);
        }

    }
}
