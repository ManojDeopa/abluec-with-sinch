package com.ablueclive.activity.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.exp_skills_item.view.*


class ExpSkillsAdapter(var list: List<MyProfileResponse.PreviousJob>) : RecyclerView.Adapter<ExpSkillsAdapter.ViewHolder>() {


    lateinit var context: Context

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.exp_skills_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        CommonMethod.loadImageResize(Constants.IMAGE_CATEGORY_URL + list[position].categoryData?.catImage, holder.itemView.ivImage)
        holder.itemView.tvTitle.text = list[position].jobTitle
        holder.itemView.tvName.text = list[position].categoryData?.name

        holder.itemView.vTop.visibility = if (position == 0) View.INVISIBLE else View.VISIBLE
        holder.itemView.vBottom.visibility = if (position == list.size - 1) View.INVISIBLE else View.VISIBLE


    }
}
