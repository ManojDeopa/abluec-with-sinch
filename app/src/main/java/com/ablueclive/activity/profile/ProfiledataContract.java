package com.ablueclive.activity.profile;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface ProfiledataContract {


    interface Model {
        interface OnFinishedListener {
            void onFinished(MyProfileResponse responseModel);

            void onFailure(Throwable t);
        }

        void ProfileDetail(ProfiledataContract.Model.OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(MyProfileResponse responseModel);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestProfileData(HashMap<String, String> param);
    }
}

