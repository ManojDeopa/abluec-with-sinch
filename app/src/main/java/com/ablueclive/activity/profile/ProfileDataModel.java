package com.ablueclive.activity.profile;

import android.util.Log;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.networkClass.RetrofitClient;
import com.ablueclive.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ProfileDataModel implements ProfiledataContract.Model {
    private static final String TAG = "profileDataModel";
    public static String TYPE = "";

    @Override
    public void ProfileDetail(OnFinishedListener onFinishedListener, HashMap<String, String> param) {

        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Observable<MyProfileResponse> apiCall = apiInterface.GetProfileData(param);

        if (TYPE.equals(Constants.TYPE_F)) {
            apiCall = apiInterface.getFriendProfileData(param);
        }
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyProfileResponse>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull MyProfileResponse responseModel) {
                        if (responseModel != null) {
                            onFinishedListener.onFinished(responseModel);
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        try {
                            Log.d(TAG, "" + e.getMessage());
                            onFinishedListener.onFailure(e);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


}

