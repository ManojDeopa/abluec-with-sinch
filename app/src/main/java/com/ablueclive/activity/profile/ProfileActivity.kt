package com.ablueclive.activity.profile

import AppUtils
import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ablueclive.R
import com.ablueclive.activity.editProfile.EditProfileActivity
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.setting.SettingActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.common.JobContainerActivity.Companion.isFromProfile
import com.ablueclive.fragment.menu_fragment.LogoutContract
import com.ablueclive.fragment.menu_fragment.LogoutPresenter
import com.ablueclive.global.fragment.BlankFragment
import com.ablueclive.global.fragment.FollowListFragment
import com.ablueclive.global.fragment.PostDataFragment

import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.network.SeeAllFriend
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.ablueclive.utils.CommonMethod.enableDisableViewGroup
import com.ablueclive.utils.ProgressD.Companion.hide
import com.ablueclive.utils.ProgressD.Companion.show
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.actvity_profile.*
import kotlinx.android.synthetic.main.contact_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ProfileActivity :
        AppCompatActivity(),
        View.OnClickListener,
        ProfiledataContract.View,
        RecyclerViewItemClick,
        LogoutContract.View {

    var postData: List<MyProfileResponse.PostDatum>? = null
    private var profileData: MyProfileResponse.GetprofileInfo? = null
    private var connectionDetector: ConnectionDetector? = null
    private var isInternetPresent = false
    private var session_token: String? = null
    private var profile_image: String? = null
    private var user_name: String? = null
    var itemPos = 0


    var workExperienceList = ArrayList<MyProfileResponse.AddExperience>()
    var addAcademicList = ArrayList<MyProfileResponse.AddAcademic>()
    var expList = ArrayList<MyProfileResponse.AddSkill>()

    lateinit var myExpertiseAdapter: MyExpertiseAdapter
    lateinit var workExperienceAdapter: WorkExperienceAdapter
    lateinit var academicAdapter: AcademicAdapter

    private val REQUEST_CODE_GALLERY = 12

    var TYPE = ""
    val TYPE_RESUME = "TYPE_RESUME"
    val TYPE_COVER_LETTER = "TYPE_COVER_LETTER"
    var isFirstTime = true


    companion object {
        private const val TAG = "ProfileActivity"
        var isPrivate = false
        var isAvailable = true
        var shouldRefresh = false
    }

    private val removePdfTopIcon = "javascript:(function() {" + "document.querySelector('[role=\"toolbar\"]').remove();})()"


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_profile)
        findView()
        getProfileData()
    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            shouldRefresh = false
            getProfileData()
        }
    }


    private fun getProfileData() {
        if (isInternetPresent) {
            requestProfileParam()
            // getPhotoVideos()
        } else {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
        }
    }

    private fun requestProfileParam() {
        val params = HashMap<String, String?>()
        params["session_token"] = session_token
        params["post_time_zone"] = TimeZone.getDefault().id
        params["page"] = "1"
        Log.d("ProfileParam", params.toString())
        val profileDataPresenter = ProfileDataPresenter(this)
        profileDataPresenter.requestProfileData(params)
    }

    private fun findView() {
        session_token = BMSPrefs.getString(applicationContext, Constants.SESSION_TOKEN)
        user_name = BMSPrefs.getString(applicationContext, Constants.USER_NAME)
        connectionDetector = ConnectionDetector(applicationContext)
        isInternetPresent = connectionDetector!!.isConnectingToInternet
        profile_image = BMSPrefs.getString(applicationContext, Constants.PROFILE_IMAGE)
        rootLayout.requestFocus()
        img_edit_profile.setOnClickListener(this)
        img_back.setOnClickListener(this)
        img_setting.setOnClickListener(this)
        img_logout.setOnClickListener(this)
        ProfileDataModel.TYPE = Constants.TYPE_M
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setDataToViews(responseModel: MyProfileResponse) {
        try {
            updateDataUI(responseModel)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun updateDataUI(responseModel: MyProfileResponse) {
        profileData = responseModel.response_data?.getprofileInfo
        postData = responseModel.response_data?.postData

        var displayName = ""
        if (!profileData?.display_name.isNullOrEmpty()) {
            displayName = " @" + profileData?.display_name?.replace("@", "")
        }

        displayName = "<font color='#1DA1F2'>$displayName</font>"
        val name = profileData?.name + displayName
        val email = profileData?.email
        val profileImage = profileData?.profileImage
        val coverImage = profileData?.cover_photo
        val mobileNo = profileData?.mobile
        val countryCode = profileData?.countryCode
        val about = profileData?.about
        isPrivate = profileData?.is_private == 1
        isAvailable = profileData?.is_available == 1


        setData(name, email, profileImage, coverImage, mobileNo, countryCode, about)

        if (postData.isNullOrEmpty()) {
            user_postLayout.visibility = View.GONE
        } else {
            Constants.showLoader = false
            user_postLayout.visibility = View.VISIBLE
            PostDataFragment.scrollView = scrollView
            AppUtils.addFragment(this, PostDataFragment.newInstance(Bundle(), Constants.TYPE_M), R.id.postContainer)
        }

        val skillList = profileData?.addSkills
        if (skillList != null) {
            expList.clear()
            expList.addAll(skillList)
        }


        if (!expList.isNullOrEmpty()) {
            myExpertiseAdapter = MyExpertiseAdapter(expList, this@ProfileActivity)
            rvExpertise.apply {
                layoutManager = LinearLayoutManager(this@ProfileActivity)
                adapter = myExpertiseAdapter
            }
        }
        tvAddExpertise.setOnClickListener {
            isFromProfile = true
            CommonMethod.callJobContainer(this, getString(R.string.select_category))
        }


        val academicList = profileData?.addAcademic
        if (academicList != null) {
            addAcademicList.clear()
            addAcademicList.addAll(academicList)
        }

        if (!addAcademicList.isNullOrEmpty()) {
            academicAdapter = AcademicAdapter(addAcademicList, this@ProfileActivity)
            rvAcademic.apply {
                layoutManager = LinearLayoutManager(this@ProfileActivity)
                adapter = academicAdapter
            }
        }

        tvAddAcademic.setOnClickListener {
            CommonMethod.callCommonContainer(this, getString(R.string.add_academic))
        }


        val experienceList = profileData?.addExperiences
        if (experienceList != null) {
            workExperienceList.clear()
            workExperienceList.addAll(experienceList)
        }


        if (!workExperienceList.isNullOrEmpty()) {
            workExperienceAdapter = WorkExperienceAdapter(workExperienceList, this@ProfileActivity)
            rvExperience.apply {
                layoutManager = LinearLayoutManager(this@ProfileActivity)
                adapter = workExperienceAdapter
            }
        }

        tvAddExperience.setOnClickListener {
            CommonMethod.callCommonContainer(this, getString(R.string.add_work_experience))
        }


        val address = profileData?.address
        if (address != null) {
            addressContainer.visibility = View.VISIBLE
            etFlatNo.setText(address.flatno)
            etStreet.setText(address.street)
            etCountry.setText(address.country)
            etState.setText(address.state)
            etCity.setText(address.city)
            etZipCode.setText(address.zip)
        } else {
            addressContainer.visibility = View.GONE
        }


        ivEditAddress.setOnClickListener {
            shouldRefresh = true
            Constants.myAddress = address
            CommonMethod.callCommonContainer(this, getString(R.string.update_address))
        }


        if (profileData?.resume.isNullOrEmpty()) {
            ivDeleteResume.visibility = View.GONE
            loadEmptyView(ivResume)
        } else {
            loadWebView(ivResume, Constants.POST_IMAGE_URL + profileData?.resume, pbResume, redirectResume, tvResumePath)
            ivDeleteResume.visibility = View.VISIBLE
            ivDeleteResume.setOnClickListener {
                showDeleteAlert("1")
            }
        }


        if (profileData?.coverLetter.isNullOrEmpty()) {
            ivDeleteCoverLetter.visibility = View.GONE
            loadEmptyView(ivCoverLetter)
        } else {
            loadWebView(ivCoverLetter, Constants.POST_IMAGE_URL + profileData?.coverLetter, pbCoverLetter, redirectCoverLetter, tvCoverLetterPath)
            ivDeleteCoverLetter.visibility = View.VISIBLE
            ivDeleteCoverLetter.setOnClickListener {
                showDeleteAlert("2")
            }

        }


        btnUploadResume.setOnClickListener {
            TYPE = TYPE_RESUME
            takeFilesFromGallery()
        }

        btnUploadCoverLetter.setOnClickListener {
            TYPE = TYPE_COVER_LETTER
            takeFilesFromGallery()
        }

        tvFollowers.text = profileData?.follow.toString() + "\n" + getString(R.string.followers)
        tvFollowing.text = profileData?.following.toString() + "\n" + getString(R.string.followings)

        tvFriends.text = profileData?.friends_count.toString() + "\n" + getString(R.string.friends)
        tvPost.text = profileData?.post_count.toString() + "\n" + getString(R.string.post)

        tvFollowers.setOnClickListener {
            val count: String = tvFollowers.text.toString().replace(getString(R.string.followers), "").trim()
            FollowListFragment.totalCount = count
            FollowListFragment.user_profileId = BMSPrefs.getString(this, Constants._ID)
            CommonMethod.callCommonContainer(this, getString(R.string.followers))
        }

        tvFollowing.setOnClickListener {

            val count: String = tvFollowing.text.toString().replace(getString(R.string.followings), "").trim()
            FollowListFragment.totalCount = count

            FollowListFragment.user_profileId = BMSPrefs.getString(this, Constants._ID)
            CommonMethod.callCommonContainer(this, getString(R.string.followings))
        }

        tvFriends.setOnClickListener {
            CommonMethod.callActivity(this, Intent(this, SeeAllFriend::class.java))
        }

    }


    private fun setData(name: String?, email: String?, profile_image: String?, coverImage: String?, mobile: String?, countryCode: String?, about: String?) {

        BMSPrefs.putString(this, Constants.USER_NAME, profileData?.name)
        BMSPrefs.putString(this, Constants.DISPLAY_NAME, profileData?.display_name)
        BMSPrefs.putString(this, Constants.USER_EMAIL, email)
        BMSPrefs.putString(this, Constants.USER_PHONE, mobile)
        BMSPrefs.putString(this, Constants.USER_COUNTRY_CODE, countryCode)
        BMSPrefs.putString(this, Constants.USER_ABOUT, about)
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE, profile_image)
        BMSPrefs.putString(this, Constants.COVER_IMAGE, coverImage)

        text_user_name.text = HtmlCompat.fromHtml(name.toString(), HtmlCompat.FROM_HTML_MODE_LEGACY)
        userContactLayout.visibility = View.VISIBLE
        tv_email.text = email
        tv_mobile_no.text = countryCode + mobile

        if (!about.isNullOrEmpty()) {
            tv_about.text = about
        }

        if (!profileData?.createdAt.isNullOrEmpty()) {
            tvJoinedOn.visibility = View.VISIBLE
            tvJoinedOn.text = "Joined On " + joinedOn(profileData?.createdAt)
        }

        Picasso.get()
                .load(Constants.COVER_IMAGE_URL + coverImage)
                .placeholder(R.drawable.blue_image)
                .error(R.drawable.blue_image)
                .into(ivCoverImage)

        ivCoverImage.setOnClickListener { v: View? ->
            val i = Intent(this@ProfileActivity, ShowFullImage::class.java)
            i.putExtra("ImageUrl", Constants.COVER_IMAGE_URL + coverImage)
            startActivity(i)
        }

        CommonMethod.loadImageCircle(Constants.PROFILE_IMAGE_URL + profile_image, img_user_profile)

        img_user_profile.setOnClickListener { v: View? ->
            val i = Intent(this@ProfileActivity, ShowFullImage::class.java)
            i.putExtra("ImageUrl", Constants.PROFILE_IMAGE_URL + profile_image)
            startActivity(i)

        }
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_logout -> {
                val builder = AlertDialog.Builder(this, R.style.alertDialogTheme)
                builder.setTitle(getString(R.string.app_name))
                builder.setMessage(getString(R.string.logout_msg))
                builder.setPositiveButton(getString(R.string.yes)) { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                    try {
                        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
                        val googleSignInClient = GoogleSignIn.getClient(this, gso)
                        googleSignInClient.signOut()
                        // for facebook logout
                        // LoginManager.getInstance().logOut()
                        val params = HashMap<String, String?>()
                        params["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
                        val logoutPresenter = LogoutPresenter(this)
                        logoutPresenter.requestLogoutRequest(params)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                builder.setNegativeButton(getString(R.string.no)) { dialogInterface, i -> dialogInterface.dismiss() }
                builder.create().show()
            }

            R.id.img_edit_profile -> {
                shouldRefresh = true
                CommonMethod.callActivity(applicationContext, Intent(applicationContext, EditProfileActivity::class.java))
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
            }
            R.id.img_back -> onBackPressed()
            R.id.img_setting -> {
                CommonMethod.callActivity(this, Intent(this, SettingActivity::class.java))
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
            }

        }
    }

    override fun showProgress() {

        if (isFirstTime) {
            show(this, "")
        } else {
            progressBar.visibility = View.VISIBLE
            enableDisableViewGroup(rootLayout, false)
        }


    }

    override fun hideProgress() {
        if (isFirstTime) {
            isFirstTime = false
            hide()
        } else {
            progressBar.visibility = View.GONE
            enableDisableViewGroup(rootLayout, true)
        }
    }


    private fun loadEmptyView(webView: WebView) {
        webView.loadDataWithBaseURL("file:///android_res/drawable/", "<img src='placeholder_fitxy.png' style='width:100%;height:100%' />", "text/html", "utf-8", null)
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(webV: WebView, url: String, progress: ProgressBar, ivRedirect: ImageView, tvPath: TextView) {
        ivRedirect.visibility = View.VISIBLE
        progress.visibility = View.VISIBLE
        webV.invalidate()
        webV.settings.javaScriptEnabled = true
        webV.settings.builtInZoomControls = false

        webV.loadUrl("http://docs.google.com/gview?embedded=true&url=$url")
        webV.webViewClient = object : WebViewClient() {
            var checkOnPageStartedCalled = false

            override fun onPageStarted(view: WebView?, urls: String?, favicon: Bitmap?) {
                checkOnPageStartedCalled = true
                Log.e("onPageStarted", "--$urls")
            }

            override fun onPageFinished(view: WebView?, urls: String?) {
                if (checkOnPageStartedCalled) {
                    progress.visibility = View.GONE
                    webV.loadUrl(removePdfTopIcon)
                    webV.setBackgroundColor(Color.TRANSPARENT)
                    webV.zoomBy(1.5f)
                    webV.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
                } else {
                    loadWebView(webV, url, progress, ivRedirect, tvPath)
                }
            }
        }

        tvPath.visibility = View.VISIBLE
        tvPath.text = url.replace(Constants.POST_IMAGE_URL, "")
        ivRedirect.setOnClickListener {
            AppUtils.callBrowserIntent(this, url)
        }
    }


    private fun takeFilesFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY)
        } else {
            /*val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "application/pdf"
            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)*/

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "application/pdf"
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            try {
                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), REQUEST_CODE_GALLERY)
            } catch (ex: java.lang.Exception) {
                println("browseClick :$ex")
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageUtils.PERMISSIONS_REQUEST_PHONE_STORAGE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takeFilesFromGallery()
            } else {
                Toast.makeText(this, "Go to setting", Toast.LENGTH_SHORT).show()
            }
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_GALLERY) {
                if (data != null) {
                    try {
                        val file = File(CommonMethods.getRealPathFromURI(this, data.data))
                        // val file = File(data.data?.path.toString())
                        uploadFiles(file)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    Log.e("onActivityResult", "you don`t pic any File")
                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun unUsedForNow(responseModel: MyProfileResponse) {
        tvCompletedCount.text = responseModel.response_data?.completedCount.toString() + "\n" + "Service Completed"
        tvRejectedCount.text = responseModel.response_data?.rejcetedCount.toString() + "\n" + "Service Rejected"

        addTabViewPager()

        val ratingList = responseModel.response_data?.serviceProviderRatings
        if (ratingList.isNullOrEmpty()) {
            tvNoReviewRating.visibility = View.VISIBLE
            rvReviewRating.visibility = View.GONE
        } else {
            tvNoReviewRating.visibility = View.GONE
            rvReviewRating.visibility = View.VISIBLE
            rvReviewRating.apply {
                layoutManager = LinearLayoutManager(this@ProfileActivity, LinearLayoutManager.HORIZONTAL, false)
                adapter = ReviewRatingsAdapter(ratingList)
            }
        }
    }

    override fun setDataToViews(response_status: String?, response_msg: String?) {
        try {
            when {
                response_status == "1" -> {
                    logout(response_msg!!)
                }
                response_msg == "Your session has been expired. Please login again." -> {
                    logout(response_msg)
                }
                else -> {
                    CommonMethod.showToastShort(response_msg, this)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, this)
        BMSPrefs.putString(this, Constants._ID, "")
        BMSPrefs.putString(this, Constants.isStoreCreated, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE_BACK, "")
        BMSPrefs.putString(this, Constants.USER_SKYPE_ID, "")
        BMSPrefs.putString(this, Constants.USER_ABOUT, "")
        BMSPrefs.putString(this, Constants.USER_NAME, "")
        CommonMethod.callActivityFinish(this, Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }

    override fun onResponseFailure(throwable: Throwable) {
        Log.d(TAG, throwable.message!!)
    }


    private fun addTabViewPager() {
        val list = listOf("Posts", "Experience & Skill")
        val adapter = ViewPagerAdapter(this)
        viewpager.adapter = adapter
        // viewpager.isUserInputEnabled = false
        TabLayoutMediator(tabs, viewpager,
                TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                    tab.text = list[position]
                }).attach()
    }


    internal inner class ViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity?) : FragmentStateAdapter(fragmentActivity!!) {
        val previousJobs = profileData?.previousJobs

        @NonNull
        override fun createFragment(position: Int): Fragment {
            val fragment: Fragment? = when (position) {
                0 -> {
                    /*PostDataFragment.newInstance(Bundle())*/BlankFragment()
                }
                else -> {
                    ExpSkillFragment(previousJobs)
                }
            }
            return fragment!!
        }

        override fun getItemCount(): Int {
            return 2
        }
    }

    override fun onRecyclerItemClick(text: String, position: Int) {

        itemPos = position

        if (text == "deleteSkill") {
            deleteSkill()
        }

        if (text == "editSkill") {
            editSkill()
        }

        if (text == "deleteExperience") {
            deleteExperience()
        }

        if (text == "deleteAcademic") {
            deleteAcademic()
        }
    }


    private fun getPhotoVideos() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getAllImagesApi(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<GetImageVideosResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: GetImageVideosResponse) {
                        hide()

                        try {
                            if (response.responseStatus == 1) {
                                val mediaList = response.responseData?.dataAllImages

                                if (mediaList.isNullOrEmpty()) {
                                    tvNoPhotoVideo.visibility = View.VISIBLE
                                    rvPhotosVideos.visibility = View.GONE
                                } else {
                                    tvNoPhotoVideo.visibility = View.GONE
                                    rvPhotosVideos.visibility = View.VISIBLE

                                    rvPhotosVideos.apply {
                                        layoutManager = LinearLayoutManager(this@ProfileActivity, LinearLayoutManager.HORIZONTAL, false)
                                        adapter = ImageVideosAdapter(mediaList)
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun deleteSkill() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["_id"] = expList[itemPos].id.toString()
        param["randNum"] = expList[itemPos].randNum.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteSkills(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                skillItemRemove()
                            }
                            CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                            // AppGlobalTask({ skillItemRemove() }, this@ProfileActivity).showAlert(response.responseData?.deleteSkills)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

    private fun skillItemRemove() {
        expList.removeAt(itemPos)
        myExpertiseAdapter.notifyItemRemoved(itemPos)
    }


    private fun experienceItemRemove() {
        workExperienceList.removeAt(itemPos)
        workExperienceAdapter.notifyItemRemoved(itemPos)
    }

    private fun academicItemRemove() {
        addAcademicList.removeAt(itemPos)
        academicAdapter.notifyItemRemoved(itemPos)
    }


    private fun editSkill() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }
        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["subCategoryId"] = expList[itemPos].subCat?.get(0)?.id.toString()
        param["skill_id"] = expList[itemPos].id.toString()
        param["randNum"] = expList[itemPos].randNum.toString()
        param["experience"] = expList[itemPos].subCat?.get(0)?.experience.toString()
        param["categoryId"] = expList[itemPos].categoryId.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        apiInterface.editSkills(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                getProfileData()
                            }
                            CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                            //AppGlobalTask({ getProfileData() }, this@ProfileActivity).showAlert(response.responseData?.editSkills)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun deleteAcademic() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["academic_id"] = addAcademicList[itemPos].id.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteAcademic(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                academicItemRemove()
                            }
                            CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }

    private fun deleteExperience() {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["experience_id"] = workExperienceList[itemPos].id.toString()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.deleteExperience(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                experienceItemRemove()
                            }
                            CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                            //AppGlobalTask({ experienceItemRemove() }, this@ProfileActivity).showAlert(response.responseMsg)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun uploadFiles(file: File) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("session_token", BMSPrefs.getString(this, Constants.SESSION_TOKEN))
        builder.addFormDataPart("user_id", BMSPrefs.getString(this, Constants._ID))

        if (TYPE == TYPE_RESUME) {
            builder.addFormDataPart("resume", file.name, file.asRequestBody("application/pdf".toMediaTypeOrNull()))
        } else {
            builder.addFormDataPart("cover_letter", file.name, file.asRequestBody("application/pdf".toMediaTypeOrNull()))
        }

        val requestBody = builder.build()

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)

        var apiCall = apiInterface.uploadCoverLetter(requestBody)
        if (TYPE == TYPE_RESUME) {
            apiCall = apiInterface.uploadResume(requestBody)
        }

        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                // showAlert(response.responseMsg)
                                getProfileData()
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    fun showAlert(text: String?) {
        val alert = AlertDialog.Builder(this, R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            getProfileData()
        }
        alert.show()
    }


    private fun joinedOn(strDate: String?): String? {
        if (strDate == null || strDate.trim { it <= ' ' }.isEmpty()) {
            return ""
        }
        try {
            /* var spf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)*/
            var spf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            val newDate = spf.parse(strDate)
            spf = SimpleDateFormat("MMM yyyy", Locale.getDefault())
            return spf.format(newDate!!)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return strDate
    }


    private fun removeAttachments(attachment_type: String) {

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), this)
            return
        }

        show(this, "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token.toString()
        param["attachment_type"] = attachment_type

        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.removeAttachments(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        hide()
                        try {
                            if (response.responseStatus == 1) {
                                getProfileData()
                                if (attachment_type == "1") {
                                    tvResumePath.visibility = View.GONE
                                    redirectResume.visibility = View.GONE
                                } else {
                                    redirectCoverLetter.visibility = View.GONE
                                    tvCoverLetterPath.visibility = View.GONE
                                }
                            } else {
                                CommonMethod.showToastShort(response.responseMsg, this@ProfileActivity)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(e: Throwable) {
                        hide()
                        println(e.message)
                    }
                })
    }


    private fun showDeleteAlert(type: String) {

        var title = getString(R.string.delete_resume)
        if (type == "2") {
            title = getString(R.string.delete_cover_letter)
        }
        val alert = AlertDialog.Builder(this, R.style.alertDialogTheme)
        alert.setTitle(title)
        alert.setMessage(getString(R.string.sure_to_delete))
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            removeAttachments(type)
        }

        alert.setNegativeButton(getString(R.string.no)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        alert.show()
    }
}