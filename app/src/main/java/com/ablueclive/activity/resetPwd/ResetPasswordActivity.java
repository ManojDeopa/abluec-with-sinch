package com.ablueclive.activity.resetPwd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.login.LoginActivity;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.ProgressD;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ablueclive.utils.CommonMethod.isValidPassword;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener, ResetPwdContract.View {
    private EditText edt_new_password, edt_confirm_password;
    private Button btn_submit;
    private ImageView img_back;
    private String new_password, confirm_password, email;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private static final String TAG = "ResetPasswordActivity";
    private ResetPwdPresenter resetPwdPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        findView();
    }


    // initialize object
    private void findView() {
        email = BMSPrefs.getString(getApplicationContext(), "email");
        connectionDetector = new ConnectionDetector(getApplicationContext());
        edt_new_password = findViewById(R.id.edt_new_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);
        btn_submit = findViewById(R.id.btn_submit);
        img_back = findViewById(R.id.img_back);
        btn_submit.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }


    // otp request param
    private void resetPwdRequest() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", new_password);
        Log.d("resetPwdParam", params.toString());
        resetPwdPresenter = new ResetPwdPresenter(this);
        resetPwdPresenter.requestResetPwd(params);
    }


    //validate form
    private String validateForm() {
        new_password = edt_new_password.getText().toString().trim();
        confirm_password = edt_confirm_password.getText().toString().trim();
        if (new_password.isEmpty()) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.valid_password);
        }

        if (!isValidPassword(new_password)) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.alpha_numeric_password_validation);
        }

        if (new_password.length() < 8) {
            edt_new_password.setFocusable(true);
            edt_new_password.requestFocus();
            return getString(R.string.valid_password_digit);
        }

        if (confirm_password.isEmpty()) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.valid_password_blank);
        }

        if (!confirm_password.contentEquals(new_password)) {
            edt_confirm_password.setFocusable(true);
            edt_confirm_password.requestFocus();
            return getString(R.string.same_password);
        }
        return "success";
    }


    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit: {
                CommonMethod.hideKeyBoard(ResetPasswordActivity.this);
                String result = validateForm();
                if (result.equalsIgnoreCase("success")) {
                    isInternetPresent = connectionDetector.isConnectingToInternet();
                    if (isInternetPresent) {
                        resetPwdRequest();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }

            case R.id.img_back: {
                onBackPressed();
                break;
            }
        }
    }

    //backpress method
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg) {
        if (response_status.equalsIgnoreCase("1")) {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
            CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), LoginActivity.class));
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
            finish();
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }
}
