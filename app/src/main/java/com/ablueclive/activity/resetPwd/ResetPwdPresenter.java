package com.ablueclive.activity.resetPwd;

import java.util.HashMap;

public class ResetPwdPresenter implements ResetPwdContract.Presenter, ResetPwdContract.Model.OnFinishedListener {
    private ResetPwdContract.View resetPwdView;
    private ResetPwdContract.Model resetPwdModel;

    public ResetPwdPresenter(ResetPwdContract.View resetPwdView) {
        this.resetPwdView = resetPwdView;
        this.resetPwdModel = new ResetPwdModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg) {
        if (resetPwdView != null) {
            resetPwdView.hideProgress();
            resetPwdView.setDataToViews(response_status, response_msg);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (resetPwdView != null) {
            resetPwdView.hideProgress();
            resetPwdView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        resetPwdView = null;
    }

    @Override
    public void requestResetPwd(HashMap<String, String> param) {
        if (resetPwdView != null) {
            resetPwdView.showProgress();
            resetPwdModel.getResetPwd(this, param);
        }

    }
}
