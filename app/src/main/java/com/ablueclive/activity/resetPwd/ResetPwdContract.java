package com.ablueclive.activity.resetPwd;

import java.util.HashMap;

public interface ResetPwdContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg);

            void onFailure(Throwable t);
        }

        void getResetPwd(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestResetPwd(HashMap<String, String> param);
    }
}
