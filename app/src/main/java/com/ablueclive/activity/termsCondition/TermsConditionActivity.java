package com.ablueclive.activity.termsCondition;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;

public class TermsConditionActivity extends AppCompatActivity implements View.OnClickListener {
    private WebView web_privacy_policy;
    private String url = "";
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        findView();
        if (isInternetPresent) {
            startWebView(url);
        } else {
            CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
        }
    }

    // initialize object
    private void findView() {
        url = /*"http://54.191.242.195/api/tnc";*/Constants.TERMS;
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        web_privacy_policy = findViewById(R.id.web_privacy_policy);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }


    // web view load
    private void startWebView(String url) {
        web_privacy_policy.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // Do something on page loading started
                // Visible the progressbar
                progressDialog = CommonMethod.showProgressDialog(progressDialog, TermsConditionActivity.this, getString(R.string.please_wait));
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // Do something when page loading finished
                CommonMethod.hideProgressDialog(progressDialog);
            }
        });
        web_privacy_policy.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                // Update the progress bar with page loading progress
                CommonMethod.hideProgressDialog(progressDialog);
            }
        });
        // Enable the javascript

        web_privacy_policy.requestFocus();
        web_privacy_policy.getSettings().setJavaScriptEnabled(true);
        web_privacy_policy.setVerticalScrollBarEnabled(true);
        web_privacy_policy.getSettings().setSupportMultipleWindows(true);
        web_privacy_policy.getSettings().setDefaultTextEncodingName("utf-8");

        // Render the web page
        web_privacy_policy.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }

    // listener
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back: {
                onBackPressed();
                break;
            }
        }
    }


}
