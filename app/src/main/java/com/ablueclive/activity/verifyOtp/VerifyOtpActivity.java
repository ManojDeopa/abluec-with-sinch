package com.ablueclive.activity.verifyOtp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.forgotPwd.ForgotPwdContract;
import com.ablueclive.activity.forgotPwd.ForgotPwdPresenter;
import com.ablueclive.activity.resetPwd.ResetPasswordActivity;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.ProgressD;

import java.util.HashMap;

public class VerifyOtpActivity extends AppCompatActivity implements View.OnClickListener, ForgotPwdContract.View {
    private Button btn_next;
    private EditText edt_verification_code;
    private ImageView img_back;
    private String otp, verify_otp_code, email;
    private TextView txt_send_code;
    private ForgotPwdPresenter forgotPwdPresenter;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private static final String TAG = "VerifyOtpActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        findView();
    }

    // initialize object
    private void findView() {
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();
        otp = BMSPrefs.getString(getApplicationContext(), "otp");
        email = BMSPrefs.getString(getApplicationContext(), "email");
        btn_next = findViewById(R.id.btn_next);
        edt_verification_code = findViewById(R.id.edt_verification_code);
        img_back = findViewById(R.id.img_back);
        txt_send_code = findViewById(R.id.txt_send_code);
        btn_next.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_send_code.setOnClickListener(this);
    }


    // otp request param
    private void forgotPwdOtpRequest() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        Log.d("forgotOtpParam", params.toString());
        forgotPwdPresenter = new ForgotPwdPresenter(this);
        forgotPwdPresenter.requestForgetPwdOtp(params);
    }


    // validate otp form
    private String validateOtpForm() {
        verify_otp_code = edt_verification_code.getText().toString().trim();

        if (verify_otp_code.isEmpty()) {
            edt_verification_code.setFocusable(true);
            edt_verification_code.requestFocus();
            return getString(R.string.valid_code);
        }

        if (!verify_otp_code.contentEquals(otp)) {
            edt_verification_code.setFocusable(true);
            edt_verification_code.requestFocus();
            return getString(R.string.valid_code);
        }
        return "success";
    }

    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next: {
                CommonMethod.hideKeyBoard(VerifyOtpActivity.this);
                String result = validateOtpForm();
                if (result.equalsIgnoreCase("success")) {
                    CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), ResetPasswordActivity.class));
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                    edt_verification_code.setText("");
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }

            case R.id.img_back: {
                onBackPressed();
                break;
            }

            case R.id.txt_send_code: {
                if (isInternetPresent) {
                    forgotPwdOtpRequest();
                } else {
                    CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                }
                break;
            }
        }
    }

    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            otp = response_data.getOTP();
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        {
            Log.d(TAG, throwable.getMessage());
        }
    }
}
