package com.ablueclive.activity.setting

import AppUtils
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ablueclive.R
import com.ablueclive.activity.login.LoginActivity
import com.ablueclive.activity.profile.ProfileActivity
import com.ablueclive.common.CommonResponse
import com.ablueclive.fragment.menu_fragment.LogoutContract
import com.ablueclive.fragment.menu_fragment.LogoutPresenter
import com.ablueclive.global.adapter.SettingsListAdapter
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.modelClass.ResponseModel
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.recycler_view.*

class SettingActivity : AppCompatActivity(), RecyclerViewItemClick, LogoutContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        img_back.setOnClickListener {
            onBackPressed()
        }

        val settingsAdapter = SettingsListAdapter(this@SettingActivity, this)
        rvCommon.apply {
            layoutManager = LinearLayoutManager(this@SettingActivity)
            adapter = settingsAdapter
        }

        switchPrivateAc.isChecked = ProfileActivity.isPrivate
        switchPrivateAc.setOnCheckedChangeListener { buttonView, isChecked ->
            ProfileActivity.isPrivate = isChecked
            setPrivateProfile(isChecked)
        }

        switchAvailable.isChecked = ProfileActivity.isAvailable
        switchAvailable.setOnCheckedChangeListener { buttonView, isChecked ->
            ProfileActivity.isAvailable = isChecked
            changeAvailabilityStatus()
        }

    }


    private fun setPrivateProfile(b: Boolean) {
        val status = if (b) "1" else "0"
        ProgressD.show(this, "")
        val param = java.util.HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        param["status"] = status
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.setProfilePrivate(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        CommonMethod.showToastlong(response.responseMsg, applicationContext)
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

    private fun changeAvailabilityStatus() {
        ProgressD.show(this, "")
        val param = java.util.HashMap<String, String>()
        param["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.changeAvailablityStatus(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ResponseModel?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}

                    override fun onNext(response: ResponseModel) {
                        ProgressD.hide()
                        CommonMethod.showToastlong(response.response_msg, applicationContext)
                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }


    override fun onRecyclerItemClick(text: String, position: Int) {


        when (text) {

            getString(R.string.share_abluec) -> {
                AppUtils.shareIntentCall(this, "Hi..I am using...\n\naBlueC: Social Media, Find Products, and Services\nPlease Download it from\n\nhttps://play.google.com/store/apps/details?id=com.ablueclive")
            }


            getString(R.string.feedback) -> {
                CommonMethod.callCommonContainer(this, text)
            }

            getString(R.string.logout) -> {

                val builder = AlertDialog.Builder(this, R.style.alertDialogTheme)
                builder.setTitle(getString(R.string.app_name))
                builder.setMessage(getString(R.string.logout_msg))
                builder.setPositiveButton(getString(R.string.yes)) { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                    try {
                        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
                        val googleSignInClient = GoogleSignIn.getClient(this, gso)
                        googleSignInClient.signOut()
                        // for facebook logout
                        // LoginManager.getInstance().logOut()
                        val params = HashMap<String, String?>()
                        params["session_token"] = BMSPrefs.getString(this, Constants.SESSION_TOKEN)
                        val logoutPresenter = LogoutPresenter(this)
                        logoutPresenter.requestLogoutRequest(params)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                builder.setNegativeButton(getString(R.string.no)) { dialogInterface, i -> dialogInterface.dismiss() }
                builder.create().show()

            }

            getString(R.string.my_orders) -> {
                return
            }

            else -> {
                CommonMethod.callCommonContainer(this, text)
            }

        }


    }

    override fun showProgress() {
        ProgressD.show(this, "")
    }

    override fun hideProgress() {
        ProgressD.hide()
    }


    override fun onResponseFailure(throwable: Throwable?) {
        println(throwable)
    }

    override fun setDataToViews(response_status: String, response_msg: String) {
        when {
            response_status == "1" -> {
                logout(response_msg)
            }
            response_msg == "Your session has been expired. Please login again." -> {
                logout(response_msg)
            }
            else -> {
                CommonMethod.showToastShort(response_msg, this)
            }
        }
    }

    private fun logout(response_msg: String) {
        CommonMethod.showToastShort(response_msg, this)
        BMSPrefs.putString(this, Constants._ID, "")
        BMSPrefs.putString(this, Constants.isStoreCreated, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE, "")
        BMSPrefs.putString(this, Constants.PROFILE_IMAGE_BACK, "")
        BMSPrefs.putString(this, Constants.USER_SKYPE_ID, "")
        BMSPrefs.putString(this, Constants.USER_ABOUT, "")
        BMSPrefs.putString(this, Constants.USER_NAME, "")
        CommonMethod.callActivityFinish(this, Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }


}