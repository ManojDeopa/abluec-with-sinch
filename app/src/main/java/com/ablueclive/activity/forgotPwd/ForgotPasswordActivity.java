package com.ablueclive.activity.forgotPwd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ablueclive.R;
import com.ablueclive.activity.verifyOtp.VerifyOtpActivity;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.ProgressD;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, ForgotPwdContract.View {
    private Button btn_next;
    private EditText edt_email;
    private ImageView img_back;
    private String email, otp;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private ProgressDialog progressDialog;
    private ForgotPwdPresenter forgotPwdPresenter;
    private static final String TAG = "ForgotPasswordActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        findView();
    }

    // initialize object
    private void findView() {
        connectionDetector = new ConnectionDetector(getApplicationContext());
        btn_next = findViewById(R.id.btn_next);
        edt_email = findViewById(R.id.edt_email);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    // validation
    private String validateForm() {
        email = edt_email.getText().toString().trim();
        if (email.isEmpty() || !CommonMethod.isEmailValid(email)) {
            edt_email.setFocusable(true);
            edt_email.requestFocus();
            return getString(R.string.valid_email);
        }
        return "success";
    }

    // otp request param
    private void forgotPwdOtpRequest() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        Log.d("ForgotParam", params.toString());
        forgotPwdPresenter = new ForgotPwdPresenter(this);
        forgotPwdPresenter.requestForgetPwdOtp(params);
    }

    // listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next: {
                CommonMethod.hideKeyBoard(ForgotPasswordActivity.this);
                String result = validateForm();
                if (result.equalsIgnoreCase("success")) {
                    isInternetPresent = connectionDetector.isConnectingToInternet();
                    if (isInternetPresent) {
                        forgotPwdOtpRequest();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }

                break;
            }

            case R.id.img_back: {
                onBackPressed();
                break;
            }
        }
    }

    // backpress
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            otp = response_data.getOTP();
            CommonMethod.showToastShort(response_msg, getApplicationContext());
            BMSPrefs.putString(getApplicationContext(), "email", email);
            BMSPrefs.putString(getApplicationContext(), "otp", otp);
            CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), VerifyOtpActivity.class));
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
    }
}
