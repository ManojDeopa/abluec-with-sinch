package com.ablueclive.activity.forgotPwd;

import android.util.Log;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.networkClass.RetrofitClient;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ForgotPwdModel implements ForgotPwdContract.Model {
    private static final String TAG = "ForgotPwdModel";

    @Override
    public void getForgetPwdOtp(OnFinishedListener onFinishedListener, HashMap<String, String> param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.forgetpassword(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseModel responseModel) {
                        if (responseModel != null) {
                            String response_status = responseModel.getResponse_status();
                            String response_msg = responseModel.getResponse_msg();
                            Response_data response_data = null;
                            if (response_status.equalsIgnoreCase("1")) {
                                response_data = responseModel.getResponse_data();
                                onFinishedListener.onFinished(response_status, response_msg, response_data);
                            }


                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG,""+ e.getMessage());
                        onFinishedListener.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
