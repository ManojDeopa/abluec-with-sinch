package com.ablueclive.activity.forgotPwd;
import com.ablueclive.modelClass.Response_data;
import java.util.HashMap;

public class ForgotPwdPresenter implements ForgotPwdContract.Presenter, ForgotPwdContract.Model.OnFinishedListener {
    private ForgotPwdContract.View forgotPwdView;
    private ForgotPwdContract.Model forgotPwdModel;

    public ForgotPwdPresenter(ForgotPwdContract.View forgotPwdView) {
        this.forgotPwdView = forgotPwdView;
        this.forgotPwdModel = new ForgotPwdModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, Response_data response_data) {
        if (forgotPwdView != null) {
            forgotPwdView.hideProgress();
            forgotPwdView.setDataToViews(response_status, response_msg, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (forgotPwdView != null) {
            forgotPwdView.hideProgress();
            forgotPwdView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        forgotPwdView = null;
    }

    @Override
    public void requestForgetPwdOtp(HashMap<String, String> param) {
        if (forgotPwdView != null) {
            forgotPwdView.showProgress();
            forgotPwdModel.getForgetPwdOtp(this, param);
        }

    }
}
