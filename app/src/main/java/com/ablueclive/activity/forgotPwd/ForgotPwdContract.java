package com.ablueclive.activity.forgotPwd;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface ForgotPwdContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, Response_data response_data);

            void onFailure(Throwable t);
        }

        void getForgetPwdOtp(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg, Response_data response_data);

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestForgetPwdOtp(HashMap<String, String> param);
    }
}
