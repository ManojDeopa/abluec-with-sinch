/*
package com.ablueclive.activity.login;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.ablueclive.R;
import com.ablueclive.activity.forgotPwd.ForgotPasswordActivity;
import com.ablueclive.activity.linkedinlogin.LinkedInLoginClass;
import com.ablueclive.activity.mainActivity.MainActivity;
import com.ablueclive.activity.signup.SignUpActivity;
import com.ablueclive.activity.userrole.RoleSwitchActvity;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.GPSTracker;
import com.ablueclive.utils.ProgressD;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.HashMap;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LoginActivityOLD extends AppCompatActivity implements View.OnClickListener, LoginContract.View, GoogleApiClient.OnConnectionFailedListener {
    int LAUNCH_LINKEDIN = 1;
    private TextView tv_linkedin;
    private EditText edt_email, edt_password;
    private String email, password, device_token;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private static final String TAG = "LoginActivity";
    private GPSTracker gpsTracker;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private LoginButton login_button;
    private CallbackManager callbackManager;
    private SignInButton gplusLogin;
    private GoogleApiClient gac;
    private final int RC_SIGN_IN = 100;
    private String device_id;
    private Button fb, gpls;
    private String displayName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findView();
    }


    private void findView() {

        if (!checkPermission()) {
            requestPermission();
        }
        getCurrentLatLong();

        device_id = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_ID);
        device_token = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_TOKEN);

        connectionDetector = new ConnectionDetector(getApplicationContext());
        TextView text_signup = findViewById(R.id.text_signup);
        TextView text_forgot_pass = findViewById(R.id.text_forgot_pass);
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        Button btn_login = findViewById(R.id.btn_login);
        login_button = findViewById(R.id.login_button);
        gplusLogin = findViewById(R.id.tv_gplus);
        tv_linkedin = findViewById(R.id.tv_linkedin);
        ImageView img_back_switch = findViewById(R.id.img_back_switch);
        fb = findViewById(R.id.fb);
        gpls = findViewById(R.id.gpls);
        text_forgot_pass.setOnClickListener(this);
        text_signup.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        img_back_switch.setOnClickListener(this);

        googleInit();
        // fbInit();
        // linkedInInit();


    }

    private void linkedInInit() {
        tv_linkedin.setOnClickListener(v -> {
            Intent i = new Intent(LoginActivityOLD.this, LinkedInLoginClass.class);
            startActivityForResult(i, LAUNCH_LINKEDIN);
            */
/*  CommonMethod.callActivity(this, new Intent(this, AddHashTagFrnds.class));*//*

            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

        });
    }

    private void googleInit() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        gac = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        gplusLogin.setSize(SignInButton.SIZE_STANDARD);
        gplusLogin.setScopes(gso.getScopeArray());
        gpls.setOnClickListener(v -> {
            displayName = "";
            gplusLogin.performClick();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(gac);
            startActivityForResult(signInIntent, RC_SIGN_IN);

        });
    }

    private void fbInit() {

        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("email", "public_profile");
        fb.setOnClickListener(v -> login_button.performClick());
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                //  handleFacebookAccessToken(loginResult.getAccessToken());
                getUserDetailFromFB();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 64206) {
            onFacebookActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null) {
                handleSignInResult(result);
            }
        }


        if (requestCode == LAUNCH_LINKEDIN) {

            String emailiD = LinkedInLoginClass.emailId;
            String id = LinkedInLoginClass.idLinkedin;
            String name = LinkedInLoginClass.firstName;
            String lastname = LinkedInLoginClass.lastName;
            if (emailiD == null) {
                Toast.makeText(gpsTracker, "Please try again later", Toast.LENGTH_SHORT).show();
            } else {
                loginRequestFromSocialLogin(name, emailiD, "li", id, "");
            }
        }
    }


    void onFacebookActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void getCurrentLatLong() {
        gpsTracker = new GPSTracker(LoginActivityOLD.this);
    }


    private void loginRequestFromServer() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("device_token", device_token);
        params.put("device_type", "1");
        params.put("device_id", device_id);
        params.put("lat", String.valueOf(gpsTracker.getLatitude()));
        params.put("long", String.valueOf(gpsTracker.getLongitude()));
        Log.d("loginParam", params.toString());
        LoginPresenter loginPresenter = new LoginPresenter(this);
        loginPresenter.requestLoginDetail(params);
    }


    private String validateLoginForm() {
        email = edt_email.getText().toString().toLowerCase().trim();
        password = edt_password.getText().toString().trim();

        if (email.isEmpty()) {
            edt_email.setFocusable(true);
            edt_email.requestFocus();
            return getString(R.string.valid_email);
        }

        if (password.isEmpty()) {
            edt_password.setFocusable(true);
            edt_password.requestFocus();
            return getString(R.string.valid_pwd);
        }

        return "success";
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_signup: {
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), SignUpActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.text_forgot_pass: {
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.btn_login: {
                CommonMethod.hideKeyBoard(LoginActivityOLD.this);
                String result = validateLoginForm();
                if (result.equalsIgnoreCase("success")) {
                    isInternetPresent = connectionDetector.isConnectingToInternet();
                    if (isInternetPresent) {
                        loginRequestFromServer();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }

            case R.id.img_back_switch: {
                CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), RoleSwitchActvity.class));
                LoginActivityOLD.this.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            BMSPrefs.putString(getApplicationContext(), Constants.SESSION_TOKEN, response_data.getProfile().getSession_token());
            BMSPrefs.putString(getApplicationContext(), Constants._ID, response_data.getProfile().get_id());
            BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, response_data.getProfile().getIsStoreCreated());
            BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, response_data.getProfile().getProfile_image());
            BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, response_data.getProfile().getBackground_image());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_NAME, response_data.getProfile().getName());
            BMSPrefs.putString(getApplicationContext(), Constants.DISPLAY_NAME, response_data.getProfile().getDisplay_name());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_EMAIL, response_data.getProfile().getEmail());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_PHONE, response_data.getProfile().getMobile());
            CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), MainActivity.class));
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
            finish();
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }


    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.e(TAG, throwable.getMessage());
    }


    // check permission
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result4 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result5 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED
                && result5 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA,RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (locationAccepted && cameraAccepted) {
                    CommonMethod.showToastShort("Permission Granted, Now you can access location", getApplicationContext());
                } else {
                    CommonMethod.showToastShort("Permission Denied, You cannot access location", getApplicationContext());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {

                            showMessageOKCancel((dialogInterface, i) -> {
                                requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
                            });
                        }
                    }
                }
            }
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivityOLD.this)
                .setMessage("You need to allow access to both the permissions")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    void getUserDetailFromFB() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d("facebook response", response.toString());

                String name = object.optString("name");
                String email = object.optString("email");
                String id = object.optString("id");
                //  String image=object.getString("profile_image");
                String image =
                        "https://graph.facebook.com/" + id + "/picture?type=large";
//                    facebookGraphUser.user_image = image
                Log.d("facebook response", name + email + image);

                isInternetPresent = connectionDetector.isConnectingToInternet();
                if (isInternetPresent) {
                    loginRequestFromSocialLogin(name, email, "fb", id, image);
                } else {
                    CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                }

            }
        });
        Bundle parameters = new Bundle();
//        parameters.putString("fields", "name,email");
        parameters.putString("fields",
                "id,name,email,gender,birthday,picture.width(500),first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginRequestFromSocialLogin(String name, String email, String social_type, String social_id, String photurl) {
        final HashMap<String, String> params = new HashMap<>();

        if (email == null) {
            params.put("email", "");
        } else {
            params.put("email", email);
        }

        params.put("name", name);
        params.put("display_name", displayName);

        switch (social_type) {
            case "fb":
                params.put("facebook_id", String.valueOf(social_id));
                params.put("request_type", "1");
                break;
            case "gp":
                params.put("gplus_id", String.valueOf(social_id));
                params.put("request_type", "2");
                break;
            case "li":
                params.put("linkedin_id", String.valueOf(social_id));
                params.put("request_type", "3");
                break;
        }
        params.put("device_token", device_token);
        params.put("device_type", "1");

        params.put("device_id", device_id);
        params.put("country_code", "+91");

        params.put("mobile", "");
        params.put("profile_image", photurl);

        params.put("lat", String.valueOf(gpsTracker.getLatitude()));
        params.put("long", String.valueOf(gpsTracker.getLongitude()));

        params.put("language", "en");
        params.put("debug_mode", "1");

        Log.d(TAG, "loginRequestFromSocialLogin: " + params);
        SocialLoginPresenter socialLoginPresenter = new SocialLoginPresenter(this);
        socialLoginPresenter.requestLoginDetail(params);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                String personName = acct.getDisplayName();
                String personPhotoUrl;
                if (acct.getPhotoUrl() == null) {
                    personPhotoUrl = "";
                } else {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }
                String email = acct.getEmail();
                String id = acct.getId();
                Log.e(TAG, "Name: " + personName + ", email: " + email + ", Image: " + "");

                if (TextUtils.isEmpty(personName)) {
                    enterNameDialog(email, id, personPhotoUrl);
                } else {
                    loginRequestFromSocialLogin(personName, email, "gp", id, personPhotoUrl);
                }
            }
        }
    }

    private void enterNameDialog(String email, String id, String personPhotoUrl) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.enter_name_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        EditText etName = alertDialog.findViewById(R.id.etName);
        EditText etDisplayName = alertDialog.findViewById(R.id.etDisplayName);

        EditText etAt = alertDialog.findViewById(R.id.etAt);
        etDisplayName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (etAt.getVisibility() == View.GONE) {
                    etAt.setVisibility(View.VISIBLE);
                }
            }
        });


        alertDialog.findViewById(R.id.btnSubmit).setOnClickListener(v -> {

            String name = etName.getText().toString().trim();
            displayName = etDisplayName.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                CommonMethod.showToastShort(getString(R.string.valid_name), this);
            } else if (TextUtils.isEmpty(displayName)) {
                CommonMethod.showToastShort(getString(R.string.valid_display_name), this);
            } else if (CommonMethod.isValidDisplayName(displayName)) {
                CommonMethod.showToastShort(getString(R.string.invalid_display_name), this);
            } else {
                loginRequestFromSocialLogin(name, email, "gp", id, personPhotoUrl);
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(gac);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NotNull GoogleSignInResult googleSignInResult) {
                    //       hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


*/
/*    private void fetchuserData() {
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                try {
                    JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                    String firstName = jsonObject.getString("firstName");

                    String userEmail = jsonObject.getString("emailAddress");
                    String name = jsonObject.getString("First Name");
                    String id = jsonObject.getString("id");

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("First Name " + firstName + "\n\n");
                    stringBuilder.append("Email " + userEmail);


                    Log.d(TAG, "onApiSuccess: " + stringBuilder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!
                Toast.makeText(getApplicationContext(), "API Error" + liApiError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }*//*


}
*/
