package com.ablueclive.activity.login;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public interface LoginContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished(String response_status, String response_msg, Response_data response_data);

            void onFailure(Throwable t);
        }

        void getLoginDetail(OnFinishedListener onFinishedListener, HashMap<String, String> param);
    }

    interface View {

        void showProgress();

        void hideProgress();

        void setDataToViews(String response_status, String response_msg, Response_data response_data);

        void onResponseFailure(Throwable throwable);

        //void setDataToViewsForSocialLogin(String response_status, String response_msg, Response_data response_data);
    }

    interface Presenter {

        void onDestroy();

        void requestLoginDetail(HashMap<String, String> param);
    }
}
