package com.ablueclive.activity.login;

import android.util.Log;

import androidx.annotation.NonNull;

import com.ablueclive.interfaces.ApiInterface;
import com.ablueclive.modelClass.ResponseModel;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.networkClass.RetrofitClient;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SocialLoginModel implements LoginContract.Model {
    private static final String TAG = "LoginModel";

    @Override
    public void getLoginDetail(OnFinishedListener onFinishedListener, HashMap<String, String> param) {
        Retrofit retrofit = RetrofitClient.getRetrofitClient();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        apiInterface.SocialLogin(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseModel>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull ResponseModel responseModel) {
                        String response_status = responseModel.getResponse_status();
                        String response_msg = responseModel.getResponse_msg();
                        Response_data response_data = null;
                        if (response_status.equals("1")) {
                            response_data = responseModel.getResponse_data();
                        }
                        onFinishedListener.onFinished(response_status, response_msg, response_data);

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try {
                            Log.d(TAG, e.getMessage());
                            onFinishedListener.onFailure(e);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}

