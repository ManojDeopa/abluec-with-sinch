package com.ablueclive.activity.login;

import com.ablueclive.modelClass.Response_data;

import java.util.HashMap;

public class SocialLoginPresenter  implements LoginContract.Presenter, LoginContract.Model.OnFinishedListener {

    private LoginContract.View loginView;
    private LoginContract.Model loginModel;

    SocialLoginPresenter(LoginContract.View loginView) {
        this.loginView = loginView;
        this.loginModel = new SocialLoginModel();
    }

    @Override
    public void onFinished(String response_status, String response_msg, Response_data response_data) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.setDataToViews(response_status, response_msg, response_data);
        }

    }

    @Override
    public void onFailure(Throwable t) {
        if (loginView != null) {
            loginView.hideProgress();
            loginView.onResponseFailure(t);
        }

    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void requestLoginDetail(HashMap<String, String> param) {
        if (loginView != null) {
            loginView.showProgress();
            loginModel.getLoginDetail(this, param);
        }

    }
}
