package com.ablueclive.activity.login;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.ablueclive.R;
import com.ablueclive.activity.forgotPwd.ForgotPasswordActivity;
import com.ablueclive.activity.linkedinlogin.LinkedInLoginClass;
import com.ablueclive.activity.mainActivity.MainActivity;
import com.ablueclive.activity.signup.SignUpActivity;
import com.ablueclive.global.activity.InviteContactsActivity;
import com.ablueclive.modelClass.Response_data;
import com.ablueclive.utils.BMSPrefs;
import com.ablueclive.utils.CommonMethod;
import com.ablueclive.utils.ConnectionDetector;
import com.ablueclive.utils.Constants;
import com.ablueclive.utils.GPSTracker;
import com.ablueclive.utils.ProgressD;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginContract.View, GoogleApiClient.OnConnectionFailedListener {
    int LAUNCH_LINKEDIN = 1;
    private TextView tv_linkedin;
    private EditText edt_email, edt_password;
    private String email, password, device_token;
    private ConnectionDetector connectionDetector;
    private boolean isInternetPresent = false;
    private static final String TAG = "LoginActivity";
    private GPSTracker gpsTracker;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private SignInButton gplusLogin;
    private GoogleApiClient gac;
    private final int RC_SIGN_IN = 100;
    private String device_id;
    private Button fb, gpls;
    private String displayName = "";
    private String LOGIN_TYPE = "0";
    private static final String SOCIAL_LOGIN = "1";
    private static final String NORMAL_LOGIN = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findView();
    }


    private void findView() {

        if (!isPermissionAllowed()) {
            requestPermission();
        }

        getCurrentLatLong();

        device_id = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_ID);
        device_token = BMSPrefs.getString(getApplicationContext(), Constants.DEVICE_TOKEN);

        connectionDetector = new ConnectionDetector(getApplicationContext());
        TextView text_signup = findViewById(R.id.text_signup);
        TextView text_forgot_pass = findViewById(R.id.text_forgot_pass);
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        Button btn_login = findViewById(R.id.btn_login);
        gplusLogin = findViewById(R.id.tv_gplus);
        tv_linkedin = findViewById(R.id.tv_linkedin);
        ImageView img_back_switch = findViewById(R.id.img_back_switch);
        fb = findViewById(R.id.fb);
        gpls = findViewById(R.id.gpls);
        text_forgot_pass.setOnClickListener(this);
        text_signup.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        img_back_switch.setOnClickListener(this);

        googleInit();
        // fbInit();
        // linkedInInit();


    }

    private void linkedInInit() {
        tv_linkedin.setOnClickListener(v -> {
            Intent i = new Intent(LoginActivity.this, LinkedInLoginClass.class);
            startActivityForResult(i, LAUNCH_LINKEDIN);
            /*  CommonMethod.callActivity(this, new Intent(this, AddHashTagFrnds.class));*/
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);

        });
    }

    private void googleInit() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        gac = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        gplusLogin.setSize(SignInButton.SIZE_STANDARD);
        gplusLogin.setScopes(gso.getScopeArray());
        gpls.setOnClickListener(v -> {
            LOGIN_TYPE = SOCIAL_LOGIN;
            displayName = "";
            gplusLogin.performClick();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(gac);
            startActivityForResult(signInIntent, RC_SIGN_IN);

        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null) {
                handleSignInResult(result);
            }
        }


        if (requestCode == LAUNCH_LINKEDIN) {

            String emailiD = LinkedInLoginClass.emailId;
            String id = LinkedInLoginClass.idLinkedin;
            String name = LinkedInLoginClass.firstName;
            String lastname = LinkedInLoginClass.lastName;
            if (emailiD == null) {
                Toast.makeText(gpsTracker, "Please try again later", Toast.LENGTH_SHORT).show();
            } else {
                loginRequestFromSocialLogin(name, emailiD, "li", id, "");
            }
        }

        if (requestCode == 101) {
            if (!isPermissionAllowed()) {
                requestPermission();
            }
        }
    }


    private void getCurrentLatLong() {
        gpsTracker = new GPSTracker(LoginActivity.this);
    }


    private void loginRequestFromServer() {
        final HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("device_token", device_token);
        params.put("device_type", "1");
        params.put("device_id", device_id);
        params.put("lat", String.valueOf(gpsTracker.getLatitude()));
        params.put("long", String.valueOf(gpsTracker.getLongitude()));
        Log.d("loginParam", params.toString());
        LoginPresenter loginPresenter = new LoginPresenter(this);
        loginPresenter.requestLoginDetail(params);
    }


    private String validateLoginForm() {
        email = edt_email.getText().toString().toLowerCase().trim();
        password = edt_password.getText().toString().trim();

        if (email.isEmpty()) {
            edt_email.setFocusable(true);
            edt_email.requestFocus();
            return getString(R.string.valid_email);
        }

        if (password.isEmpty()) {
            edt_password.setFocusable(true);
            edt_password.requestFocus();
            return getString(R.string.valid_pwd);
        }

        return "success";
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_signup: {
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), SignUpActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.text_forgot_pass: {
                CommonMethod.callActivity(getApplicationContext(), new Intent(getApplicationContext(), ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                break;
            }

            case R.id.btn_login: {
                LOGIN_TYPE = NORMAL_LOGIN;
                CommonMethod.hideKeyBoard(LoginActivity.this);
                String result = validateLoginForm();
                if (result.equalsIgnoreCase("success")) {
                    isInternetPresent = connectionDetector.isConnectingToInternet();
                    if (isInternetPresent) {
                        loginRequestFromServer();
                    } else {
                        CommonMethod.showToastShort(getString(R.string.internet_toast), getApplicationContext());
                    }
                } else {
                    CommonMethod.showToastShort(result, getApplicationContext());
                }
                break;
            }


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void showProgress() {
        ProgressD.Companion.show(this, "");
    }

    @Override
    public void hideProgress() {
        ProgressD.Companion.hide();
    }

    @Override
    public void setDataToViews(String response_status, String response_msg, Response_data response_data) {
        if (response_status.equalsIgnoreCase("1")) {
            BMSPrefs.putString(getApplicationContext(), Constants.SESSION_TOKEN, response_data.getProfile().getSession_token());
            BMSPrefs.putString(getApplicationContext(), Constants._ID, response_data.getProfile().get_id());
            BMSPrefs.putString(getApplicationContext(), Constants.isStoreCreated, response_data.getProfile().getIsStoreCreated());
            BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE, response_data.getProfile().getProfile_image());
            BMSPrefs.putString(getApplicationContext(), Constants.PROFILE_IMAGE_BACK, response_data.getProfile().getBackground_image());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_NAME, response_data.getProfile().getName());
            BMSPrefs.putString(getApplicationContext(), Constants.DISPLAY_NAME, response_data.getProfile().getDisplay_name());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_EMAIL, response_data.getProfile().getEmail());
            BMSPrefs.putString(getApplicationContext(), Constants.USER_PHONE, response_data.getProfile().getMobile());

            if (LOGIN_TYPE.equals(SOCIAL_LOGIN)) {
                BMSPrefs.putBoolean(getApplicationContext(), Constants.ARE_INVITED, false);
                CommonMethod.callActivity(this, new Intent(this, InviteContactsActivity.class));
            } else {
                BMSPrefs.putBoolean(getApplicationContext(), Constants.ARE_INVITED, true);
                CommonMethod.callActivityFinish(getApplicationContext(), new Intent(getApplicationContext(), MainActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
            }
            finish();
        } else {
            CommonMethod.showToastShort(response_msg, getApplicationContext());
        }
    }


    @Override
    public void onResponseFailure(Throwable throwable) {
        Log.e(TAG, throwable.getMessage());
    }


    // check permission
    private boolean isPermissionAllowed() {

        int result = ActivityCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ActivityCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        int result2 = ActivityCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        int result3 = ActivityCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result4 = ActivityCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result5 = ActivityCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        int result6 = ActivityCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        int permissionGranted = PackageManager.PERMISSION_GRANTED;
        return result == permissionGranted
                && result1 == permissionGranted
                && result2 == permissionGranted
                && result3 == permissionGranted
                && result4 == permissionGranted
                && result5 == permissionGranted
                && result6 == permissionGranted;
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, RECORD_AUDIO)) {

            showMessageOKCancel((dialogInterface, i) -> {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 101);
            });

        } else {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION, READ_CONTACTS, READ_EXTERNAL_STORAGE, CAMERA, RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {

                boolean allowAllPermissions = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        allowAllPermissions = false;
                        break;
                    }
                }

                if (allowAllPermissions) {
                    CommonMethod.showToastShort("Permission Granted, Now you can use Application", getApplicationContext());
                } else {
                    showMessageOKCancel((dialogInterface, i) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 101);
                    });
                }
            }
        }
    }


    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this, R.style.alertDialogTheme)
                .setTitle(getString(R.string.app_name))
                .setMessage(R.string.allow_permission_text)
                .setPositiveButton(getString(R.string.ok), okListener)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> finish())
                .create()
                .show();
    }


    private void loginRequestFromSocialLogin(String name, String email, String social_type, String social_id, String photurl) {
        final HashMap<String, String> params = new HashMap<>();

        if (email == null) {
            params.put("email", "");
        } else {
            params.put("email", email);
        }

        params.put("name", name);
        params.put("display_name", displayName);

        switch (social_type) {
            case "fb":
                params.put("facebook_id", String.valueOf(social_id));
                params.put("request_type", "1");
                break;
            case "gp":
                params.put("gplus_id", String.valueOf(social_id));
                params.put("request_type", "2");
                break;
            case "li":
                params.put("linkedin_id", String.valueOf(social_id));
                params.put("request_type", "3");
                break;
        }
        params.put("device_token", device_token);
        params.put("device_type", "1");

        params.put("device_id", device_id);
        params.put("country_code", "+" + getCountryCode());

        params.put("mobile", "");
        params.put("profile_image", photurl);

        params.put("lat", String.valueOf(gpsTracker.getLatitude()));
        params.put("long", String.valueOf(gpsTracker.getLongitude()));

        params.put("language", "en");
        params.put("debug_mode", "1");

        Log.d(TAG, "loginRequestFromSocialLogin: " + params);
        SocialLoginPresenter socialLoginPresenter = new SocialLoginPresenter(this);
        socialLoginPresenter.requestLoginDetail(params);
    }

    private String getCountryCode() {
        String countryCode = "91";
        try {
            List<Address> addresses = new Geocoder(this).getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 10);
            if (!addresses.isEmpty()) {
                Address address = addresses.get(0);
                String CountryID = address.getCountryCode().toUpperCase();
                String[] arrCountryCode = getResources().getStringArray(R.array.DialingCountryCode);
                for (String s : arrCountryCode) {
                    String[] arrDial = s.split(",");
                    if (arrDial[1].trim().equals(CountryID.trim())) {
                        countryCode = arrDial[0];
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return countryCode;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            if (acct != null) {
                String personName = acct.getDisplayName();
                String personPhotoUrl;
                if (acct.getPhotoUrl() == null) {
                    personPhotoUrl = "";
                } else {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }
                String email = acct.getEmail();
                String id = acct.getId();
                Log.e(TAG, "Name: " + personName + ", email: " + email + ", Image: " + "");

                if (TextUtils.isEmpty(personName)) {
                    enterNameDialog(email, id, personPhotoUrl);
                } else {
                    loginRequestFromSocialLogin(personName, email, "gp", id, personPhotoUrl);
                }
            }
        }
    }

    private void enterNameDialog(String email, String id, String personPhotoUrl) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.enter_name_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        EditText etName = alertDialog.findViewById(R.id.etName);
        EditText etDisplayName = alertDialog.findViewById(R.id.etDisplayName);

        EditText etAt = alertDialog.findViewById(R.id.etAt);
        etDisplayName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (etAt.getVisibility() == View.GONE) {
                    etAt.setVisibility(View.VISIBLE);
                }
            }
        });


        alertDialog.findViewById(R.id.btnSubmit).setOnClickListener(v -> {

            String name = etName.getText().toString().trim();
            displayName = etDisplayName.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                CommonMethod.showToastShort(getString(R.string.valid_name), this);
            } else if (TextUtils.isEmpty(displayName)) {
                CommonMethod.showToastShort(getString(R.string.valid_display_name), this);
            } else if (CommonMethod.isValidDisplayName(displayName)) {
                CommonMethod.showToastShort(getString(R.string.invalid_display_name), this);
            } else {
                loginRequestFromSocialLogin(name, email, "gp", id, personPhotoUrl);
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(gac);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NotNull GoogleSignInResult googleSignInResult) {
                    //       hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


/*    private void fetchuserData() {
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                try {
                    JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                    String firstName = jsonObject.getString("firstName");

                    String userEmail = jsonObject.getString("emailAddress");
                    String name = jsonObject.getString("First Name");
                    String id = jsonObject.getString("id");

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("First Name " + firstName + "\n\n");
                    stringBuilder.append("Email " + userEmail);


                    Log.d(TAG, "onApiSuccess: " + stringBuilder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!
                Toast.makeText(getApplicationContext(), "API Error" + liApiError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }*/

}
