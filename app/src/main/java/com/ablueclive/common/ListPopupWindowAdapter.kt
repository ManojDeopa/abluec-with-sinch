package com.ablueclive.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.ablueclive.R


class ListPopupWindowAdapter(private val items: List<ListPopupItem>) : BaseAdapter() {

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(i: Int): ListPopupItem {
        return items[i]
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(position: Int, view: View, parent: ViewGroup): View {


        var convertView = view

        val holder: ViewHolder
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.context).inflate(R.layout.item_list_popup, parent, false)
            holder = ViewHolder(convertView)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        holder.tvTitle.text = getItem(position).title
        holder.ivImage.setImageResource(getItem(position).image)
        return view
    }

    internal class ViewHolder(view: View) {
        var tvTitle: TextView = view.findViewById(R.id.text)
        var ivImage: ImageView = view.findViewById(R.id.image)

    }
}