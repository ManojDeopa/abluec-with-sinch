package com.ablueclive.common

import AppUtils
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.storeSection.fragments.*
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.common_activity_container.*


class StoreContainerActivity : AppCompatActivity() {

    companion object {
        var consolidate = false
        var selectedCategoryId = "0"
        var selectedCatName = ""
        var selectedStoreName = ""
        var jobType = ""
        var headerText = ""
        var typeSection = ""
        var isEdit = false
        var tvHeader: TextView? = null

        var isFromContainer = false

        var runningActivities: MutableSet<Activity> = HashSet()

        fun addActivities(activity: Activity) {
            runningActivities.add(activity)
        }

        fun killActivities() {
            for (stackActivity in runningActivities) {
                stackActivity.finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_activity_container)
        try {
            tvHeader = findViewById(R.id.tvTitle)
            ivBack.setOnClickListener {
                onBackPressed()
            }
            intent.getStringExtra(Constants.TITLE)?.let { loadFragment(it) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun loadFragment(titleText: String) {

        var title = titleText
        val fragment: Fragment?

        when (title) {

            getString(R.string.buy_now) -> {
                headerLayout.visibility = View.GONE
                fragment = FindStoreFragment()
            }

            getString(R.string.manage_your_store) -> {
                fragment = ManageStoreFragment()
            }

            getString(R.string.add_store) -> {
                isEdit = false
                fragment = AddStoreFragment()
            }

            getString(R.string.edit_store) -> {
                isEdit = true
                fragment = AddStoreFragment()
            }

            getString(R.string.manage_orders) -> {

                fragment = if (consolidate) {
                    ConsolidateManageOrderTab()
                } else {
                    ManageOrderTabFragment()
                }
            }

            getString(R.string.store_detail) -> {
                title = selectedStoreName
                fragment = StoreDetailFragment()
            }

            getString(R.string.manage_categories) -> {
                headerLayout.visibility = View.GONE
                fragment = ManageCategoriesFragment()
            }

            getString(R.string.add_category) -> {
                fragment = AddCategoryFragment()
            }

            getString(R.string.manage_products) -> {
                headerLayout.visibility = View.GONE
                fragment = ManageProductsFragment()
            }

            getString(R.string.add_product) -> {
                selectedCategoryId = "0"
                selectedCatName = ""
                isEdit = false
                fragment = AddProductFragment()
            }

            getString(R.string.edit_product) -> {
                isEdit = true
                fragment = AddProductFragment()
            }

            getString(R.string.manage_agent) -> {
                headerLayout.visibility = View.GONE
                fragment = ManageAgentFragment()
            }

            getString(R.string.add_agent) -> {
                fragment = AddAgentFragment()
            }

            getString(R.string.store_calendar) -> {
                fragment = StoreCalendarFragment()
            }

            getString(R.string.order_details) -> {
                fragment = StoreOrderDetailFragment()
            }

            getString(R.string.order_completed) -> {
                isFromContainer = true
                fragment = StoreOrderListFragment(title)
            }

            getString(R.string.select_category) -> {
                if (selectedCategoryId != "0") {
                    title = selectedCatName
                }
                fragment = SelectStoreCategoryFragment()
            }

            else -> {
                finish()
                return
            }
        }

        tvTitle.text = title

        headerText = tvTitle.text.toString()

        AppUtils.addFragment(this, fragment, R.id.jobContainer)

    }

}