package com.ablueclive.common

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SearchDisplayResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int? = null

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_invalid")
    @Expose
    var responseInvalid: Int? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    class ResponseData {
        @SerializedName("searchUserlist")
        @Expose
        var searchUserlist: List<SearchUserlist>? = null
    }

    class SearchUserlist {

        @SerializedName("_id")
        @Expose
        var id: String? = null

        @SerializedName("email")
        @Expose
        var email: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("mobile")
        @Expose
        var mobile: String? = null

        @SerializedName("display_name")
        @Expose
        var displayName: String? = null

        @SerializedName("profile_image")
        @Expose
        var profileImage: String? = null

        @SerializedName("thumb_profile_image")
        @Expose
        var thumbProfileImage: String? = null
    }

}