package com.ablueclive.common

import com.ablueclive.modelClass.PostData
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class SimpleResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int = 0

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null


}