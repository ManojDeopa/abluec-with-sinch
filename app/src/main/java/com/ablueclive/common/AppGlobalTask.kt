package com.ablueclive.common

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ablueclive.R
import kotlinx.android.synthetic.main.custom_alert.view.*

class AppGlobalTask(private val task: () -> Unit, var context: Context) {


    fun showAlert(text: String?) {

        val act = context as Activity
        val viewGroup: ViewGroup = act.findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(act).inflate(R.layout.custom_alert, viewGroup, false)
        val builder = AlertDialog.Builder(act)
        builder.setView(dialogView)

        val alertDialog = builder.create()
        alertDialog.show()


        dialogView.tvTitle.text = context.getString(R.string.app_name)
        dialogView.tvText.text = text

        dialogView.tvOk.setOnClickListener {
            task()
            alertDialog.dismiss()
        }
    }
}