package com.ablueclive.common

import com.ablueclive.modelClass.PostData
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class CommonResponse {

    @SerializedName("response_status")
    @Expose
    var responseStatus: Int = 0

    @SerializedName("response_msg")
    @Expose
    var responseMsg: String? = null

    @SerializedName("response_data")
    @Expose
    var responseData: ResponseData? = null

    class ResponseData {


        @SerializedName("sendjobrequest")
        @Expose
        var sendjobrequest: String? = null

        @SerializedName("updateJobStatus")
        @Expose
        var updateJobStatus: String? = null

        @SerializedName("rejectJobRequestApi")
        @Expose
        var rejectJobRequestApi: String? = null

        @SerializedName("acceptJobRequestApi")
        @Expose
        var acceptJobRequestApi: String? = null

        @SerializedName("completeRequestApi")
        @Expose
        var completeRequestApi: String? = null

        @SerializedName("deletejobUser")
        @Expose
        var deletejobUser: String? = null

        @SerializedName("jobRequestApi")
        @Expose
        var jobRequestApi: String? = null

        @SerializedName("saveJobForLater")
        @Expose
        var saveJobForLater: String? = null

        @SerializedName("saveIgnoredJob")
        @Expose
        var saveIgnoredJob: String? = null

        @SerializedName("rateProvider")
        @Expose
        var rateServiceProvider: String? = null

        @SerializedName("rateJobProvider")
        @Expose
        var rateUser: String? = null

        @SerializedName("addStore")
        @Expose
        var addStore: String? = null

        @SerializedName("deleteStore")
        @Expose
        var deleteStore: String? = null

        @SerializedName("deleteSkills")
        @Expose
        var deleteSkills: String? = null

        @SerializedName("addSkills")
        @Expose
        var addSkills: String? = null

        @SerializedName("editSkills")
        @Expose
        var editSkills: String? = null

        @SerializedName("address")
        @Expose
        var address: String? = null

        @SerializedName("removeFavProvider")
        @Expose
        var removeFavProvider: String? = null

        @SerializedName("getcancelAppliedjobList")
        @Expose
        var getcancelAppliedjobList: String="Job Cancelled Successfully"

        @SerializedName("postData")
        @Expose
        var postData: List<PostData>? = null


    }

}