package com.ablueclive.common

import AppUtils
import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.fragment.allFriendListChat.MessagingListFragment
import com.ablueclive.fragment.feedFragment.FeedDetailFragment
import com.ablueclive.fragment.feedFragment.TotalWaveFragment
import com.ablueclive.fragment.friendList.FriendListFragment
import com.ablueclive.fragment.notificationListFragment.NotificationListFragment
import com.ablueclive.global.fragment.*
import com.ablueclive.storeSection.fragments.*
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.common_activity_container.*

class CommonContainerActivity : AppCompatActivity() {

    companion object {

        var feedDetailTitle = ""
        var headerText = ""


        var runningActivities: MutableSet<Activity> = HashSet()

        fun addActivities(activity: Activity) {
            runningActivities.add(activity)
        }

        fun killActivities() {
            for (stackActivity in runningActivities) {
                stackActivity.finish()
            }
        }
    }

    lateinit var bundle: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_activity_container)
        try {
            bundle = Bundle()
            ivBack.setOnClickListener {
                onBackPressed()
            }
            intent.getStringExtra(Constants.TITLE)?.let { loadFragment(it) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun loadFragment(titleText: String) {

        var title = titleText
        val fragment: Fragment?

        when (title) {

            getString(R.string.update_address) -> {
                if (UpdateAddressFragment.isChangeAddress) {
                    title = getString(R.string.change_address)
                }
                fragment = UpdateAddressFragment.newInstance(bundle)
            }

            getString(R.string.search_all) -> {
                fragment = SearchTabFragment.newInstance(bundle)
            }

            getString(R.string.favourite_service_provider) -> {
                fragment = FavSProvidersFragment.newInstance(bundle)
            }

            getString(R.string.add_work_experience) -> {
                fragment = AddExperienceFragment.newInstance(bundle)
            }

            getString(R.string.search_store_detail) -> {
                headerLayout.visibility = View.GONE
                fragment = SearchStoreDetailFragment(title)
            }


            getString(R.string.categories) -> {
                headerLayout.visibility = View.GONE
                fragment = SearchStoreDetailFragment(title)
            }

            getString(R.string.featured_products_text) -> {
                headerLayout.visibility = View.GONE
                fragment = SearchStoreDetailFragment(title)
            }

            getString(R.string.popular_products) -> {
                headerLayout.visibility = View.GONE
                fragment = SearchStoreDetailFragment(title)
            }

            getString(R.string.check_out) -> {
                fragment = CheckOutFragment.newInstance(bundle)
            }

            getString(R.string.favourite_product) -> {
                fragment = FavProductListFragment.newInstance(bundle)
            }

            getString(R.string.your_order) -> {
                fragment = StoreCOrdersTabFragment.newInstance(bundle)
            }

            getString(R.string.order_details) -> {
                fragment = OrderDetailFragment.newInstance(bundle)
            }

            getString(R.string.search_work_detail) -> {
                fragment = SearchWorkDetailFragment.newInstance(bundle)
            }

            getString(R.string.wall) -> {
                fragment = PostDataFragment.newInstance(bundle, Constants.TYPE_P)
            }

            getString(R.string.notifications) -> {
                fragment = NotificationListFragment.newInstance(bundle)
            }

            getString(R.string.friend_list) -> {
                fragment = FriendListFragment()
            }

            getString(R.string.my_orders) -> {
                fragment = StoreCOrdersTabFragment.newInstance(bundle)
            }

            getString(R.string.messaging) -> {
                fragment = MessagingListFragment.newInstance(bundle)
            }

            getString(R.string.followers) -> {
                fragment = FollowListFragment(title)
            }

            getString(R.string.followings) -> {
                fragment = FollowListFragment(title)
            }

            getString(R.string.feedback) -> {
                fragment = FeedbackFragment.newInstance(bundle)
            }

            getString(R.string.feed_details) -> {
                title = feedDetailTitle
                fragment = FeedDetailFragment(false)
            }

            getString(R.string.quote_wave) -> {
                if (feedDetailTitle.isNotEmpty()) {
                    title = feedDetailTitle
                }
                fragment = FeedDetailFragment(true)
            }

            getString(R.string.wave) -> {
                fragment = TotalWaveFragment.newInstance(bundle)
            }


            getString(R.string.add_academic) -> {
                fragment = AddAcademicFragment.newInstance(bundle)
            }

            else -> {
                finish()
                return
            }
        }

        tvTitle.text = title

        headerText = tvTitle.text.toString()

        AppUtils.addFragment(this, fragment, R.id.jobContainer)

    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (headerText == getString(R.string.check_out)) {
            ProductDetailsFragment.shouldRefresh = true
        }
    }
}