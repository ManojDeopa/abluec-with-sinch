package com.ablueclive.common

import AppUtils
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.jobSection.addJobDetail.AddJobDetailFragment
import com.ablueclive.jobSection.categoryFragment.SelectCategoryFragment
import com.ablueclive.jobSection.jobfragment.AddJobFragment
import com.ablueclive.jobSection.jobfragment.JobPostTabFragment
import com.ablueclive.jobSection.newJobFragment.NewJobDetailFragment
import com.ablueclive.jobSection.rateReview.RateReviewFragment
import com.ablueclive.jobSection.runningJob.PJobDetailFragment
import com.ablueclive.jobSection.searchJob.SJobDetailFragment
import com.ablueclive.jobSection.searchJob.SearchJobTabFragment
import com.ablueclive.jobSection.serviceProviders.ServiceProvidersFragment
import com.ablueclive.jobSection.subCategory.SubCategoryFragment
import com.ablueclive.utils.CommonMethod
import com.ablueclive.utils.Constants
import kotlinx.android.synthetic.main.common_activity_container.*

class JobContainerActivity : AppCompatActivity() {

    companion object {
        var selectedCategoryId = ""
        var selectedCategoryName = ""
        var selectedSubCategoryId = ""
        var selectedServiceProviderId = ""
        var jobType = ""
        var headerText = ""
        var typeSection = ""
        var isEdit = false
        var isJobAdded = false
        var isFromProfile = false
        lateinit var imgEdit: ImageView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_activity_container)
        imgEdit = ivEdit
        ivBack.setOnClickListener {
            onBackPressed()
        }

        intent.getStringExtra(Constants.TITLE)?.let { loadFragment(it) }
    }

    private fun loadFragment(titleText: String) {

        var title = titleText
        val fragment: Fragment?

        when (title) {

            getString(R.string.job_post) -> {
                title = /*getString(R.string.works)*/""
                fragment = JobPostTabFragment()
            }

            getString(R.string.search_job) -> {
                title = getString(R.string.jobs)
                fragment = SearchJobTabFragment()
            }

            getString(R.string.add_job) -> {
                fragment = AddJobFragment()
            }

            getString(R.string.select_category) -> {
                selectedCategoryId = ""
                fragment = SelectCategoryFragment()
            }

            getString(R.string.select_sub_category) -> {
                selectedSubCategoryId = ""
                title = selectedCategoryName
                fragment = SubCategoryFragment()
            }

            getString(R.string.select_service_provider) -> {
                selectedServiceProviderId = ""
                fragment = ServiceProvidersFragment()
            }

            getString(R.string.add_job_detail) -> {
                isEdit = false
                fragment = AddJobDetailFragment()
            }

            getString(R.string.edit_job_details) -> {
                isEdit = true
                fragment = AddJobDetailFragment()
            }


            getString(R.string.new_job_detail) -> {
                fragment = NewJobDetailFragment()
            }


            getString(R.string.requested_job_detail) -> {
                fragment = SJobDetailFragment()
            }

            getString(R.string.running_job_detail) -> {
                fragment = if (typeSection == getString(R.string.job_post)) {
                    PJobDetailFragment()
                } else {
                    SJobDetailFragment()
                }
            }

            getString(R.string.applicants_job_detail) -> {
                fragment = PJobDetailFragment()
            }

            getString(R.string.complete_job_detail) -> {
                fragment = if (typeSection == getString(R.string.job_post)) {
                    PJobDetailFragment()
                } else {
                    SJobDetailFragment()
                }
            }

            getString(R.string.rate_review) -> {
                fragment = RateReviewFragment()
            }

            else -> {
                finish()
                return
            }
        }

        tvTitle.text = title

        headerText = tvTitle.text.toString()

        AppUtils.addFragment(this, fragment, R.id.jobContainer)

    }


    override fun onBackPressed() {
        if (isJobAdded) {
            isJobAdded = false
            finishAffinity()
            CommonMethod.callActivity(this, Intent(this, MainActivity::class.java))
        } else {
            super.onBackPressed()
        }
    }

}