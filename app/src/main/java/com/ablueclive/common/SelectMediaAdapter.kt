package com.ablueclive.common

import android.content.Context
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ablueclive.R
import com.ablueclive.listener.onMenuItemListener
import com.ablueclive.modelClass.Files
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recycler_add_layout.view.*
import kotlinx.android.synthetic.main.upload_photo_video_adapter.view.*
import java.io.File
import java.util.*

class SelectMediaAdapter(private val context: Context, private val mResults: ArrayList<Files>, private val onMenuItemListener: onMenuItemListener) : RecyclerView.Adapter<SelectMediaAdapter.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(viewType, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if (position == mResults.size) {
            viewHolder.itemView.addLayout.setOnClickListener {
                onMenuItemListener.onMenuItemClick(2)
            }
        } else {
            loadView(mResults[position], viewHolder)
            viewHolder.itemView.img_close.setOnClickListener {
                onMenuItemListener.onMenuItemClick(position)
            }
        }
    }

    private fun loadView(model: Files, viewHolder: ViewHolder) {

        if (model.type == "1") {
            viewHolder.itemView.img_play.visibility = View.GONE
        } else if (model.type == "2") {
            viewHolder.itemView.img_play.visibility = View.VISIBLE
        }

        if (isValidUrl(model.media_path)) {
            Glide.with(context)
                    .load(model.media_path)
                    .skipMemoryCache(true)
                    .error(R.drawable.user_profile)
                    .into(viewHolder.itemView.img_photo)
        } else {
            Glide.with(context)
                    .load(File(model.media_path))
                    .skipMemoryCache(true)
                    .error(R.drawable.user_profile)
                    .into(viewHolder.itemView.img_photo)
        }


    }

    override fun getItemCount(): Int {
        return mResults.size + 1
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    fun isValidUrl(url: String): Boolean {
        val p = Patterns.WEB_URL
        val m = p.matcher(url.toLowerCase(Locale.getDefault()))
        return m.matches()
    }


    override fun getItemViewType(position: Int): Int {
        return if (position == mResults.size) {
            R.layout.recycler_add_layout
        } else {
            R.layout.upload_photo_video_adapter
        }
    }
}