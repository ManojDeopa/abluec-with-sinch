package com.ablueclive.common

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ablueclive.R
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.fragment.allFriendListChat.MessagingListFragment
import com.ablueclive.interfaces.ApiInterface
import com.ablueclive.networkClass.RetrofitClient
import com.ablueclive.utils.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.feedback_fragment.*

import java.util.*

class FeedbackFragment : Fragment() {

    lateinit var connectionDetector: ConnectionDetector
    private var isInternetPresent = false
    private var session_token = ""

    companion object{
        fun newInstance(bundle: Bundle): FeedbackFragment {
            val fragment = FeedbackFragment()
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.feedback_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()

        doOnClicks()

    }

    private fun doOnClicks() {

        btnSubmit.setOnClickListener {
            addFeedback()
        }
    }

    private fun init() {
        session_token = BMSPrefs.getString(requireActivity(), Constants.SESSION_TOKEN)
        connectionDetector = ConnectionDetector(requireActivity())
        isInternetPresent = connectionDetector.isConnectingToInternet
        resetFocus()
    }

    private fun resetFocus() {
        rootLayout.requestFocus()
        CommonMethod.hideKeyBoard(requireActivity())
    }


    fun showAlert(text: String?) {
        val alert = androidx.appcompat.app.AlertDialog.Builder(requireActivity(), R.style.alertDialogTheme)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(text)
        alert.setCancelable(false)
        alert.setPositiveButton(getString(R.string.ok)) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            requireActivity().finish()
        }
        alert.show()
    }

    private fun isValidate(): Boolean {

        return when {

            etFeedback.text.toString().trim().isEmpty() -> {
                CommonMethod.showToastlong(getString(R.string.please_enter_feedback), requireContext())
                false
            }

            else -> true
        }
    }


    private fun addFeedback() {


        if (!isValidate()) return

        if (!isInternetPresent) {
            CommonMethod.showToastlong(getString(R.string.internet_toast), requireContext())
            return
        }

        ProgressD.show(requireActivity(), "")

        val param = HashMap<String, String>()
        param["session_token"] = session_token
        param["note"] = etFeedback.text.toString()


        val retrofit = RetrofitClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.feedback(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<CommonResponse?> {

                    override fun onSubscribe(d: Disposable) {}

                    override fun onComplete() {}

                    override fun onNext(response: CommonResponse) {
                        ProgressD.hide()
                        try {
                            CommonMethod.showToastShort(response.responseMsg, requireActivity())
                            if (response.responseStatus == 1) {
                                requireActivity().finishAffinity()
                                startActivity(Intent(requireActivity(), MainActivity::class.java))
                                // AppGlobalTask({ requireActivity().finish() }, requireContext()).showAlert(response.responseMsg)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun onError(e: Throwable) {
                        ProgressD.hide()
                        println(e.message)
                    }
                })
    }

}