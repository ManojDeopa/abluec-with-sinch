package com.ablueclive.fcmClass

import android.app.*
import android.app.ActivityManager.RunningAppProcessInfo
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.ablueclive.R
import com.ablueclive.activity.commentActivity.CommentActivity
import com.ablueclive.activity.mainActivity.MainActivity
import com.ablueclive.activity.startChat.StartChatActivity
import com.ablueclive.common.CommonContainerActivity
import com.ablueclive.common.JobContainerActivity
import com.ablueclive.fcmClass.MyFirebaseMessagingService
import com.ablueclive.sinch.IncomingTransparentCallActivity
import com.ablueclive.sinch.NotificationCallVo
import com.ablueclive.utils.BMSPrefs
import com.ablueclive.utils.Constants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.sinch.android.rtc.SinchHelpers
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {
    var title: String? = ""
    var alert = ""
    var type: String? = ""
    var post_id: String? = ""
    var name: String? = ""
    var mobile: String? = ""
    var TAG = MyFirebaseMessagingService::class.java.simpleName

    companion object {
        var STATUS_LIKE_POST = "2008009"
        var STATUS_COMMENTED_POST = "1001"
        var STATUS_CHAT_MESSAGE = "1003"
        var STATUS_HASH_TAG_ADD = "2001"
        var STATUS_SPECEFIC_SELECTED_JOB = "2021129"
        var STATUS_NEARBY_JOB_REQUEST = "2021128"
        var STATUS_JOB_ACCEPTED = "2021126"
        lateinit var myFirebaseMessagingService: MyFirebaseMessagingService
        var remoteData: Map<String, String>? = null
        var isOnChatScreen = false
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        myFirebaseMessagingService = this
        Log.e(TAG, "onMessageReceived From: " + remoteMessage.from)
        try {
            if (SinchHelpers.isSinchPushPayload(remoteMessage.data)) {

                if (TextUtils.isEmpty(BMSPrefs.getString(applicationContext, Constants._ID))) {
                    return
                }
                val data: Map<*, *> = remoteMessage.data
                remoteData = remoteMessage.data
                Log.e(TAG, "onMessageReceived data: $data")
                val map = remoteMessage.data
                val dataHashMap = if (map is HashMap<*, *>) map else HashMap(map)
                if (SinchHelpers.isSinchPushPayload(map)) {

                    if (applicationInForeground()) {
                        return
                    }
                    val intent = Intent(applicationContext, IncomingTransparentCallActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    val callVo = NotificationCallVo()
                    callVo.setData(dataHashMap as HashMap<String, String>?)
                    intent.putExtra(Constants.PARCELABLE, callVo)
                    startActivity(intent)
                }
            } else {
                val data = remoteMessage.data
                remoteData = data
                title = data["title"]
                alert = data["alert"].toString()
                type = data["type"]
                post_id = data["id"]
                name = data["name"]
                mobile = data["mobile"]
                if (applicationInForeground()) {
                    sendBroadcastMessage()
                }
                showNotification()
                Log.e(TAG, "onMessageReceived data: $data")
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun showNotification() {

        if (type == STATUS_CHAT_MESSAGE && isOnChatScreen) {
            return
        }

        val intent = makeIntent(type)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val randomId = Random().nextInt(61) + 20
        val channelId = "channel-01"
        val channelName = "Channel Name"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(mChannel)
        }
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.new_logo)
        val mBuilder = NotificationCompat.Builder(applicationContext, channelId)
                .setSmallIcon(notificationIcon)
                .setLargeIcon(largeIcon)
                .setContentTitle(title)
                .setContentText(alert)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(longArrayOf(1, 1, 1))
                .setDefaults(Notification.DEFAULT_SOUND)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
        notificationManager.notify(randomId, mBuilder.build())
    }

    private fun makeIntent(type: String?): Intent {
        var intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        if (type != null) {
            when (type) {

                STATUS_COMMENTED_POST -> {
                    intent = Intent(applicationContext, CommentActivity::class.java)
                    BMSPrefs.putString(applicationContext, Constants.FEED_POST_ID, post_id)
                }

                STATUS_CHAT_MESSAGE -> {
                    BMSPrefs.putString(applicationContext, "status", STATUS_CHAT_MESSAGE)
                    BMSPrefs.putString(applicationContext, Constants.TO_ID, post_id)
                    BMSPrefs.putString(applicationContext, Constants.CHAT_USER_NAME, name)
                    BMSPrefs.putString(applicationContext, Constants.FRIEND_MOBILE, mobile)
                    BMSPrefs.putString(applicationContext, Constants.CHAT_USER_IMAGE, "")
                    intent = Intent(applicationContext, StartChatActivity::class.java)
                }
                STATUS_HASH_TAG_ADD -> {
                    CommonContainerActivity.feedDetailTitle = name.toString()
                    BMSPrefs.putString(applicationContext, Constants.FEED_POST_ID, post_id)
                    intent = Intent(applicationContext, CommonContainerActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    intent.putExtra(Constants.TITLE, getString(R.string.feed_details))
                }

                "1002" -> {
                    intent = Intent(applicationContext, CommentActivity::class.java)
                    BMSPrefs.putString(applicationContext, Constants.FEED_POST_ID, post_id)
                }

                STATUS_SPECEFIC_SELECTED_JOB -> {
                    intent = Intent(applicationContext, JobContainerActivity::class.java)
                    intent.putExtra(Constants.TITLE, getString(R.string.search_job))
                }

                STATUS_NEARBY_JOB_REQUEST -> {
                    intent = Intent(applicationContext, JobContainerActivity::class.java)
                    intent.putExtra(Constants.TITLE, getString(R.string.search_job))
                }

                STATUS_JOB_ACCEPTED -> {
                    intent = Intent(applicationContext, JobContainerActivity::class.java)
                    intent.putExtra(Constants.TITLE, getString(R.string.job_post))
                }

                else -> {
                    intent = Intent(applicationContext, MainActivity::class.java)
                }
            }
        }
        return intent
    }

    private val notificationIcon: Int
        get() {
            val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) R.drawable.new_logo else R.drawable.new_logo
        }

    override fun onNewToken(refreshedToken: String) {
        super.onNewToken(refreshedToken)
        Log.e(TAG, "onNewToken: $refreshedToken")
        BMSPrefs.putString(applicationContext, Constants.DEVICE_TOKEN, refreshedToken)
    }

    private fun applicationInForeground(): Boolean {
        val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        var services: List<RunningAppProcessInfo>? = null
        services = activityManager.runningAppProcesses
        var isActivityFound = false
        if (services != null && services[0].processName == packageName
                && services[0].importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
            isActivityFound = true
        }
        return isActivityFound
    }

    private fun sendBroadcastMessage() {
        val broadCastIntent = Intent(Constants.NOTIFICATION_ACTION_FILTER)
        broadCastIntent.putExtra("type", type)
        broadCastIntent.putExtra("title", title)
        broadCastIntent.putExtra("alert", alert)
        broadCastIntent.putExtra("post_id", post_id)
        broadCastIntent.putExtra("name", name)
        broadCastIntent.putExtra("mobile", mobile)
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadCastIntent)
    }


}