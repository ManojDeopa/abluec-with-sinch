package com.ablueclive.listener;

public interface Pagination {

     public  void  paginate(int pageNo);
     public  void  onProductItemListener(int position);
}
