package com.ablueclive.listener;

public interface HashTagFriendListner {
    void onFriendClick(String friend_id, String name, int position);
    void onFriendRemove(String friend_id, String name, int position);
}
