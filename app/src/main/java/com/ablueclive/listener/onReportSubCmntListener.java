package com.ablueclive.listener;

import com.ablueclive.modelClass.Reply_comments;

import java.util.ArrayList;

public interface onReportSubCmntListener {
    void onReportSubCmntClick(String comment_id, int position,ArrayList<Reply_comments> reply_commentsArrayList,String reply_id);
}
